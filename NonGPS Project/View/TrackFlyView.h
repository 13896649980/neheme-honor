//
//  TrackFlyView.h
//  NonGPS Project
//
//  Created by Xu pan on 2020/11/11.
//  Copyright © 2020 Xu pan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol TrackFlyViewDelegate <NSObject>

-(void)getCurrentPositionAngle:(float)angle;
-(void)droneAnimationEnded;

@end

@interface TrackFlyView : UIView

@property   (nonatomic,assign)      id<TrackFlyViewDelegate>            delegate;

-(void)cancelTrackFlyAction;

@end

NS_ASSUME_NONNULL_END
