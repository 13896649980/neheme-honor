//
//  ControllerView.m
//  NonGPS Project
//
//  Created by Xu pan on 2019/8/14.
//  Copyright © 2019 Xu pan. All rights reserved.
//

#import "ControllerView.h"
#import "TrackFlyView.h"

#pragma mark -------- 这里设置的图片 --------

//返回按钮
#define Back_ImageName_Normal @"图层4"
#define Back_ImageName_Selected @"图层4拷贝"
//录像按钮
#define Record_ImageName_Normal @"图层6"
#define Record_ImageName_Selected @"图层6拷贝"
//拍照按钮
#define Takephoto_ImageName_Normal @"图层5"
#define TakePhoto_ImageName_Selected @"图层5拷贝"
//速度档按钮
#define Speed_ImageName_Normal @"档1"
#define Speed_ImageName_Selected @"档1"
//媒体按钮
#define Media_ImageName_Normal @"图层7"
#define Media_ImageName_Selected @"图层7拷贝"
//重力
#define Gravity_ImageName_Normal @"图层12"
#define Gravity_ImageName_Selected @"图层12拷贝"
//显示摇杆按钮
#define TurnOnorOff_ImageName_Normal @"图层9"
#define TurnOnOrOff_ImageName_Selected @"图层9拷贝"
//画面反转按钮
#define ImageRevers_ImageName_Normal @""
#define ImageRevers_ImageName_Selected @""
//3D VR按钮
#define VR_ImageName_Normal @"图层15"
#define VR_ImageName_Selected @"图层15拷贝"
//无头按钮
#define NoHead_ImageName_Normal @"图层17"
#define NoHead_ImageName_Selected @"图层17拷贝"
//校准按钮
#define Calibration_ImageName_Normal @"图层18"
#define Calibration_ImageName_Selected @"图层18拷贝"
//定高按钮
#define Dinggao_ImageName_Normal @"图层16"
#define Dinggao_ImageName_Selected @"图层16拷贝"
//起飞按钮
#define OnekeyFly_ImageName_Normal @"图层2"
#define OneKeyFly_ImageName_Selected @"图层2拷贝"
//下降按钮
#define OneKeyDrop_ImageName_Normal @"图层3"
#define OneKeyDrop_ImageName_Selected @"图层3拷贝"
//紧急停止按钮
#define EmergencyStop_ImageName_Normal @"STOP"
#define EmergencyStop_ImageName_Selected @""
//打开轨迹飞行按钮
#define OpenTrack_ImageName_Normal @"图层13"
#define OpenTrack_ImageName_Selected @"图层13拷贝"
//飞机360翻转按钮
#define Flip_ImageName_Normal @"图层10"
#define Flip_ImageName_Selected @"图层10拷贝"
//手势拍照录像按钮
#define Gesture_ImageName_Normal @"图层8"
#define Gesture_ImageName_Selected @"图层8拷贝"
//云台上
#define CameraUp_ImageName_Normal @""
#define CameraUp_ImageName_Selected @""
//云台下
#define CameraDown_ImageName_Normal @""
#define CameraDown_ImageName_Selected @""
//图像放大
#define ImageAmplification_ImageName_Normal @""
#define ImageAmplification_ImageName_Selected @""
//图像缩小
#define imageNarrow_ImageName_Normal @""
#define imageNarrow_ImageName_Selected @""
//录制声音
#define Voice_ImageName_Normal @"图层19"
#define Voice_ImageName_Selected @"图层19拷贝"
//添加音乐
#define MusicMix_ImageName_Normal @""
#define MusicMix_ImageName_Selected @""
//滤镜
#define Filter_ImageName_Normal @"图层14"
#define Filter_ImageName_Selected @"图层14拷贝"
//镜头切换
#define SwitchLens_ImageName_Normal @""
#define SwitchLens_ImageName_Selected @""

//打开图像跟随
#define OpenFollow_ImageName_Normal @""
#define OpenFollow_ImageName_Selected @""
//开始图像跟随
#define StartFollow_ImageName_Normal @""
#define StartFollow_ImageName_Selected @""
//摇杆背景
#define LeftRockerImageName @"遥感大圆"
#define RightRockerImageName @"遥感大圆"





@interface ControllerView()<TrackFlyViewDelegate,DragCircleTouchEndDelegate,LeftTouchDownDelegate,RightTouchDownDelegate>
{
    UIStackView *stackView;
}


@property   (nonatomic,assign)      BOOL                    IsGyro;
@property   (nonatomic,strong)      TrackFlyView            *trackView;
@property   (nonatomic,assign)      int                     trackCount;
@property   (nonatomic,strong)      NSMutableArray          *trackPoint;
@property   (nonatomic,strong)      UIView                  *recordTimeBgView;
@property   (nonatomic,strong)      UILabel                 *recordDurtionLabel;
@property   (nonatomic,assign)      NSInteger               recordTime;

@end

@implementation ControllerView

#pragma mark  ---------- lazyLoad subViews ----------
-(UIButton *)backBtn{
    if (!_backBtn) {
        _backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backBtn setImage:[UIImage imageNamed:TranslationString(Back_ImageName_Normal)] forState:UIControlStateNormal];
        [_backBtn setImage:[UIImage imageNamed:TranslationString(Back_ImageName_Selected)] forState:UIControlStateSelected];
        [self addSubview:_backBtn];
        @weakify(self);
        [[_backBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            [self.backSignal sendNext:nil];
        }];
    }
    return _backBtn;
}

-(UIButton *)recordBtn{
    if (!_recordBtn) {
        _recordBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_recordBtn setImage:[UIImage imageNamed:TranslationString(Record_ImageName_Normal)] forState:UIControlStateNormal];
        [_recordBtn setImage:[UIImage imageNamed:TranslationString(Record_ImageName_Selected)] forState:UIControlStateSelected];
        [self addSubview:_recordBtn];
        @weakify(self);
        [[_recordBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            [self.recordSignal sendNext:nil];
        }];
    }
    return _recordBtn;
}

-(UIButton *)takePhothBtn{
    if (!_takePhothBtn) {
        _takePhothBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_takePhothBtn setImage:[UIImage imageNamed:TranslationString(Takephoto_ImageName_Normal)] forState:UIControlStateNormal];
        [_takePhothBtn setImage:[UIImage imageNamed:TranslationString(TakePhoto_ImageName_Selected)] forState:UIControlStateSelected];
        [self addSubview:_takePhothBtn];
        @weakify(self);
        [[_takePhothBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            [self.takePhothSignal sendNext:nil];
        }];
    }
    return _takePhothBtn;
}

-(UIButton *)speedBtn{
    if (!_speedBtn) {
        _speedBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_speedBtn setImage:[UIImage imageNamed:TranslationString(Speed_ImageName_Normal)] forState:UIControlStateNormal];
        [_speedBtn setImage:[UIImage imageNamed:TranslationString(Speed_ImageName_Selected)] forState:UIControlStateSelected];
        [self addSubview:_speedBtn];
        @weakify(self);
        [[_speedBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            [self.speedSignal sendNext:nil];
        }];
    }
    return _speedBtn;
}
-(UIButton *)mediaBtn{
    if (!_mediaBtn) {
        _mediaBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_mediaBtn setImage:[UIImage imageNamed:TranslationString(Media_ImageName_Normal)] forState:UIControlStateNormal];
        [_mediaBtn setImage:[UIImage imageNamed:TranslationString(Media_ImageName_Selected)] forState:UIControlStateSelected];
        [self addSubview:_mediaBtn];
        @weakify(self);
        [[_mediaBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            [self.mediaSignal sendNext:nil];
        }];
    }
    return _mediaBtn;
}

-(UIButton *)gravityBtn{
    if (!_gravityBtn) {
        _gravityBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_gravityBtn setImage:[UIImage imageNamed:TranslationString(Gravity_ImageName_Normal)] forState:UIControlStateNormal];
        [_gravityBtn setImage:[UIImage imageNamed:TranslationString(Gravity_ImageName_Selected)] forState:UIControlStateSelected];
        [self addSubview:_gravityBtn];
        @weakify(self);
        [[_gravityBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            self.gravityBtn.selected = self.IsGyro = !self.gravityBtn.selected;
            if (!self.gravityBtn.selected) {
                [self.rightRocker setDragCircleCenter];
            }
        }];
        [self initCotionManager];
    }
    return _gravityBtn;
}

- (UIImageView *)batteryImageView{
    if (!_batteryImageView) {
        _batteryImageView = [[UIImageView alloc]initWithImage:IMAGEWITHNAME(@"图层14拷贝")];
        _batteryImageView.contentMode = UIViewContentModeCenter;
        [self addSubview:_batteryImageView];
    }
    return _batteryImageView;
}

-(UIButton *)circleBtn{
    if (!_circleBtn) {
        _circleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_circleBtn setImage:IMAGEWITHNAME(@"图层13") forState:UIControlStateNormal];
        [_circleBtn setImage:IMAGEWITHNAME(@"图层13拷贝") forState:UIControlStateSelected];
        [self addSubview:_circleBtn];
        @weakify(self);
        [[_circleBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            self.circleBtn.selected = !self.circleBtn.selected;
        }];
    }
    return _circleBtn;
}
-(UIButton *)turnOnOrOffBtn{
    if (!_turnOnOrOffBtn) {
        _turnOnOrOffBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_turnOnOrOffBtn setImage:[UIImage imageNamed:TranslationString(TurnOnorOff_ImageName_Normal)] forState:UIControlStateNormal];
        [_turnOnOrOffBtn setImage:[UIImage imageNamed:TranslationString(TurnOnOrOff_ImageName_Selected)] forState:UIControlStateSelected];
        [self addSubview:_turnOnOrOffBtn];
        @weakify(self);
        [[_turnOnOrOffBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            self.turnOnOrOffBtn.selected = !self.turnOnOrOffBtn.selected;
            [self.turnOnOrOffSignal sendNext:nil];
        }];
    }
    return _turnOnOrOffBtn;
}

-(UIButton *)imageReversBtn{
    if (!_imageReversBtn) {
        _imageReversBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_imageReversBtn setImage:[UIImage imageNamed:TranslationString(ImageRevers_ImageName_Normal)] forState:UIControlStateNormal];
        [_imageReversBtn setImage:[UIImage imageNamed:TranslationString(ImageRevers_ImageName_Selected)] forState:UIControlStateSelected];
        [self addSubview:_imageReversBtn];
        @weakify(self);
        [[_imageReversBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            [self.imageReversSignal sendNext:nil];
        }];
    }
    return _imageReversBtn;
}

-(UIButton *)VRBtn{
    if (!_VRBtn) {
        _VRBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_VRBtn setImage:[UIImage imageNamed:TranslationString(VR_ImageName_Normal)] forState:UIControlStateNormal];
        [_VRBtn setImage:[UIImage imageNamed:TranslationString(VR_ImageName_Selected)] forState:UIControlStateSelected];
        [self addSubview:_VRBtn];
        @weakify(self);
        [[_VRBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
//            self.VRBtn.selected = !self.VRBtn.selected ;
            [self.VRSignal sendNext:nil];
        }];
    }
    return _VRBtn;
}

-(UIButton *)noHeadBTn{
    if (!_noHeadBTn) {
        _noHeadBTn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_noHeadBTn setImage:[UIImage imageNamed:TranslationString(NoHead_ImageName_Normal)] forState:UIControlStateNormal];
        [_noHeadBTn setImage:[UIImage imageNamed:TranslationString(NoHead_ImageName_Selected)] forState:UIControlStateSelected];
        [self addSubview:_noHeadBTn];
        @weakify(self);
        [[_noHeadBTn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            self.noHeadBTn.selected = !self.noHeadBTn.selected ;
            [self.noHeadSignal sendNext:nil];
        }];
    }
    return _noHeadBTn;
}

-(UIButton *)calibrationBtn{
    if (!_calibrationBtn) {
        _calibrationBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_calibrationBtn setImage:[UIImage imageNamed:TranslationString(Calibration_ImageName_Normal)] forState:UIControlStateNormal];
        [_calibrationBtn setImage:[UIImage imageNamed:TranslationString(Calibration_ImageName_Selected)] forState:UIControlStateSelected];
        [self addSubview:_calibrationBtn];
        @weakify(self);
        [[_calibrationBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            [self.calibrationSignal sendNext:nil];
        }];
    }
    return _calibrationBtn;
}

-(UIButton *)dinggaoBtn{
    if (!_dinggaoBtn) {
        _dinggaoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_dinggaoBtn setImage:[UIImage imageNamed:TranslationString(Dinggao_ImageName_Normal)] forState:UIControlStateNormal];
        [_dinggaoBtn setImage:[UIImage imageNamed:TranslationString(Dinggao_ImageName_Selected)] forState:UIControlStateSelected];
        [self addSubview:_dinggaoBtn];
        @weakify(self);
        [[_dinggaoBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            [self.dinggaoSignal sendNext:nil];
        }];
    }
    return _dinggaoBtn;
}


-(UIButton *)oneKeyFlyBtn{
    if (!_oneKeyFlyBtn) {
        _oneKeyFlyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_oneKeyFlyBtn setImage:[UIImage imageNamed:TranslationString(OnekeyFly_ImageName_Normal)] forState:UIControlStateNormal];
        [_oneKeyFlyBtn setImage:[UIImage imageNamed:TranslationString(OneKeyFly_ImageName_Selected)] forState:UIControlStateSelected];
        [self addSubview:_oneKeyFlyBtn];
        @weakify(self);
        [[_oneKeyFlyBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            [self.oneKeyFlySignal sendNext:nil];
        }];
    }
    return _oneKeyFlyBtn;
}


-(UIButton *)oneKeyDropBtn{
    if (!_oneKeyDropBtn) {
        _oneKeyDropBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_oneKeyDropBtn setImage:[UIImage imageNamed:TranslationString(OneKeyDrop_ImageName_Normal)] forState:UIControlStateNormal];
        [_oneKeyDropBtn setImage:[UIImage imageNamed:TranslationString(OneKeyDrop_ImageName_Selected)] forState:UIControlStateSelected];
        [self addSubview:_oneKeyDropBtn];
        @weakify(self);
        [[_oneKeyDropBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            [self.oneKeyDropSignal sendNext:nil];
        }];
    }
    return _oneKeyDropBtn;
}


-(UIButton *)openTrackBtn{
    if (!_openTrackBtn) {
        _openTrackBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_openTrackBtn setImage:[UIImage imageNamed:TranslationString(OpenTrack_ImageName_Normal)] forState:UIControlStateNormal];
        [_openTrackBtn setImage:[UIImage imageNamed:TranslationString(OpenTrack_ImageName_Selected)] forState:UIControlStateSelected];
        [self addSubview:_openTrackBtn];
        @weakify(self);
        [[_openTrackBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            [self trackAction];
        }];
        [self initTrackFlyView];
    }
    return _openTrackBtn;
}


-(UIButton *)emergencyStopBtn{
    if (!_emergencyStopBtn) {
        _emergencyStopBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_emergencyStopBtn setImage:[UIImage imageNamed:TranslationString(EmergencyStop_ImageName_Normal)] forState:UIControlStateNormal];
        [_emergencyStopBtn setImage:[UIImage imageNamed:TranslationString(EmergencyStop_ImageName_Selected)] forState:UIControlStateSelected];
        _emergencyStopBtn.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;
        [self addSubview:_emergencyStopBtn];
        @weakify(self);
        [[_emergencyStopBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            [self.emergencyStopSignal sendNext:nil];
        }];
    }
    return _emergencyStopBtn;
}


-(UIButton *)flipBtn{
    if (!_flipBtn) {
        _flipBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_flipBtn setImage:[UIImage imageNamed:TranslationString(Flip_ImageName_Normal)] forState:UIControlStateNormal];
        [_flipBtn setImage:[UIImage imageNamed:TranslationString(Flip_ImageName_Selected)] forState:UIControlStateSelected];
        [self addSubview:_flipBtn];
        @weakify(self);
        [[_flipBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
//            [self.flipSignal sendNext:nil];
            self.flipBtn.selected =  !self.flipBtn.selected ;
        }];
    }
    return _flipBtn;
}
-(UIButton *)rotateBtn{
    if (!_rotateBtn) {
        _rotateBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_rotateBtn setImage:IMAGEWITHNAME(@"图层16") forState:UIControlStateNormal];
        [_rotateBtn setImage:IMAGEWITHNAME(@"图层16拷贝") forState:UIControlStateSelected];
        [self addSubview:_rotateBtn];
        @weakify(self);
        [[_rotateBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            self.rotateBtn.selected = !self.rotateBtn.selected;
        }];
    }
    return _rotateBtn;
}

-(UIButton *)cameraUpBtn{
    if (!_cameraUpBtn) {
        _cameraUpBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_cameraUpBtn setImage:[UIImage imageNamed:TranslationString(CameraUp_ImageName_Normal)] forState:UIControlStateNormal];
        [_cameraUpBtn setImage:[UIImage imageNamed:TranslationString(CameraUp_ImageName_Selected)] forState:UIControlStateSelected];
        [self addSubview:_cameraUpBtn];
        @weakify(self);
        [[_cameraUpBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            [self.cameraUpSignal sendNext:nil];
        }];
    }
    return _cameraUpBtn;
}


-(UIButton *)cameraDownBtn{
    if (!_cameraDownBtn) {
        _cameraDownBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_cameraDownBtn setImage:[UIImage imageNamed:TranslationString(CameraDown_ImageName_Normal)] forState:UIControlStateNormal];
        [_cameraDownBtn setImage:[UIImage imageNamed:TranslationString(CameraDown_ImageName_Selected)] forState:UIControlStateSelected];
        [self addSubview:_cameraDownBtn];
        @weakify(self);
        [[_cameraDownBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            [self.cameraDownSignal sendNext:nil];
        }];
    }
    return _cameraDownBtn;
}

-(UIButton *)filterBtn{
    if (!_filterBtn) {
        _filterBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_filterBtn setImage:[UIImage imageNamed:TranslationString(Filter_ImageName_Normal)] forState:UIControlStateNormal];
        [_filterBtn setImage:[UIImage imageNamed:TranslationString(Filter_ImageName_Selected)] forState:UIControlStateSelected];
        [self addSubview:_filterBtn];
        @weakify(self);
        [[_filterBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            [self.filterSignal sendNext:nil];
        }];
    }
    return _filterBtn;
}

-(UIButton *)musicMicBtn{
    if (!_musicMicBtn) {
        _musicMicBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_musicMicBtn setImage:[UIImage imageNamed:TranslationString(MusicMix_ImageName_Normal)] forState:UIControlStateNormal];
        [_musicMicBtn setImage:[UIImage imageNamed:TranslationString(MusicMix_ImageName_Selected)] forState:UIControlStateSelected];
        [self addSubview:_musicMicBtn];
        @weakify(self);
        [[_musicMicBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            [self.musicMixSignal sendNext:nil];
        }];
    }
    return _musicMicBtn;
}

-(UIButton *)imageNarrowBtn{
    if (!_imageNarrowBtn) {
        _imageNarrowBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_imageNarrowBtn setImage:[UIImage imageNamed:TranslationString(imageNarrow_ImageName_Normal)] forState:UIControlStateNormal];
        [_imageNarrowBtn setImage:[UIImage imageNamed:TranslationString(imageNarrow_ImageName_Selected)] forState:UIControlStateSelected];
        [self addSubview:_imageNarrowBtn];
        @weakify(self);
        [[_imageNarrowBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            [self.imageNarrowSignal sendNext:nil];
        }];
    }
    return _imageNarrowBtn;
}

-(UIButton *)imageAmplificationBtn{
    if (!_imageAmplificationBtn) {
        _imageAmplificationBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_imageAmplificationBtn setImage:[UIImage imageNamed:TranslationString(ImageAmplification_ImageName_Normal)] forState:UIControlStateNormal];
        [_imageAmplificationBtn setImage:[UIImage imageNamed:TranslationString(imageNarrow_ImageName_Selected)] forState:UIControlStateSelected];
        [self addSubview:_imageAmplificationBtn];
        @weakify(self);
        [[_imageAmplificationBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            [self.imageAmplificationSignal sendNext:nil];
        }];
    }
    return _imageAmplificationBtn;
}

-(UIButton *)voiceBtn{
    if (!_voiceBtn) {
        _voiceBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_voiceBtn setImage:[UIImage imageNamed:TranslationString(Voice_ImageName_Normal)] forState:UIControlStateNormal];
        [_voiceBtn setImage:[UIImage imageNamed:TranslationString(Voice_ImageName_Selected)] forState:UIControlStateSelected];
        [self addSubview:_voiceBtn];
        @weakify(self);
        [[_voiceBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            [self.voiceSignal sendNext:nil];
        }];
    }
    return _voiceBtn;
}

-(UIButton *)switchLensBtn{
    if (!_switchLensBtn) {
        _switchLensBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_switchLensBtn setImage:[UIImage imageNamed:TranslationString(SwitchLens_ImageName_Normal)] forState:UIControlStateNormal];
        [_switchLensBtn setImage:[UIImage imageNamed:TranslationString(SwitchLens_ImageName_Selected)] forState:UIControlStateSelected];
        [self addSubview:_switchLensBtn];
        @weakify(self);
        [[_switchLensBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            [self.switchLensSignal sendNext:nil];
        }];
    }
    return _switchLensBtn;
}

#ifdef OPENCV_ENABLE
-(UIButton *)gestureBtn{
    if (!_gestureBtn) {
        _gestureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_gestureBtn setImage:[UIImage imageNamed:TranslationString(Gesture_ImageName_Normal)] forState:UIControlStateNormal];
        [_gestureBtn setImage:[UIImage imageNamed:TranslationString(Gesture_ImageName_Selected)] forState:UIControlStateSelected];
        [self addSubview:_gestureBtn];
        @weakify(self);
        [[_gestureBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            self.gestureBtn.selected = !self.gestureBtn.selected;
            [self.gestureSignal sendNext:nil];
        }];
    }
    return _gestureBtn;
}

-(UIButton *)openFollowBtn{
    if (!_openFollowBtn) {
        _openFollowBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_openFollowBtn setImage:[UIImage imageNamed:TranslationString(OpenFollow_ImageName_Normal)] forState:UIControlStateNormal];
        [_openFollowBtn setImage:[UIImage imageNamed:TranslationString(OpenFollow_ImageName_Selected)] forState:UIControlStateSelected];
        [self addSubview:_openFollowBtn];
        @weakify(self);
        [[_openFollowBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            [self.openFollowSignal sendNext:nil];
        }];
    }
    return _openFollowBtn;
}

-(UIButton *)startFollowBtn{
    if (!_startFollowBtn) {
        _startFollowBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_startFollowBtn setImage:[UIImage imageNamed:TranslationString(StartFollow_ImageName_Normal)] forState:UIControlStateNormal];
        [_startFollowBtn setImage:[UIImage imageNamed:TranslationString(StartFollow_ImageName_Selected)] forState:UIControlStateSelected];
        [self addSubview:_startFollowBtn];
        @weakify(self);
        [[_startFollowBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            [self.startFollowSignal sendNext:nil];
        }];
    }
    return _startFollowBtn;
}
#endif

-(UIImageView *)leftRockerImageView{
    if (!_leftRockerImageView ) {
        _leftRockerImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:LeftRockerImageName]];
        _leftRockerImageView.userInteractionEnabled = YES;
        [self addSubview:_leftRockerImageView];
    }
    return _leftRockerImageView;
}

-(DragCircle *)leftRocker{
    if (!_leftRocker) {
        _leftRocker = [[DragCircle alloc]init];
        [self addSubview:_leftRocker];
    }
    return _leftRocker;
}

-(LeftDragBK *)leftRockerDragBK{
    if (!_leftRockerDragBK) {
        _leftRockerDragBK = [[LeftDragBK alloc]init];
        _leftRockerDragBK.userInteractionEnabled = YES;
        [self addSubview:_leftRockerDragBK];
    }
    return _leftRockerDragBK;
}

-(TiaoJieView *)leftTiaoJieView{
    if (!_leftTiaoJieView) {
        _leftTiaoJieView = [[TiaoJieView alloc]init];
        _leftTiaoJieView.userInteractionEnabled = YES;
        _leftTiaoJieView.type   =       0;
        [self addSubview:_leftTiaoJieView];
    }
    return _leftTiaoJieView;
}

-(TiaoJieView *)rightTiaoJieView{
    if (!_rightTiaoJieView) {
        _rightTiaoJieView = [[TiaoJieView alloc]init];
        _rightTiaoJieView.userInteractionEnabled = YES;
        _rightTiaoJieView.type   =       0;
        [self addSubview:_rightTiaoJieView];
    }
    return _rightTiaoJieView;
}


-(TiaoJieView *)cemianTiaoJieView{
    if (!_cemianTiaoJieView) {
        _cemianTiaoJieView = [[TiaoJieView alloc]init];
        _cemianTiaoJieView.userInteractionEnabled = YES;
        _cemianTiaoJieView.type   =       1;
        [self addSubview:_cemianTiaoJieView];
    }
    return _cemianTiaoJieView;
}



-(UIImageView *)rightRockerImageView{
    if (!_rightRockerImageView ) {
        _rightRockerImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:RightRockerImageName]];
        _rightRockerImageView.userInteractionEnabled = YES;
        [self addSubview:_rightRockerImageView];
    }
    return _rightRockerImageView;
}

-(DragCircle *)rightRocker{
    if (!_rightRocker) {
        _rightRocker = [[DragCircle alloc]init];
        [self addSubview:_rightRocker];
    }
    return _rightRocker;
}

-(RightDragBK *)rightRockerDragBK{
    if (!_rightRockerDragBK) {
        _rightRockerDragBK = [[RightDragBK alloc]init];
        _rightRockerDragBK.userInteractionEnabled = YES;
        [self addSubview:_rightRockerDragBK];
    }
    return _rightRockerDragBK;
}

-(WeitiaoView *)rotateAdjustView{
    if (!_rotateAdjustView) {
        _rotateAdjustView = [[WeitiaoView alloc]init];
        [self addSubview:_rotateAdjustView];
    }
    return _rotateAdjustView;
}

-(WeitiaoView *)leftOrRightAdjustView{
    if (!_leftOrRightAdjustView) {
        _leftOrRightAdjustView = [[WeitiaoView alloc]init];
        [self addSubview:_leftOrRightAdjustView];
    }
    return _leftOrRightAdjustView;
}

-(WeitiaoView *)fontdOrBackAdjustView{
    if (!_fontdOrBackAdjustView) {
        _fontdOrBackAdjustView = [[WeitiaoView alloc]init];
        [self addSubview:_fontdOrBackAdjustView];
    }
    return _fontdOrBackAdjustView;
}

#pragma mark ---------- lazyLoad-Signal ----------

-(RACSubject *)backSignal{
    if (!_backSignal) {
        _backSignal = [RACSubject subject];
    }
    return _backSignal;
}

-(RACSubject *)recordSignal{
    if (!_recordSignal) {
        _recordSignal = [RACSubject subject];
    }
    return _recordSignal;
}

-(RACSubject *)takePhothSignal{
    if (!_takePhothSignal) {
        _takePhothSignal = [RACSubject subject];
    }
    return _takePhothSignal;
}

-(RACSubject *)speedSignal{
    if (!_speedSignal) {
        _speedSignal = [RACSubject subject];
    }
    return _speedSignal;
}

-(RACSubject *)mediaSignal{
    if (!_mediaSignal) {
        _mediaSignal = [RACSubject subject];
    }
    return _mediaSignal;
}

-(RACSubject *)turnOnOrOffSignal{
    if (!_turnOnOrOffSignal) {
        _turnOnOrOffSignal = [RACSubject subject];
    }
    return _turnOnOrOffSignal;
}

-(RACSubject *)imageReversSignal{
    if (!_imageReversSignal) {
        _imageReversSignal = [RACSubject subject];
    }
    return _imageReversSignal;
}

-(RACSubject *)VRSignal{
    if (!_VRSignal) {
        _VRSignal = [RACSubject subject];
    }
    return _VRSignal;
}

-(RACSubject *)noHeadSignal{
    if (!_noHeadSignal) {
        _noHeadSignal = [RACSubject subject];
    }
    return _noHeadSignal;
}

-(RACSubject *)calibrationSignal{
    if (!_calibrationSignal) {
        _calibrationSignal = [RACSubject subject];
    }
    return _calibrationSignal;
}

-(RACSubject *)dinggaoSignal{
    if (!_dinggaoSignal) {
        _dinggaoSignal = [RACSubject subject];
    }
    return _dinggaoSignal;
}

-(RACSubject *)oneKeyFlySignal{
    if (!_oneKeyFlySignal) {
        _oneKeyFlySignal = [RACSubject subject];
    }
    return _oneKeyFlySignal;
}

-(RACSubject *)oneKeyDropSignal{
    if (!_oneKeyDropSignal) {
        _oneKeyDropSignal = [RACSubject subject];
    }
    return _oneKeyDropSignal;
}

-(RACSubject *)emergencyStopSignal{
    if (!_emergencyStopSignal) {
        _emergencyStopSignal = [RACSubject subject];
    }
    return _emergencyStopSignal;
}

-(RACSubject *)flipSignal{
    if (!_flipSignal) {
        _flipSignal = [RACSubject subject];
    }
    return _flipSignal;
}

-(RACSubject *)cameraUpSignal{
    if (!_cameraUpSignal) {
        _cameraUpSignal = [RACSubject subject];
    }
    return _cameraUpSignal;
}

-(RACSubject *)cameraDownSignal{
    if (!_cameraDownSignal) {
        _cameraDownSignal = [RACSubject subject];
    }
    return _cameraDownSignal;
}

-(RACSubject *)imageNarrowSignal{
    if (!_imageNarrowSignal) {
        _imageNarrowSignal = [RACSubject subject];
    }
    return _imageNarrowSignal;
}

-(RACSubject *)imageAmplificationSignal{
    if (!_imageAmplificationSignal) {
        _imageAmplificationSignal = [RACSubject subject];
    }
    return _imageAmplificationSignal;
}

-(RACSubject *)musicMixSignal{
    if (!_musicMixSignal) {
        _musicMixSignal = [RACSubject subject];
    }
    return _musicMixSignal;
}

-(RACSubject *)filterSignal{
    if (!_filterSignal) {
        _filterSignal = [RACSubject subject];
    }
    return _filterSignal;
}

-(RACSubject *)voiceSignal{
    if (!_voiceSignal) {
        _voiceSignal = [RACSubject subject];
    }
    return _voiceSignal;
}

-(RACSubject *)switchLensSignal{
    if (!_switchLensSignal) {
        _switchLensSignal = [RACSubject subject];
    }
    return _switchLensSignal;
}

#ifdef OPENCV_ENABLE
-(RACSubject *)gestureSignal{
    if (!_gestureSignal) {
        _gestureSignal = [RACSubject subject];
    }
    return _gestureSignal;
}
-(RACSubject *)openFollowSignal{
    if (!_openFollowSignal) {
        _openFollowSignal = [RACSubject subject];
    }
    return _openFollowSignal;
}

-(RACSubject *)startFollowSignal{
    if (!_startFollowSignal) {
        _startFollowSignal = [RACSubject subject];
    }
    return _startFollowSignal;
}
#endif

-(UIView *)recordTimeBgView{
    if (!_recordTimeBgView) {
        self.recordTime = 1;
        
        _recordTimeBgView = [[UIView alloc]init];
        _recordTimeBgView.hidden = YES;
        _recordTimeBgView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.5];
        _recordTimeBgView.layer.cornerRadius = LAYOUT_WIDTH(5);
        [self addSubview:_recordTimeBgView];
        
        UIView *redPoint = [[UIView alloc]init];
        redPoint.backgroundColor = [UIColor redColor];
        redPoint.layer.cornerRadius = LAYOUT_WIDTH(4);
        redPoint.clipsToBounds = YES;
        [_recordTimeBgView addSubview:redPoint];
        [redPoint mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(LAYOUT_WIDTH(5));
            make.centerY.mas_equalTo(self.recordTimeBgView);
            make.height.width.mas_equalTo(LAYOUT_WIDTH(8));
        }];
        
        self.recordDurtionLabel = [[UILabel alloc]init];
        self.recordDurtionLabel.text = @"00:00";
        self.recordDurtionLabel.textColor = [UIColor whiteColor];
        self.recordDurtionLabel.font = [UIFont fontWithName:PF_LIGHT size:10 * FontScale];
        [_recordTimeBgView addSubview:self.recordDurtionLabel];
        [self.recordDurtionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(redPoint.mas_right).offset(LAYOUT_WIDTH(5));
            make.centerY.mas_equalTo(self.recordTimeBgView);
            make.right.mas_equalTo(LAYOUT_WIDTH(-5));
            make.height.mas_equalTo(20);
            make.top.bottom.mas_equalTo(self.recordTimeBgView);
        }];
    }
    return _recordTimeBgView;
}


#pragma mark - init

-(instancetype)init{
    if (self = [super init]) {
        [self configSubviewConstraints];
    }
    return self;
}

#pragma mark - 初始化重力感应
-(void)initCotionManager{
    self.motionManager = [[CMMotionManager alloc] init];
    self.motionManager.deviceMotionUpdateInterval = 0.1;
    SEL selector = @selector(handleDeviceMotion:);
    [self.motionManager startDeviceMotionUpdatesToQueue : [NSOperationQueue currentQueue]
                                          withHandler:^(CMDeviceMotion *motion, NSError *error) {
                                              [self performSelectorOnMainThread:selector
                                                                     withObject:motion waitUntilDone:YES];
                                          }];
}

-(void)handleDeviceMotion:(CMDeviceMotion*)motion{
    
    if (self.IsGyro == NO){
        return;
    }
    UIInterfaceOrientation orientation =[UIApplication sharedApplication].statusBarOrientation;
    CMAcceleration GSenser = motion.gravity;
    unsigned char motionX  = 0;
    unsigned char motionY  = 0;
    
    if (orientation == UIInterfaceOrientationLandscapeLeft){
        motionX  = (1 + GSenser.x) * [self.rightRocker GetCircleX];
        motionY  = (1 + GSenser.y) * [self.rightRocker GetCircleY];
    }else if (orientation == UIInterfaceOrientationLandscapeRight){
        motionX  = (1 - GSenser.x) * [self.rightRocker GetCircleX];
        motionY  = (1 - GSenser.y) * [self.rightRocker GetCircleY];
    }
    
    if (motionX > 48 && motionX < 80){
        motionX = 64;
    }
    else if(motionX <= 48 && motionX >= 16){
        motionX = (motionX << 1) - 32;
    }
    else if (motionX < 16){
        motionX = 0;
    }
    else if (motionX >= 112){
        motionX = 127;
    }
    else{
        motionX = ((motionX - 64)<<1) +32;
    }
    
    
    if (motionY > 48 && motionY < 80){
        motionY = 64;
    }
    else if(motionY <= 48 && motionY >= 16){
        motionY = (motionY << 1) - 32;
    }
    else if (motionY < 16){
        motionY = 0;
    }
    else if (motionY >= 112){
        motionY = 127;
    }
    else{
        motionY = ((motionY - 64)<<1) +32;
    }
    
    [self.rightRocker setDragCircleXandY:motionY DragCircleY:motionX];
}

#pragma mark - 初始化轨迹飞行视图
-(void)initTrackFlyView{
    
    self.trackView = [[TrackFlyView alloc]init];
    self.trackView.layer.cornerRadius = 10;
    self.trackView.clipsToBounds = YES;
    self.trackView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.3];
    self.trackView.delegate = self;
    self.trackView.hidden = YES;
    [self insertSubview:self.trackView belowSubview:self.rightRockerImageView];
    [self.trackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(LAYOUT_WIDTH(60));
        make.bottom.mas_equalTo(LAYOUT_WIDTH(-30));
        make.right.mas_equalTo(LAYOUT_WIDTH(IS_iPhoneX_TYPE ? -75 : -50));
        make.left.mas_equalTo(self.mas_centerX).offset(LAYOUT_WIDTH(-30));
    }];
}

#pragma mark - TrackFlyViewDelegate
-(void)getCurrentPositionAngle:(float)angle{
    int x_value = 0;
    int y_value = 0;
    //斜边64
    if (angle >= 0 && angle < 90) {
        
        x_value = (int)(64 + sin(angle) * 63);
        y_value = (int)(64 + cos(angle) * 63);
        
    }else if (angle >= 90 && angle <= 180){
        
        angle = angle - 90;
        x_value = (int)(64 + cos(angle) * 63);
        y_value = (int)(64 - sin(angle) * 63);
        
    }else if (angle > -90 && angle <= 0){
        
        x_value = (int)(64 - sin(-angle) * 63);
        y_value = (int)(64 + cos(-angle) * 63);
        
    }else if (angle >= -180 && angle <= -90){
        
        angle = -angle - 90;
        x_value = (int)(64 - cos(angle) * 63);
        y_value = (int)(64 - sin(angle) * 63);
    }
    [self.rightRocker setDragCircleXandY:x_value DragCircleY:y_value];
}

-(void)droneAnimationEnded{
    [self.rightRocker setDragCircleXandY:64 DragCircleY:64];
}

#pragma mark - 点击轨迹飞行按钮事件
-(void)trackAction{
    //如果开启了重力感应则关闭
    if (self.IsGyro) {
        self.gravityBtn.selected = NO;
        self.IsGyro = NO;
        self.rightRocker.userInteractionEnabled = YES;
        [self.rightRocker setDragCircleCenter];
    }
    
    self.openTrackBtn.selected = !self.openTrackBtn.selected;
    self.turnOnOrOffBtn.selected = self.gravityBtn.enabled = !self.openTrackBtn.selected;
    
    [self setTrackViewHidden:!self.openTrackBtn.selected];
}

- (void)setTrackViewHidden:(BOOL)hidden
{
    self.trackView.hidden = hidden;
    
    [self bringSubviewToFront:self.trackView];
    
    if (hidden) {
        if (self.rightRockerImageView.hidden) {
            self.rightRocker.hidden = self.rightRockerImageView.hidden = self.leftOrRightAdjustView.hidden = self.fontdOrBackAdjustView.hidden = NO;
        }
    }else{
        self.rightRocker.hidden = self.rightRockerImageView.hidden = self.leftOrRightAdjustView.hidden = self.fontdOrBackAdjustView.hidden = YES;
    }
    
    self.dinggaoBtn.enabled = hidden;
}

#pragma mark - 轨迹飞行
- (void)trackCancelled{
    [self.trackView cancelTrackFlyAction];
    [self.rightRocker setDragCircleCenter];
}

#pragma mark - 录像开始/结束UI操作
-(void)setStatueWithStarRecord{
    if (self.recordDurtionTimer) {
        [self.recordDurtionTimer invalidate];
        self.recordDurtionTimer = nil;
        self.recordTime = 1;
    }
    self.recordDurtionTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(recordTimeAction) userInfo:nil repeats:YES];
    self.recordDurtionLabel.text = @"00:01";
    self.recordTimeBgView.hidden = NO;
}

-(void)recordTimeAction{
    self.recordTime += 1;
    self.recordDurtionLabel.text = [Utility formatTimeInterval:self.recordTime];
}

-(void)setStatueWithEndRecord{
    if (self.recordDurtionTimer) {
        [self.recordDurtionTimer invalidate];
        self.recordDurtionTimer = nil;
    }
    self.recordTime = 1;
    self.recordTimeBgView.hidden = YES;
}

#pragma mark ------------ 在这里布局子视图 ------------
-(void)configSubviewConstraints{
    CGSize size = CGSizeMake(LAYOUT_HEIGHT(35), LAYOUT_WIDTH(35));
    CGFloat space = (SCREEN_WIDTH-LAYOUT_HEIGHT(35)*9)/10.0;
    [self.backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self).offset(LAYOUT_WIDTH(15));
        make.size.mas_equalTo(size);
        make.left.mas_equalTo(space);
    }];

    [self.takePhothBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.backBtn);
        make.size.mas_equalTo(size);
        make.left.mas_equalTo(self.backBtn.mas_right).mas_offset(space);
    }];

    [self.recordBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.backBtn);
        make.size.mas_equalTo(size);
        make.left.mas_equalTo(self.takePhothBtn.mas_right).mas_offset(space);
    }];
    
    [self.recordTimeBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.backBtn.mas_bottom).mas_offset(LAYOUT_WIDTH(10));
        make.centerX.mas_equalTo(self.recordBtn);
        make.size.mas_equalTo(CGSizeMake(LAYOUT_HEIGHT(50), LAYOUT_WIDTH(25)));
    }];
    
    [self.mediaBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.backBtn);
        make.size.mas_equalTo(size);
        make.left.mas_equalTo(self.recordBtn.mas_right).mas_offset(space);
    }];
    
    [self.gestureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.backBtn);
        make.size.mas_equalTo(size);
        make.left.mas_equalTo(self.mediaBtn.mas_right).mas_offset(space);
    }];
    
    [self.turnOnOrOffBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.backBtn);
        make.size.mas_equalTo(size);
        make.left.mas_equalTo(self.gestureBtn.mas_right).mas_offset(space);
    }];
    
    [self.flipBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.gestureBtn);
        make.top.mas_equalTo(self.turnOnOrOffBtn.mas_bottom).mas_offset(LAYOUT_WIDTH(15));
        make.size.mas_equalTo(size);
    }];
    
    
    [self.speedBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.backBtn);
        make.size.mas_equalTo(size);
        make.left.mas_equalTo(self.turnOnOrOffBtn.mas_right).mas_offset(space);
    }];
    
    
    
    [self.gravityBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.backBtn);
        make.size.mas_equalTo(size);
        make.left.mas_equalTo(self.speedBtn.mas_right).mas_offset(space);
    }];

    [self.circleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.backBtn);
        make.size.mas_equalTo(size);
        make.left.mas_equalTo(self.gravityBtn.mas_right).mas_offset(space);
    }];
    
//    [self.batteryImageView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(self.backBtn);
//        make.size.mas_equalTo(size);
//        make.left.mas_equalTo(self.circleBtn.mas_right).mas_offset(space);
//    }];
    
    
    [self.noHeadBTn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self).offset(-LAYOUT_WIDTH(IS_iPhoneX_TYPE?45:15));
        make.centerY.equalTo(self);
        make.size.mas_equalTo(size);
    }];
    
    [self.rotateBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self).offset(-LAYOUT_WIDTH(IS_iPhoneX_TYPE?45:15));
        make.bottom.mas_equalTo(self.noHeadBTn.mas_top).mas_offset(-LAYOUT_WIDTH(15));
        make.size.mas_equalTo(size);
    }];
    
    [self.VRBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self).offset(-LAYOUT_WIDTH(IS_iPhoneX_TYPE?45:15));
        make.bottom.mas_equalTo(self.rotateBtn.mas_top).mas_offset(-LAYOUT_WIDTH(15));
        make.size.mas_equalTo(size);
    }];
    
    [self.calibrationBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self).offset(-LAYOUT_WIDTH(IS_iPhoneX_TYPE?45:15));
        make.top.mas_equalTo(self.noHeadBTn.mas_bottom).mas_offset(LAYOUT_WIDTH(15));
        make.size.mas_equalTo(size);
    }];
    

    
    [self.voiceBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self).offset(-LAYOUT_WIDTH(IS_iPhoneX_TYPE?45:15));
        make.top.mas_equalTo(self.calibrationBtn.mas_bottom).mas_offset(LAYOUT_WIDTH(15));
        make.size.mas_equalTo(size);
    }];
    
    [self.emergencyStopBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self);
        make.centerX.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(LAYOUT_WIDTH(55), LAYOUT_WIDTH(22)));
    }];
    
    [self.oneKeyFlyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(LAYOUT_WIDTH(30));
        make.bottom.mas_equalTo(self).offset(LAYOUT_WIDTH(-15));
        make.size.mas_equalTo(size);
    }];
    
    [self.oneKeyDropBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self).offset(LAYOUT_WIDTH(-30));
        make.bottom.mas_equalTo(self).offset(LAYOUT_WIDTH(-15));
        make.size.mas_equalTo(size);
    }];

    
    
#pragma mark ---------摇杆view
    [self.leftRockerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_left).offset(MAX_LEN/4.0);
        make.centerY.mas_equalTo(self.oneKeyFlyBtn).offset(-120);
        make.height.width.mas_equalTo(LAYOUT_WIDTH(150));
    }];
    
    [self.leftRocker mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.leftRockerImageView);
        make.height.width.mas_equalTo(LAYOUT_WIDTH(150));
    }];
    
    [self.rightRockerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_right).offset(-MAX_LEN/4.0);
        make.centerY.mas_equalTo(self.oneKeyFlyBtn).offset(-120);
        make.height.width.mas_equalTo(LAYOUT_WIDTH(150));
    }];
    
    [self.rightRocker mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.rightRockerImageView);
        make.height.width.mas_equalTo(LAYOUT_WIDTH(150));
    }];
    
    self.leftRocker.delegate = self;
    self.rightRocker.delegate = self;
    
    self.leftRockerDragBK.delegate = self;
    [self.leftRockerDragBK mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self);
        make.left.mas_equalTo(self.oneKeyFlyBtn.mas_right).offset(LAYOUT_WIDTH(5));
        make.right.mas_equalTo(self.mas_centerX).offset(LAYOUT_WIDTH(-30));
        make.top.mas_equalTo(self);
    }];
    
    self.rightRockerDragBK.delegate = self;
    [self.rightRockerDragBK mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self);
        make.right.mas_equalTo(self.oneKeyDropBtn.mas_left).offset(LAYOUT_WIDTH(5));
        make.left.mas_equalTo(self.mas_centerX).offset(LAYOUT_WIDTH(30));
        make.top.mas_equalTo(self);
    }];
    
    [self sendSubviewToBack:self.leftRockerDragBK];
    [self sendSubviewToBack:self.rightRockerDragBK];
    
    
//    [self.leftTiaoJieView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.equalTo(self.oneKeyFlyBtn);
//        make.centerX.equalTo(self.leftRockerImageView);
//    }];
//
//    [self.rightTiaoJieView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.equalTo(self.oneKeyDropBtn);
//        make.centerX.equalTo(self.rightRockerImageView);
//    }];
//
//
//
//    [self.cemianTiaoJieView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.mas_equalTo(self).offset(20);
//        make.centerY.equalTo(self);
//    }];
//
//    CGAffineTransform a = CGAffineTransformIdentity;
//    [self.cemianTiaoJieView setTransform:CGAffineTransformRotate(a, M_PI/2)];
//
//    [self layoutIfNeeded];
    
    [self.rotateAdjustView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.oneKeyFlyBtn);
        make.centerX.equalTo(self.leftRockerImageView);
    }];
    
    [self.leftOrRightAdjustView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.oneKeyDropBtn);
        make.centerX.equalTo(self.rightRockerImageView);
    }];
    
    
    
    [self.fontdOrBackAdjustView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self).offset(20);
        make.centerY.equalTo(self);
    }];
    
    CGAffineTransform a = CGAffineTransformIdentity;
    [self.fontdOrBackAdjustView setTransform:CGAffineTransformRotate(a, M_PI/2)];
    

    self.leftRockerDragBK.hidden = self.leftRockerDragBK.hidden = self.leftRockerImageView.hidden = self.leftRocker.hidden =
    self.rightRocker.hidden = self.rightRockerDragBK.hidden =  self.rightRockerImageView.hidden =
    self.flipBtn.hidden  =    self.oneKeyFlyBtn.hidden   =  self.oneKeyDropBtn.hidden     =   self.emergencyStopBtn.hidden =  self.rotateAdjustView.hidden=
    self.leftOrRightAdjustView.hidden=
    self.fontdOrBackAdjustView.hidden =YES;
    
    [self layoutIfNeeded];
}

#pragma mark - 触摸回调
//left
- (void)leftTouchDown:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if ([self.leftRocker respondsToSelector:@selector(touchesBegan:withEvent:)]) {
        [self.leftRocker touchesBegan:touches withEvent:event];
    }
}

- (void)leftTouchMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if ([self.leftRocker respondsToSelector:@selector(touchesMoved:withEvent:)]) {
        [self.leftRocker touchesMoved:touches withEvent:event];
    }
}

- (void)leftTouchEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    if ([self.leftRocker respondsToSelector:@selector(touchesEnded:withEvent:)]) {
        [self.leftRocker touchesEnded:touches withEvent:event];
    }
}

//right
- (void)rightTouchDown:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if ([self.rightRocker respondsToSelector:@selector(touchesBegan:withEvent:)]) {
        [self.rightRocker touchesBegan:touches withEvent:event];
    }
}

- (void)rightTouchMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if ([self.rightRocker respondsToSelector:@selector(touchesMoved:withEvent:)]) {
        [self.rightRocker touchesMoved:touches withEvent:event];
    }
}

- (void)rightTouchEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    if ([self.rightRocker respondsToSelector:@selector(touchesEnded:withEvent:)]) {
        [self.rightRocker touchesEnded:touches withEvent:event];
    }
}

- (void)DragCircleTouchBegan:(UIView *)view touches:(NSSet *)touches withEvent:(UIEvent *)event{
    if (view == self.leftRocker)
    {
        CGPoint pt = [[touches anyObject] locationInView:self];
        if (pt.x > MAX_LEN/2.0 - LAYOUT_WIDTH(80)) {
            pt = CGPointMake(MAX_LEN/2.0 - LAYOUT_WIDTH(80), pt.y);
        }
        [self.leftRockerImageView setCenter:pt];
        [self.leftRocker setCenter:pt];
    }else if (view == self.rightRocker)
    {
        CGPoint pt = [[touches anyObject] locationInView:self];
        if (pt.x < MAX_LEN/2.0 + LAYOUT_WIDTH(80)) {
            pt = CGPointMake(MAX_LEN/2.0 + LAYOUT_WIDTH(80), pt.y);
        }
        [self.rightRockerImageView setCenter:pt];
        [self.rightRocker setCenter:pt];
    }
}

-(void)DragCircleTouchMoved:(UIView *)view touches:(NSSet *)touches withEvent:(UIEvent *)event{
    
    self.vrDurtion = 2;
    
    if (view == self.leftRocker){
        self.leftRockerImageView.hidden = NO;
        self.leftRocker.hidden = NO;
        
    }else if (view == self.rightRocker){
        self.leftRockerImageView.hidden = NO;
        self.rightRocker.hidden = NO;
    }
}

- (void)DragCircleTouchEnded:(UIView *)view touches:(NSSet *)touches withEvent:(UIEvent *)event{
    if (view == self.leftRocker){

        self.leftRockerImageView.center = self.leftRocker.center = CGPointMake(MAX_LEN/4.0, self.oneKeyFlyBtn.center.y-120);
        
    }else if (view == self.rightRocker){

        self.rightRockerImageView.center = self.rightRocker.center = CGPointMake(MAX_LEN/4.0 * 3, self.oneKeyDropBtn.center.y-120);
    }
    
    [self layoutIfNeeded];
}

-(UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event{
    UIView* view = [super hitTest:point withEvent:event];
    
    if (self.VRBtn.selected) {
        if (self.vrTimer) {
            self.vrDurtion = 2;
        }else{
            [self setVrHidden:NO];
            self.vrTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(VRHiddenAction) userInfo:nil repeats:YES];
            self.vrDurtion = 2;
        }
    }
    
    
    return view;
}


-(void)VRHiddenAction{
    if (self.vrDurtion <= 0) {
        if (self.vrTimer) {
            [self.vrTimer invalidate];
            self.vrTimer = nil;
        }
        [self setVrHidden:YES];
    }
    
    self.vrDurtion -= 1;
}

-(void)setVrHidden:(BOOL)hidden{
    if (hidden) {
        
        for (UIView *view in self.subviews) {
            
            [UIView animateWithDuration:.5 animations:^{
               
                view.alpha = 0;
                
            }];
        }

    }else{
        
        for (UIView *view in self.subviews) {
            
            [UIView animateWithDuration:.5 animations:^{
               
                view.alpha = 1;
                
            }];
        }

    }
}


@end
