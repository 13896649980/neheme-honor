//
//  ControllerView.h
//  NonGPS Project
//
//  Created by Xu pan on 2019/8/14.
//  Copyright © 2019 Xu pan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DragCircle.h"
#import "LeftDragBK.h"
#import "RightDragBK.h"
#import "WeitiaoView.h"
#import "TiaoJieView.h"
#import <CoreMotion/CoreMotion.h>



@interface ControllerView : UIView

@property   (nonatomic,strong)      UIButton            *backBtn; //返回
@property   (nonatomic,strong)      UIButton            *recordBtn; //录像
@property   (nonatomic,strong)      UIButton            *takePhothBtn; //拍照
@property   (nonatomic,strong)      UIButton            *speedBtn; //速度
@property   (nonatomic,strong)      UIButton            *mediaBtn; //媒体
@property   (nonatomic,strong)      UIButton            *gravityBtn; //重力
@property   (nonatomic,strong)      UIButton            *turnOnOrOffBtn; //开关
@property   (nonatomic,strong)      UIButton            *imageReversBtn; //画面反转
@property   (nonatomic,strong)      UIButton            *VRBtn; //3D
@property   (nonatomic,strong)      UIButton            *noHeadBTn; //无头
@property   (nonatomic,strong)      UIButton            *calibrationBtn; //校准
@property   (nonatomic,strong)      UIButton            *dinggaoBtn; //定高
@property   (nonatomic,strong)      UIButton            *oneKeyFlyBtn; //一键起飞
@property   (nonatomic,strong)      UIButton            *oneKeyDropBtn; //一键下降
@property   (nonatomic,strong)      UIButton            *emergencyStopBtn; //紧急停止
@property   (nonatomic,strong)      UIButton            *openTrackBtn; //打开轨迹飞行
@property   (nonatomic,strong)      UIButton            *flipBtn; //飞机翻转
@property   (nonatomic,strong)      UIButton            *cameraUpBtn; //云台上
@property   (nonatomic,strong)      UIButton            *cameraDownBtn; //云台下
@property   (nonatomic,strong)      UIButton            *filterBtn; //滤镜
@property   (nonatomic,strong)      UIButton            *musicMicBtn;//添加音乐
@property   (nonatomic,strong)      UIButton            *imageAmplificationBtn;//图像放大
@property   (nonatomic,strong)      UIButton            *imageNarrowBtn;//图像缩小
@property   (nonatomic,strong)      UIButton            *voiceBtn;//录制声音
@property   (nonatomic,strong)      UIButton            *switchLensBtn;//镜头切换
@property   (nonatomic,strong)      UIButton            *circleBtn;//环绕飞行
@property   (nonatomic,strong)      UIButton            *rotateBtn;//旋转飞行

@property   (nonatomic,strong)      UIImageView         *batteryImageView; //电量图片


#pragma mark - 摇杆
@property   (nonatomic,strong)      UIImageView         *leftRockerImageView; //摇杆图片
@property   (nonatomic,strong)      DragCircle          *leftRocker; //摇杆
@property   (nonatomic,strong)      LeftDragBK          *leftRockerDragBK; //触碰出摇杆背景view
@property   (nonatomic,strong)      UIImageView         *rightRockerImageView;
@property   (nonatomic,strong)      DragCircle          *rightRocker;
@property   (nonatomic,strong)      RightDragBK         *rightRockerDragBK;


@property   (nonatomic,strong)      TiaoJieView         *leftTiaoJieView;

@property   (nonatomic,strong)      TiaoJieView         *rightTiaoJieView;

@property   (nonatomic,strong)      TiaoJieView         *cemianTiaoJieView;


#pragma mark - 微调
@property   (nonatomic,strong)      WeitiaoView         *rotateAdjustView;
@property   (nonatomic,strong)      WeitiaoView         *leftOrRightAdjustView;
@property   (nonatomic,strong)      WeitiaoView         *fontdOrBackAdjustView;


#ifdef OPENCV_ENABLE
#pragma mark - 手势拍照录像，图像跟随
@property   (nonatomic,strong)      UIButton                *startFollowBtn;
@property   (nonatomic,strong)      UIButton                *openFollowBtn;
@property   (nonatomic,strong)      UIButton                *gestureBtn; //手势拍照录像
@property   (nonatomic,strong)      RACSubject              *gestureSignal; //手势拍照录像
@property   (nonatomic,strong)      RACSubject              *openFollowSignal; //打开跟随画框
@property   (nonatomic,strong)      RACSubject              *startFollowSignal; //开始跟随

#endif

#pragma mark - 按钮点击信号
@property   (nonatomic,strong)      RACSubject              *backSignal; //返回
@property   (nonatomic,strong)      RACSubject              *recordSignal; //录像
@property   (nonatomic,strong)      RACSubject              *takePhothSignal; //拍照
@property   (nonatomic,strong)      RACSubject              *speedSignal; //速度
@property   (nonatomic,strong)      RACSubject              *mediaSignal; //媒体
@property   (nonatomic,strong)      RACSubject              *turnOnOrOffSignal; //开关
@property   (nonatomic,strong)      RACSubject              *imageReversSignal; //画面反转
@property   (nonatomic,strong)      RACSubject              *VRSignal; //3D
@property   (nonatomic,strong)      RACSubject              *noHeadSignal; //无头
@property   (nonatomic,strong)      RACSubject              *calibrationSignal; //校准
@property   (nonatomic,strong)      RACSubject              *dinggaoSignal; //定高
@property   (nonatomic,strong)      RACSubject              *oneKeyFlySignal; //一键起飞
@property   (nonatomic,strong)      RACSubject              *oneKeyDropSignal; //一键下降
@property   (nonatomic,strong)      RACSubject              *emergencyStopSignal; //紧急停止
@property   (nonatomic,strong)      RACSubject              *openTrackSignal; //打开轨迹飞行
@property   (nonatomic,strong)      RACSubject              *flipSignal; //飞机翻转
@property   (nonatomic,strong)      RACSubject              *cameraUpSignal; //云台上
@property   (nonatomic,strong)      RACSubject              *cameraDownSignal; //云台下
@property   (nonatomic,strong)      RACSubject              *filterSignal; //滤镜
@property   (nonatomic,strong)      RACSubject              *musicMixSignal;//混合音乐
@property   (nonatomic,strong)      RACSubject              *imageAmplificationSignal;//图像放大
@property   (nonatomic,strong)      RACSubject              *imageNarrowSignal;//图像缩小
@property   (nonatomic,strong)      RACSubject              *voiceSignal;//录制声音
@property   (nonatomic,strong)      RACSubject              *switchLensSignal;//镜头切换

@property   (nonatomic,strong)      CMMotionManager         *motionManager;

@property   (nonatomic,strong)      NSTimer                 *recordDurtionTimer;

@property   (nonatomic,assign)      int                     vrDurtion;

@property   (nonatomic,strong)      NSTimer                 *vrTimer;




-(void)trackCancelled;
#pragma mark - 录像计时UI操作
-(void)setStatueWithStarRecord;
-(void)setStatueWithEndRecord;

@end


