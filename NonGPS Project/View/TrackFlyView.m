//
//  TrackFlyView.m
//  NonGPS Project
//
//  Created by Xu pan on 2020/11/11.
//  Copyright © 2020 Xu pan. All rights reserved.
//

#import "TrackFlyView.h"

@interface TrackFlyView()<CAAnimationDelegate>

@property   (nonatomic,strong)      UIBezierPath            *path;
@property   (nonatomic,strong)      UIImageView             *trackPlaneImageView;
@property   (nonatomic,strong)      CAKeyframeAnimation     *trackAnimation;
@property   (nonatomic,strong)      NSTimer                 *calculateTimer;

@end

@implementation TrackFlyView{
    
    NSMutableArray *totalPoints;
    float           totalPathLen;
    CGPoint         lastPlanePoint;
    CGFloat         durtion;
}

- (UIBezierPath *)path{
    if (!_path) {
        _path = [[UIBezierPath alloc] init];
        _path.lineJoinStyle = kCGLineJoinRound;
        _path.lineCapStyle = kCGLineCapRound;
        _path.lineWidth = 5;
    }
    return _path;
}

-(UIImageView *)trackPlaneImageView{
    if (!_trackPlaneImageView) {
        _trackPlaneImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        _trackPlaneImageView.backgroundColor = [UIColor clearColor];
        _trackPlaneImageView.image = [UIImage imageNamed:@"tab_device_down"];
        _trackPlaneImageView.hidden = YES;
        [self addSubview:_trackPlaneImageView];
    }
    return _trackPlaneImageView;
}

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.3];
        self.clipsToBounds = YES;
        totalPoints = [NSMutableArray array];
        totalPathLen = 0;
    }
    return self;
}

-(instancetype)init{
    self = [super init];
    if (self){
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.3];
        self.clipsToBounds = YES;
        totalPoints = [NSMutableArray array];
        totalPathLen = 0;
    }
    return self;
}

-(void)cancelTrackFlyAction{
    self.trackPlaneImageView.hidden = YES;
    [self.trackPlaneImageView.layer removeAllAnimations];
    [totalPoints removeAllObjects];
    totalPathLen = 0;
    durtion = 0;
    lastPlanePoint = CGPointZero;
    [self.path removeAllPoints];
    [self setNeedsDisplay];
    
    if (self.calculateTimer) {
        [self.calculateTimer invalidate];
        self.calculateTimer = nil;
    }
}

- (void)drawRect:(CGRect)rect{
    //draw path
    [[UIColor clearColor] setFill];
    [[[UIColor whiteColor] colorWithAlphaComponent:.7] setStroke];
    [self.path stroke];
}


#pragma mark - 移动触摸事件
//手指开始移动
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self cancelTrackFlyAction];
    //get the starting point
    CGPoint point = [[touches anyObject] locationInView:self];
    //move the path drawing cursor to the starting point
    [self.path moveToPoint:point];
    
    [totalPoints addObject:[NSValue valueWithCGPoint:point]];//添加xy坐标
}

//手指正在移动
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    //get the current point
    CGPoint point = [[touches anyObject] locationInView:self];
    //add a new line segment to our path
    [self.path addLineToPoint:point];
    //redraw the view
    [self setNeedsDisplay];
    [totalPoints addObject:[NSValue valueWithCGPoint:point]];//添加xy坐标
}

//手指离开屏幕
- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event{
    
    if (totalPoints.count <= 1){
        [totalPoints removeAllObjects];
        totalPathLen = 0;
        lastPlanePoint = CGPointZero;
        [self.path removeAllPoints];
        [self setNeedsDisplay];
        return;
    }
    
    //先算出所有点连接的总长
    NSValue *lastValue;
    for (int i = 0; i < totalPoints.count; i++) {
        if (lastValue) {
            CGPoint lastPoint = [lastValue CGPointValue];
            CGPoint currentPoint = [totalPoints[i] CGPointValue];
            totalPathLen += [self distanceFromPointX:lastPoint distanceToPointY:currentPoint];
        }
        lastValue = totalPoints[i];
    }
    
    //飞机运动动画
    lastPlanePoint = CGPointZero;
    self.trackPlaneImageView.hidden = NO;
    [self.trackPlaneImageView.layer removeAllAnimations];
    
    if (self.trackAnimation) {
        self.trackAnimation.delegate = nil;
        self.trackAnimation = nil;
    }
    self.trackAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    self.trackAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    self.trackAnimation.removedOnCompletion = NO;
    self.trackAnimation.fillMode = @"forwards";
    self.trackAnimation.delegate = self;
    durtion = self.trackAnimation.duration = totalPathLen/70;
    self.trackAnimation.path = self.path.CGPath;
    [self.trackPlaneImageView.layer addAnimation:self.trackAnimation forKey:@"trackDrawView"];
    
    //定时器算飞机偏移
    if (self.calculateTimer) {
        [self.calculateTimer invalidate];
        self.calculateTimer = nil;
    }
    self.calculateTimer = [NSTimer scheduledTimerWithTimeInterval:0.04 target:self selector:@selector(calculateAction) userInfo:nil repeats:YES];
}

#pragma mark - AnimationDelegate
- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    if (flag == NO){
        return;
    }
    if (anim.duration == durtion) {
        if (self.calculateTimer) {
            [self.calculateTimer invalidate];
            self.calculateTimer = nil;
        }
    
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            if (anim.duration == self->durtion) {
                [self.path removeAllPoints];
                [self setNeedsDisplay];
                self.trackPlaneImageView.hidden = YES;
            }
        });
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(droneAnimationEnded)]) {
            [self.delegate droneAnimationEnded];
        }
    }
}

#pragma mark - 计算两个点之间的距离
-(float)distanceFromPointX:(CGPoint)start distanceToPointY:(CGPoint)end{
    float distance;
    CGFloat xDist = (end.x - start.x);
    CGFloat yDist = (end.y - start.y);
    distance = sqrt((xDist * xDist) + (yDist * yDist));
    return distance;
}

#pragma mark - 定时器计算飞机夹角
-(void)calculateAction{
    if (!lastPlanePoint.x && !lastPlanePoint.y) {
        CGRect frame = [self.trackPlaneImageView.layer.presentationLayer frame];
        lastPlanePoint = CGPointMake(frame.origin.x + frame.size.width/2.0, frame.origin.y + frame.size.height/2.0);
    }else{
        CGRect frame = [self.trackPlaneImageView.layer.presentationLayer frame];
        CGPoint currentPoint = CGPointMake(frame.origin.x + frame.size.width/2.0, frame.origin.y + frame.size.height/2.0);
        if (self.delegate && [self.delegate respondsToSelector:@selector(getCurrentPositionAngle:)]) {
            [self.delegate getCurrentPositionAngle:[self angleFromPointX:lastPlanePoint distanceToPointY:currentPoint]];
        }
        lastPlanePoint = currentPoint;
    }
}

#pragma mark - 计算两个点之间的角度
-(float)angleFromPointX:(CGPoint)start distanceToPointY:(CGPoint)end{
    CGFloat rads = atan2(end.x - start.x, end.y - start.y);
    return rads;
//    return ((rads) * (180.0 / M_PI));
}

@end
