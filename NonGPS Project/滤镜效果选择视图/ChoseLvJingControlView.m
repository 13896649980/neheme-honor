//
//  MenuControlView.m
//  SSMenuScrollView
//
//  Created by feng on 2017/8/23.
//  Copyright © 2017年 皮蛋. All rights reserved.
//


#import "ChoseLvJingControlView.h"
#import "CustomerScrollViewModel.h"
#import "MenuCollectionCell.h"

@interface ChoseLvJingControlView()<UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate>{

    CGRect _frame;

}
@property (strong, nonatomic) UIPageControl *pageControl;
@property (strong, nonatomic) UICollectionView *collectionView;
/// 一页可容纳的最多Item数目
@property (nonatomic,assign) NSInteger numberOfSinglePage;
@property (strong, nonatomic) NSMutableArray *dataArray;
@property (nonatomic,assign) NSInteger choseIndex;

@end

@implementation ChoseLvJingControlView

-(RACSubject *)hideSignal{
    if (!_hideSignal) {
        _hideSignal = [RACSubject subject];
    }
    return _hideSignal;
}


- (instancetype)initWithmacCol:(NSInteger )maxCol maxRow:(NSInteger )maxRow
{
    if (self = [super init]) {
        self.frame =CGRectMake(0, MIN_LEN-150, MAX_LEN, 150) ;
        _frame =CGRectMake(0, MIN_LEN-150, MAX_LEN, 150);
        self.backgroundColor = [UIColor clearColor];
        _maxRow = maxRow==0?1:maxRow;
        _maxCol = maxCol==0?1:maxCol;
        _dataArray = [NSMutableArray array];
        NSArray * names = @[@"原图",@"童话",@"日落",@"浪漫",@"复古",@"怀旧",@"平静",@"拿铁",@"温柔",@"冰冷",@"常青"];
        
        NSArray * images = @[@"filter_thumb_original",@"filter_thumb_fairytale",@"filter_thumb_sunset",@"filter_thumb_romance",@"filter_thumb_antique",@"filter_thumb_nostalgia",@"filter_thumb_calm",@"filter_thumb_latte",@"filter_thumb_tender",@"filter_thumb_cool",@"filter_thumb_evergreen"];

   
        NSArray * filterNames = @[@"原图",@"童话",@"日落",@"浪漫",@"复古",@"怀旧",@"平静",@"拿铁",@"温柔",@"冰冷",@"常青"];
    
        for (int i=0; i<filterNames.count; i++) {
            CustomerScrollViewModel * model = [[CustomerScrollViewModel alloc ] init];
            model.name = TranslationString(names[i]);
            model.icon =    images[i];
            if (i==0){
                model.color = RGB_COLORWITHRGB(190, 190, 190);
            }else if (i==1){
                model.color = RGB_COLORWITHRGB(48, 187, 224);
            }else if (i==2){
                model.color = [UIColor orangeColor];
            }else if (i==3||i==4||i==5){
                model.color = RGB_COLORWITHRGB(53, 150, 53);
            }else if (i==6||i==7||i==8){
                model.color = RGB_COLORWITHRGB(194, 209, 36);
            }else if (i==9){
                model.color = RGB_COLORWITHRGB(84, 52, 255);
            }else if (i==10){
                model.color = RGB_COLORWITHRGB(28, 48, 81);
            }else if (i==11){
                model.color = RGB_COLORWITHRGB(190, 190, 190);
            }
            model.filterName = TranslationString(filterNames[i]);
            [_dataArray addObject:model];
        }
        [self setViews];
        
        [self.collectionView reloadData];
    }
    return self;
}

- (void)setViews
{
    //添加到主视图
    [self addSubview:self.collectionView];
    self.numberOfSinglePage = _maxCol * _maxRow;
    
    NSInteger pageCount = _dataArray.count / self.numberOfSinglePage;
    if (_dataArray.count % self.numberOfSinglePage > 0) {
        pageCount += 1;
    }
    self.pageControl.numberOfPages = pageCount;//指定页面个数
    self.pageControl.currentPage = 0;//指定pagecontroll的值，默认选中的小白点（第一个）
//    [self addSubview:self.pageControl];
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:button];
    button.backgroundColor = [UIColor whiteColor];
    [button setImage:[UIImage imageNamed:@"ic_filters_bar_close_normal"]  forState:UIControlStateNormal];
    [button addTarget:self action:@selector(hide) forControlEvents:UIControlEventTouchUpInside];
    button.frame= CGRectMake(0, _frame.size.height-20, MAX_LEN, 20);
}

- (void)hide{
    [self.hideSignal sendNext:nil];
    self.hidden = YES;
}

- (void)reloadData{
    [self.collectionView reloadData];
}


#pragma mark - UICollectionView
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    return _dataArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MenuCollectionCell *cell =
    [MenuCollectionCell cellWithCollectionView:collectionView
                                    forIndexPath:indexPath];
    CustomerScrollViewModel * model = _dataArray[indexPath.row];
    cell.model  =model;
    if (_choseIndex==indexPath.row) {
        cell.choseImg.hidden = NO;
        CGFloat red = 0.0;
        CGFloat green = 0.0;
        CGFloat blue = 0.0;
        CGFloat alpha = 0.0;
        [model.color getRed:&red green:&green blue:&blue alpha:&alpha];
        cell.choseImg.backgroundColor = [UIColor colorWithRed:red green:green blue:blue alpha:0.5];
    }else{
        cell.choseImg.hidden = YES;
    }
    return cell;
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat width = 100;
    CGFloat height = 130;
    return CGSizeMake(width, height);
    
}
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    return UIEdgeInsetsMake(0, 0, 0, 0);
}
// 两行之间的最小间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}

// 两个cell之间的最小间距，是由API自动计算的，只有当间距小于该值时，cell会进行换行
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
//    int page = scrollView.contentOffset.x / scrollView.frame.size.width;
//    self.pageControl.currentPage = page;
//}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    _choseIndex = indexPath.row;
    [self reloadData];
    CustomerScrollViewModel * model = _dataArray[indexPath.row];
    [self.lvJingType sendNext:model.color];
    self.userInteractionEnabled = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.userInteractionEnabled = YES;
    });
}

- (UIPageControl *)pageControl{
    if (!_pageControl) {
        _pageControl = [[UIPageControl alloc] init];
        _pageControl.frame = CGRectMake(0, _frame.size.height-10, _frame.size.width, 10);
        _pageControl.pageIndicatorTintColor = [UIColor redColor];
        _pageControl.currentPageIndicatorTintColor = [UIColor greenColor];
        _pageControl.hidesForSinglePage = YES;
        _pageControl.backgroundColor = [UIColor blueColor];
    }
    return _pageControl;
}

- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        //设置滚动方向为垂直滚动，说明方块是从左上到右下的布局排列方式
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        //创建容器视图
        CGRect frame = CGRectMake(0,0, _frame.size.width,130);
        _collectionView = [[UICollectionView alloc] initWithFrame:frame
                                                              collectionViewLayout:layout];
        _collectionView.delegate = self;//设置代理
        _collectionView.dataSource = self;//设置数据源
        _collectionView.backgroundColor = [UIColor whiteColor];//设置背景，默认为黑色
//        _collectionView.pagingEnabled = YES;
        _collectionView.bounces = YES;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.showsHorizontalScrollIndicator = NO;
        
        [_collectionView registerClass:[MenuCollectionCell class]
           forCellWithReuseIdentifier:[MenuCollectionCell cellIdentifier]];

        
    }
    return _collectionView;
}


-(RACSubject *)lvJingType{
    if (!_lvJingType) {
        _lvJingType = [RACSubject subject];
    }
    return _lvJingType;
}



@end
