//
//  MenuControlView.h
//  SSMenuScrollView
//
//  Created by feng on 2017/8/23.
//  Copyright © 2017年 皮蛋. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <ReactiveObjC/ReactiveObjC.h>

@interface ChoseLvJingControlView : UIView
- (instancetype)initWithmacCol:(NSInteger )maxCol maxRow:(NSInteger )maxRow;

@property (nonatomic,assign) NSInteger maxCol; // 列
@property (nonatomic,assign) NSInteger maxRow; // 行
@property   (nonatomic,strong)  RACSubject      *lvJingType;
@property   (nonatomic,strong)  RACSubject      *hideSignal;

- (void)reloadData;

@end
