//
//  MenuCollectionCell.m
//  SSMenuScrollView
//
//  Created by feng on 2017/8/23.
//  Copyright © 2017年 皮蛋. All rights reserved.
//


#import "MenuCollectionCell.h"

@interface MenuCollectionCell ()



@end

@implementation MenuCollectionCell

+ (NSString *)cellIdentifier{
    static NSString *cellIdentifier = @"CollectionViewCellIdentifier";
    return cellIdentifier;
}
+ (instancetype)cellWithCollectionView:(UICollectionView *)collectionView
                          forIndexPath:(NSIndexPath *)indexPath
{
    MenuCollectionCell *cell =
    [collectionView dequeueReusableCellWithReuseIdentifier:[MenuCollectionCell cellIdentifier]
                                              forIndexPath:indexPath];
    return cell;
}
- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
       
        CGFloat itemHeight = frame.size.height-20;
        CGFloat itemWidth = frame.size.width-10;
        _imageBtn = [UIButton buttonWithType:UIButtonTypeCustom ];
        _imageBtn.frame =CGRectMake(10, 10,itemWidth , itemHeight);
        _choseImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"take_filter_confirm_btn_skin_flat"]];
        _choseImg.contentMode =  UIViewContentModeCenter;
        _choseImg.hidden = YES;
        _choseImg.frame =CGRectMake(10, 10,itemWidth , itemHeight-20);
        [self.contentView addSubview:_imageBtn];

        [self.contentView addSubview:_choseImg];
        _imageBtn.userInteractionEnabled = NO;
        _titleLable = [[UILabel alloc] initWithFrame:CGRectMake(10, self.frame.size.height - 30, itemWidth, 20)];
        _titleLable.textAlignment = NSTextAlignmentCenter;
        _titleLable.textColor = [UIColor whiteColor];
        _titleLable.backgroundColor = [UIColor grayColor];

        _titleLable.font = [UIFont systemFontOfSize:13];
        [self.contentView  addSubview:_titleLable];
    
    }
    return self;
}


- (void)setModel:(CustomerScrollViewModel *)model{
    _model = model;
    [_imageBtn setImage:[UIImage imageNamed:model.icon] forState:UIControlStateNormal];
    _titleLable.text = model.name;
    _titleLable.backgroundColor = model.color;
}

@end
