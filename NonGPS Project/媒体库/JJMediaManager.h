//
//  JJPhotoManager.h
//  JJRC
//
//  Created by huang mindong on 17/4/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JJFile.h"

@interface JJMediaManager : NSObject


+ (void)savePhoto:(UIImage *)photo
         withName:(NSString *)name
  addToPhotoAlbum:(BOOL)addToPhotoAlbum
         callback:(void(^)(void))callback;

+ (void)deletePhoto:(NSArray<NSString *> *)names callback:(void(^)(void))callback;

+ (void)saveVideoThumb:(UIImage *)videoThumb
              withName:(NSString *)name
              callback:(void(^)(void))callback;

+ (void)saveVideoPhoto:(UIImage *)videoPhoto
              withName:(NSString *)name
              callback:(void(^)(void))callback;

+ (void)deleteVideoThumb:(NSArray<NSString *>  *)name callback:(void(^)(void))callback;

+ (void)exportPhotoWithName:(NSArray<NSString *> *)names exportCallback:(void(^)(void))callback;
+ (void)exportVideoWithName:(NSArray<NSString *> *)names callback:(void(^)(void))callback;

+ (void)saveFileToAlbum:(NSArray <JJFile *>*)files callback:(void(^)(int ret))callback;
+ (void)deleteFile:(NSArray <JJFile *>*)files callback:(void(^)(int ret))callback;

+ (void )fetchPhoneDocumentsFileAttributeWithDirName:(NSString *)dirName callback:(void (^)(NSArray <NSDictionary *>*fileAttbs))callback;

+ (NSMutableArray <NSDictionary *>*)fetchFilesAttrib:(NSString *)path;
@end
