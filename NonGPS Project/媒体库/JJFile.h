//
//  JJFile.h
//  JJRC
//
//  Created by mac on 17/4/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

#define        FILE_TYPE_DIR        1 //目录
#define     FILE_TYPE_VIDEO        2 //视频
#define     FILE_TYPE_AUDIO        3 //音频
#define     FILE_TYPE_PIC        4 //图片
#define     FILE_TYPE_DOC        5 //文档
#define        FILE_TYPE_ALL_FILE    6 //视频、 音频、 图片、 文档
#define        FILE_TYPE_COMP        7 //压缩文件
#define        FILE_TYPE_ELSE        8 //其他文件
#define     FILE_TYPE_DOWN        9 //正在下载的文件
#define        FILE_TYPE_ALL        10 //全部文件
#define     MSG_TYPE_SMSCNT        11 //短信文件
#define     MSG_TYPE_PBCNT        12 //通讯录文件

#define        FILE_TYPE_HID        128 //隐藏文件
#define        FILE_TYPE_SYS        256 //系统文件
typedef enum
{
    JJ_Folder = 1,  //文件夹
    JJ_Video = 2, //视频
    JJ_Audio = 3,  //音频
    JJ_Picture = 4, //图片
    JJ_Doc = 5, //文档
    //    JJ_ALL_FILE = 6,//视频、 音频、 图片、 文档
    JJ_Zip = 7, //压缩文档
    JJ_Other = 8, //其他
    JJ_Down = 9,//正在下载的文件
    //    JJ_ALL = 10,//全部文件
    //   JJ_SMS = 11,//短信文件
    JJ_Contact = 12,//联系人
    
}JJ_FileType;

@interface JJFile : NSObject
@property(nonatomic,assign)JJ_FileType type; //文件 文件夹 图片 音频 视频 其他
@property(nonatomic,assign)long long size; //byte
@property(nonatomic,copy)NSString *name; //名字
@property(nonatomic,copy)NSString *creationDate; //创建日期
@property(nonatomic,copy)NSString *modifiCationDate; //修改日期
@property(nonatomic,copy)NSString *path;
@property(nonatomic,copy)NSString *sourcePath;

@property(nonatomic,copy)NSString *videoPath;
@property (nonatomic,assign)BOOL isSelect;
@property (nonatomic,assign)BOOL isPlaying;


- (id)initWithFileAttribs:(NSDictionary *)attribs;

@end


