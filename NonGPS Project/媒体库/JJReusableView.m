//
//  JJReusableView.m
//  JJRC
//
//  Created by mac on 17/4/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "JJReusableView.h"

@implementation JJReusableView
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = RGB_COLORWITHRGB(18, 28, 38);
        UILabel *label = [[UILabel alloc]initWithFrame:self.bounds];
        label.textAlignment = NSTextAlignmentLeft;
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor whiteColor];
        [self addSubview:label];
        
        UIView *yuandian = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 5, 5)];
        yuandian.backgroundColor = [UIColor whiteColor];
        yuandian.layer.borderColor = [UIColor clearColor].CGColor;
        yuandian.layer.borderWidth = 0;
        yuandian.layer.cornerRadius = yuandian.bounds.size.width/2;
        yuandian.layer.masksToBounds = YES;
        yuandian.hidden = YES;
        [self addSubview:yuandian];
        
        
        __weak typeof(self)weakSelf = self;
        
        [yuandian mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(weakSelf.mas_centerY);
            make.left.equalTo(weakSelf.mas_left).offset(15);
            make.size.mas_equalTo(CGSizeMake(5, 5));
        }];
        
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(weakSelf.mas_top);
            make.left.equalTo(self.mas_left).offset(12);
            make.right.equalTo(weakSelf.mas_right).offset(-50);
            make.bottom.equalTo(weakSelf.mas_bottom);
        }];

    }
    return self;
}

-(void)setText:(NSString *)text
{
    _text = text;
    ((UILabel *)self.subviews[0]).text = text;
}

@end
