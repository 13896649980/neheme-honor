//
//  HMDVideoPlayerViewController.m
//  720P_TARGET
//
//  Created by mac on 17/3/15.
//  Copyright © 2017年 huang mindong. All rights reserved.
//

#import "HMDVideoPlayerViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVKit/AVKit.h>
#import "ZFPlayer.h"

@interface HMDVideoPlayerViewController ()
@property(nonatomic,strong)ZFPlayerView *zfPlayerView;
@end

@implementation HMDVideoPlayerViewController

- (void)dealloc
{
    
}

- (void)interfaceOrientation:(UIInterfaceOrientation)orientation
{
    if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)])
    {
        SEL selector             = NSSelectorFromString(@"setOrientation:");
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[UIDevice instanceMethodSignatureForSelector:selector]];
        [invocation setSelector:selector];
        [invocation setTarget:[UIDevice currentDevice]];
        int val                  = orientation;
        [invocation setArgument:&val atIndex:2];
        [invocation invoke];
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [HMDVideoPlayerViewController attemptRotationToDeviceOrientation];
    [self interfaceOrientation:UIInterfaceOrientationLandscapeLeft];
    // Do any additional setup after loading the view.
    self.view.backgroundColor  = [ UIColor blackColor];
    
    ZFPlayerView *view = [[ZFPlayerView alloc] initWithFrame:self.view.bounds];
    view.videoURL = _movieURL;
    [view autoPlayTheVideo];
    [self.view addSubview:view];
    
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.equalTo(self.view.mas_top);
        make.bottom.equalTo(self.view.mas_bottom);
        make.right.equalTo(self.view.mas_right);
    }];
   
    __weak typeof(self)weakSelf = self;
    view.goBackBlock = ^(){

        if ([view respondsToSelector:@selector(moviePlayDidEnd:)]) {
            [view performSelector:@selector(moviePlayDidEnd:) withObject:nil];
        }
        [weakSelf dimissVC];
    };
    
    _zfPlayerView = view;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscapeLeft;
}

-(BOOL)shouldAutorotate
{
    return NO;
}
- (void)dimissVC
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
