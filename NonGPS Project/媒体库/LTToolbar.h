//
//  LTToolbar.h
//  LetingUdisk
//
//  Created by mac on 16/7/11.
//  Copyright © 2016年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
//Block
typedef void(^ButtonActionBlock)(UIButton *sender);
#define LTToolbarHeight 49 //54 

@interface LTToolbar : UIToolbar
@property(nonatomic,readonly,strong)UIButton *barButton;//指当有一个button时
-(void)buttonActionBlock:(ButtonActionBlock)didClickButton;

- (id)initWithFrame:(CGRect)frame buttonItems:(NSArray <UIButton *>*)buttons;
- (id)initWithFrame:(CGRect)frame title:(NSString *)title;

- (id)initWithFrame:(CGRect)frame
        selectImags:(NSArray <UIImage *>*)selectImags
      unSelectImags:(NSArray <UIImage *>*)unSelectImags
             titles:(NSArray <NSString *>*)titles
   selectTitleColor:(UIColor *)selectTitleColor
  nselectTitleColor:(UIColor *)unselectTitleColor;


- (id)initWithFrame:(CGRect)frame
        selectImags:(NSArray <UIImage *>*)selectImags
      unSelectImags:(NSArray <UIImage *>*)unSelectImags;

- (void)setToolbarEnable:(BOOL)enable;
@end
