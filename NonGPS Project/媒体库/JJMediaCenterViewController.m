//
//  JJMediaCenterViewController.m
//  JJRC
//
//  Created by mac on 17/4/7.
//  Copyright © 2017年 mac. All rights reserved.
//


#import "JJMediaCenterViewController.h"
#import "JJPhotoViewController.h"
#import "JJVideoViewController.h"
#import "UIImage+Extension.h"

#define kSearchBarWidth (60.0f)

@interface JJMediaCenterViewController ()
{
    UIButton *_editButton;
}
@property (nonatomic, strong)  NSArray *menuList;
@property(nonatomic,strong)UIBarButtonItem *defaultLeftBarItem;
@property(nonatomic,strong)UIBarButtonItem *cancelBarItem;
@end

@implementation JJMediaCenterViewController



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dismissVC
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor =  [UIColor whiteColor];
    
    self.magicView.navigationInset = UIEdgeInsetsMake(0, kSearchBarWidth, 0, 0);
    
    NSMutableArray *menuList = [[NSMutableArray alloc] initWithCapacity:100];
    
    [menuList addObject:NSLocalizedString(@"Photos",nil)];
    [menuList addObject:NSLocalizedString(@"Videos",nil)];


    _menuList = menuList;
    
    self.magicView.navigationInset = UIEdgeInsetsMake(0, kSearchBarWidth, 0, 0);
    self.magicView.navigationColor = RGBCOLOR(18, 28, 38);//RGBCOLOR(126, 126, 126);
    self.magicView.sliderColor = RGBCOLOR(108, 210, 189);
    self.magicView.switchStyle = VTSwitchStyleDefault;
    self.magicView.layoutStyle = VTLayoutStyleCenter;
    self.magicView.separatorHeight = 0;
    self.magicView.navigationHeight = 54.f;
    self.magicView.againstStatusBar = NO;
    self.magicView.dataSource = self;
    self.magicView.delegate = self;
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton addTarget:self action:@selector(popViewController) forControlEvents:UIControlEventTouchUpInside];
    [backButton setImage:[UIImage imageNamed:@"FPV-32"] forState:UIControlStateNormal];
    backButton.backgroundColor = [UIColor clearColor];
    backButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    backButton.frame = CGRectMake(0, 0, kSearchBarWidth, 54);
    [self.magicView setLeftNavigatoinItem:backButton];
    
    
    UIButton *editButton = [UIButton buttonWithType:UIButtonTypeCustom];
  
    [editButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [editButton addTarget:self action:@selector(editAction:) forControlEvents:UIControlEventTouchUpInside];
    editButton.backgroundColor = [UIColor clearColor];
    editButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    editButton.frame = CGRectMake(0, 0, kSearchBarWidth, 54);
    
    [editButton setTitle:NSLocalizedString(@"Edit",nil) forState:UIControlStateNormal];
    [editButton setTitle:NSLocalizedString(@"Cancel",nil) forState:UIControlStateSelected];
    
    [self.magicView setRightNavigatoinItem:editButton];
    
    [self.magicView reloadData];
    
    [self.view addSubview:self.toolbar1];
    
    __weak typeof(self)weakSelf = self;
    
    [self.toolbar1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.view.mas_left);
        make.right.equalTo(weakSelf.view.mas_right);
        make.top.equalTo(weakSelf.view.mas_bottom);
        make.height.equalTo(@(LTToolbarHeight));
    }];

}



- (void)viewDidAppear:(BOOL)animated
{
//    (UIScrollView*)self.magicView.tintAdjustmentMode;
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



- (LTToolbar *)toolbar1
{
    if (!_toolbar1) {
        NSArray *unSelectimgs = @[[UIImage imageNamed:@"SJ_FILE_SELECT"],[UIImage imageNamed:@"SJ_FILE_SHARE"],[UIImage imageNamed:@"SJ_FILE_SAVE"],[UIImage imageNamed:@"SJ_FILE_DELETE"]];
        
        NSArray *selectimgs = @[[UIImage imageNamed:@"SJ_FILE_SELECT1"],[UIImage imageNamed:@"SJ_FILE_SHARE"],[UIImage imageNamed:@"SJ_FILE_SAVE"],[UIImage imageNamed:@"SJ_FILE_DELETE"]];
        
        LTToolbar *customToolBar = [[LTToolbar alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-64-LTToolbarHeight-40, SCREEN_WIDTH,LTToolbarHeight) selectImags:selectimgs unSelectImags:unSelectimgs];

        [customToolBar setBackgroundImage:[UIImage imageWithColor:[UIColor blackColor]] forToolbarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];

        __weak typeof(self)weakSelf = self;
        [customToolBar buttonActionBlock:^(UIButton *sender) {
            [weakSelf handleToolBarButton1:sender];
        }];
        
        [customToolBar setToolbarEnable:YES];
        _toolbar1 = customToolBar;
    }
    return _toolbar1;
}


- (void)statusBarOrientationChange:(NSNotification *)notification
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscapeLeft;
}

-(BOOL)shouldAutorotate
{
    return NO;
}

#pragma mark - actions
- (void)editAction:(UIButton *)sender
{ 
    self.tabBarController.tabBar.hidden = sender.selected = !sender.selected;
    if (self.delegate && [self.delegate respondsToSelector:@selector(edit:)])
    {
        [self.delegate edit:sender.selected];
    }
    
    self.magicView.switchEnabled = !sender.selected;
    self.magicView.scrollEnabled = !sender.selected;
    
    if (sender.selected)
    {
        [self.toolbar1 mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view.mas_left);
            make.right.equalTo(self.view.mas_right);
            make.bottom.equalTo(self.view.mas_bottom);
            make.height.equalTo(@(LTToolbarHeight));
        }];
        
        
        [UIView animateWithDuration:0.3 animations:^{
            CGRect frame = CGRectMake(self.tabBarController.tabBar.frame.origin.x, MAX_LEN+self.tabBarController.tabBar.frame.size.height, self.tabBarController.tabBar.frame.size.width, self.tabBarController.tabBar.frame.size.height);
            self.tabBarController.tabBar.frame = frame;
            
            [self.toolbar1 layoutIfNeeded];
        }];
    }else
    {
        [self.toolbar1 mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view.mas_left);
            make.right.equalTo(self.view.mas_right);
            make.top.equalTo(self.view.mas_bottom);
            make.height.equalTo(@(LTToolbarHeight));
        }];
        
        [UIView animateWithDuration:0.3 animations:^{
            [self.toolbar1 layoutIfNeeded];
            CGRect frame = CGRectMake(self.tabBarController.tabBar.frame.origin.x, MAX_LEN- self.tabBarController.tabBar.frame.size.height, self.tabBarController.tabBar.frame.size.width, self.tabBarController.tabBar.frame.size.height);
            self.tabBarController.tabBar.frame = frame;
        }];
    }
}

- (void)popViewController
{
    if (self.navigationController) {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - VTMagicViewDataSource

- (NSArray<NSString *> *)menuTitlesForMagicView:(VTMagicView *)magicView
{
    return _menuList;
}

- (UIButton *)magicView:(VTMagicView *)magicView menuItemAtIndex:(NSUInteger)itemIndex
{
    static NSString *itemIdentifier = @"itemIdentifier";
    UIButton *menuItem = [magicView dequeueReusableItemWithIdentifier:itemIdentifier];
    if (!menuItem) {
        menuItem = [UIButton buttonWithType:UIButtonTypeCustom];
        [menuItem setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [menuItem setTitleColor:RGBCOLOR(108, 210, 189) forState:UIControlStateSelected];
    }
    return menuItem;
}


- (UIViewController *)magicView:(VTMagicView *)magicView viewControllerAtPage:(NSUInteger)pageIndex
{
    UIViewController *recomViewController = nil;
    if (0 == pageIndex) {
        static NSString *photosid = @"photos.identifier";
        recomViewController = [magicView dequeueReusablePageWithIdentifier:photosid];
        
        if (!recomViewController) {
            recomViewController = [[JJPhotoViewController alloc] init];
        }

    }else if (1 == pageIndex)
    {
        static NSString *videosid = @"videos.identifier";
        recomViewController = [magicView dequeueReusablePageWithIdentifier:videosid];
        
        if (!recomViewController) {
            recomViewController = [[JJVideoViewController alloc] init];
        }
    }
#if TARGET_BUILD == 24
  else if (2 == pageIndex) {
        static NSString *photosid = @"sdcard.identifier";
        recomViewController = [magicView dequeueReusablePageWithIdentifier:photosid];
        if (!recomViewController) {
            recomViewController = [[SDcardViewController alloc] init];
        }
    }
#endif
//    if ([recomViewController respondsToSelector:@selector(setVt_magicController:)]) {
//        [recomViewController performSelectorOnMainThread:@selector(setVt_magicController:) withObject:self waitUntilDone:NO];
//    }
    return recomViewController;
}


- (void)magicView:(VTMagicView *)magicView viewDidDisappear:(UIViewController *)viewController atPage:(NSUInteger)pageIndex
{
    viewController.navigationController.delegate = nil;
}

- (void)magicView:(VTMagicView *)magicView viewDidAppear:(UIViewController *)viewController atPage:(NSUInteger)pageIndex
{
    self.delegate = viewController;
}

- (void)magicView:(VTMagicView *)magicView didSelectItemAtIndex:(NSUInteger)itemIndex
{
}



#pragma mark - 重写父类方法
-(void)handleToolBarButton1:(UIButton *)sender
{
    switch (sender.tag)
    {
        case 400: //全选
        {
            sender.selected = !sender.selected;
            if (self.delegate && [self.delegate respondsToSelector:@selector(selectAll:)])
            {
                [self.delegate selectAll:sender.selected];
            }
        }
            break;
            
        case 401://分享
        {
            sender.selected = !sender.selected;
            if (self.delegate && [self.delegate respondsToSelector:@selector(shareFiles)])
            {
                [self.delegate shareFiles];
            }
        }
            break;
            
        case 402: //保存
        {
            sender.selected = !sender.selected;
            if (self.delegate && [self.delegate respondsToSelector:@selector(saveFiles)])
            {
                [self.delegate saveFiles];
            }
        }
            break;
            
        case 403: //删除
        {
            sender.selected = !sender.selected;
            if (self.delegate && [self.delegate respondsToSelector:@selector(deleteFiles)])
            {
                [self.delegate deleteFiles];
            }
        }
            break;
            
        default:
            break;
    }
}

- (void)cancelSelect
{
    
}

- (void)selectAll:(UIButton *)sender
{
    
}



#pragma mark - UIAlertDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
 
}


- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return  UIInterfaceOrientationPortrait;
}
@end
