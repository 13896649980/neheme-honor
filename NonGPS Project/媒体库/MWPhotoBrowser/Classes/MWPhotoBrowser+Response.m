//
//  MWPhotoBrowser+Response.m
//  LetingUdisk_V3.0
//
//  Created by mac on 16/9/29.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "MWPhotoBrowser+Response.h"
#import "MWPhotoBrowserPrivate.h"
#import <objc/runtime.h>
#import "LTShareManager.h"
#import "JJMediaManager.h"


@implementation MWPhotoBrowser (Response)
@dynamic refetchBlock;


- (reFetchPhoneAssets)refetchBlock
{
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setRefetchBlock:(reFetchPhoneAssets)refetchBlock
{
    objc_setAssociatedObject(self, @selector(refetchBlock), refetchBlock, OBJC_ASSOCIATION_COPY);
}

- (reFetchPhoneAssets)refetchMoreBlock
{
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setRefetchMoreBlock:(reFetchPhoneAssets)refetchMoreBlock
{
    objc_setAssociatedObject(self, @selector(refetchMoreBlock), refetchMoreBlock, OBJC_ASSOCIATION_COPY);
}

- (refetchDelete)refetchDeleteBlock
{
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setRefetchDeleteBlock:(refetchDelete)refetchDeleteBlock
{
    objc_setAssociatedObject(self, @selector(refetchDeleteBlock), refetchDeleteBlock, OBJC_ASSOCIATION_COPY);
}


- (downloadCallbackBlock)downloadCallbackBlock
{
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setDownloadCallbackBlock:(downloadCallbackBlock)downloadCallbackBlock
{
    objc_setAssociatedObject(self, @selector(downloadCallbackBlock), downloadCallbackBlock, OBJC_ASSOCIATION_COPY);
}

- (uploadCallbackBlock)uploadCallbackBlock
{
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setUploadCallbackBlock:(uploadCallbackBlock)uploadCallbackBlock
{
    objc_setAssociatedObject(self, @selector(uploadCallbackBlock), uploadCallbackBlock, OBJC_ASSOCIATION_COPY);
}

#pragma mark - 处理自定义toolBar的点击事件
static const char associatedkey;
-(void)handleToolBarButton:(UIButton *)sender
{
    NSInteger tag = sender.tag;
    MWPhoto *photo = (MWPhoto *)[self photoAtIndex:_currentPageIndex];
    
    switch (tag)
    {
            
        case 400: //分享
        {
            [self shareFiles:@[photo.file] callback:nil];
        }
            break;
           
        case 401: //保存
        {
            [self downLoadFilestoAlbum:@[photo.file] callback:nil];
        }
            break;
            
        case 402: //删除
        {
            //删除手机沙盒照片
            [self deleteFilesWithcallback:nil];
          
        }
            break;
            
        default:
            break;
    }
}


#pragma mark 删除文件
- (void)deleteFilesWithcallback:(void (^)())callback
{
    UIAlertView *alert ;
    
    alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Note",nil) message:NSLocalizedString(@"Are you sure you want to delete?",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",nil) otherButtonTitles:NSLocalizedString(@"OK",nil), nil];
    
    alert.tag = 10000;
    [alert show];
}

#pragma mark  下载文件到手机系统相册
- (void)downLoadFilestoAlbum:(NSArray <JJFile *>*)files callback:(void (^)())callback
{
    __weak typeof(self)weakSelf = self;


        [SVProgressHUD showWithStatus:NSLocalizedString(@"Saving...", nil)];

    [JJMediaManager saveFileToAlbum:files callback:^(int ret) {
        if (ret == 0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{

                    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Save complete", nil)];
                
            });
        }else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Save complete", nil)];
            });
        }
    }];

}

#pragma mark  分享文件
- (void)shareFiles:(NSArray <JJFile *>*)files callback:(void (^)())callback
{
    [LTShareManager showShareMenuWithSandboxFiles:files barButtonItem:self.shareBarItem showViewController:self];
}




- (void)deleteSanboxFiles
{
    //删除手机沙盒照片
    MWPhoto *photo = (MWPhoto *)[self photoAtIndex:_currentPageIndex];
    __weak typeof(self)weakSelf = self;


    [SVProgressHUD showWithStatus:NSLocalizedString(@"Deleting...",nil)];

    [JJMediaManager deleteFile:@[photo.file] callback:^(int ret) {
        if (ret == 0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                
            });
            
            if (weakSelf.refetchDeleteBlock)
            {
                weakSelf.refetchDeleteBlock(^(){
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [weakSelf reloadData];
                    });

                },nil,NO);
            }
        }
    }];

}


#pragma mark - UIAlertDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        if (alertView.tag == 10000)//删除手机照片
        {
            [self deleteSanboxFiles];
        }
    }
}


@end
