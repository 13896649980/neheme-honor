//
//  MWPhotoBrowser+Response.h
//  LetingUdisk_V3.0
//
//  Created by mac on 16/9/29.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "MWPhotoBrowser.h"


typedef void(^downloadCallbackBlock)(int fileCount,float progress);
typedef void(^uploadCallbackBlock)(int fileCount,float progress);

typedef void(^callback)(void);
typedef void(^reFetchPhoneAssets)(callback);

typedef void(^refetchDelete)(callback,NSArray <JJFile*> *files,BOOL isEncrypt);//加密 解密 删除回调

int myMWDownloadCallback(void* data,long long allsize,long long currentSize);
int myMWUploadCallback(void* data,long long allsize,long long currentSize);

@interface MWPhotoBrowser (Response)<UIAlertViewDelegate>


@property(nonatomic,copy)downloadCallbackBlock downloadCallbackBlock;
@property(nonatomic,copy)uploadCallbackBlock uploadCallbackBlock;
@property (nonatomic, copy) reFetchPhoneAssets refetchBlock;
@property (nonatomic, copy) reFetchPhoneAssets refetchMoreBlock;
@property (nonatomic, copy) refetchDelete refetchDeleteBlock;


#pragma mark - 处理自定义toolBar的点击事件
-(void)handleToolBarButton:(UIButton *)sender;


#pragma mark 删除文件
- (void)deletePhoneFiles:(NSArray <JJFile *>*)files callback:(void (^)())callback;

#pragma mark  下载文件到手机系统相册
- (void)downLoadFilestoAlbum:(NSArray <JJFile *>*)files callback:(void (^)())callback;

#pragma mark  分享文件
- (void)shareFiles:(NSArray <JJFile *>*)files callback:(void (^)())callback;



@end
