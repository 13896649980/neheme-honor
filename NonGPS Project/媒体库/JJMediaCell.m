//
//  JJMediaCell.m
//  JJRC
//
//  Created by mac on 17/4/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "JJMediaCell.h"
#import "UIImageView+WebCache.h"

@implementation JJMediaCell
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    imageView.image = nil;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubViews];
        
        // Listen for photo loading notifications
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleMWPhotoLoadingDidEndNotification:)
                                                     name:MWPHOTO_LOADING_DID_END_NOTIFICATION
                                                   object:nil];
        
    }
    return self;
}

- (void)initSubViews
{
    imageView = [UIImageView new];
    [self.contentView addSubview:imageView];
    imageView.backgroundColor = [UIColor clearColor];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    imageView.autoresizesSubviews = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;

    maskView = [UIView new];
    maskView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.6f];
    maskView.hidden = YES;
    [imageView addSubview:maskView];
    
    selectImgView = [UIImageView new];
    selectImgView.backgroundColor = [UIColor clearColor];
    [imageView addSubview:selectImgView];
    
    videoIcon = [UIImageView new];
    [imageView addSubview:videoIcon];
    videoIcon.image = [UIImage imageNamed:@"ico_media_movie_nor"];
    videoIcon.hidden = YES;
    
    __weak typeof(self)weakSelf = self;
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.mas_top);
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.bottom.equalTo(weakSelf.mas_bottom);
    }];
    

    [maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.mas_top);
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.bottom.equalTo(weakSelf.mas_bottom);
    }];
    
    [selectImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(imageView.mas_bottom).offset(-6);
        make.right.equalTo(weakSelf.mas_right).offset(-6);
    }];
    
    
    [videoIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.mas_centerX);
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
    
    NSString *resourePath = [[NSBundle mainBundle] resourcePath];
    
    placeholderImage = [UIImage imageWithContentsOfFile:[resourePath stringByAppendingPathComponent:@"ico_download_pic_small"]];
    imageView.image = placeholderImage;
    
    selectImgView.image = [UIImage imageWithContentsOfFile:[resourePath stringByAppendingPathComponent:@"ico_media_select_down"]];
    
    selectImgView.hidden = YES;
    [imageView bringSubviewToFront:selectImgView];
}


- (void)setIsVideo:(BOOL)isVideo
{
    _isVideo = isVideo;
    videoIcon.hidden = isVideo?NO:YES;
}



#pragma mark - Image Handling

- (void)setPhoto:(MWPhoto*)photo {
    _photo = photo;
    selectImgView.hidden = !photo.isSelect;
    maskView.hidden = ![photo isSelect];
    [imageView sd_setImageWithURL:nil placeholderImage:placeholderImage];
}

- (void)displayImage {
    [imageView sd_setImageWithURL:nil placeholderImage:_photo.underlyingImage];
}

#pragma mark - Notifications

- (void)handleMWPhotoLoadingDidEndNotification:(NSNotification *)notification {
    id <MWPhoto> photo = [notification object];
    if (photo == _photo) {
        if ([photo underlyingImage])
        {
            // Successful load
            [self displayImage];
        }
    }
}

@end
