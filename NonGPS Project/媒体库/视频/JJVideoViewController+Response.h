//
//  JJVideoViewController+Response.h
//  JJRC
//
//  Created by mac on 17/4/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "JJVideoViewController.h"

@interface JJVideoViewController (Response)
- (void)fetchVideos;
- (void)fetchVideosWithCallback:(void (^)())callback;

- (NSString *)setionOfKey:(NSInteger)setion;
- (NSArray *)assetThumbsAtSection:(NSInteger )section;
@end
