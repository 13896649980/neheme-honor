//
//  LTShareManager.h
//  LetingUdisk
//
//  Created by mac on 16/8/17.
//  Copyright © 2016年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <UIKit/UIDocumentInteractionController.h>

#import "JJFile.h"

@interface LTShareItem : NSObject
-(instancetype)initWithImage:(UIImage *)img andfile:(NSURL *)fileURl;
@end

@interface LTActivity : UIActivity
{
    UIDocumentInteractionController *_documentController;
}
//title是当前类型，shareImage是分享图标，URL是要分享的地址，contents保存用户要分享的内容//
@property (nonatomic,strong) UIImage *shareImage;
@property (nonatomic,weak)UIViewController *showViewController;
@property (nonatomic,copy)NSString *title;
@property (nonatomic,strong)NSArray *contents;

- (instancetype)initWithImage:(UIImage *)shareImage title:(NSString *)title  showViewController:(UIViewController *)viewController contentArray:(NSArray *)contentArray;
@end


@interface LTActivityItemProvider : UIActivityItemProvider
{
    NSURL *_url;
    UIImage *_image;
}
- (id)initWithUrl:(NSURL*)url image:(UIImage*)image;
@end


@interface LTShareManager : NSObject



/**
 *  分享手机沙盒里面的文件
 *
 *  @param files              手机沙盒里面的文件
 *  @param barButtonItem      分享的view的显示在barButtonItem位置
 *  @param showViewController 显示分享控件的controller
 */
+ (void)showShareMenuWithSandboxFiles:(NSArray <JJFile *> *)files barButtonItem:(UIBarButtonItem *)barButtonItem  showViewController:(UIViewController *)showViewController;


@end
