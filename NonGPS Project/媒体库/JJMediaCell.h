//
//  JJMediaCell.h
//  JJRC
//
//  Created by mac on 17/4/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JJFile.h"
#import "MWPhoto.h"

@interface JJMediaCell : UICollectionViewCell
{
    UIImageView *imageView;
    UIImage *placeholderImage;
    UIImageView *selectImgView;
    UIImageView *videoIcon;
    UIView *maskView;
}
@property (nonatomic) NSUInteger index;
@property (nonatomic,strong) MWPhoto *photo;
@property (nonatomic) BOOL isSelected;
@property (nonatomic) BOOL isVideo;

- (void)displayImage;
@end
