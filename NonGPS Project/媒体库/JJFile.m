//
//  JJFile.m
//  JJRC
//
//  Created by mac on 17/4/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "JJFile.h"

@implementation JJFile
- (id)initWithFileAttribs:(NSDictionary *)attribs
{
    self = [super init];
    if (self) {
//        NSDateFormatter *df = [[NSDateFormatter alloc] init];
//        NSTimeZone *timeZone = [NSTimeZone localTimeZone];
//        [df setTimeZone:timeZone];
//
//        [df setDateFormat:@"yyyy-MM-dd"];//HH:mm:ss
//        self.creationDate = [df stringFromDate:attribs[NSFileCreationDate]];
//        self.modifiCationDate = [df stringFromDate:attribs[NSFileModificationDate]];
//
//        self.size = [attribs[NSFileSize] longLongValue];
//        self.path = attribs[@"NSFilePath"];
//        self.name = self.path.lastPathComponent;
//
//        [df setDateFormat:@"yyyyMMddHHmmss"];
//        NSString *dateString = self.name.stringByDeletingPathExtension;
//        NSDate *date = [df dateFromString:dateString];
        
 //       [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"]; // HH:mm:ss
  //      NSString *modifData = [df stringFromDate:date];
//        self.modifiCationDate = modifData;
        
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"yyyy-MM-dd"];//HH:mm:ss
        
        [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];//HH:mm:ss
        
        self.creationDate = [df stringFromDate:attribs[NSFileCreationDate]];
        
        
        self.modifiCationDate = [df stringFromDate:attribs[NSFileModificationDate]];
        self.size = [attribs[NSFileSize] longLongValue];
        self.path = attribs[@"NSFilePath"];
        self.name = self.path.lastPathComponent;

        
        self.isSelect = NO;
        self.isPlaying = NO;
        [self handleFiletype:attribs];
        
        NSArray *searchPaths =NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *docPath = [searchPaths objectAtIndex: 0];
        if (self.type == JJ_Video)
        {
            self.videoPath = [NSString stringWithFormat:@"%@/%@/%@",docPath,DIR_VIDEO,self.name];
            self.sourcePath = [NSString stringWithFormat:@"%@/%@/%@",docPath,DIR_VIDEO_PHOTO,self.name];
        }else if (self.type == JJ_Picture)
        {
            self.sourcePath = [NSString stringWithFormat:@"%@/%@/%@",docPath,DIR_PHOTO,self.name];
        }
    }
    return self;
}



- (void)handleFiletype:(NSDictionary *)attribs
{
    if ([attribs[NSFileType] isEqualToString:NSFileTypeRegular])
    {
        //这里修改类型
        NSString *fileName = self.name.lowercaseString;
        if ([fileName hasSuffix:@"mp3"]||[fileName hasSuffix:@"ape"]||[fileName hasSuffix:@"wma"]||[fileName hasSuffix:@"wav"]||[fileName hasSuffix:@"ogg"]||[fileName hasSuffix:@"aac"]||[fileName hasSuffix:@"m4a"]||[fileName hasSuffix:@"flac"])
        {
            self.type = JJ_Audio;
            
        }else if ([fileName hasSuffix:@"jpg"]  || [fileName hasSuffix:@"gif" ] || [fileName hasSuffix:@"png" ] ||[fileName hasSuffix:@"bmp" ]||[fileName hasSuffix:@"jpeg" ]
                  ||[fileName hasSuffix:@"tif"])
        {
            self.type = JJ_Picture;
            
        }else if ([fileName hasSuffix:@"mov"]  || [fileName hasSuffix:@"mp4"]
                  || [fileName hasSuffix:@"mkv"] || [fileName hasSuffix:@"avi"] || [fileName hasSuffix:@"3gp"] || [fileName hasSuffix:@"rmvb"] || [fileName hasSuffix:@"wmv"]
                  || [fileName hasSuffix:@"f4v"] || [fileName hasSuffix:@"flv"] || [fileName hasSuffix:@"vob"] || [fileName hasSuffix:@"mpg"]|| [fileName hasSuffix:@"m4v"]|| [fileName hasSuffix:@"m2ts"]|| [fileName hasSuffix:@"mts"])
        {
            self.type = JJ_Video;
            
        }else if ([fileName hasSuffix:@"doc"] || [fileName hasSuffix:@"txt"] || [fileName hasSuffix:@"pdf"] || [fileName hasSuffix:@"xml"]  || [fileName hasSuffix:@"html"]|| [fileName hasSuffix:@"pptx"]||[fileName hasSuffix:@"docx"] ||
                  [fileName hasSuffix:@"docm"] ||[fileName hasSuffix:@"dotx"] ||[fileName hasSuffix:@"dotm"] ||[fileName hasSuffix:@"xls"] ||[fileName hasSuffix:@"xlsx"] ||[fileName hasSuffix:@"xlsm"] ||[fileName hasSuffix:@"xltx"] ||
                  [fileName hasSuffix:@"xltm"] ||[fileName hasSuffix:@"pptm"] ||[fileName hasSuffix:@"ppsx"] ||[fileName hasSuffix:@"ppsm"] ||[fileName hasSuffix:@"potx"] ||
                  [fileName hasSuffix:@"potm"] ||[fileName hasSuffix:@"log"]||[fileName hasSuffix:@"ppt"]||[fileName hasSuffix:@"htm"] ||[fileName hasSuffix:@"m"]||[fileName hasSuffix:@"h"])
        {
            self.type = JJ_Doc;
        }else if ([fileName hasSuffix:@"zip"]|| [fileName hasSuffix:@"rar"]|| [fileName hasSuffix:@"7z"])
        {
            self.type = JJ_Zip;
        }else
        {
            self.type = JJ_Other;
        }
        
    }else if ([attribs[NSFileType] isEqualToString:NSFileTypeDirectory])
    {
        self.type = JJ_Folder;
    }
    
}
@end


