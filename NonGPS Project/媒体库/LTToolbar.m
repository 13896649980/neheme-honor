//
//  LTToolbar.m
//  LetingUdisk
//
//  Created by mac on 16/7/11.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "LTToolbar.h"
#import "UIImage+Extension.h"

@interface LT_Button:UIButton
-(void)layoutSubviews;
@end

@implementation LT_Button

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    // Center image
    CGPoint center = self.imageView.center;
    center.x = self.frame.size.width/2 ;
    center.y = self.imageView.frame.size.height/2;
    self.imageView.center = center;
    
    //Center text
    CGSize size = [self.titleLabel.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:self.titleLabel.font,NSFontAttributeName, nil]];
    CGRect newFrame = [self titleLabel].frame;
    newFrame.origin.x = 0;
    newFrame.origin.y = 25;
    newFrame.size.width = size.width; //
    newFrame.size.height = size.height;//10
    
    self.titleLabel.frame = newFrame;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.center = CGPointMake(self.imageView.center.x, self.titleLabel.center.y);
    if (size.height == 0) {
        self.imageView.center = center;
    }else
    {
        self.imageView.center = CGPointMake(self.imageView.center.x, self.imageView.center.y-size.height+15);
    }

}


@end


@interface LTToolbar ()

@property (nonatomic, copy) ButtonActionBlock buttonActionBlock;
@property (nonatomic, strong) NSMutableArray *itemArray;
@property (nonatomic, assign) BOOL enable;
@property(nonatomic,strong)UIButton *barButton;//指当有一个button时
@end

@implementation LTToolbar
- (id)initWithFrame:(CGRect)frame buttonItems:(NSArray <UIButton *>*)buttons
{  
    self = [super initWithFrame:frame];
    if (self) {
        [self setTranslucent:NO];
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self setBarStyle:UIBarStyleDefault];
        [self createToolBarButton:buttons];
//        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin |UIViewAutoresizingFlexibleTopMargin;
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
        selectImags:(NSArray <UIImage *>*)selectImags
      unSelectImags:(NSArray <UIImage *>*)unSelectImags
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setTranslucent:NO];
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self setBarStyle:UIBarStyleDefault];
        [self createToolBarWtihSelectImags:selectImags unSelectImags:unSelectImags];
        self.userInteractionEnabled = _enable = YES;
    }
    return self;
    
}

- (id)initWithFrame:(CGRect)frame
        selectImags:(NSArray <UIImage *>*)selectImags
      unSelectImags:(NSArray <UIImage *>*)unSelectImags
             titles:(NSArray <NSString *>*)titles
   selectTitleColor:(UIColor *)selectTitleColor
  nselectTitleColor:(UIColor *)unselectTitleColor
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setTranslucent:NO];
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self setBarStyle:UIBarStyleDefault];
        [self createToolBarWtihSelectImags:selectImags unSelectImags:unSelectImags titles:titles selectTitleColor:selectTitleColor unselectTitleColor:unselectTitleColor];
        self.userInteractionEnabled = _enable = YES;
    }
    return self;

}
- (id)initWithFrame:(CGRect)frame title:(NSString *)title
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setTranslucent:NO];
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self setBarStyle:UIBarStyleDefault];
        [self createToolBarButtonWithTitle:title];
    }
    return self;
}

-(void)createToolBarWtihSelectImags:(NSArray <UIImage *>*)selectImags
                      unSelectImags:(NSArray <UIImage *>*)unSelectImags
{

    UIBarButtonItem *itemPlace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
 
    //创建button
    NSMutableArray *array = [NSMutableArray array];
    for (int i = 0; i < selectImags.count; i ++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:unSelectImags[i] forState:UIControlStateNormal];// setBackgroundImage
        [button setImage:selectImags[i] forState:UIControlStateSelected];

        button.frame = CGRectMake(0, 0, 35, 35);
        button.tag = 400 + i;
        [button addTarget:self action:@selector(handleToolBarButton:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:button];
        [self.itemArray addObject:item];
        [array addObject:item];
        if (selectImags.count > 1)
        {
            if ( i < selectImags.count-1) {
                [array addObject:itemPlace];
            }
        }
        
    }
    if (array.count ==1) {
        [array insertObject:itemPlace atIndex:0];
        [array addObject:itemPlace];
    }
    self.items = array;
    
}

-(void)createToolBarWtihSelectImags:(NSArray <UIImage *>*)selectImags
                      unSelectImags:(NSArray <UIImage *>*)unSelectImags
                             titles:(NSArray <NSString *>*)titles
                   selectTitleColor:(UIColor *)selectTitleColor
                 unselectTitleColor:(UIColor *)unselectTitleColor
{
    NSAssert(selectImags.count == unSelectImags.count, @"selectImags.count != unSelectImags.count");
//    NSAssert(titles&&titles.count, @"titles不能为空");
    //占位符
    UIBarButtonItem *itemPlace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    //创建button
    NSMutableArray *array = [NSMutableArray array];
    for (int i = 0; i < selectImags.count; i ++) {
        LT_Button *button = [LT_Button buttonWithType:UIButtonTypeCustom];
        [button setImage:unSelectImags[i] forState:UIControlStateNormal];// setBackgroundImage
        [button setImage:selectImags[i] forState:UIControlStateSelected];
        button.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        if (titles) {
            [button setTitle:titles[i] forState:UIControlStateNormal];
        }
        
        [button setTitleColor:selectTitleColor forState:UIControlStateSelected];
        [button setTitleColor:unselectTitleColor forState:UIControlStateNormal];
        
        [button.titleLabel setFont:[UIFont systemFontOfSize:10]];
        button.frame = CGRectMake(0, 0, 35, 35);
        button.tag = 400 + i;
        [button addTarget:self action:@selector(handleToolBarButton:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:button];
        [self.itemArray addObject:item];
        
        [array addObject:item];
        
        if (selectImags.count > 1)
        {
            if ( i < selectImags.count-1) {
                [array addObject:itemPlace];
            }
        }
        
    }
    if (array.count ==1) {
        [array insertObject:itemPlace atIndex:0];
        [array addObject:itemPlace];
    }
    self.items = array;
}

- (void)setToolbarEnable:(BOOL)enable
{
//    if (_enable == enable) return;
//    
//    _enable = enable;
//    self.userInteractionEnabled = _enable;
//    for (UIBarButtonItem *item in self.items)
//    {
//        UIButton *btn = item.customView;
//        btn.selected = _enable;
//    }
}

-(void)createToolBarButton:(NSArray <UIButton *> *)buttons
{
    //占位符
    UIBarButtonItem *itemPlace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    //创建button
    NSMutableArray *array = [NSMutableArray array];
    for (int i = 0; i < buttons.count; i ++) {
        UIButton *button = buttons[i];
        button.frame = CGRectMake(0, 0, 30, 30);
        button.tag = 400 + i;
        [button addTarget:self action:@selector(handleToolBarButton:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:button];
        [self.itemArray addObject:item];
        [array addObject:item];
        
        if (buttons.count > 1)
        {
            if ( i < buttons.count-1) {
                [array addObject:itemPlace];
            }
        }
        
    }
    self.items = array;
    
}

-(void)createToolBarButtonWithTitle:(NSString *)title
{
    //占位符
    UIBarButtonItem *itemPlace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    //创建button
    NSMutableArray *array = [NSMutableArray array];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, self.frame.size.width -60, 40);
    button.tag = 400 + 1;
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageWithColor:RGB_COLORWITHRGB(234 ,85 ,41)] forState:UIControlStateNormal];
//    [button setBackgroundImage:[UIImage imageWithColor:[UIColor lightGrayColor]] forState:UIControlStateSelected];
    button.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [button setTitle:title forState:UIControlStateNormal];
//    [button setTitle:NSLocalizedStringFromTable(@"取消",@"Localizable",nil) forState:UIControlStateSelected];
    
    
    button.layer.cornerRadius = 20;
    button.layer.masksToBounds = YES;
    
    self.barButton = button;
//    if ([title isEqualToString:NSLocalizedStringFromTable(@"取消",@"Localizable",nil)]) {
//        [button setBackgroundImage:[UIImage imageWithColor:[UIColor grayColor]] forState:UIControlStateNormal];
//    }
    [button addTarget:self action:@selector(handleToolBarButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:button];
    [self.itemArray addObject:item];
    
    [array addObject:itemPlace];
    [array addObject:item];
    [array addObject:itemPlace];
    
    
    self.items = array;
    
}



//处理自定义toolBar的点击事件
-(void)handleToolBarButton:(UIButton *)button
{
    self.buttonActionBlock(button);
}

-(void)buttonActionBlock:(ButtonActionBlock)didClickButton
{
    self.buttonActionBlock = didClickButton;
}




@end
