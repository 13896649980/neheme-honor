//
//  JJPhotoManager.m
//  JJRC
//
//  Created by huang mindong on 17/4/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "JJMediaManager.h"
#import <sys/mount.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/PHPhotoLibrary.h>
#import <Photos/PHAssetCollectionChangeRequest.h>
#import "UIImage+Extension.h"

#define ALBUM_NAME [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"]

@interface JJMediaManager()
{
    NSString *documentPath_;
    NSString *photosPath_;
    NSString *photothumbnailsPath_;
    NSString *videosPath_;
    NSString *videothumbnailsPath_;
    NSString *videoPhotosPath_;

    
    NSOperationQueue *queue_;
    dispatch_queue_t myQueue;
    
    ALAssetsLibrary *lib;
}


@end

@implementation JJMediaManager

- (ALAssetsLibrary *)assetsLibrary
{
    if (!lib)
    {
        lib = [[ALAssetsLibrary alloc] init];
    }
    return lib;
}

-(void)addAssetURL:(NSURL*)assetURL toAlbum:(NSString*)albumName withCompletionBlock:(void(^)(NSError *error))completionBlock
{
    //相册存在标示
    __block BOOL albumWasFound = NO;
    
    ALAssetsLibrary *assetsLibrary = [[ALAssetsLibrary alloc] init];
    //search all photo albums in the library
    [assetsLibrary enumerateGroupsWithTypes:ALAssetsGroupAlbum usingBlock:^(ALAssetsGroup *group,BOOL *stop)
     {
         
         //判断相册是否存在
         if ([albumName compare: [group valueForProperty:ALAssetsGroupPropertyName]]==NSOrderedSame) {
             
             //存在
             albumWasFound = YES;
             
             //get a hold of the photo's asset instance
             [assetsLibrary assetForURL: assetURL
                            resultBlock:^(ALAsset *asset) {
                                
                                //add photo to the target album
                                [group addAsset: asset];
                                
                                //run the completion block
                                completionBlock(nil);
                                
                            } failureBlock: completionBlock];
             return;
         }
         
         //如果不存在该相册创建
         if (group==nil && albumWasFound==NO)
         {
             
             if ([[Utility getAppCurrentSystemVersion] intValue] > 8) {
                 __weak ALAssetsLibrary* weakSelf = assetsLibrary;
                 [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
                     [PHAssetCollectionChangeRequest creationRequestForAssetCollectionWithTitle:albumName];
                 } completionHandler:^(BOOL success, NSError *error) {
                     if (success) {
                         [weakSelf enumerateGroupsWithTypes:ALAssetsGroupAlbum usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
                             if ([albumName compare: [group valueForProperty:ALAssetsGroupPropertyName]]==NSOrderedSame) {
                                 [weakSelf assetForURL: assetURL
                                           resultBlock:^(ALAsset *asset) {
                                               
                                               //add photo to the target album
                                               [group addAsset: asset];
                                               
                                               //run the completion block
                                               [self performSelectorOnMainThread:@selector(saveCompletionSuccess:) withObject:completionBlock waitUntilDone:YES];
                                               
                                           } failureBlock: completionBlock];
                             }
                         } failureBlock:completionBlock];
                         
                     }
                     
                 }];
             }
             else{
             
             __weak ALAssetsLibrary* weakSelf = assetsLibrary;
             
             //创建相册
             [assetsLibrary addAssetsGroupAlbumWithName:albumName resultBlock:^(ALAssetsGroup *group)
              {
                  
                  //get the photo's instance
                  [weakSelf assetForURL: assetURL
                            resultBlock:^(ALAsset *asset)
                   {
                       
                       //add photo to the newly created album
                       [group addAsset: asset];
                       
                       //call the completion block
                       completionBlock(nil);
                       
                   } failureBlock: completionBlock];
                  
              } failureBlock: completionBlock];
             return;
         }
        }
     }failureBlock:completionBlock];
   
}


- (id)init
{
    self = [super init];
    if (self) {
        myQueue = dispatch_queue_create("myqueue", NULL);
    }
    return self;
}

static JJMediaManager *_instance = nil;
+ (instancetype)shareInstance
{
    static dispatch_once_t onceToken ;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init] ;
    }) ;
    
    return _instance;
}



+ (void)savePhoto:(UIImage *)photo
         withName:(NSString *)name 
  addToPhotoAlbum:(BOOL)addToPhotoAlbum
         callback:(void(^)(void))callback
{
    if (photo){
        NSString *fileName = [NSString stringWithFormat:@"%@.jpg", name];
        NSArray *searchPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *localPath = [searchPaths objectAtIndex: 0];
        NSString *filePath = [localPath  stringByAppendingPathComponent:DIR_PHOTO];
        NSString *photoPath = [filePath stringByAppendingPathComponent:fileName];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if ([fileManager fileExistsAtPath:photoPath]) {
            [[JJMediaManager shareInstance] saveSourcePhoto:photo addToPath:photoPath];//原图本地
        }else{
            [[JJMediaManager shareInstance] savePhoto:photo asThumbnailNamed:fileName]; //d缩略图
            [[JJMediaManager shareInstance] saveSourcePhoto:photo addToPath:photoPath];//原图沙河
            [[JJMediaManager shareInstance] savePhoto:photo named:fileName addToPhotoAlbum:addToPhotoAlbum];//原图手机相册
        }
    }
    if(callback)
    {
        callback();
    }
 
}

+ (void)deletePhoto:(NSArray<NSString *>  *)names callback:(void(^)(void))callback
{
    for (NSString *name in names) {
        [[JJMediaManager shareInstance] deletePhotoNamed:name];
    }
    if(callback)
    {
        callback();
    }
}

+ (void)saveVideoThumb:(UIImage *)videoThumb
              withName:(NSString *)name
              callback:(void(^)(void))callback
{
    if (videoThumb)
    {
        [[JJMediaManager shareInstance] saveVideoThumb:videoThumb withName:name];
    }
  
    if(callback)
    {
        callback();
    }
}

+ (void)saveVideoPhoto:(UIImage *)videoPhoto
              withName:(NSString *)name
              callback:(void(^)(void))callback
{
    if (videoPhoto)
    {
         [[JJMediaManager shareInstance] saveVideoPhoto:videoPhoto withName:name];
    }
   
    if(callback)
    {
        callback();
    }
}

+ (void)deleteVideoThumb:(NSArray<NSString *>  *)names callback:(void(^)(void))callback
{
    for (NSString *name in names) {
        [[JJMediaManager shareInstance] deleteVideoNamed:name];
    }
    
    if(callback)
    {
        callback();
    }
}

+ (void)exportPhotoWithName:(NSArray<NSString *> *)names exportCallback:(void(^)(void))callback
{
    int author = [ALAssetsLibrary authorizationStatus];
    if(author == ALAuthorizationStatusRestricted || author == ALAuthorizationStatusDenied)
    {
        // The user has explicitly denied permission for media capture.
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alert ;
            
            alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Cannot use album", nil)
                                               message:NSLocalizedString(@"Please allow access to photos in the iPhone's settings-privacy-Photos", nil)
                                              delegate:self
                                     cancelButtonTitle:NSLocalizedString(@"OK",nil)
                                     otherButtonTitles:nil];
            
            [alert show];
            
            if (callback) {
                callback();
            }
        });
        return;
    }else
    {
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
        
        for (NSString *name in names)
        {
            @autoreleasepool
            {
                NSString *path = [[[JJMediaManager shareInstance] photosPath]stringByAppendingPathComponent:name];

                ALAssetsLibrary *lib = [JJMediaManager shareInstance].assetsLibrary;
                NSData *data = [NSData dataWithContentsOfFile:path];
                
                dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
                
                [lib writeImageDataToSavedPhotosAlbum:data metadata:nil completionBlock:^(NSURL *assetURL, NSError *error) {
                    
                    [[JJMediaManager shareInstance] addAssetURL:assetURL toAlbum:ALBUM_NAME withCompletionBlock:^(NSError *error) {
                        
                        [[JJMediaManager shareInstance] addAssetURL:assetURL toAlbum:ALBUM_NAME withCompletionBlock:^(NSError *error) {
                            
                        }];
                        dispatch_semaphore_signal(semaphore);
                    }];
                    
                }];
                
            }
        }

    }
    if(callback)
    {
        callback();
    }
}

+ (void)exportVideoWithName:(NSArray<NSString *> *)names callback:(void(^)(void ))callback
{
    int author = [ALAssetsLibrary authorizationStatus];
    if(author == ALAuthorizationStatusRestricted || author == ALAuthorizationStatusDenied)
    {
        // The user has explicitly denied permission for media capture.
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alert ;
            alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Cannot use album", nil)
                                               message:NSLocalizedString(@"Please allow access to photos in the iPhone's settings-privacy-Photos", nil)
                                              delegate:self
                                     cancelButtonTitle:NSLocalizedString(@"OK",nil)
                                     otherButtonTitles:nil];
            
            
            [alert show];
            
            if (callback) {
                callback();
            }
        });

    }else
    {
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
        
        for (NSString *name in names)
        {
            @autoreleasepool
            {
                NSString *path = [[[JJMediaManager shareInstance] videoPath]stringByAppendingPathComponent:name];
                
                ALAssetsLibrary *lib = [JJMediaManager shareInstance].assetsLibrary;
                
                dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
                
                [lib writeVideoAtPathToSavedPhotosAlbum:[NSURL fileURLWithPath:path] completionBlock:^(NSURL *assetURL, NSError *error) {
                    
                    [[JJMediaManager shareInstance] addAssetURL:assetURL toAlbum:ALBUM_NAME withCompletionBlock:^(NSError *error) {
                        
                    }];
                    dispatch_semaphore_signal(semaphore);
                }];
            }
        }

    }
    
    if(callback)
    {
        callback();
    }
}

+ (void)saveFileToAlbum:(NSArray <JJFile *>*)files callback:(void(^)(int ret))callback
{
    int author = [ALAssetsLibrary authorizationStatus];
    if(author == ALAuthorizationStatusRestricted || author == ALAuthorizationStatusDenied)
    {
        // The user has explicitly denied permission for media capture.
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alert ;
            
            alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Cannot use album", nil)
                                               message:NSLocalizedString(@"Please allow access to photos in the iPhone's settings-privacy-Photos", nil)
                                              delegate:self
                                     cancelButtonTitle:NSLocalizedString(@"OK",nil)
                                     otherButtonTitles:nil];
            
            [alert show];
            
            if (callback) {
                callback(-1);
            }
        });
        
    }else
    {
        dispatch_async([JJMediaManager shareInstance]->myQueue, ^{
            
            int ret = -1;
            long long totalSize = 0;
            for (int i = 0; i < files.count; i++)
            {
                JJFile *file = files[i];
                totalSize += file.size;
            }
            if (totalSize  > [self getAvailableDiskSize])
            {
                //文件大小 大于 手机可用空间，停止线程下载
                NSString *msg = @"";
                msg = NSLocalizedString(@"Insufficient storage available on your phone,please clean up the space", nil);
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Note", nil) message:msg delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil ];
                    [alert show];
                });
                
                if (callback) {
                    callback(ret);
                }
                return ;
            }
            
            
            for (int i = 0; i < files.count; i++)
            {
                JJFile *file = files[i];
                @autoreleasepool
                {
                    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
                    
                    if ( file.type == JJ_Picture)
                    {
                        
                        NSString *path = file.sourcePath;
                        NSData *data = [NSData dataWithContentsOfFile:path];
                        
                        ALAssetsLibrary *lib = [JJMediaManager shareInstance].assetsLibrary;
                        [lib writeImageDataToSavedPhotosAlbum:data metadata:nil completionBlock:^(NSURL *assetURL, NSError *error) {
                            [[JJMediaManager shareInstance] addAssetURL:assetURL toAlbum:ALBUM_NAME withCompletionBlock:^(NSError *error) {
                                
                            }];
                            dispatch_semaphore_signal(semaphore);
                        }];
                        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
                        
                    }else if (file.type == JJ_Video)
                    {
                        
                        NSString *path = file.videoPath;
                        ALAssetsLibrary *lib = [JJMediaManager shareInstance].assetsLibrary;
                        [lib writeVideoAtPathToSavedPhotosAlbum:[NSURL fileURLWithPath:path] completionBlock:^(NSURL *assetURL, NSError *error) {
                            
                            [[JJMediaManager shareInstance] addAssetURL:assetURL toAlbum:ALBUM_NAME withCompletionBlock:^(NSError *error) {
                                
                            }];
                            dispatch_semaphore_signal(semaphore);
                        }];
                        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
                        
                    }
                }
            }
            
            ret = 0;
            if (callback) {
                callback(ret);
            }
        });
    }
}


+ (void)deleteFile:(NSArray <JJFile *>*)files callback:(void(^)(int ret))callback
{
    dispatch_async([JJMediaManager shareInstance]->myQueue, ^{
        
        int ret = -1;

        for (int i = 0; i < files.count; i++)
        {
            JJFile *file = files[i];
            @autoreleasepool
            {
                if (file.type == JJ_Picture)
                {
                    [[JJMediaManager shareInstance] deletePhotoNamed:file.name];
                }else if (file.type == JJ_Video)
                {
                    [[JJMediaManager shareInstance] deleteVideoNamed:file.name];
                }
            }
        }
        
        ret = 0;
        if (callback) {
            callback(ret);
        }
    });
}

+(long long)getAvailableDiskSize
{
    struct statfs buf;
    unsigned long long freeSpace = -1;
    if (statfs("/var", &buf) >= 0)
    {
        freeSpace = (unsigned long long)(buf.f_bsize * buf.f_bavail);
    }
    
    long long freeSpace1 = freeSpace - 200 *1024 *1024;
    if (freeSpace1 < 0) {
        return 0;
    }
    return freeSpace - 200 *1024 *1024;
}

+ (void )fetchPhoneDocumentsFileAttributeWithDirName:(NSString *)dirName callback:(void (^)(NSArray <NSDictionary *>*fileAttbs))callback
{
    __weak typeof(self)weakSelf = self;
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *fileDirectory = [NSString stringWithFormat:@"%@/%@/",docDir,dirName];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSArray *filesAttb = [weakSelf fetchFilesAttrib:fileDirectory];
        if (callback) {
            callback(filesAttb);
        }
    });
}
#pragma mark -
#pragma mark File Names and Paths

// Creates the path if it does not exist.
- (void)ensurePathAt:(NSString *)path {
    NSFileManager *fm = [NSFileManager defaultManager];
    if ( [fm fileExistsAtPath:path] == false ) {
        [fm createDirectoryAtPath:path
      withIntermediateDirectories:YES
                       attributes:nil
                            error:NULL];
    }
}

- (NSString *)documentPath {
    if ( ! documentPath_ ) {
        NSArray *searchPaths =NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentPath_ = [searchPaths objectAtIndex: 0];
    }
    return documentPath_;
}

- (NSString *)photosPath {
    if ( ! photosPath_ ) {
        photosPath_ = [[self documentPath] stringByAppendingPathComponent:DIR_PHOTO];
        [self ensurePathAt:photosPath_];
    }
    return photosPath_;
}

- (NSString *)videoPath {
    if ( ! videosPath_ ) {
        videosPath_ = [[self documentPath] stringByAppendingPathComponent:DIR_VIDEO];
        [self ensurePathAt:videosPath_];
    }
    return videosPath_;
}


- (NSString *)photothumbnailsPath {
    if ( ! photothumbnailsPath_ )
    {
        photothumbnailsPath_ = [[self documentPath] stringByAppendingPathComponent:DIR_PHOTO_THUMB];
        [self ensurePathAt:photothumbnailsPath_];
    }
    return photothumbnailsPath_;
}

- (NSString *)videothumbnailsPath {
    if ( ! videothumbnailsPath_ ) {
        videothumbnailsPath_ = [[self documentPath] stringByAppendingPathComponent:DIR_VIDEO_THUMB];
        [self ensurePathAt:videothumbnailsPath_];
    }
    return videothumbnailsPath_;
}

- (NSString *)videoPhotosPath {
    if ( ! videoPhotosPath_ ) {
        videoPhotosPath_ = [[self documentPath] stringByAppendingPathComponent:DIR_VIDEO_PHOTO];
        [self ensurePathAt:videoPhotosPath_];
    }
    return videoPhotosPath_;
}



#pragma mark - 
- (void)savePhoto:(UIImage *)photo asThumbnailNamed:(NSString *)name {
    UIImage *thumbnail = [photo imageScaleAndCropToMaxSize:CGSizeMake(75,75)];
    NSString *thumbnailPath = [[self photothumbnailsPath] stringByAppendingPathComponent:name];
    [self savePhoto:thumbnail toPath:thumbnailPath];
}

- (void)savePhoto:(UIImage *)photo named:(NSString *)name addToPhotoAlbum:(BOOL)addToPhotoAlbum{
    // Save full size image. Note we will scale down the full size
    // image in order to improve display performance
    NSString *path = [[self photosPath] stringByAppendingPathComponent:name];
    if (addToPhotoAlbum) {
        // Save the photo to the Photo.app Photo Library.
//        UIImageWriteToSavedPhotosAlbum(photo, nil, nil, nil);

        int author = [ALAssetsLibrary authorizationStatus];
        if(author == ALAuthorizationStatusRestricted || author == ALAuthorizationStatusDenied)
        {
            // The user has explicitly denied permission for media capture.
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView *alert ;
                
                alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Cannot use album", nil)
                                                   message:NSLocalizedString(@"Please allow access to photos in the iPhone's settings-privacy-Photos", nil)
                                                  delegate:self
                                         cancelButtonTitle:NSLocalizedString(@"OK",nil)
                                         otherButtonTitles:nil];
                [alert show];
            });
            
        }else
        {
            dispatch_async([JJMediaManager shareInstance]->myQueue, ^{
                dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
                ALAssetsLibrary *lib = [JJMediaManager shareInstance].assetsLibrary;
                [lib writeImageToSavedPhotosAlbum:photo.CGImage metadata:nil completionBlock:^(NSURL *assetURL, NSError *error) {
                    [[JJMediaManager shareInstance] addAssetURL:assetURL toAlbum:ALBUM_NAME withCompletionBlock:^(NSError *error) {
                        
                    }];
                    dispatch_semaphore_signal(semaphore);
                }];
                
                dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
            });
        }
    }
}

-(void)saveSourcePhoto:(UIImage *)photo addToPath:(NSString *)path{
    [self savePhoto:photo toPath:path];
}

- (void)savePhoto:(UIImage *)photo toPath:(NSString *)path
{
    @autoreleasepool {
        NSData *jpg = UIImageJPEGRepresentation(photo, 1);  // 1.0 = least compression, best quality
        [jpg writeToFile:path atomically:NO];
    }
}

- (void)deletePhotoAtPath:(NSString *)path
{
    BOOL exist = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (exist) {
        NSError *error = NULL;
        [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
    }
}

- (void)deletePhotoNamed:(NSString *)name
{
    NSString *path = [[self photosPath] stringByAppendingPathComponent:name];
    [self deletePhotoAtPath:path];
    
    NSString *thumbnailPath = [[self photothumbnailsPath] stringByAppendingPathComponent:name];
    [self deletePhotoAtPath:thumbnailPath];
}


- (void)deleteVideoAtPath:(NSString *)path
{
    [[NSFileManager defaultManager] removeItemAtPath:path error:NULL];
}

- (void)deleteVideoNamed:(NSString *)name
{
    NSString *path = [[self videoPath] stringByAppendingPathComponent:name];
    [self deleteVideoAtPath:path];
    
    NSString *thumbnailPath = [[self videothumbnailsPath] stringByAppendingPathComponent:name];
    [self deleteVideoAtPath:thumbnailPath];
    
    NSString *videoPhoto = [[self videoPhotosPath] stringByAppendingPathComponent:name];
    [self deleteVideoAtPath:videoPhoto];
}


- (void)saveVideoThumb:(UIImage *)videoThumb withName:(NSString *)name
{
    UIImage *thumbnail = [videoThumb imageScaleAndCropToMaxSize:CGSizeMake(75,75)];
    NSString *thumbnailPath = [[self videothumbnailsPath] stringByAppendingPathComponent:name];
    [self savePhoto:thumbnail toPath:thumbnailPath];
    
}


- (void)saveVideoPhoto:(UIImage *)videoPhoto withName:(NSString *)name
{
    NSString *videoPath = [[self videoPhotosPath] stringByAppendingPathComponent:name];
    [self savePhoto:videoPhoto toPath:videoPath];
}

/**
 *  获取文件下所以文件属性
 *
 *  @param path 文件路径
 *
 *  @return 文件属性的数组
 */
+ (NSMutableArray <NSDictionary *>*)fetchFilesAttrib:(NSString *)path
{
    NSMutableArray *ma = [NSMutableArray array];
    NSFileManager *fm = [[NSFileManager alloc] init];
    NSString *folder = path;
    NSMutableArray *contents = (NSMutableArray *)[fm contentsOfDirectoryAtPath:folder error:nil];
    
    for (NSString *filename in contents) {
        
        if (filename.length > 0 && [filename characterAtIndex:0] != '.')
        {
            NSString *path = [folder stringByAppendingPathComponent:filename];
            NSDictionary *attr = [fm attributesOfItemAtPath:path error:nil];
            if (attr)
            {
                id fileType = [attr valueForKey:NSFileType];
                if ([fileType isEqual: NSFileTypeRegular] ||
                    [fileType isEqual: NSFileTypeSymbolicLink])
                {
#if 0
                    NSString *ext = path.pathExtension.lowercaseString;
                    if ([ext isEqualToString:@"mp3"] ||
                        [ext isEqualToString:@"caff"]||
                        [ext isEqualToString:@"aiff"]||
                        [ext isEqualToString:@"ogg"] ||
                        [ext isEqualToString:@"wma"] ||
                        [ext isEqualToString:@"m4a"] ||
                        [ext isEqualToString:@"m4v"] ||
                        [ext isEqualToString:@"wmv"] ||
                        [ext isEqualToString:@"3gp"] ||
                        [ext isEqualToString:@"mp4"] ||
                        [ext isEqualToString:@"mov"] ||
                        [ext isEqualToString:@"avi"] ||
                        [ext isEqualToString:@"mkv"] ||
                        [ext isEqualToString:@"mpeg"]||
                        [ext isEqualToString:@"mpg"] ||
                        [ext isEqualToString:@"flv"] ||
                        [ext isEqualToString:@"vob"] ||
                        [ext isEqualToString:@"png"] ||
                        [ext isEqualToString:@"jpg"] ||
                        [ext isEqualToString:@"rmvb"]||
                        [ext isEqualToString:@"txt"]||
                        [ext isEqualToString:@"log"]
                        )
#endif
                    {
                        NSDictionary *d = @{@"NSFilePath":path};
                        NSMutableDictionary *md = [NSMutableDictionary dictionary];
                        [md addEntriesFromDictionary:d];
                        [md addEntriesFromDictionary:attr];
                        [ma addObject:md];
                    }
                }else if ([fileType isEqualToString:NSFileTypeDirectory])
                {
                    NSDictionary *d = @{@"NSFilePath":path};
                    NSMutableDictionary *md = [NSMutableDictionary dictionary];
                    [md addEntriesFromDictionary:d];
                    [md addEntriesFromDictionary:attr];
                    [ma addObject:md];
                }
            }
        }
    }
    return ma;
}

@end
