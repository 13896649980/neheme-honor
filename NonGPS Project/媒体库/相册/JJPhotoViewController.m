//
//  JJPhotoViewController.m
//  JJRC
//
//  Created by mac on 17/4/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "JJPhotoViewController.h"
#import "XLPlainFlowLayout.h"
#import "JJMediaCell.h"
#import "JJReusableView.h"
#import "JJPhotoViewController+Response.h"
#import "JJMediaCenterViewController.h"

#define CELL_IDENTIFIER @"WaterfallCell_phone_pic"
#define HEADER_IDENTIFIER @"WaterfallHeader_phone_pic"
#define FOOTER_IDENTIFIER @"WaterfallFooter_phone_pic"

@interface JJPhotoViewController ()<JJMediaCenterViewControllerDelegate>
@property (nonatomic,strong) NSDateFormatter *dateFormatter;
@end

@implementation JJPhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor =  [UIColor whiteColor];
    
    [self.view addSubview:self.collectionView];
    
    __weak typeof(self)weakSelf = self;
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.view.mas_left);
        make.right.equalTo(weakSelf.view.mas_right);
        make.bottom.equalTo(weakSelf.view.mas_bottom);
        make.top.equalTo(weakSelf.view.mas_top);
    }];

    MJRefreshNormalHeader *mjHeader = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(fetchPhotos)];
    mjHeader.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
    
    UIActivityIndicatorView *loadingView = [mjHeader valueForKey:@"loadingView"];
    loadingView.color = [UIColor grayColor];
    
    mjHeader.stateLabel.textColor = [UIColor blackColor];
    mjHeader.lastUpdatedTimeLabel.textColor = [UIColor blackColor];
    mjHeader.stateLabel.hidden = YES;
    mjHeader.lastUpdatedTimeLabel.hidden = YES;
    
    self.collectionView.mj_header = mjHeader;
    [self.collectionView.mj_header beginRefreshing];
    

}

- (void)searchAction:(UIButton *)sender
{
    JJPhotoViewController *vc = [[JJPhotoViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - LazyLoad
- (NSDateFormatter *)dateFormatter
{
    if (!_dateFormatter) {
        _dateFormatter = [[NSDateFormatter alloc] init];
        [_dateFormatter setDateFormat:@"yyyy-MM-dd"];
    }
    return _dateFormatter;
}

- (UICollectionView *)collectionView
{
    if (!_collectionView) {

        CGFloat allFlowWidth = (MIN_LEN -3)/4;
        CGFloat itemSpacing = 1;
        CGFloat lineSpacing = 1;
        
        XLPlainFlowLayout *layout = [XLPlainFlowLayout new];
        layout.itemSize = CGSizeMake(allFlowWidth, allFlowWidth);
        layout.minimumInteritemSpacing = itemSpacing;
        layout.minimumLineSpacing = lineSpacing;

        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_WIDTH) collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor clearColor];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        
#if iOS10_OR_LATER //iOS 10 禁用这个属性,否则会崩溃
        if(IOS_VERSION >= 10.0)
            _collectionView.prefetchingEnabled = NO;
#endif
        [self.collectionView registerClass:[JJMediaCell class] forCellWithReuseIdentifier:CELL_IDENTIFIER];
        [self.collectionView registerClass:[JJReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:HEADER_IDENTIFIER];
    }
    return _collectionView;
}

- (NSMutableDictionary *)allKeyValues
{
    if (!_allKeyValues) {
        _allKeyValues = [[NSMutableDictionary alloc] init];
    }
    return _allKeyValues;
}

- (NSMutableArray *)tempSelectArray
{
    if (!_tempSelectArray) {
        _tempSelectArray = [[NSMutableArray alloc] init];
    }
    return _tempSelectArray;
}

- (NSMutableArray *)files
{
    if (!_files) {
        _files = [[NSMutableArray alloc] init];
    }
    return _files;
}

- (UIButton *)seleteButton
{
    if (!_seleteButton) {
        for (UIBarButtonItem *barItem in self.vt_magicController.toolbar1.items) {
            if (barItem.customView.tag == 400)
            {
                _seleteButton = barItem.customView;
                break;
            }
        }
    }
    return _seleteButton;
}


- (UIButton *)editBuuton
{
    if (!_editBuuton) {
        _editBuuton = (UIButton *)self.vt_magicController.magicView.rightNavigatoinItem;
    }
    return _editBuuton;
}

- (UIBarButtonItem *)shareBarItem
{
    if (!_shareBarItem) {
        for (UIBarButtonItem *barItem in self.vt_magicController.toolbar1.items) {
            if (barItem.customView.tag == 401)
            {
                _shareBarItem = barItem;
                break;
            }
        }
    }
    return _shareBarItem;
}
#pragma mark -  MWPhotoBrowser
- (MWPhotoBrowser *)photoBrowser
{
    if (_photoBrowser == nil) {
        _photoBrowser = [[MWPhotoBrowser alloc] initWithDelegate:self];
        
        BOOL displayActionButton = YES;
        BOOL displaySelectionButtons = NO;
        BOOL displayNavArrows = YES;
        BOOL enableGrid = YES;
        BOOL startOnGrid = NO;
        BOOL autoPlayOnAppear = NO;
        _photoBrowser.displayActionButton = displayActionButton;
        _photoBrowser.displayNavArrows = displayNavArrows;
        _photoBrowser.displaySelectionButtons = displaySelectionButtons;
        _photoBrowser.alwaysShowControls = displaySelectionButtons;
        _photoBrowser.zoomPhotosToFill = YES;
        _photoBrowser.enableGrid = enableGrid;
        _photoBrowser.startOnGrid = startOnGrid;
        _photoBrowser.enableSwipeToDismiss = NO;
        _photoBrowser.autoPlayOnAppear = autoPlayOnAppear;
    }
    return _photoBrowser;
}

- (UINavigationController *)photoNavigationController
{
    if (_photoNavigationController == nil) {
        _photoNavigationController = [[UINavigationController alloc] initWithRootViewController:self.photoBrowser];
        _photoNavigationController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    }
    return _photoNavigationController;
}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser
{
    return [self.photos count];
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index
{
    if (index < self.photos.count)
    {
        return [self.photos objectAtIndex:index];
    }
    return nil;
}

- (NSString *)photoBrowser:(MWPhotoBrowser *)photoBrowser descriptionForPhotoAtIndex:(NSUInteger)index
{
    return [NSString stringWithFormat:@"%lu of %lu",(unsigned long)index+1,(unsigned long)self.photos.count];
}

- (NSString *)photoBrowser:(MWPhotoBrowser *)photoBrowser titleForPhotoAtIndex:(NSUInteger)index
{
    MWPhoto *photo = self.photos[index];
    JJFile *file = photo.file;
    return file.creationDate;
}

- (void)photoBrowserDidFinishModalPresentation:(MWPhotoBrowser *)photoBrowser
{
#if 1
    [photoBrowser dismissViewControllerAnimated:NO completion:nil];
#else
    [photoBrowser.navigationController popViewControllerAnimated:YES ];
#endif
}


#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return self.allKeyValues.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self assetThumbsAtSection:section].count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    JJMediaCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:CELL_IDENTIFIER forIndexPath:indexPath];
    cell.isVideo = NO;
    [self configCell:cell indexPath:indexPath];
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    JJReusableView *header = [collectionView  dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:HEADER_IDENTIFIER forIndexPath:indexPath];
    header.tag = indexPath.row;
    
    header.text =  [self setionOfKey:indexPath.section];
    
    return header;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake(100, 44);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self handleDidSelectRowAtIndexPath:indexPath];
}


@end
