//
//  JJPhotoViewController+Response.h
//  JJRC
//
//  Created by mac on 17/4/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "JJPhotoViewController.h"

@interface JJPhotoViewController (Response)

- (void)fetchPhotos;
- (void)fetchPhotosWithCallback:(void (^)())callback;

- (NSString *)setionOfKey:(NSInteger)setion;
- (NSArray *)assetThumbsAtSection:(NSInteger )section;
@end
