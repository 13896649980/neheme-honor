//
//  JJPhotoViewController+Response.m
//  JJRC
//
//  Created by mac on 17/4/18.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "JJPhotoViewController+Response.h"
#import "JJFile.h"
#import "JJMediaCell.h"
#import "JJMediaCenterViewController.h"
#import "JJMediaManager.h"
#import "LTShareManager.h"
#import "MWPhotoBrowser+Response.h"
#import "UIViewController+ActivityIndicator.h"

@implementation JJPhotoViewController (Response)

#pragma mark - public
- (void)fetchPhotos
{
    [self fetchPhotosWithCallback:nil];
}
- (void)fetchPhotosWithCallback:(void (^)())callback
{
    __weak typeof(self)weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf showActivityIndicatorInView:weakSelf.view];
    });
    
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [JJMediaManager fetchPhoneDocumentsFileAttributeWithDirName:DIR_PHOTO_THUMB callback:^(NSArray<NSDictionary *> *fileAttbs) {
            
            NSMutableArray *files = [NSMutableArray array];
            NSMutableArray *allkeys = [NSMutableArray array];
            NSMutableArray *photos = [NSMutableArray array];
            NSMutableArray *thumbs = [[NSMutableArray alloc] init];
            
            NSMutableDictionary *allkeyValues = [NSMutableDictionary dictionary];
            
            for (int i = 0; i < fileAttbs.count; i++) {
                NSDictionary *fileAttb = fileAttbs[i];
                JJFile *file = [[JJFile alloc] initWithFileAttribs:fileAttb];
                [files addObject:file];
                
                NSString *dateString = [[file.modifiCationDate componentsSeparatedByString:@" "] firstObject];
                if (dateString) {
                    [allkeys addObject:dateString];
                }
            }
            
            //按时间排序
            NSArray *dateStringArray1 = files;
            NSDateFormatter *dateFormatter1 = [NSDateFormatter new];
            dateFormatter1.dateFormat = @"yyyy-MM-dd HH:mm:ss";
            files = [[dateStringArray1 sortedArrayUsingComparator:^NSComparisonResult(JJFile *file1, JJFile *file2) {
                NSDate *date1 = [dateFormatter1 dateFromString:file1.modifiCationDate];
                NSDate *date2 = [dateFormatter1 dateFromString:file2.modifiCationDate];
                return [date2 compare:date1];
            }] mutableCopy];
            
            
            
            NSSet *set = [NSSet setWithArray:allkeys];
            NSArray *allsetKeys = [set allObjects];
            
            //按时间排序
            
            NSArray *dateStringArray = allsetKeys;
            NSDateFormatter *dateFormatter = [NSDateFormatter new];
            dateFormatter.dateFormat = @"yyyy-MM-dd";
            allsetKeys = [dateStringArray sortedArrayUsingComparator:^(NSString *string1, NSString *string2) {
                NSDate *date1 = [dateFormatter dateFromString:string1];
                NSDate *date2 = [dateFormatter dateFromString:string2];
                return [date2 compare:date1];
            }];
            

            for (int i = 0; i < allsetKeys.count; i++)
            {
                NSMutableArray *allvalues = [NSMutableArray array];
                for (int j = 0; j < files.count; j++)
                {
                    JJFile  *file = files[j];
                    if ([file.modifiCationDate hasPrefix:allsetKeys[i]]) {
                        
                        MWPhoto *thumb = [MWPhoto photoWithPhoneFile:file];
                        
                        [allvalues addObject:thumb];
                        [thumbs addObject:thumb];
                        
                        [photos addObject:[MWPhoto photoWithPhoneFile:file]];
                       
                    }
                }
                
                [allkeyValues setObject:allvalues forKey:allsetKeys[i]];
            }
            
            weakSelf.files = files ;
            weakSelf.thumbs = thumbs;
            weakSelf.photos = photos;
            weakSelf.allKeyValues = allkeyValues;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf hideActivityIndicatorView];
                [weakSelf.collectionView reloadData];
                [weakSelf.collectionView.mj_header endRefreshing];
            });
            
            if (callback) {
                callback();
            }
            
        }];

    });
}


#pragma mark - Private
- (UIImage *)imageForPhoto:(id<MWPhoto>)photo {
    if (photo) {
        // Get image or obtain in background
        if ([photo underlyingImage]) {
            return [photo underlyingImage];
        } else {
            [photo loadUnderlyingImageAndNotify:YES];
        }
    }
    return nil;
}

- (NSArray *)assetThumbsAtSection:(NSInteger )section
{
    NSArray* arr = [self.allKeyValues allKeys];
    arr = [arr sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2){
        NSComparisonResult result = [obj1 compare:obj2];
        return result == NSOrderedAscending;
    }];
    NSString *key = arr[section];
    return [self.allKeyValues objectForKey:key];
}

- (NSString *)setionOfKey:(NSInteger)setion
{
    NSArray* arr = [self.allKeyValues allKeys];
    arr = [arr sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2){
        NSComparisonResult result = [obj1 compare:obj2];
        return result == NSOrderedAscending;
    }];
    
    return  arr[setion];
}

- (NSMutableArray *)setionOfAllkeyValues:(NSInteger)setion
{
    NSArray* arr = [self.allKeyValues allKeys];
    arr = [arr sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2){
        NSComparisonResult result = [obj1 compare:obj2];
        return result == NSOrderedAscending;
    }];
    NSString *key = arr[setion];
    return [self.allKeyValues objectForKey:key];
}


- (NSInteger)indexOfFile:(JJFile *)file
{
    NSInteger index = 0;
    for (int i = 0; i < self.photos.count; i ++) {
        MWPhoto *photo = self.photos[i];
        if (photo.file == file) {
            index = i;
            break;
        }
    }
    return index;
}

- (void)showBrowserAtindex:(NSInteger)index
{
    __weak typeof(self)weakSelf = self;
    
    self.photoBrowser.refetchDeleteBlock = ^(callback callback ,NSArray <JJFile *> *files,BOOL isEncrypt){
        [weakSelf fetchPhotosWithCallback:callback];
    };
    

    [self.photoBrowser setCurrentPhotoIndex:index];
    [self presentViewController:self.photoNavigationController animated:YES completion:^{
        [weakSelf.photoBrowser reloadData];
    }];
}


#pragma mark - 重写父类方法
- (void)handleDidSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (self.edit)
    {
        NSArray *array = [self assetThumbsAtSection:indexPath.section];
        MWPhoto *photo = array[indexPath.row];
        photo.isSelect = !photo.isSelect;
        
        if (photo.isSelect) {
            if (![self.tempSelectArray containsObject:photo]) {
                [self.tempSelectArray addObject:photo];
            }
        }else
        {
            if ([self.tempSelectArray containsObject:photo]) {
                [self.tempSelectArray removeObject:photo];
            }
        }
        UIButton *button = self.seleteButton;

        if (self.tempSelectArray.count && self.tempSelectArray.count == self.photos.count) {
            //已经全选了
            button.selected = YES;
            
        }else
        {
            //没有全选了
            button.selected = NO;
        }

        __weak typeof(self)weakSelf = self;
        [UIView performWithoutAnimation:^{
            [weakSelf.collectionView reloadItemsAtIndexPaths:@[indexPath]];
        }];
        
        
    }else
    {
        NSArray *thumbs = [self assetThumbsAtSection:indexPath.section];
        MWPhoto *photo = thumbs[indexPath.row];
        
        int index = 0;
        for (index = 0 ; index < self.photos.count; index++)
        {
            MWPhoto *p = [self.photos objectAtIndex:index];
            if ([p.file.name isEqualToString:photo.file.name])
            {
                break;
            }
        }
        [self showBrowserAtindex:index];
    }
}



- (void)configCell:(__kindof UIView *)cell indexPath:(NSIndexPath *)indexPath
{
    JJMediaCell *photoCell = cell;
    NSArray *thumbs = [self assetThumbsAtSection:indexPath.section];
    
    MWPhoto *photo = thumbs[indexPath.row];
    photoCell.index = indexPath.row;
    photoCell.photo = photo;
    UIImage *img = [self imageForPhoto:photoCell.photo];
    if (img)
    {
        [cell displayImage];
    }
}


#pragma mark - JJMediaCenterViewControllerDelegate

- (void)edit:(BOOL)edit
{
    self.collectionView.mj_header.hidden = edit;
    self.edit = edit;
    if (!edit)
    {
        for (int i = 0; i < self.thumbs.count; i++) {
            MWPhoto *photo = self.thumbs[i];
            photo.isSelect = NO;
        }
        [self.tempSelectArray removeAllObjects];
        
        self.collectionView.mj_header.hidden = NO;
        
        [self.collectionView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view.mas_left);
            make.right.equalTo(self.view.mas_right);
            make.bottom.equalTo(self.view.mas_bottom);
            make.top.equalTo(self.view.mas_top);
        }];
        
        __weak typeof(self)weakSelf = self;
        [UIView performWithoutAnimation:^{
            [weakSelf.collectionView reloadItemsAtIndexPaths:[weakSelf.collectionView indexPathsForVisibleItems]];
        }];
        self.seleteButton.selected = NO;
    }else
    {
        [self.collectionView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view.mas_left);
            make.right.equalTo(self.view.mas_right);
            make.bottom.equalTo(self.view.mas_bottom).offset(-LTToolbarHeight);
            make.top.equalTo(self.view.mas_top);
        }];
    }
}

- (void)shareFiles
{
    if (self.tempSelectArray.count <= 0)
    {
        NSString *msg = @"";

            msg = NSLocalizedString(@"Please select a file first", nil);
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Note", nil) message:msg delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil ];
            [alert show];
        });
    }else
    {
        NSMutableArray *files = [NSMutableArray array];
        for (MWPhoto *photo in self.tempSelectArray) {
            [files addObject:photo.file];
        }
        
        [LTShareManager showShareMenuWithSandboxFiles:files barButtonItem:self.shareBarItem showViewController:self];
    }
    
}

- (void)deleteFiles
{
    if (self.tempSelectArray.count <= 0)
    {
        NSString *msg = @"";

            msg = NSLocalizedString(@"Please select a file first", nil);
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Note", nil) message:msg delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil ];
            [alert show];
        });
    }else
    {
        UIAlertView *alert ;

            alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Note",nil) message:NSLocalizedString(@"Are you sure you want to delete?",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",nil) otherButtonTitles:NSLocalizedString(@"OK",nil), nil];
        
        [alert show];
    }
}

- (void)saveFiles
{
    if (self.tempSelectArray.count <= 0)
    {
        NSString *msg = @"";

            msg = NSLocalizedString(@"Please select a file first", nil);
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Note", nil) message:msg delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil ];
            [alert show];
        });
    }else
    {
        __weak typeof(self)weakSelf = self;

            [SVProgressHUD showWithStatus:NSLocalizedString(@"Saving...", nil)];
        
        NSMutableArray *files = [NSMutableArray array];
        for (MWPhoto *photo in self.tempSelectArray) {
            [files addObject:photo.file];
        }
        [JJMediaManager saveFileToAlbum:files callback:^(int ret) {
            if (ret == 0)
            {
                dispatch_async(dispatch_get_main_queue(), ^{

                        [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Save complete", nil)];
                
                    
                    [weakSelf.vt_magicController performSelectorOnMainThread:@selector(editAction:) withObject:weakSelf.editBuuton waitUntilDone:NO];
                });
            }else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Save Fail", nil)];
                    
                    [weakSelf.vt_magicController performSelectorOnMainThread:@selector(editAction:) withObject:weakSelf.editBuuton waitUntilDone:NO];
                });
            }
        }];
    }
}

- (void)selectAll:(BOOL)select
{
    if (select)
    {
        for (int i = 0; i < self.thumbs.count; i++)
        {
            MWPhoto *photo = self.thumbs[i];
            photo.isSelect = YES;
            if (![self.tempSelectArray containsObject:photo]) {
                [self.tempSelectArray addObject:photo];
            }
        }
        
        __weak typeof(self)weakSelf = self;
        [UIView performWithoutAnimation:^{
            [weakSelf.collectionView reloadItemsAtIndexPaths:[weakSelf.collectionView indexPathsForVisibleItems]];
        }];
    }else
    {
        for (int i = 0; i < self.thumbs.count; i++) {
            MWPhoto *photo = self.thumbs[i];
            photo.isSelect = NO;
        }
        [self.tempSelectArray removeAllObjects];
        
        __weak typeof(self)weakSelf = self;
        [UIView performWithoutAnimation:^{
            [weakSelf.collectionView reloadItemsAtIndexPaths:[weakSelf.collectionView indexPathsForVisibleItems]];
        }];
    }
    
}

#pragma mark - UIAlertDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        
        __weak typeof(self)weakSelf = self;
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Deleting...", nil)];
        NSMutableArray *files = [NSMutableArray array];
        for (MWPhoto *photo in self.tempSelectArray) {
            [files addObject:photo.file];
        }
        [JJMediaManager deleteFile:files callback:^(int ret) {
            if (ret == 0)
            {
                dispatch_async(dispatch_get_main_queue(), ^{

                    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Delete complete", nil)];
                    [weakSelf.vt_magicController editAction:weakSelf.editBuuton];
                    [weakSelf fetchPhotos];

                               
                });
            }
        }];
    }else
    {
          [self.vt_magicController editAction:self.editBuuton];
    }
}


@end
