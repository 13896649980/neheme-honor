//
//  JJPhotoViewController.h
//  JJRC
//
//  Created by mac on 17/4/17.
//  Copyright © 2017年 mac. All rights reserved.
//


#import "MWPhotoBrowser.h"

static NSString *PhotosCellid = @"PhotosCellid";
static NSString *PhotosReusableViewid = @"PhotosReusableViewid";
static NSString *myPhotos = @"Photos";
static NSString *Thumbs = @"PhotosThumbs";

@class JJMediaCenterViewController;

@interface JJPhotoViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,MWPhotoBrowserDelegate,UIAlertViewDelegate>

@property(nonatomic,strong)UICollectionView *collectionView;

@property (strong, nonatomic) UINavigationController *photoNavigationController;
@property (strong, nonatomic)MWPhotoBrowser *photoBrowser;
@property (nonatomic,strong) NSMutableDictionary *allKeyValues;
@property (nonatomic,strong) NSMutableArray *tempSelectArray;
@property (nonatomic, strong) NSMutableArray *photos;
@property (nonatomic, strong) NSMutableArray *thumbs;
@property(nonatomic,strong)NSMutableArray <JJFile *>*files;
@property(nonatomic,weak)JJMediaCenterViewController *vt_magicController;

@property (nonatomic,strong)UIBarButtonItem *shareBarItem;//toolbar上面的share
@property (nonatomic,strong)UIButton *seleteButton;//toolbar上面的
@property (nonatomic,strong)UIButton *editBuuton;//JJMediaCenterViewController上面的editbutton

@property(nonatomic,assign)BOOL edit;

- (void)configCell:(__kindof UIView *)cell indexPath:(NSIndexPath *)indexPath;
- (void)handleDidSelectRowAtIndexPath:(NSIndexPath *)indexPath;


@end
