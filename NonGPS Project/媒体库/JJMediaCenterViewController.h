//
//  JJMediaCenterViewController.h
//  JJRC
//
//  Created by mac on 17/4/7.
//  Copyright © 2017年 mac. All rights reserved.
//
#import "VTMagicController.h"
#import "LTToolbar.h"

@protocol JJMediaCenterViewControllerDelegate <NSObject>

@optional
- (void)edit:(BOOL)edit;
- (void)shareFiles;
- (void)deleteFiles;
- (void)saveFiles;
- (void)selectAll:(BOOL)select;
@end

@interface JJMediaCenterViewController : VTMagicController
@property(nonatomic,strong)LTToolbar *toolbar1;
@property (nonatomic,weak)id<JJMediaCenterViewControllerDelegate>delegate;

- (void)editAction:(UIButton *)sender;
@end
