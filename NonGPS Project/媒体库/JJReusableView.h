//
//  JJReusableView.h
//  JJRC
//
//  Created by mac on 17/4/17.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JJReusableView : UICollectionReusableView
@property (nonatomic, strong) NSString *text;
@end
