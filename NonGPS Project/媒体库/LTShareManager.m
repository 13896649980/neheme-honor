//
//  LTShareManager.m
//  LetingUdisk
//
//  Created by mac on 16/8/17.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "LTShareManager.h"
#import "SVProgressHUD.h"
#import <MessageUI/MFMailComposeViewController.h>
//#import "MediaViewController.h"

@interface LTShareItem ()<UIActivityItemSource>
//传入的图片
@property (nonatomic, strong) UIImage *img;
//保存图片的本地路径
@property (nonatomic, strong) NSURL *filePath;
@end

@implementation LTShareItem

-(instancetype)initWithImage:(UIImage *)img andfile:(NSURL *)fileURl
{
    self = [super init];
    if (self) {
        _img = img;
        _filePath = fileURl;
    }
    return self;
}


#pragma mark - UIActivityItemSource代理
//@required
-(id)activityViewControllerPlaceholderItem:(UIActivityViewController *)activityViewController
{
    return _img;
}
//@required
-(id)activityViewController:(UIActivityViewController *)activityViewController itemForActivityType:(NSString *)activityType
{
    if ([activityType isEqualToString:@"com.apple.UIKit.activity.Mail"]) {
        MFMailComposeViewController *controller =
        [[MFMailComposeViewController alloc] init];
        if (!controller) {
            // 在设备还没有添加邮件账户的时候mailViewController为空，下面的present view controller会导致程序崩溃，这里要作出判断
            NSLog(@"设备还没有添加邮件账户");
            return nil;
        }
    }
    return _filePath;
}

//@optional
-(NSString*)activityViewController:(UIActivityViewController *)activityViewController subjectForActivityType:(NSString *)activityType
{
    return nil;
}

//@optional
-(UIImage *)activityViewController:(UIActivityViewController *)activityViewController thumbnailImageForActivityType:(NSString *)activityType suggestedSize:(CGSize)size{
    return nil;
}

//@optional
-(NSString *)activityViewController:(UIActivityViewController *)activityViewController dataTypeIdentifierForActivityType:(NSString *)activityType
{
    return nil;
}
@end



@implementation LTActivity
- (instancetype)initWithImage:(UIImage *)shareImage title:(NSString *)title  showViewController:(UIViewController *)viewController contentArray:(NSArray *)contentArray
{
    if(self = [super init]){
        _shareImage = shareImage;
        _showViewController = viewController;
        _title = title;
        _contents = contentArray;
    }
    return self;

}
//以下方法都是自定义UiActivity需要重写的方法
+(UIActivityCategory)activityCategory
{
    return UIActivityCategoryShare;
}
//设置类型
-(NSString *)activityType
{
    return _title;
}
//设置现实的标题
-(NSString *)activityTitle
{
    return _title;
}
//图片
-(UIImage *)activityImage
{
    return _shareImage;
}
//返回yes就行，这个表示该分享是不是在controller中显示
-(BOOL)canPerformWithActivityItems:(NSArray *)activityItems
{
    return YES;
}
//是不是自定义的controller，如果为空，则调用performActivity方法
- (UIViewController *)activityViewController
{
    return nil;
}
//点击分享图标之后触发的方法
-(void)performActivity
{
    [self activityDidFinish:YES];
    return;
    if(nil == _title) {
        return;
    }
    
    NSURL *url = [_contents objectAtIndex:0];
    _documentController = [UIDocumentInteractionController interactionControllerWithURL:url];
    [_documentController presentOpenInMenuFromRect:CGRectMake(300, 300, 0, 0) inView:_showViewController.view animated:YES];
}

@end

@implementation LTActivityItemProvider

- (id)initWithUrl:(NSURL*)url image:(UIImage*)image
{
    self = [super initWithPlaceholderItem:url];
    
    _url = url;
    _image = image;
    
    return self;
}

- (id)item
{
    if([self.activityType isEqualToString:@"com.tencent.mqq.ShareExtension"])
    {
        //url to image;
        return _image;
    }
    return _url;
}
@end


@interface LTShareManager()<UIPopoverControllerDelegate>

typedef void(^downloadCallbackBlock)(int fileCount,float progress);

@property(nonatomic,copy)downloadCallbackBlock downloadCallbackBlock;
@property (nonatomic,strong)UIPopoverController *activityPopover;
@property (nonatomic,strong)NSOperation *operation;
@property (nonatomic,strong)NSURLSessionDownloadTask *downLoadTask;


- (void)showShareMenuWithSandboxFiles:(NSArray <JJFile *> *)files barButtonItem:(UIBarButtonItem *)barButtonItem  showViewController:(UIViewController *)showViewController;

@end

@implementation LTShareManager



static LTShareManager *_instance = nil;
+ (instancetype)shareInstance
{
    static dispatch_once_t onceToken ;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init] ;
    }) ;
    
    return _instance;
}



#pragma mark - private

- (void)showShareMenuWithSandboxFiles:(NSArray <JJFile *> *)files barButtonItem:(UIBarButtonItem *)barButtonItem  showViewController:(UIViewController *)showViewController
{
    __weak typeof(showViewController)weak_ShowViewController = showViewController;

    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        NSMutableArray *array = [NSMutableArray array];

        if (files.count == 0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:TranslationString(@"Note")   message:TranslationString(@"Please select a file first") delegate:nil cancelButtonTitle:TranslationString(@"OK") otherButtonTitles:nil ];
                [alert show];
            });
            return;
        }
        
        /*
        
        for (JJFile *file in files) {
            if(file.type == JJ_Folder){
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Note", nil) message:NSLocalizedString(@"Share files only", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil ];
                        [alert show];
                });
                return;
            }
        }
        if (files.count > 1) {
            for (JJFile *file in files) {
                if (file.type != JJ_Picture) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIAlertView *alert= [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Note",  nil) message:NSLocalizedString(@"Share single file at a time", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil ];
                            [alert show];
                    });
                    return;
                }
            }
        }
        if(files.count > 9){
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView *alert= [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Note",  nil) message:NSLocalizedString(@"Share a maximum of 9 images at a time", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil ];
                    [alert show];
            });
            return;
        }
         */
        
        if(files.count > 9){
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView *alert= [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Note",  nil) message:NSLocalizedString(@"Share a maximum of 9 files at a time", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil ];
                [alert show];
            });
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD show];
        });
        
        for (int i =0; i < files.count; i++)
        {
            @autoreleasepool {
                JJFile *file = files[i];
        
                if (file.type == JJ_Picture)
                {
                    LTShareItem *item = [[LTShareItem alloc] initWithImage:[UIImage imageNamed:@"ico_media_share"] andfile:[NSURL fileURLWithPath:file.sourcePath]];
                    [array addObject:item];
                }else if (file.type == JJ_Video)
                {
                    LTShareItem *item = [[LTShareItem alloc] initWithImage:[UIImage imageNamed:@"ico_media_share"] andfile:[NSURL fileURLWithPath:file.videoPath]];
                    [array addObject:item];
                }else if (file.type == JJ_Doc)
                {
                    LTShareItem *item = [[LTShareItem alloc] initWithImage:[UIImage imageNamed:@"ico_media_share"] andfile:[NSURL fileURLWithPath:file.path]];
                    [array addObject:item];
                }
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:array applicationActivities:nil];
            
            activityViewController.excludedActivityTypes = @[UIActivityTypeAssignToContact,UIActivityTypeCopyToPasteboard,UIActivityTypeAssignToContact,UIActivityTypeAddToReadingList,UIActivityTypeSaveToCameraRoll];
  
            activityViewController.completionWithItemsHandler = ^(UIActivityType  _Nullable activityType, BOOL completed, NSArray * _Nullable returnedItems, NSError * _Nullable activityError) {
                
                /*
                if ([showViewController isKindOfClass:[MediaViewController class]]) {
                    MediaViewController *vc = (MediaViewController *)showViewController;
                    [vc setToolBarHidden:NO];
                }
                 */
            };
        
            /*
            activityViewController.completionHandler = ^(NSString *  activityType, BOOL completed){
                
                if ([weak_ShowViewController isKindOfClass:[NSClassFromString(@"MediaVideoViewController") class]] || [weak_ShowViewController isKindOfClass:[NSClassFromString(@"MediaPhotoViewController") class]])
                {
                    UIViewController *vt_magicController =  [weak_ShowViewController valueForKey:@"_vt_magicController"];
                    UIButton *editBtn = [weak_ShowViewController valueForKey:@"editBuuton"];
                    if ([vt_magicController respondsToSelector:@selector(editAction:)]) {
                        [vt_magicController performSelectorOnMainThread:@selector(editAction:) withObject:editBtn waitUntilDone:YES];
                    }
                }
            };
             */
            
            /*
            if ([showViewController isKindOfClass:[MediaViewController class]]) {
                MediaViewController *vc = (MediaViewController *)showViewController;
                [vc setToolBarHidden:YES];
            }
             */

            [weak_ShowViewController presentViewController:activityViewController animated:YES completion:nil];
            /*
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                //iPhone 从底部向上滑出view
                [weak_ShowViewController presentViewController:activityViewController animated:YES completion:nil];
            } else {
                //iPad, 弹出view
                activityViewController.popoverPresentationController.barButtonItem = barButtonItem;
                [weak_ShowViewController presentViewController:activityViewController animated:YES completion:nil];
            }
             */
        });
    });
}

#pragma mark - UIPopoverControllerDelegate
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    popoverController = nil;
}


#pragma mark - public
+ (void)showShareMenuWithSandboxFiles:(NSArray <JJFile *> *)files barButtonItem:(UIBarButtonItem *)barButtonItem  showViewController:(UIViewController *)showViewController
{
    [[LTShareManager shareInstance] showShareMenuWithSandboxFiles:files barButtonItem:barButtonItem showViewController:showViewController];
}



@end
