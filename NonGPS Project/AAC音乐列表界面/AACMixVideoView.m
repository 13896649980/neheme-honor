//
//  ACCMixVideoView.m
//  VS GPS
//
//  Created by Cc on 2019/3/29.
//  Copyright © 2019 huang mindong. All rights reserved.
//

#import "AACMixVideoView.h"
#import <AVFoundation/AVFoundation.h>

@interface AACMixVideoView ()

@property   (nonatomic,strong)  UIButton        *titleBtn;
@property   (nonatomic,strong)  UILabel         *durtionLabel;
@property   (nonatomic,strong)  AVAudioPlayer   *player;
@property   (nonatomic,strong)  NSTimer         *timer;

@property   (nonatomic,strong)  UIView          *selectMusicBgView;
@property   (nonatomic,strong)  NSString        *musicDurtion;

@property   (nonatomic,strong)  UIView          *recordDurtionBgView;
@property   (nonatomic,strong)  UIView          *currentDurtionView;
@property   (nonatomic,strong)  UIButton        *recordBtn;
@end

@implementation AACMixVideoView


-(RACSubject *)dissmisSignal{
    if (!_dissmisSignal) {
        _dissmisSignal = [RACSubject subject];
    }
    return _dissmisSignal;
}

-(RACSubject *)recordSignal{
    if (!_recordSignal) {
        _recordSignal = [RACSubject subject];
    }
    return _recordSignal;
}

-(RACSubject *)stopRecordSignal{
    if (!_stopRecordSignal) {
        _stopRecordSignal = [RACSubject subject];
    }
    return _stopRecordSignal;
}


-(instancetype)init{
    if (self = [super init]) {
        self.frame = [UIScreen mainScreen].bounds;
        [self initSubviews];
        self.backgroundColor = [UIColor clearColor];
        //静音播放
        AVAudioSession *audioSession = [AVAudioSession sharedInstance];
        [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
    }
    return self;
}


-(void)initSubviews{
    
    UIImageView *imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"play_music"]];
    [self addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(LAYOUT_WIDTH(20));
        make.height.width.mas_equalTo(LAYOUT_WIDTH(40));
    }];
    
    self.titleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.titleBtn.titleLabel.font = FONT_SCALE(16);
    [self.titleBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.titleBtn setTitle:NSLocalizedString(@"点击选择音乐>>", nil) forState:UIControlStateNormal];
    [self addSubview:self.titleBtn];
    [self.titleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(imageView);
        make.left.mas_equalTo(imageView.mas_right).offset(5);
    }];
#pragma mark - 点击选择音乐
    @weakify(self);
    [[self.titleBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self);
        
        if (self.timer) {
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"录制中，无法选择!", nil)];
            [SVProgressHUD dismissWithDelay:1];
            return ;
        }
        [self showSelectMusicBgView];
    }];
    
    UIButton *recordBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [recordBtn setBackgroundImage:[UIImage imageNamed:@"ic_record_video_off"] forState:UIControlStateNormal];
    [self addSubview:recordBtn];
    [recordBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self);
        make.right.mas_equalTo(LAYOUT_WIDTH(IS_iPhoneX ? -40 : -20));
        make.width.height.mas_equalTo(LAYOUT_WIDTH(50));
    }];
    self.recordBtn = recordBtn;
#pragma mark - 点击录像
    [[recordBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self);
        if (self.selectMusicBgView) {
            self.selectMusicBgView.hidden = YES;
        }
        
        if (!self.superController.playing) {
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"图像未连接!", nil) ];
            [SVProgressHUD dismissWithDelay:1.0f];
            return ;
        }
        
        if (!self.fileName || [self.fileName isEqualToString:@"无"]) {
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"请选择一段音乐", nil) ];
            [SVProgressHUD dismissWithDelay:1.0f];
            return ;
        }
#pragma mark - 未播放完音乐结束”摄像“
        if (self.timer) {
            UIAlertController *aleat = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Prompt", nil) message:NSLocalizedString(@"是否停止视频录制", nil)  preferredStyle:UIAlertControllerStyleAlert];
            __weak typeof(aleat)    weakAlert = aleat;
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                [weakAlert dismissViewControllerAnimated:YES completion:nil];
            }];
            UIAlertAction *done = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.stopRecordSignal sendNext:self.fileName];
                [recordBtn setBackgroundImage:[UIImage imageNamed:@"ic_record_video_off"] forState:UIControlStateNormal];
                [weakAlert dismissViewControllerAnimated:YES completion:nil];
                [self.timer invalidate];
                self.timer = nil;
                [self.player stop];
                self.durtionLabel.superview.hidden = YES;
                [self.currentDurtionView removeFromSuperview];
                [self.recordDurtionBgView removeFromSuperview];
            }];
            [aleat addAction:cancel];
            [aleat addAction:done];
            [self.superController presentViewController:aleat animated:YES completion:nil];
            
            return;
        }
        
        
        UIAlertController *aleat = [UIAlertController alertControllerWithTitle:@"" message:[NSString stringWithFormat:NSLocalizedString(@"你已选择：%@，总时长：%@秒\n最长视频可录制%@秒\n点击确认开始录像", nil),NSLocalizedString(self.fileName, nil),self.musicDurtion,self.musicDurtion] preferredStyle:UIAlertControllerStyleAlert];
        __weak typeof(aleat)    weakAlert = aleat;
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [weakAlert dismissViewControllerAnimated:YES completion:nil];
        }];
        UIAlertAction *done = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [recordBtn setBackgroundImage:[UIImage imageNamed:@"ic_record_video_on"] forState:UIControlStateNormal];
            [self starRecord];
            [self.recordSignal sendNext:nil];
        }];
        
        [aleat addAction:cancel];
        [aleat addAction:done];
        [self.superController presentViewController:aleat animated:YES completion:nil];

    }];
    
    UIButton *dissmisBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [dissmisBtn setBackgroundImage:[UIImage imageNamed:@"ic_close"] forState:UIControlStateNormal];
    [self addSubview:dissmisBtn];
    [dissmisBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(LAYOUT_WIDTH(20));
        make.right.mas_equalTo(LAYOUT_WIDTH(IS_iPhoneX ? -40 : -20));
        make.height.width.mas_equalTo(LAYOUT_WIDTH(35));
    }];
    
    
    [[dissmisBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self);
        if (self.timer) {
#pragma mark - 音乐播放未完成点击”返回“按钮，提示停止录像，
            UIAlertController *aleat = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Prompt", nil) message:NSLocalizedString(@"是否停止视频录制", nil) preferredStyle:UIAlertControllerStyleAlert];
            __weak typeof(aleat)    weakAlert = aleat;
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                [weakAlert dismissViewControllerAnimated:YES completion:nil];
            }];
            UIAlertAction *done = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.stopRecordSignal sendNext:self.fileName];
                [recordBtn setBackgroundImage:[UIImage imageNamed:@"ic_record_video_off"] forState:UIControlStateNormal];
                [weakAlert dismissViewControllerAnimated:YES completion:nil];
                [self.timer invalidate];
                [self.dissmisSignal sendNext:nil];
                [self.currentDurtionView removeFromSuperview];
                [self.recordDurtionBgView removeFromSuperview];
            }];
            [aleat addAction:cancel];
            [aleat addAction:done];
            [self.superController presentViewController:aleat animated:YES completion:nil];
            
            return ;
        }
        [self.dissmisSignal sendNext:nil];
    }];
}

-(void)setFileName:(NSString *)fileName{
    _fileName = fileName;
}

#pragma mark - 开始录音
-(void)starRecord{
    
    NSURL *url = [[NSBundle mainBundle]  URLForResource:[NSString stringWithFormat:@"%@.m4a",self.fileName] withExtension:nil];
    //初始化播放器对象
    self.player = [[AVAudioPlayer alloc]initWithContentsOfURL:url error:nil];
    //设置播放进度
    self.player.numberOfLoops = 1;
    CGFloat durtion = self.player.duration;
    [self.player prepareToPlay];
    [self.player play];

    if (!self.durtionLabel) {
        UIView *bgView = [[UIView alloc]init];
        bgView.layer.cornerRadius = LAYOUT_WIDTH(8);
        bgView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.7];
        [self addSubview:bgView];
        [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self.titleBtn);
            make.centerX.mas_equalTo(self);
            make.height.mas_equalTo(LAYOUT_WIDTH(30));
        }];
        
        UIView *redDot = [[UIView alloc]init];
        redDot.layer.cornerRadius = LAYOUT_WIDTH(3);
        redDot.backgroundColor = [UIColor redColor];
        [bgView addSubview:redDot];
        [redDot mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(bgView);
            make.height.width.mas_equalTo(LAYOUT_WIDTH(6));
            make.left.mas_equalTo(LAYOUT_WIDTH(15));
        }];
        
        UILabel *durtionLabel = [[UILabel alloc]init];
        durtionLabel.textColor = [UIColor whiteColor];
        durtionLabel.font = FONT_SCALE(15);
        [bgView addSubview:durtionLabel];
        [durtionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(bgView);
            make.left.mas_equalTo(redDot.mas_right).offset(LAYOUT_WIDTH(7));
            make.right.mas_equalTo(LAYOUT_WIDTH(-15));
        }];
        self.durtionLabel = durtionLabel;
    }else{
        self.durtionLabel.superview.hidden = NO;
    }
    
    UIView *durtionBgView = [[UIView alloc]init];
    durtionBgView.backgroundColor = [UIColor blackColor];
    [self addSubview:durtionBgView];
    [durtionBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self);
        make.height.mas_equalTo(LAYOUT_WIDTH(3));
    }];
    self.recordDurtionBgView = durtionBgView;
    
    UIView *currentDurtionLine = [[UIView alloc]init];
    currentDurtionLine.backgroundColor = RGB_COLORWITHRGB(7,210,245);
    [durtionBgView addSubview:currentDurtionLine];
    [currentDurtionLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(durtionBgView);
    }];
    self.currentDurtionView = currentDurtionLine;
    self.durtionLabel.text = @"00:00";
    __weak typeof(self)  weakSelf = self;
    __block CGFloat recordTimeCount = 0;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1 repeats:YES block:^(NSTimer * _Nonnull timer) {
        recordTimeCount += 1;
        self.durtionLabel.text = [weakSelf formatTimeInterval:recordTimeCount];
        
        [currentDurtionLine mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.right.mas_equalTo(durtionBgView);
            make.width.mas_equalTo((durtion - recordTimeCount)/durtion * MAX_LEN);
        }];
        
        if (recordTimeCount >= durtion) {
            [self.timer invalidate];
            self.timer = nil;
            [self.player stop];
            [durtionBgView removeFromSuperview];
            [currentDurtionLine removeFromSuperview];
            self.durtionLabel.superview.hidden = YES;
            [self.recordBtn setBackgroundImage:[UIImage imageNamed:@"ic_record_video_off"] forState:UIControlStateNormal];
#pragma mark - 音乐播放完成，发送停止录像按钮
            [self.stopRecordSignal sendNext:self.fileName];
        }
    }];
    [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
}


-(NSString *)formatTimeInterval:(CGFloat)seconds
{
    seconds = MAX(0, seconds);
    
    NSInteger s = seconds;
    NSInteger m = s / 60;
    NSInteger h = m / 60;
    
    s = s % 60;
    m = m % 60;
    
    if (h > 0) {
        return [NSString stringWithFormat:@"%ld:%0.2ld:%0.2ld", (long)h,(long)m,(long)s];
    }
    return [NSString stringWithFormat:@"%0.2ld:%0.2ld",(long)m,(long)s];
}


-(void)dealloc{
    NSLog(@"%@",self);
}


-(void)showSelectMusicBgView{
    
    if (self.selectMusicBgView) {
        self.selectMusicBgView.hidden = NO;
        return;
    }
    
    self.selectMusicBgView = [[UIView alloc]init];
    self.selectMusicBgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.selectMusicBgView];
    [self.selectMusicBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.mas_equalTo(self);
        make.height.mas_equalTo(LAYOUT_WIDTH(180));
    }];
    
    UIToolbar *toolBar = [[UIToolbar alloc] init];
    toolBar.barStyle = UIBarStyleBlackTranslucent; // 改变barStyle
    [self.selectMusicBgView addSubview:toolBar];
    [toolBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.selectMusicBgView);
    }];
    
    UIScrollView *scroller = [[UIScrollView alloc]init];
    scroller.backgroundColor = [UIColor clearColor];
    scroller.showsVerticalScrollIndicator = NO;
    scroller.showsHorizontalScrollIndicator = NO;
    [self.selectMusicBgView addSubview:scroller];
    [scroller mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.selectMusicBgView);
        make.top.mas_equalTo(LAYOUT_WIDTH(15));
        make.height.mas_equalTo(LAYOUT_WIDTH(100));
    }];
    
    UIView *scrollerContentView = [[UIView alloc]init];
    scrollerContentView.backgroundColor = [UIColor clearColor];
    [scroller addSubview:scrollerContentView];
    [scrollerContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.height.mas_equalTo(scroller);
    }];
    
    NSArray *musicTitleArr = @[@"无",@"爱恋",@"动感",@"回忆",@"积极",@"日落",@"时尚",@"史诗",@"运动",@"振奋",@"震撼"];
    NSArray *imagesArr = @[@"ic_music_item0",@"ic_music_item3",@"ic_music_item10",@"ic_music_item1",@"ic_music_item2",@"ic_music_item5",@"ic_music_item9",@"ic_music_item8",@"ic_music_item7",@"ic_music_item4",@"ic_music_item6"];
    UIView *lastbgview;
    __block UIButton *selectedBtn;
    __block UILabel *selectedLabel;
    for (NSInteger i = 0; i < musicTitleArr.count; i++) {
        
        UIView *bgView = [[UIView alloc]init];
        bgView.backgroundColor = [UIColor clearColor];
        [scrollerContentView addSubview:bgView];
        [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.mas_equalTo(scrollerContentView);
            make.width.mas_equalTo(LAYOUT_WIDTH(90));
            lastbgview ? (make.left.mas_equalTo(lastbgview.mas_right).offset(LAYOUT_WIDTH(10))) : (make.left.mas_equalTo(LAYOUT_WIDTH(15)));
            i == musicTitleArr.count - 1 ? (make.right.mas_equalTo(LAYOUT_WIDTH(IS_iPhoneX ? -40 : -15))) : nil;
        }];
        lastbgview = bgView;
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.layer.cornerRadius = LAYOUT_WIDTH(25);
        btn.clipsToBounds = YES;
        [btn setBackgroundImage:[UIImage imageNamed:imagesArr[i]] forState:UIControlStateNormal];
        [bgView addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(bgView);
            make.height.width.mas_equalTo(LAYOUT_WIDTH(50));
            make.top.mas_equalTo(LAYOUT_WIDTH(10));
        }];
        
        UILabel *label = [[UILabel alloc]init];
        label.text = NSLocalizedString(musicTitleArr[i],nil);
        label.textColor = [UIColor whiteColor];
        label.backgroundColor = [UIColor clearColor];
        label.font = Font(13);
        [bgView addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(LAYOUT_WIDTH(-10));
            make.centerX.mas_equalTo(bgView);
        }];
        
        NSURL *url = [[NSBundle mainBundle]  URLForResource:[NSString stringWithFormat:@"%@.m4a",musicTitleArr[i]] withExtension:nil];
        //初始化播放器对象
        self.player = [[AVAudioPlayer alloc]initWithContentsOfURL:url error:nil];
        //设置播放进度
        self.player.meteringEnabled = YES;
        self.player.numberOfLoops = 1;
        CGFloat durtion = self.player.duration;

        UILabel *durtionLabel = [[UILabel alloc]init];
        durtionLabel.text = [self formatTimeInterval:durtion];
        durtionLabel.textColor = [UIColor whiteColor];
        durtionLabel.backgroundColor = [UIColor clearColor];
        durtionLabel.font = Font(12);
        [btn addSubview:durtionLabel];
        [durtionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(btn);
            make.bottom.mas_equalTo(LAYOUT_WIDTH((-7)));
        }];
#pragma mark - 点击选择音乐
        @weakify(self);
        [[btn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
            @strongify(self);
            NSURL *url = [[NSBundle mainBundle]  URLForResource:[NSString stringWithFormat:@"%@.m4a",musicTitleArr[i]] withExtension:nil];
            self.player = [[AVAudioPlayer alloc]initWithContentsOfURL:url error:nil];
            self.player.meteringEnabled = YES;
            self.player.numberOfLoops = 1;
            [self.player prepareToPlay];
            [self.player play];
            
            if (!selectedBtn) {
                btn.layer.borderColor = RGB_COLORWITHRGB(30, 148, 219).CGColor;
                btn.layer.borderWidth = 2;
                label.textColor = RGB_COLORWITHRGB(30, 148, 219);
                selectedBtn = btn;
                selectedLabel = label;
            }else{
                if (btn != selectedBtn) {
                    btn.layer.borderColor = RGB_COLORWITHRGB(30, 148, 219).CGColor;
                    btn.layer.borderWidth = 2;
                    label.textColor = RGB_COLORWITHRGB(30, 148, 219);
                    
                    selectedBtn.layer.borderColor = [UIColor clearColor].CGColor;
                    selectedLabel.textColor = [UIColor whiteColor];
                    selectedBtn = btn;
                    selectedLabel = label;
                }
            }
            self.fileName = musicTitleArr[i];
            self.musicDurtion = durtionLabel.text;
            [self.titleBtn setTitle:[NSString stringWithFormat:NSLocalizedString(@"已选择:%@ 时长:%@", nil),NSLocalizedString(self.fileName, nil),self.musicDurtion] forState:UIControlStateNormal];
            [self.titleBtn setTitleColor:RGB_COLORWITHRGB(30, 148, 219) forState:UIControlStateNormal];
        }];
    }
    
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundImage:[UIImage imageNamed:@"ic_music_item_selected"] forState:UIControlStateNormal];
    [self.selectMusicBgView addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.selectMusicBgView);
        make.height.width.mas_equalTo(LAYOUT_WIDTH(45));
        make.bottom.mas_equalTo(LAYOUT_WIDTH(-15));
    }];
    
    @weakify(self);
    [[btn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self);
        self.selectMusicBgView.hidden = YES;
        [self.player stop];
    }];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if (self.selectMusicBgView) {
        [self.player stop];
        self.selectMusicBgView.hidden = YES;
    }
}


@end
