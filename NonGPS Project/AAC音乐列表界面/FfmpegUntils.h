//
//  FfmpegUntils.h
//  VISON_GPS
//
//  Created by pyz on 2019/3/8.
//  Copyright © 2019 huang mindong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FfmpegUntils : NSObject
+(int)ffmpegWithvideo_url:(const char * )video_url music_url:(const char *)music_url out_video_url:(const char *)out_video_url;

@end
