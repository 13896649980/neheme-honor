//
//  ACCMixVideoView.h
//  VS GPS
//
//  Created by Cc on 2019/3/29.
//  Copyright © 2019 huang mindong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ReactiveObjC/ReactiveObjC.h>
#import "VisonPlayViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface AACMixVideoView : UIView

@property   (nonatomic,strong)  RACSubject      *recordSignal;
@property   (nonatomic,strong)  RACSubject      *dissmisSignal;
@property   (nonatomic,strong)  RACSubject      *stopRecordSignal;

@property   (nonatomic,strong)  NSString        *fileName;

@property   (nonatomic,strong)  VisonPlayViewController        *superController;

@end

NS_ASSUME_NONNULL_END
