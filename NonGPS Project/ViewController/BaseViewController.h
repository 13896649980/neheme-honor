//
//  BaseViewController.h
//  NonGPS Project
//
//  Created by Xu pan on 2019/8/14.
//  Copyright © 2019 Xu pan. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface BaseViewController : UIViewController

- (void)interfaceOrientation:(UIInterfaceOrientation)orientation;

@end


