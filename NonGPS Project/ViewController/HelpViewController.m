//
//  HelpViewController.m
//  NonGPS Project
//
//  Created by Xu pan on 2019/8/14.
//  Copyright © 2019 Xu pan. All rights reserved.
//

#import "HelpViewController.h"
#import <WebKit/WebKit.h>
@interface HelpViewController ()

@end

@implementation HelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    UIImageView * imageV = [[UIImageView alloc]initWithImage:IMAGEWITHNAME(@"功能说明")];
//    [self.view addSubview:imageV];
//    [imageV mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.mas_equalTo(self.view);
//    }];
//
    NSString *path = [[NSBundle mainBundle] pathForResource:@"H855-操控说明.pdf" ofType:nil];
    NSURL *url = [NSURL fileURLWithPath:path];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    WKWebView * webview = [[WKWebView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [webview loadRequest:request];
    [self.view addSubview:webview];
    
    
    UIButton * backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.4];
    backBtn.layer.masksToBounds = YES;
    backBtn.layer.cornerRadius = 5;
    [backBtn setImage:IMAGEWITHNAME(@"图层4") forState:UIControlStateNormal];
    [self.view addSubview:backBtn];
    [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.mas_equalTo(LAYOUT_WIDTH(15));
        make.width.height.mas_equalTo(LAYOUT_WIDTH(45));
    }];
    @weakify(self);
    [[backBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    // Do any additional setup after loading the view.
}

#pragma mark -------- 在这里设置横竖屏 --------
-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    //    return UIInterfaceOrientationMaskPortrait; //仅仅竖屏
    return UIInterfaceOrientationMaskLandscapeLeft; //仅仅横屏
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [HelpViewController attemptRotationToDeviceOrientation];
    //    [self interfaceOrientation:UIInterfaceOrientationPortrait]; //进来竖屏
    [self interfaceOrientation:UIInterfaceOrientationLandscapeLeft]; //进来横屏
}
-(BOOL)shouldAutorotate{
    return YES;
}

@end
