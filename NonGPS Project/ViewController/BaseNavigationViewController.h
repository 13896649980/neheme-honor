//
//  BaseNavigationViewController.h
//  博坦创新
//
//  Created by Xu pan on 2019/5/23.
//  Copyright © 2019 Xu pan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseNavigationViewController : UINavigationController

@end

NS_ASSUME_NONNULL_END
