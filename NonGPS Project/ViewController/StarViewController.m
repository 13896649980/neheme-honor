//
//  StarViewController.m
//  NonGPS Project
//
//  Created by Xu pan on 2019/8/14.
//  Copyright © 2019 Xu pan. All rights reserved.
//

#import "StarViewController.h"
#import "HelpViewController.h"
#import "GuideViewController.h"
#import "SettingViewController.h"
#import "ReactiveObjC.h"
#import "PlayViewController.h"

#pragma mark -------- 这里设置的图片 --------

#define SettingBtn_ImageName @""
#define GuidegBtn_ImageName @""
#define HelpBtn_ImageName @"help"
#define StartBtn_ImageName @"connect"
#define Background_ImageName @"主界面"

@interface StarViewController ()

@property   (nonatomic,strong)          UIImageView     *backgroundImageView;
@property   (nonatomic,strong)          UIButton        *startBtn;
@property   (nonatomic,strong)          UIButton        *helpBtn;
@property   (nonatomic,strong)          UIButton        *guideBtn;
@property   (nonatomic,strong)          UIButton        *settingBtn;
@property   (nonatomic,strong)          UILabel         *verLabel;
@property   (nonatomic,strong)          NSTimer         *getVerTimer;

@end

@implementation StarViewController

#pragma mark - lazyLoad

-(UIImageView *)backgroundImageView{
    if (!_backgroundImageView) {
        _backgroundImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:TranslationString(Background_ImageName)]];
        [self.view addSubview:_backgroundImageView];
    }
    return _backgroundImageView;
}

-(UIButton *)settingBtn{
    if (!_settingBtn) {
        _settingBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_settingBtn setImage:[UIImage imageNamed:TranslationString(SettingBtn_ImageName)] forState:UIControlStateNormal];
        [self.view addSubview:_settingBtn];
        @weakify(self);
        [[_settingBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
            @strongify(self);
            [self.navigationController pushViewController:[[SettingViewController alloc]init] animated:YES];
        }];
    }
    return _settingBtn;
}

-(UIButton *)startBtn{
    if (!_startBtn) {
        _startBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_startBtn setImage:[UIImage imageNamed:TranslationString(StartBtn_ImageName)] forState:UIControlStateNormal];
        [self.view addSubview:_startBtn];
        @weakify(self);
        [[_startBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
            @strongify(self);
            [self.navigationController pushViewController:[[PlayViewController alloc]init] animated:YES];
        }];
    }
    return _startBtn;
}

-(UIButton *)helpBtn{
    if (!_helpBtn) {
        _helpBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_helpBtn setImage:[UIImage imageNamed:TranslationString(HelpBtn_ImageName)] forState:UIControlStateNormal];
        [self.view addSubview:_helpBtn];
        @weakify(self);
        [[_helpBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
            @strongify(self);
            [self.navigationController pushViewController:[[HelpViewController alloc]init] animated:YES];
        }];
    }
    return _helpBtn;
}

-(UIButton *)guideBtn{
    if (!_guideBtn) {
        _guideBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_guideBtn setImage:[UIImage imageNamed:TranslationString(GuidegBtn_ImageName)] forState:UIControlStateNormal];
        [self.view addSubview:_guideBtn];
        @weakify(self);
        [[_guideBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
            @strongify(self);
            [self.navigationController pushViewController:[[GuideViewController alloc]init] animated:YES];
        }];
    }
    return _guideBtn;
}

-(UILabel *)verLabel{
    if (!_verLabel) {
        _verLabel = [[UILabel alloc]init];
        _verLabel.font = [UIFont systemFontOfSize:15 * FontScale];
        _verLabel.textColor = [UIColor blackColor];
        _verLabel.numberOfLines = 0;
        _verLabel.textAlignment = NSTextAlignmentCenter;
        [self.view addSubview:_verLabel];
        [_verLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            if (SCREEN_WIDTH_A > SCREEN_HEIGHT_A) {
                make.top.mas_equalTo(LAYOUT_WIDTH(10));
                make.right.mas_equalTo(LAYOUT_WIDTH(IS_iPhoneX_TYPE ? -35 : -10));
            }else{
                make.right.mas_equalTo(LAYOUT_WIDTH(-5));
                make.top.mas_equalTo(LAYOUT_WIDTH(IS_iPhoneX_TYPE ? 35 : 10));
            }
        }];
    }
    return _verLabel;
}

#pragma mark - lifeCycle

#pragma mark -------- 在这里设置横竖屏 --------
-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    //    return UIInterfaceOrientationMaskPortrait; //仅仅竖屏
    return UIInterfaceOrientationMaskLandscapeLeft; //仅仅横屏
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [StarViewController attemptRotationToDeviceOrientation];
    //    [self interfaceOrientation:UIInterfaceOrientationPortrait]; //进来竖屏
    [self interfaceOrientation:UIInterfaceOrientationLandscapeLeft]; //进来横屏
}
-(BOOL)shouldAutorotate{
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //右上角显示版本号
    self.verLabel.text = [NSString stringWithFormat:@"AppVer:%@",[Utility getAppCurrentVersion]];
    @weakify(self);
    [[[NSNotificationCenter defaultCenter] rac_addObserverForName:VERSON_NOTI_NAME object:nil] subscribeNext:^(NSNotification * _Nullable x) {
         @strongify(self);
        self.verLabel.text = [NSString stringWithFormat:@"AppVer:V%@\n%@",[Utility getAppCurrentVersion],x.object];
    }];
    
    [self setupSubviews];

}

#pragma mark -------- 在这里布局子视图 --------
-(void)setupSubviews{

    [self.backgroundImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.view);
        make.width.mas_equalTo(self.view);
        make.height.mas_equalTo(self.view);
    }];
    
    [self.startBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.height.width.mas_equalTo(100);
        make.bottom.mas_equalTo(self.view).mas_offset(-50);
        make.centerX.mas_equalTo(self.view).mas_offset(140);
    }];
    
    [self.helpBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.height.width.mas_equalTo(100);
        make.bottom.mas_equalTo(self.view).mas_offset(-50);
        make.centerX.mas_equalTo(self.view).mas_offset(-140);
    }];
    
    [self.backgroundImageView addSubview:self.verLabel];
}

@end
