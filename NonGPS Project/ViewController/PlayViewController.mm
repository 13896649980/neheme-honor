//
//  PlayViewController.m
//  NonGPS Project
//
//  Created by Xu pan on 2019/8/14.
//  Copyright © 2019 Xu pan. All rights reserved.
//

#import "PlayViewController.h"
#import "HHCountdowLabel.h"
#import "AACMixVideoView.h"
#import "ChoseLvJingControlView.h"
#import "TakephotoImageHandle.h"
#import "JJMediaCenterViewController.h"
#ifdef OPENCV_ENABLE
#include "detector.h"
#import "TrackCalculation.h"
#include "track_main.h"
#endif


#pragma mark - 这是发送飞控的时间频率
#define SendFlightDataInterval 0.02f
#define BackgroundImageName @"操控页面bg"



@interface PlayViewController (){
    unsigned  long lastTakePhotoMS;
    int timeCount1;

#ifdef OPENCV_ENABLE
    BOOL IS_STARTRACK;
    UIView *trackView;
    UIView *trackfillView;
    UIView *borderV;
    uint32_t outRect[4];
    uint32_t outPeopleRect[4] ;
    uint32_t followRect[4];
    CGPoint startP;
    BOOL readyTrack;
    CGRect trackRect;
    CGRect orgRect;
    UIPanGestureRecognizer *pan;
#endif
}

@property   (nonatomic,strong)      NSTimer             *sendFlightDataTimer;//飞控发送定时器

@property   (nonatomic,assign)          char        speedValue;
@property   (nonatomic,assign)          char        noHeadValue;
@property   (nonatomic,assign)          char        flipValue;
@property   (nonatomic,assign)          char        oneKeyFlyValue;
@property   (nonatomic,assign)          char        oneKeyReturnValue;
@property   (nonatomic,assign)          char        oneKeyDropValue;
@property   (nonatomic,assign)          char        emergencyStopValue;
@property   (nonatomic,assign)          char        powerValue;
@property   (nonatomic,assign)          char        rotateValue;
@property   (nonatomic,assign)          char        forwardOrBackValue;
@property   (nonatomic,assign)          char        leftOrRightValue;
@property   (nonatomic,assign)          char        rotateAdjustValue;
@property   (nonatomic,assign)          char        forwardOrBackAdjustValue;
@property   (nonatomic,assign)          char        leftOrRightAdjustValue;
@property   (nonatomic,assign)          char        cameraUpValue;
@property   (nonatomic,assign)          char        cameraDownValue;
@property   (nonatomic,assign)          char        calibrationValue; //校准
@property   (nonatomic,assign)          int         flipFlySendCount;
@property   (nonatomic,assign)          int         circleValue;
@property   (nonatomic,assign)          int         rotateFlyValue;

#ifdef OPENCV_ENABLE
@property   (nonatomic,assign)          BOOL        gesturePhoto;//是否开启手势拍照录像
@property   (nonatomic,assign)          BOOL        isFollow;//是否开启图像跟随
#endif

@property   (nonatomic,strong)          AACMixVideoView  *aacMusicView;
//是否混合音乐
@property (nonatomic,assign)            BOOL            is_mixMusic;
@property (nonatomic,strong)            NSString        *aacMusicName;

//滤镜选择视图
@property (nonatomic,strong)     ChoseLvJingControlView  *choseLvJingView;

@property (nonatomic,assign)    int        photoMaichong;//拍照脉冲
@property (nonatomic,assign)    int        videoMaichong;//录像脉冲
@property (nonatomic,assign)  int  lastplaneV;
@end

@implementation PlayViewController

-(AACMixVideoView *)aacMusicView{
    if (!_aacMusicView) {
        _aacMusicView = [[AACMixVideoView alloc]init];
        _aacMusicView.superController = self;
        [self.view addSubview:_aacMusicView];
        _aacMusicView.hidden = YES;
#pragma mark - aac音乐列表界面消失
        @weakify(self);
        [_aacMusicView.dissmisSignal subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            self.mControllView.hidden = NO;
            [self.aacMusicView removeFromSuperview];
            self.aacMusicView = nil;
        }];
#pragma mark - aac音乐录像动作
        [_aacMusicView.recordSignal subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            [self record];
        }];
#pragma mark - ac停止录像动作
        [_aacMusicView.stopRecordSignal subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            self.is_mixMusic = YES;
            self.aacMusicName = x;
            [self record];
        }];
    }
    return _aacMusicView;
}

- (ChoseLvJingControlView *)choseLvJingView{
    if (!_choseLvJingView) {
        _choseLvJingView = [[ChoseLvJingControlView alloc] initWithmacCol:6 maxRow:1];
        [self.view addSubview:_choseLvJingView];
#pragma mark ============= 选择滤镜i效果
        @weakify(self);
        [_choseLvJingView.lvJingType subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            UIColor *color = x;
            const CGFloat *components = CGColorGetComponents(color.CGColor);
            [self.playerView setRGBFilterWithRed:components[0] green:components[1] blue:components[2]];
        }];
        [_choseLvJingView.hideSignal subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            self.mControllView.filterBtn.selected = NO;
        }];
    }
    return _choseLvJingView;
}

#pragma mark - getMethod
-(char)powerValue{
    return [self.mControllView.leftRocker GetPowerLR];
}

-(char)rotateValue{
    return [self.mControllView.leftRocker GetPowerFB];
}

-(char)forwardOrBackValue{
    return [self.mControllView.rightRocker GetPowerLR];
}

-(char)leftOrRightValue{
    return [self.mControllView.rightRocker GetPowerFB];
}

-(char)rotateAdjustValue{
    return [self.mControllView.rotateAdjustView getValue];
}

-(char)leftOrRightAdjustValue{
    return [self.mControllView.leftOrRightAdjustView getValue];
}

-(char)forwardOrBackAdjustValue{
    return [self.mControllView.fontdOrBackAdjustView getValue];
}

#pragma mark -------- 在这里设置横竖屏 --------
-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    //    return UIInterfaceOrientationMaskPortrait; //仅仅竖屏
    return UIInterfaceOrientationMaskLandscapeLeft; //仅仅横屏
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [PlayViewController attemptRotationToDeviceOrientation];
    //    [self interfaceOrientation:UIInterfaceOrientationPortrait]; //进来竖屏
    [self interfaceOrientation:UIInterfaceOrientationLandscapeLeft]; //进来横屏
}

-(BOOL)shouldAutorotate{
    return YES;
}

#pragma mark - lifeCycle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.showView.image = [UIImage imageNamed:BackgroundImageName];
    
    self.mControllView = [[ControllerView alloc]init];
    [self.view addSubview:self.mControllView];
    [self.mControllView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    
    [self configSendFlightTimer];
    [self setupSubviews];
    [self setupControllViewSignal];
        
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(commandRecord:) name:@"commandRecord" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(commandTakePhoto:) name:@"commandTakePhoto" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(receiveHyPlaneData:) name:TransferTCPData object:nil];

}

#pragma mark - 设置飞控
-(void)configSendFlightTimer{
    self.sendFlightDataTimer = [NSTimer scheduledTimerWithTimeInterval:SendFlightDataInterval target:self selector:@selector(sendFlightAction) userInfo:nil repeats:YES];
}

#pragma mark - 飞控视图信号响应
-(void)setupControllViewSignal{
    
#pragma mark - 点击返回
    @weakify(self);
    [self.mControllView.backSignal subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        [self popViewController];
    }];
    
#pragma mark - 点击拍照
    [self.mControllView.takePhothSignal subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        [self takePhoto];
    }];
    
#pragma mark - 点击录像
    [self.mControllView.recordSignal subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        [self record];
    }];
    
#pragma mark - 打开媒体
    [self.mControllView.mediaSignal subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        [self openMedia];
    }];
    
#pragma mark - VR
    [self.mControllView.VRSignal subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        [self setVREnable];
    }];
    
#pragma mark - 换面反转
    [self.mControllView.imageReversSignal subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        [self lensTurnAction];
    }];
    
#pragma mark - 点击速度档
    [self.mControllView.speedSignal subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        self.speedValue ? (self.speedValue == 1 ? (self.speedValue = 2) : (self.speedValue = 0)) : (self.speedValue = 1);
        [self.mControllView.speedBtn setImage:[UIImage imageNamed:[NSString stringWithFormat:@"档%d",self.speedValue + 1]] forState:UIControlStateNormal];
    }];
    
#pragma mark - 点击轨迹飞行
    [self.mControllView.openTrackSignal subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        if (self.mControllView.openTrackBtn.selected) {
            self.speedValue = 0;
            [self.mControllView.speedBtn setImage:[UIImage imageNamed:@"ic_speed_1"] forState:UIControlStateNormal];
        }
    }];
    
#pragma mark - 点击无头
    [self.mControllView.noHeadSignal subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        self.noHeadValue = self.mControllView.noHeadBTn.selected ? 1 : 0;
    }];
    
#pragma mark - 点击校准
    [self.mControllView.calibrationSignal subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        self.mControllView.calibrationBtn.enabled = NO;
        self.calibrationValue = 1;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self.mControllView.calibrationBtn.enabled = YES;
            self.calibrationValue = 0;
        });
    }];
    
#pragma mark - 点击飞机360反转
    [self.mControllView.flipSignal subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        self.flipFlySendCount = 0;
    }];
    
#pragma mark - 点击一键起飞
    [self.mControllView.oneKeyFlySignal subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        self.oneKeyFlyValue = 1;
        self.mControllView.oneKeyFlyBtn.enabled = NO;
        self.mControllView.oneKeyDropBtn.enabled = NO;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self.oneKeyFlyValue = 0;
            self.mControllView.oneKeyFlyBtn.enabled = YES;
            self.mControllView.oneKeyDropBtn.enabled = YES;
        });
    }];
    
#pragma mark - 点击一键下降
    [self.mControllView.oneKeyDropSignal subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        self.oneKeyDropValue = 1;
        self.mControllView.oneKeyFlyBtn.enabled = NO;
        self.mControllView.oneKeyDropBtn.enabled = NO;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self.oneKeyDropValue = 0;
            self.mControllView.oneKeyFlyBtn.enabled = YES;
            self.mControllView.oneKeyDropBtn.enabled = YES;
        });
    }];
    
#pragma mark - 点击紧急停止
    [self.mControllView.emergencyStopSignal subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        self.emergencyStopValue = 1;
        self.mControllView.emergencyStopBtn.selected = YES;
        self.mControllView.emergencyStopBtn.enabled = NO;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self.emergencyStopValue = 0;
            self.mControllView.emergencyStopBtn.selected = NO;
            self.mControllView.emergencyStopBtn.enabled = YES;
        });
    }];
    
#pragma mark - 点击录制声音
    [self.mControllView.voiceSignal subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        self.mControllView.voiceBtn.selected = !self.mControllView.voiceBtn.selected;
    }];
    
#pragma mark - 点击"缩小"
    [self.mControllView.imageNarrowSignal subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        if (!self.playing) {
            [SVProgressHUD showErrorWithStatus:TranslationString(@"图像未连接!")];
            [SVProgressHUD dismissWithDelay:1];
            return ;
        }
        if (self.playerView.scaleLevel - 0.2 < 1) {
            self.playerView.scaleLevel = 1;
            return;
        }
        self.playerView.scaleLevel -= 0.2;
    }];
    
#pragma mark - 点击”混合音乐“
    [self.mControllView.musicMixSignal subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        self.aacMusicView.hidden = NO;
        self.mControllView.hidden = YES;
    }];
    
#pragma mark - 点击“镜头切换"
    [self.mControllView.switchLensSignal subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        [self switchLensAction];
    }];
    
#pragma mark - 点击"放大"
    [self.mControllView.imageAmplificationSignal subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        if (!self.playing) {
            [SVProgressHUD showErrorWithStatus:TranslationString(@"图像未连接!")];
            [SVProgressHUD dismissWithDelay:1];
            return ;
        }
        if (self.playerView.scaleLevel + 0.2 > 2) {
            self.playerView.scaleLevel = 2;
            return;
        }
        self.playerView.scaleLevel += 0.2;
    }];
    
#pragma mark - 点击“滤镜”
    [self.mControllView.filterSignal subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        if (!self.playing) {
            [SVProgressHUD showErrorWithStatus:TranslationString(@"图像未连接!")];
            [SVProgressHUD dismissWithDelay:1];
            return ;
        }
        self.mControllView.filterBtn.selected=!self.mControllView.filterBtn.selected;
        self.mControllView.VRBtn.enabled = !self.mControllView.filterBtn.selected;
        self.choseLvJingView.hidden = !self.mControllView.filterBtn.selected;
        [self.view bringSubviewToFront:self.choseLvJingView];
    }];

#pragma mark - 显示隐藏摇杆
    [self.mControllView.turnOnOrOffSignal subscribeNext:^(id  _Nullable x) {
        if (self.mControllView.leftRockerDragBK.hidden)
        {
            self.mControllView.leftRockerDragBK.hidden = self.mControllView.leftRockerDragBK.hidden = self.mControllView.leftRockerImageView.hidden = self.mControllView.leftRocker.hidden =
            self.mControllView.rightRocker.hidden = self.mControllView.rightRockerDragBK.hidden =  self.mControllView.rightRockerImageView.hidden = self.mControllView.flipBtn.hidden  =    self.mControllView.oneKeyFlyBtn.hidden   =  self.mControllView.oneKeyDropBtn.hidden     =   self.mControllView.emergencyStopBtn.hidden =  self.mControllView.rotateAdjustView.hidden=
                self.mControllView.leftOrRightAdjustView.hidden=
                self.mControllView.fontdOrBackAdjustView.hidden =  NO;
        }else
        {
                self.mControllView.leftRockerDragBK.hidden = self.mControllView.leftRockerDragBK.hidden = self.mControllView.leftRockerImageView.hidden = self.mControllView.leftRocker.hidden =
                self.mControllView.rightRocker.hidden = self.mControllView.rightRockerDragBK.hidden =  self.mControllView.rightRockerImageView.hidden =
                self.mControllView.flipBtn.hidden  =    self.mControllView.oneKeyFlyBtn.hidden   =  self.mControllView.oneKeyDropBtn.hidden     =   self.mControllView.emergencyStopBtn.hidden =  self.mControllView.rotateAdjustView.hidden=
                self.mControllView.leftOrRightAdjustView.hidden=
                self.mControllView.fontdOrBackAdjustView.hidden =YES;
        }
    }];
    
    
#ifdef OPENCV_ENABLE
    pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan:)];
    
#pragma mark - 点击打开图像跟随
    [self.mControllView.openFollowSignal subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        if (!self.playing) {
            return ;
        }
        self.mControllView.openFollowBtn.selected = !self.mControllView.openFollowBtn.selected;
        [self trackviewYellowhidden:self.mControllView.openFollowBtn.selected];//显示虚框
        
        if (self.mControllView.openFollowBtn.selected) {
            self.mControllView.gestureBtn.hidden = YES;
            self.mControllView.startFollowBtn.hidden = NO;
            self.mControllView.leftRocker.hidden = YES;
            self.mControllView.rightRocker.hidden = YES;
            self.mControllView.leftRockerImageView.hidden = YES;
            self.mControllView.rightRockerImageView.hidden = YES;

            IS_STARTRACK = YES;
            [self.view addGestureRecognizer:pan];
            [self setSpeedValue:0];
            
        }else{
            self.mControllView.gestureBtn.hidden = NO;
            self.mControllView.startFollowBtn.hidden = YES;
            self.mControllView.startFollowBtn.enabled = NO;
            self.mControllView.leftRocker.hidden = NO;
            self.mControllView.rightRocker.hidden = NO;
            self.mControllView.leftRockerImageView.hidden = NO;
            self.mControllView.rightRockerImageView.hidden = NO;

            readyTrack = NO;
            
            [self.view removeGestureRecognizer:pan];
        }
        
        if (!readyTrack)
        {
            readyTrack = NO;
            self.isFollow = NO;

            
            trackRect = CGRectZero;
            [trackView removeFromSuperview],trackView = nil;
            [trackfillView removeFromSuperview],trackfillView = nil;
            
            [self cleanTrack];
        }
    }];
    
#pragma mark - 点击“GO",发送跟随数据
    [[self.mControllView.startFollowBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self);
        self.mControllView.startFollowBtn.selected = !self.mControllView.startFollowBtn.selected;
        if (self.mControllView.startFollowBtn.selected) { //发送跟随
            borderV.hidden = YES;
            readyTrack = YES;
            
            self.mControllView.leftRocker.hidden = NO;
            self.mControllView.rightRocker.hidden = NO;
            self.mControllView.leftRockerImageView.hidden = NO;
            self.mControllView.rightRockerImageView.hidden = NO;

            [self.view removeGestureRecognizer:pan];
            
        }else{ //重新画框
            borderV.hidden = NO;
            readyTrack = NO;
            self.mControllView.leftRocker.hidden = YES;
            self.mControllView.rightRocker.hidden = YES;
            self.mControllView.leftRockerImageView.hidden = YES;
            self.mControllView.rightRockerImageView.hidden = YES;

            [self.view addGestureRecognizer:pan];
        }
    }];
    
#endif
}

#pragma mark - 手机录像拍照
-(void)takePhoto{
    [self takePhoto:self.mControllView.takePhothBtn];
}
-(void)record{
    
    if (![self playing]) return;
    
    if (self.movieWriter.isWriting) {
        [self.movieWriter stopWriteMovieComplete:^(NSString *moviePath) {
            //创建影片缩略图，保存沙盒
            [Utility saveMoviewToPhotoAlbum:moviePath complete:^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.mControllView.recordBtn setImage:[UIImage imageNamed:@"图层6"] forState:UIControlStateNormal];
                    [self.mControllView setStatueWithEndRecord];
                });
#pragma mark - 混合音乐
                if (self.is_mixMusic) {
                    [Utility video:moviePath mixMusic:self.aacMusicName complateBlock:^{
                        self.is_mixMusic = NO;
                    }];
                }
            }];
        }];
        
    }else{
        
        [self.movieWriter starWriteMovieWithType:Source_PixelBuff_Type_YUV
                                        FilePath:[Utility getMoviewPath]
                                       frameRate:25
                                       frameSize:CGSizeMake(1920, 1080)
                                     audioEnable:self.mControllView.voiceBtn.selected
                                        complete:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.mControllView.recordBtn setImage:[UIImage imageNamed:@"图层6拷贝"] forState:UIControlStateNormal];
                [self.mControllView setStatueWithStarRecord];
            });
            [Utility playSoundwithRecordAction];
        }];
    }
    
}

#pragma mark - 遥控器录像
- (void)commandRecord:(NSNotification *)notify{
    bool isRecord = [notify.object boolValue];
    if (isRecord) {
        if (self.movieWriter.isWriting){
            //防止发送多次通知录像
            return;
        }
        [self record];
        NSLog(@"遥控器录像动作 ----  开始");
    }else{
        if (self.movieWriter.isWriting) {
            [self record];
            NSLog(@"遥控器录像动作 ----  结束");
        }
    }
}


-(void)takePhoto:(UIButton *)sender{
    if (self.movieWriter.isWriting) {//正在录视频时不能点击
        return;
    }
    if ([self playing]){
        
        [Utility playSoundwithTakePhotoAction];
        self.mControllView.takePhothBtn.enabled = NO;
        //流里取图片
        
        [VisonWifiBaseLibraryMethods asyncTakePhotoFromStream:self.streamManager
                                              needSourceImage:YES
                                                     complete:^(UIImage * _Nullable streamImage, float progress, UIImage * _Nullable sourceImage) {
            
            self.mControllView.takePhothBtn.enabled = YES;
            
            //没有取到原图,保存流里取的图片
            if (sourceImage) {
                [Utility saveImage:sourceImage toPhotoAlbumComplete:nil];
            }else{
                [Utility saveImage:[VisonWifiBaseLibraryUtility scaleImageWithOriginalImage:streamImage targetSize:[VisonWifiBaseLibraryUtility getTheTakePhotoSize]] toPhotoAlbumComplete:nil];
            }
            
        }];
    }
}

#pragma mark - 遥控器拍照
- (void)commandTakePhoto:(NSNotification *)notify{
    unsigned  long long curMS = [Utility getMS];
    if (curMS - lastTakePhotoMS >= 1000){
        lastTakePhotoMS = curMS;
        [self takePhoto];
        NSLog(@"遥控器拍照动作");
    }
}

#ifdef OPENCV_ENABLE

#pragma mark - <StreamControlManagerDelegate>
#pragma mark - 用于手势识别线程的YUV数据回调
-(BOOL)gestureDetectWithBuf:(char *)yuv bufW:(int)bufW bufH:(int)bufH bufSize:(float)bufSize{
    
    int ret = detect_yuv(yuv, bufW, bufH, bufSize);
    
    switch (ret){
        case 0:
        {
            if(self.movieWriter.isWriting){
                NSLog(@"手势录像 - 结束");
            }else{
                NSLog(@"手势录像 - 开始");
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
                SystemSoundID soundID;
                NSString *path = [[NSBundle mainBundle] pathForResource:@"video_rec" ofType:@"mp3"];
                AudioServicesCreateSystemSoundID((__bridge CFURLRef)[NSURL fileURLWithPath:path], &soundID);
                AudioServicesPlaySystemSound(soundID);
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
                [self record];
            });
            return YES;
        }
            break;
        case 1:
        {
            if (self.movieWriter.isWriting) return NO;//录像时阻止拍照识别
            dispatch_async(dispatch_get_main_queue(), ^{
                HHCountdowLabel *countLabel = [[HHCountdowLabel alloc] initWithFrame:CGRectMake(0, 0, MAX_LEN, MIN_LEN)];
                countLabel.center = self.view.center;
                countLabel.font = [UIFont boldSystemFontOfSize:100];
                countLabel.textColor = [UIColor whiteColor];
                countLabel.count = 3; //不设置的话，默认是3
                [self.view addSubview:countLabel];
                [countLabel startCount];
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self takePhoto];
                    NSLog(@"手势拍照");
                });
            });
            
            return YES;
        }
            break;
        default:
            return NO;
            break;
    }
    
}

#pragma mark - 用于图像跟随的buff数据回调
- (void)imageFollowDectecWithBuf:(CVPixelBufferRef)pBuf bufCount:(int)bufCount
{
    if (self.isFollow)
    {
        static int coutn = 0;
        coutn++;
        
        int width;
        int height;
        char *yuvData = NULL;
        
        [self getByteWithCVPixelBuffer:pBuf byte:&yuvData width:&width height:&height];
        
        
        if (bufCount > 3)
        {
            if(coutn%2 == 0)
            {
                [self trackWithYdata:yuvData width:width height:height];
            }
        }else
        {
            [self trackWithYdata:yuvData width:width height:height];
        }
        
        
        free(yuvData);
        
    }else
    {
        [self cleanTrack];
    }
    
}
- (void)trackWithYdata:(void *)Ydata width:(int)width height:(int)height
{
    

    int lost = setYUVData(Ydata, width, height, NULL, outRect, outPeopleRect, _isFollow, followRect);
    if (outRect[0] != 0 && outRect[1] != 0 && outRect[3] != 0 && outRect[2] != 0)
    {
        
    }
    if (lost == 1)
    {
        self.isFollow = NO;
        readyTrack = NO;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [SVProgressHUD showInfoWithStatus:@"丢失，请重新选择目标"];
            [SVProgressHUD dismissWithDelay:1];
            
            
            trackRect = CGRectZero;
            [trackView removeFromSuperview],trackView = nil;
            [trackfillView removeFromSuperview],trackfillView = nil;
            
            //丢失时左右置为64
            [self.mControllView.rightRocker setDragCircleXandY:64 DragCircleY:64];
            [self.mControllView.leftRocker setDragCircleXandY:64 DragCircleY:64];
            [self.mControllView.rotateAdjustView setTrimValue:16];
            [self cleanTrack];
            
            
            self.mControllView.openFollowBtn.selected = NO;
            self.mControllView.startFollowBtn.selected = NO;
            self.mControllView.gestureBtn.hidden = NO;
            self.mControllView.startFollowBtn.hidden = YES;
            self.mControllView.startFollowBtn.enabled = NO;
            self.mControllView.leftRocker.hidden = NO;
            self.mControllView.leftRocker.hidden = NO;
            self.mControllView.leftRockerImageView.hidden = NO;
            self.mControllView.rightRockerImageView.hidden = NO;
            //            mControllView.m360Btn.hidden = NO;
            borderV.hidden = YES;
            
            [self.view removeGestureRecognizer:pan];
            
        });
        
        return;
        
    }
    
    static float scale_factor = 0;
    int distanceResult = 64;
    int powerfulResult = 64;
    int rotateTrimResult = 16;
    
    
    if (readyTrack)//no go before
    {
        [TrackCalculation trackCalculationWithTargetScaleFactor:scale_factor
                                                 NowScaleFactor:_get_scale_factor()
                                             NowRectWidthCenter:outRect[0]  + outRect[2] / 2
                                            NowRectHeightCenter:outRect[1]  + outRect[3] / 2
                                                 distanceResult:&distanceResult
                                                 powerfulResult:&powerfulResult
                                               rotateTrimResult:&rotateTrimResult];

        
    }else{
        scale_factor =  _get_scale_factor();
    }
    
    /*
     
     dispatch_async(dispatch_get_main_queue(), ^{
     [mControllView.mRightDragCircle setDragCircleXandY:64 DragCircleY:distanceResult];//左右
     [mControllView.mLeftDragCircle setDragCircleXandY:64 DragCircleY:powerfulResult];//上下
     [mControllView.mLeftTrim setTrimValue:rotateTrimResult];//旋转
     //        NSLog(@"左右：%i ------------  \n上下： %i\n --------------旋转：%i----------------",distanceResult,powerfulResult,rotateTrimResult);
     });
     
     */
    
    //    NSLog(@"前后。%d 上下。%d   旋转。 %d   wifitype  %@",distanceResult,powerfulResult,rotateTrimResult,wifitype);
    int Width = [[WIFIDeviceModel shareInstance] getPixelSize:[WIFIDeviceModel shareInstance].mainLensTransmissionPixelSize].width,Height = [[WIFIDeviceModel shareInstance] getPixelSize:[WIFIDeviceModel shareInstance].mainLensTransmissionPixelSize].height;
    if (Width > 1280) {
        Width = 1280;
        Height = 720;
    }
    
    
    CGFloat widthScaleBy = SCREEN_WIDTH / Width;
    CGFloat heightScaleBy = SCREEN_HEIGHT / Height;
    
    CGRect rect = CGRectMake(outRect[0], outRect[1], outRect[2], outRect[3]);
    CGRect commonRect;
    commonRect = rect;
    
    rect.size.width *= widthScaleBy;
    rect.size.height *= heightScaleBy;
    rect.origin.x *= widthScaleBy;
    rect.origin.y *= heightScaleBy;
    
    if (_isFollow) {
        dispatch_async(dispatch_get_main_queue(), ^{
            trackView.frame = rect;
            trackfillView.frame = rect;
        });
    }
    
}
float kalmanFilter_A(float inData)
{
    static float p=10, q=0.0001, r=0.005, kGain=0, prevData = 0;
    p = p+q;
    kGain = p/(p+r);
    
    inData = prevData+(kGain*(inData-prevData));
    p = (1-kGain)*p;
    
    prevData = inData;
    
    return inData;
}
- (int)calculateFaceHorizontalPostionInScreen:(float)centerX
{
    float w = MAX_LEN;
    float h = MIN_LEN;
    
    int horLev = -100;
    
    CGRect lvlRect_0 = CGRectMake(0, 0, w/7.0f, h);
    CGRect lvlRect_1 = CGRectMake(w/7.0f, 0, w/7.0f, h);
    CGRect lvlRect_2 = CGRectMake(w/7.0f*2, 0, w/7.0f, h);
    CGRect lvlRect_3 = CGRectMake(w/7.0f*3, 0, w/7.0f, h);
    CGRect lvlRect_4 = CGRectMake(w/7.0f*4, 0, w/7.0f, h);
    CGRect lvlRect_5 = CGRectMake(w/7.0f*5, 0, w/7.0f, h);
    CGRect lvlRect_6 = CGRectMake(w/7.0f*6, 0, w/7.0f, h);
    
    if (centerX <= lvlRect_0.origin.x + lvlRect_0.size.width)
    {
        horLev = 1;
    }else if (centerX <= lvlRect_1.origin.x + lvlRect_1.size.width && centerX > lvlRect_0.origin.x + lvlRect_0.size.width)
    {
        horLev = 2;
    }else if (centerX <= lvlRect_2.origin.x + lvlRect_2.size.width && centerX > lvlRect_1.origin.x + lvlRect_1.size.width)
    {
        horLev = 3;
    }else if (centerX <= lvlRect_3.origin.x + lvlRect_3.size.width && centerX > lvlRect_2.origin.x + lvlRect_2.size.width)
    {
        horLev = 4;
    }else if (centerX <= lvlRect_4.origin.x + lvlRect_4.size.width && centerX > lvlRect_3.origin.x + lvlRect_3.size.width)
    {
        horLev = 5;
    }else if (centerX <= lvlRect_5.origin.x + lvlRect_5.size.width && centerX > lvlRect_4.origin.x + lvlRect_4.size.width)
    {
        horLev = 6;
    }else if (centerX <= lvlRect_6.origin.x + lvlRect_6.size.width && centerX > lvlRect_5.origin.x + lvlRect_5.size.width)
    {
        horLev = 7;
    }
    
    return horLev;
}

- (int)calculateFaceVerticalPostionInScreen:(float)centerY
{
    float w = MAX_LEN;
    float h = MIN_LEN;
    int verLev = -100;
    
#if 1
    CGRect lvlRect_0 = CGRectMake(0, 0, w, h/7.0f);
    CGRect lvlRect_1 = CGRectMake(0, h/7.0f, w, h/7.0f);
    CGRect lvlRect_2 = CGRectMake(0, h/7.0f*2, w, h/7.0f);
    CGRect lvlRect_3 = CGRectMake(0, h/7.0f*3, w, h/7.0f);
    CGRect lvlRect_4 = CGRectMake(0, h/7.0f*4, w, h/7.0f);
    CGRect lvlRect_5 = CGRectMake(0, h/7.0f*5, w, h/7.0f);
    CGRect lvlRect_6 = CGRectMake(0, h/7.0f*6, w, h/7.0f);
    
    
    if (centerY <= lvlRect_0.origin.y + lvlRect_0.size.height)
    {
        verLev = 1;
    }else if (centerY <= lvlRect_1.origin.y + lvlRect_1.size.height && centerY > lvlRect_0.origin.y + lvlRect_0.size.height)
    {
        verLev = 2;
    }else if (centerY <= lvlRect_2.origin.y + lvlRect_2.size.height && centerY > lvlRect_1.origin.y + lvlRect_1.size.height)
    {
        verLev = 3;
    }else if (centerY <= lvlRect_3.origin.y + lvlRect_3.size.height && centerY > lvlRect_2.origin.y + lvlRect_2.size.height)
    {
        verLev = 4;
    }else if (centerY < lvlRect_4.origin.y + lvlRect_4.size.height && centerY > lvlRect_3.origin.y + lvlRect_3.size.height)
    {
        verLev = 5;
    }else if (centerY <= lvlRect_5.origin.y + lvlRect_5.size.height && centerY > lvlRect_4.origin.y + lvlRect_4.size.height)
    {
        verLev = 6;
    }else if (centerY <= lvlRect_6.origin.y + lvlRect_6.size.height && centerY > lvlRect_5.origin.y + lvlRect_5.size.height)
    {
        verLev = 7;
    }
#else
    CGRect lvlRect_0 = CGRectMake(0, 0, w, h/3.0f);
    CGRect lvlRect_1 = CGRectMake(0, h/3.0f, w, h/3.0f);
    CGRect lvlRect_2 = CGRectMake(0, h/3.0f*2, w, h/3.0f);
    
    
    if (centerY <= lvlRect_0.origin.y + lvlRect_0.size.height)
    {
        verLev = -1;
    }else if (centerY <= lvlRect_1.origin.y + lvlRect_1.size.height && centerY > lvlRect_0.origin.y + lvlRect_0.size.height)
    {
        verLev = 0;
    }else if (centerY <= lvlRect_2.origin.y + lvlRect_2.size.height && centerY > lvlRect_1.origin.y + lvlRect_1.size.height)
    {
        verLev = 1;
    }
#endif
    return verLev;
}


- (void)getByteWithCVPixelBuffer:(CVPixelBufferRef)pixelBuf byte:(char **)yuvData width:(int *)width height:(int *)height
{
    unsigned char *YuvData = NULL;
    
    CVPixelBufferLockBaseAddress(pixelBuf, 0);
    if (CVPixelBufferIsPlanar(pixelBuf))
    {
        size_t w = CVPixelBufferGetWidth(pixelBuf);
        size_t h = CVPixelBufferGetHeight(pixelBuf);
        
        *width = w;
        *height = h;
        
        YuvData =(unsigned char *) malloc((*width)*(*height)*3/2);
        
        size_t d = CVPixelBufferGetBytesPerRowOfPlane(pixelBuf, 0);
        unsigned char* Ydata = (unsigned char*) CVPixelBufferGetBaseAddressOfPlane(pixelBuf, 0);
        unsigned char* dst = YuvData;
        
        for (unsigned int rIdx = 0; rIdx < h; ++rIdx, dst += w, Ydata += d) {
            memcpy(dst, Ydata, w);
        }
        
        d = CVPixelBufferGetBytesPerRowOfPlane(pixelBuf, 1);
        unsigned char *UVdata = (unsigned char *) CVPixelBufferGetBaseAddressOfPlane(pixelBuf, 1);
        
        h = h >> 1;
        for (unsigned int rIdx = 0; rIdx < h; ++rIdx, dst += w, UVdata += d) {
            memcpy(dst, UVdata, w);
        }
        
        *yuvData = (char *)YuvData;
    }
    CVPixelBufferUnlockBaseAddress(pixelBuf, 0);
}
//跟随
-(void)pan:(UIPanGestureRecognizer*)pan{
    
    if (!IS_STARTRACK) {
        return;
    }
    if(pan.state == UIGestureRecognizerStateBegan)
    {
        if (trackView) {
            [self cleartrackview];
        }
        startP = [pan locationInView:self.view];
        
        if (!CGRectContainsPoint(borderV.frame, startP)) {
            return;
        }
        
        
        //获得点击的起点
        //        if (!trackView) {
        trackView = [[UIView alloc] initWithFrame:CGRectMake(startP.x, startP.y, 1, 1)];
        trackView.backgroundColor = [UIColor clearColor];
        trackView.layer.borderColor = [UIColor greenColor].CGColor;
        trackView.layer.borderWidth = 3;
        [self.mControllView addSubview:trackView];
        //        }
        trackfillView = [[UIView alloc] initWithFrame:CGRectMake(startP.x, startP.y, 1, 1)];
        trackfillView.backgroundColor = [UIColor greenColor];
        trackfillView.alpha = 0.3;
        [self.mControllView addSubview:trackfillView];
        [self.mControllView sendSubviewToBack:trackView];
        [self.mControllView sendSubviewToBack:trackfillView];
    }
    else if(pan.state == UIGestureRecognizerStateChanged)
    {
        //求偏移量
        CGPoint curP = [pan locationInView:self.view];
        
        CGFloat offsetX = curP.x - startP.x;
        
        CGFloat offsetY = curP.y - startP.y;
        
        if (!startP.x || !CGRectContainsPoint(borderV.frame, curP)) {
            return;
        }
        
        trackRect = CGRectMake(startP.x,startP.y, abs(offsetX), abs(offsetY));
        if (offsetX<0.0) {
            trackRect = CGRectMake(startP.x + offsetX,startP.y, abs(offsetX), abs(offsetY));
        }
        if (offsetY<0.0) {
            trackRect = CGRectMake(startP.x,startP.y +offsetY, abs(offsetX), abs(offsetY));
        }
        if (offsetX<0.0 && offsetY<0.0) {
            trackRect = CGRectMake(startP.x + offsetX ,startP.y +offsetY, abs(offsetX), abs(offsetY));
        }
        
        
        
        //修改好frame值就好了
        //  trackRect = CGRectMake(x,y, offsetX, offsetY);
        trackView.frame = trackRect;
        trackfillView.frame = trackRect;
        //  CLS_LOG(@"trackRect x %f y %f width %f  height %f  offsetX %f  offsety %f",startP.x,startP.y,abs(offsetX),abs(offsetY),offsetX,offsetY);
    }
    else if(pan.state == UIGestureRecognizerStateEnded)
    {
        //       deletVbtn.hidden = sw.hidden = NO;
        //  mControllView.mtrackdeletebtn.hidden = mControllView.mtrackfollowbtn.hidden =NO;
        //  if (trackRect.size.width < 10 || trackRect.size.height <= 10) {
        if (trackRect.size.width/trackRect.size.height <= 0.25 || trackRect.size.height/trackRect.size.width <= 0.25) {
            trackRect = CGRectZero;
            [[[UIAlertView alloc] initWithTitle:TranslationString(@"Following box width or height to 0, please reselect the box") message:nil delegate:nil cancelButtonTitle:TranslationString(@"OK") otherButtonTitles:nil]
             show];
            self.mControllView.startFollowBtn.enabled = NO;
            return;
        }
        if(CGRectContainsPoint(borderV.frame, startP)){
            self.mControllView.startFollowBtn.enabled = YES;
            self.mControllView.startFollowBtn.selected = NO;
        }else{
            self.mControllView.startFollowBtn.enabled = NO;
        }
        
        orgRect = trackRect;
        int Width = [[WIFIDeviceModel shareInstance] getPixelSize:[WIFIDeviceModel shareInstance].mainLensTransmissionPixelSize].width,Height = [[WIFIDeviceModel shareInstance] getPixelSize:[WIFIDeviceModel shareInstance].mainLensTransmissionPixelSize].height;
        if (Width > 1280) {
            Width = 1280;
            Height = 720;
        }
        
        
        CGFloat widthScaleBy = SCREEN_WIDTH / Width;
        CGFloat heightScaleBy = SCREEN_HEIGHT / Height;
        trackRect.size.width /= widthScaleBy;
        trackRect.size.height /= heightScaleBy;
        trackRect.origin.x /= widthScaleBy;
        trackRect.origin.y /= heightScaleBy;
        
        int out_x;
        int out_y;
        int out_height;
        int out_width;
        
        outRect[0] = 0;
        outRect[1] = 0;
        outRect[2] = 0;
        outRect[3] = 0;
        
        
        outPeopleRect[0] = 0;
        outPeopleRect[1] = 0;
        outPeopleRect[2] = 0;
        outPeopleRect[3] = 0;
        
        
        followRect[0] = (int)trackRect.origin.x;
        followRect[1] = (int)trackRect.origin.y;
        followRect[2] = (int)trackRect.size.width;
        followRect[3] = (int)trackRect.size.height;
    
        _isFollow = YES;
    }
}

- (void)cleartrackview
{
    readyTrack = NO;
    _isFollow = NO;
    
    trackRect = CGRectZero;
    [trackView removeFromSuperview],trackView = nil;
    [trackfillView removeFromSuperview],trackfillView = nil;
    
    [self cleanTrack];

}
- (void)cleanTrack
{
    setYUVData(NULL, 0, 0, NULL, outRect, outPeopleRect, 0, followRect);
    // isLeftControl = NO;
}

- (void)trackviewYellowhidden:(BOOL)hidden
{
#define     kHflyRatio1 SCREEN_MAX_LENGTH/667
#define     kWflyRatio1 SCREEN_MIN_LENGTH/375
    
    if (!borderV) {
        borderV = [[UIView alloc]init];
        borderV.backgroundColor = [UIColor clearColor];
        
        borderV.layer.borderColor = [UIColor yellowColor].CGColor;
        borderV.layer.borderWidth = 1;
        [self.mControllView addSubview:borderV];
        [self.view insertSubview:borderV belowSubview:self.mControllView];
        
        [borderV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.mas_equalTo(self.mControllView);
            make.height.width.mas_equalTo(LAYOUT_WIDTH(300));
        }];   
    }
    borderV.hidden =!hidden;
}

#endif



#pragma mark ----------- 在这里设置发送飞控的数据 -----------
-(void)sendFlightAction{
    
    unsigned char planeData[9];
    bzero(planeData, 9);
    
    int leftRightValue = [self.mControllView.rightRocker GetPowerFB]*2; //左右;
    int forwordBackValue = 0xff - [self.mControllView.rightRocker GetPowerLR]*2; //前后
    int powerValue = 0xff - [self.mControllView.leftRocker GetPowerLR]*2; //油门
    int rotateValue = [self.mControllView.leftRocker GetPowerFB]*2; //旋转
    
    
    leftRightValue = 128 + (leftRightValue - 128) * (self.speedValue == 0 ? 0.3 : (self.speedValue == 1 ? 0.6 : 1));
    forwordBackValue = 128 + (forwordBackValue - 128) * (self.speedValue == 0 ? 0.3 : (self.speedValue == 1 ? 0.6 : 1));
    rotateValue = 128 + (rotateValue - 128) * (self.speedValue == 0 ? 0.3 : (self.speedValue == 1 ? 0.6 : 1));
    
    //微调
    int adjustRotateValue = [self.mControllView.rotateAdjustView getValue];
    int adjustForwordBack = [self.mControllView.fontdOrBackAdjustView getValue];
    int adjustLeftright = [self.mControllView.leftOrRightAdjustView getValue];
    rotateValue = rotateValue + (adjustRotateValue - 16) * 2;
    forwordBackValue = forwordBackValue + (adjustForwordBack - 16) * 2;
    leftRightValue = leftRightValue + (adjustLeftright - 16) * 2;
    
    forwordBackValue = forwordBackValue     == 127 ? 128 : forwordBackValue;
    powerValue       = powerValue           == 127 ? 128 : powerValue;
    leftRightValue   = leftRightValue       == 127 ? 128 : leftRightValue;
    rotateValue      = rotateValue          == 127 ? 128 : rotateValue;
    
    planeData[0] = 0x66;
    planeData[1] = leftRightValue > 255 ? 255 : (leftRightValue < 0 ? 0 : leftRightValue);
    planeData[2] = forwordBackValue > 255 ? 255 : (forwordBackValue < 0 ? 0 : forwordBackValue);
    planeData[3] = powerValue;
    planeData[4] = rotateValue > 255 ? 255 : (rotateValue < 0 ? 0 : rotateValue);
    
    static BOOL found = NO;
    if ((planeData[2] >= 158 || planeData[2] <= 98 || planeData[1] >= 158 || planeData[1] <= 98 ) && (self.mControllView.flipBtn.selected))
    {
        found = YES;
    }

    if (found)
    {
        //ICON正常显示
        self.mControllView.flipBtn.selected = NO;

        self.flipValue = 1;
        if(timeCount1 < 10)
        {
            timeCount1++;
        }
        NSLog(@"timeCount1 = %d",timeCount1);

        if (planeData[2] >= 158) {
            planeData[2] = 0xff;
        }
        if (planeData[2] <= 98) {
            planeData[2] = 0x01;
        }
        if (planeData[1] >= 158) {
            planeData[1] = 0xff;
        }
        if (planeData[1] <= 98) {
            planeData[1] = 0x01;
        }

        if(timeCount1 >= 10)
        {

            self.flipValue = 0;
            timeCount1 = 0;

            found = NO;
        }
    }
    else
    {
        timeCount1 = 0;
        self.flipValue = 0;
    }
    
    planeData[5] = planeData[5] | (self.oneKeyFlyValue); //一键起飞
    planeData[5] = planeData[5] | (self.oneKeyDropValue << 1); //一键下降
    planeData[5] = planeData[5] | (self.emergencyStopValue << 2); //一键急停
    planeData[5] = planeData[5] | (self.flipValue << 3); //一键翻滚
    planeData[5] = planeData[5] | (self.noHeadValue << 4); //无头
    planeData[5] = planeData[5] | (0 << 5); //一键返航
    planeData[5] = planeData[5] | (1 << 6); //定高
    planeData[5] = planeData[5] | (self.calibrationValue << 7); //一键校准
    planeData[6] = planeData[6] | ((self.mControllView.circleBtn.selected?1:0)); //一键环绕
    int roat = self.mControllView.rotateBtn.selected?1:0;
    planeData[6] = planeData[6] | (roat << 1); //一键旋转
    
    planeData[7] = (planeData[1]^planeData[2]^planeData[3]^planeData[4]^planeData[5]^planeData[6])&0xff;
    planeData[8] = 0x99;
    
    NSData *aData = [NSData dataWithBytes:planeData length:9];

    if (self.mControllView.turnOnOrOffBtn.selected) {
        [[SendCommandManager shareInstance]sendUdpCommand:aData tag:1];
    }
    
    NSLog(@"左右=%d,前后=%d,油门=%d,旋转=%d ",planeData[1],planeData[2],planeData[3],planeData[4]);
    
}

#pragma mark ----------- 在这里布局子视图 -----------
-(void)setupSubviews{
    
}


#pragma mark - PopViewController
-(void)popViewController{
    
    self->isPopViewController = YES;
    
    if (self.movieWriter.isWriting) {
        [self.movieWriter stopWriteMovieComplete:^(NSString *moviePath) {
            //创建影片缩略图，保存沙盒
            [Utility saveMoviewToPhotoAlbum:moviePath complete:^{
                [self.mControllView.recordBtn setImage:[UIImage imageNamed:@"拍照"] forState:UIControlStateSelected];
                [self.mControllView setStatueWithEndRecord];
#pragma mark - 混合音乐
                if (self.is_mixMusic) {
                    [Utility video:moviePath mixMusic:self.aacMusicName complateBlock:^{
                        self.is_mixMusic = NO;
                    }];
                }
            }];
        }];
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    if (self.sendFlightDataTimer) {
        [self.sendFlightDataTimer invalidate];
        self.sendFlightDataTimer = nil;
    }
    
    [self.mControllView.motionManager stopDeviceMotionUpdates];
    self.mControllView.motionManager = nil;
    
    [self.mControllView trackCancelled];
    
    if (self.mControllView) {
        [self.mControllView removeFromSuperview];
        self.mControllView = nil;
    }
    
    [self clean3DShow];
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 打开媒体
-(void)openMedia{
    [self.navigationController pushViewController:[[JJMediaCenterViewController alloc] init] animated:YES];
}

#pragma mark - VR
-(void)setVREnable{
    self.playerView.VREnable = !self.playerView.VREnable;
}

-(void)clean3DShow{
    self.playerView.VREnable = NO;
}

#pragma mark - 镜头反转
-(void)lensTurnAction{
    
    [[SocketControlManager shareInstance] sendUdpCommand:[Device_Wifi_Config_Command getFlipLensData] tag:10001];
    
    //修复反转镜头红屏的问题
    UIImage * image = [self.playerView snapShot];
    self.showView.image = image;
    self.showView.alpha = 1;
    self.playerView.alpha = 0;
}

#pragma mark - 切换主副（光流镜头）
-(void)switchLensAction{
    if (!self.playing) {
        return;
    }
    
    if ([WIFIDeviceModel shareInstance].isNewVersion) {
        
        if ([WIFIDeviceModel shareInstance].isDoubleCamera == 1) {
            
            UserInfoSingle *user = [Utility getUserInfo];
            if (user.lensType == LensType_Main) {
                [[SocketControlManager  shareInstance] sendUdpCommand:[Device_Wifi_Config_Command getAuxiliaryLensCommand] tag:10000];
            }else{
                [[SocketControlManager  shareInstance] sendUdpCommand:[Device_Wifi_Config_Command getChangeMainLensCommand] tag:10000];
            }
            return;
            
        }else if ([WIFIDeviceModel shareInstance].isDoubleCamera == 2){
            
            UIImage * image = [self.playerView snapShot];
            self.showView.image = image;
            
            self.showView.alpha = 1;
            self.playerView.alpha = 0;
            
            UserInfoSingle *user = [Utility getUserInfo];
            if (user.lensType == LensType_Main) {
                user.lensType = LensType_Vice;
            }else{
                user.lensType = LensType_Main;
            }
            [Utility saveUserInfo:user];
            
            [self.streamManager stopReceiveStreamComplete:nil];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.streamManager startReceiveStreamcComplete:nil];
            });
        }
        
    }else{
        UIImage * image = [self.playerView snapShot];
        self.showView.image = image;
        
        self.showView.alpha = 1;
        self.playerView.alpha = 0;
        
        UserInfoSingle *user = [Utility getUserInfo];
        if (user.lensType == LensType_Main) {
            user.lensType = LensType_Vice;
        }else{
            user.lensType = LensType_Main;
        }
        [Utility saveUserInfo:user];
        
        [self.streamManager stopReceiveStreamComplete:nil];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.streamManager startReceiveStreamcComplete:nil];
        });
    }
}



#pragma mark - 接收到飞机数据
-(void)receiveHyPlaneData:(NSNotification *)notify{
//    @synchronized (self)
//       {
//            NSMutableData *data = [notify.object mutableCopy];
//            Byte *bytes = (Byte *)data.bytes;
//         
//            if (data.length==10 ) {
//                 
//                 int  planeV = *(int *)&bytes[1];
//                 if(_lastplaneV==0){
//                      _lastplaneV = planeV;
//                 }
//                 if(planeV<=_lastplaneV){
//                     
//                     CGFloat x = planeV/10.0;
//                     {
//                         if (x <=4.5&&x>0) {
//                             if (x >3.7) {
//                                 self.mControllView.batteryImageView.image = [UIImage imageNamed:@"图层14"];
//                             }else if (x >3.6) {
//                                 self.mControllView.batteryImageView.image = [UIImage imageNamed:@"图层14"];
//                             }else if (x >3.5) {
//                                 self.mControllView.batteryImageView.image = [UIImage imageNamed:@"图层14"];
//                             }else if (x >3.3) {
//                                 self.mControllView.batteryImageView.image = [UIImage imageNamed:@"图层14"];
//                             }else if (x>3.2) {
//                                 self.mControllView.batteryImageView.image = [UIImage imageNamed:@"图层14"];
//                             }else  if (x<3.2){
//                                 self.mControllView.batteryImageView.image = [UIImage imageNamed:@"图层14"];
//                             }
//                         }
//                     }
//                     
//                      _lastplaneV = planeV;
//
//                 }
//                 
//                 if (bytes[2] >>0& 0x01) {//拍照脉冲
//                      self.photoMaichong=1;
//                 }else{
//                      if (self.photoMaichong==1) {
//                          [self takePhoto];
//                      }
//                      self.photoMaichong=0;
//                 }
//                 
//                 if (bytes[2] >> 1 & 0x01) {//录像脉冲
//                      self.videoMaichong=1;
//                 }else{
//                      if (self.videoMaichong==1) {
//                          [self record];
//                      }
//                      self.videoMaichong=0;
//                 }
//            }
//       }
}

-(void)dealloc{
    NSLog(@"dealloc - %@",self);
}

@end
