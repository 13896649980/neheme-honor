//
//  VisonPlayViewController.m
//  GPS Project
//
//  Created by Xu pan on 2020/8/17.
//  Copyright © 2020 Xu pan. All rights reserved.
//

#import "VisonPlayViewController.h"
#import <CoreTelephony/CTCallCenter.h>
#import <CoreTelephony/CTCall.h>
#import "JJFile.h"
#import "JJMediaCenterViewController.h"
#import "JJMediaManager.h"
#import "UIImage+KTCategory.h"
#import "AppDelegate.h"

@interface VisonPlayViewController (){
    
}

@property (nonatomic,strong)        UIImage                     *lastImage;
@property (nonatomic,strong)        CTCallCenter                *callCenter;
@end

@implementation VisonPlayViewController

#pragma mark - lazyLoad
-(StreamControlManager *)streamManager{
    if (!_streamManager) {
        _streamManager = [[StreamControlManager alloc]initWithGestureEnable:YES imageFollowEnable:NO];
        _streamManager.delegate = self;
    }
    return _streamManager;
}

-(MovieWriter *)movieWriter{
    if (!_movieWriter) {
        _movieWriter = [[MovieWriter alloc]init];
    }
    return _movieWriter;
}

#pragma mark - lifeCycle
- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"viewDidLoad - %@",self);
    self.view.backgroundColor = [UIColor blackColor];
    [VisonPlayViewController attemptRotationToDeviceOrientation];
    [self interfaceOrientation:UIInterfaceOrientationLandscapeLeft];
    //背景图
    self.showView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@""]];
    self.showView.userInteractionEnabled = YES;
    [self.view addSubview:self.showView];
    [self.showView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.top.equalTo(self.view.mas_top);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    //电话事件
    __weak typeof(self)weakSelf = self;
    self.callCenter = [[CTCallCenter alloc] init];
    self.callCenter.callEventHandler = ^(CTCall *call){
        if ([call.callState isEqualToString:CTCallStateIncoming]) {
            [weakSelf.streamManager stopReceiveStreamComplete:nil];
        }else if ([call.callState isEqualToString:CTCallStateDisconnected]){
                //连接上设备
            if ([VisonWifiBaseLibraryMethods isConnectedVisonWifiDevice]) {
                //开始接收视频流
                [weakSelf.streamManager startReceiveStreamcComplete:nil];
            }
        }
    };
    //播放视图
    self.playerView = [[GPUImagePlayView alloc]init];
    [self.view insertSubview:self.playerView belowSubview:self.showView];
    [self.playerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(readyToPlay) name:STREAM_NOTI_READYTOPLAY object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(WIFIDisConnected) name:@"WIFIDISCONNECT" object:nil];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSLog(@"viewWillAppear - %@",self);
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    NSLog(@"viewDidAppear - %@",self);
    //连接上设备
    if ([VisonWifiBaseLibraryMethods isConnectedVisonWifiDevice]) {
        //开始接收视频流
        [self.streamManager startReceiveStreamcComplete:nil];
    }
    
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];//不让锁屏
    
    if (self.lastImage) {
        self.showView.image = self.lastImage;
        self.showView.alpha = 0;
    }
    
#ifdef OPENCV_ENABLE
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    if (app.isLoad) return;
    [app loadtrack];
#endif
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    NSLog(@"viewDidDisappear - %@",self);
    //跳转新界面
    if (!isPopViewController){
        UIImage *image = self.streamManager.isReceiving?[self.playerView snapShot]:nil;
        self.lastImage = image;
    }
    
    if (self.lastImage) {
        self.showView.image = self.lastImage;
        self.showView.alpha = 1;
    }
    //停止接收视频流
    [self.streamManager stopReceiveStreamComplete:nil];
    self.playing = NO;
}

-(void)dealloc{
    self.streamManager.delegate = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"dealloc - %@",self);
}

#pragma mark - WIFI断开通知
-(void)WIFIDisConnected{
    UIImage * image = self.streamManager.isReceiving?[self.playerView snapShot]:nil;
    if (self.movieWriter.isWriting) {
        //要停止录像
        [self.movieWriter stopWriteMovieComplete:^(NSString *moviePath) {
            //创建影片缩略图，保存沙盒
            [Utility saveMoviewToPhotoAlbum:moviePath complete:nil];
        }];
    }
    //停止接收流
    [self.streamManager stopReceiveStreamComplete:nil];
    self.playing = NO;
    if (image) {
        self.showView.image = image;
        self.showView.alpha = 1;
    }
}

#pragma mark - SocketControlManagerDelegate
-(void)readyToPlay{
    //换渲染方式
    [self.playerView setImageInput];
    //开始接收视频流
    [self.streamManager startReceiveStreamcComplete:nil];
    
#ifdef OPENCV_ENABLE
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    if (app.isLoad) return;
    [app loadtrack];
#endif
}

#pragma mark - StreamControlManagerDelegate
//解码YUV CVPixel回调
-(void)processCVPixelBuffer:(CVPixelBufferRef)pixelBuf
                  frameTime:(CMTime)frameTime
                 frameWidth:(int)frameWidth
                frameHeight:(int)frameHeight{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.showView.alpha){
            NSLog(@"出图，开始播放...");
            [UIView animateWithDuration:1 animations:^{
                self.showView.alpha = 0;
            }];
        }
    });
    self.playing = YES;
    [self.playerView processCVPixelBuffer:pixelBuf frameTime:frameTime];
    self.movieWriter.writingPixelBuff = [Utility YUVBufferCopyWithPixelBuffer:pixelBuf];
}
//解码UIImage回调
-(void)processImage:(UIImage *)image
          frameTime:(CMTime)frameTime
         frameWidth:(int)frameWidth
        frameHeight:(int)frameHeight{
    
    if (self.showView.alpha){
        NSLog(@"出图，开始播放...");
        [UIView animateWithDuration:1 animations:^{
            self.showView.alpha = 0;
        }];
    }
    self.playing = YES;
    [self.playerView processImage:image frameTime:frameTime];
    self.movieWriter.writingPixelBuff = [Utility pixelBufferFromCGImage:image.CGImage];
}


@end
