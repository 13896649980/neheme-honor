//
//  VisonPlayViewController.h
//  GPS Project
//
//  Created by Xu pan on 2020/8/17.
//  Copyright © 2020 Xu pan. All rights reserved.
//

#import "BaseViewController.h"
#import "GPUImagePlayView.h"
#import "MovieWriter.h"



NS_ASSUME_NONNULL_BEGIN

@interface VisonPlayViewController : BaseViewController<StreamControlManagerDelegate>{
    BOOL isPopViewController;
}

@property (nonatomic,strong)        StreamControlManager        *streamManager;
@property (nonatomic,assign)        BOOL                        playing;
@property (nonatomic,strong)        GPUImagePlayView            *playerView;
@property (nonatomic,strong)        MovieWriter                 *movieWriter;
@property (strong, nonatomic)       UIImageView                 *showView;

@end

NS_ASSUME_NONNULL_END
