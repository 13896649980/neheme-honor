//
//  GuideViewController.m
//  NonGPS Project
//
//  Created by Xu pan on 2019/8/14.
//  Copyright © 2019 Xu pan. All rights reserved.
//

#import "GuideViewController.h"

@interface GuideViewController ()

@end

@implementation GuideViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark -------- 在这里设置横竖屏 --------
-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    //    return UIInterfaceOrientationMaskPortrait; //仅仅竖屏
    return UIInterfaceOrientationMaskLandscapeLeft; //仅仅横屏
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [GuideViewController attemptRotationToDeviceOrientation];
    //    [self interfaceOrientation:UIInterfaceOrientationPortrait]; //进来竖屏
    [self interfaceOrientation:UIInterfaceOrientationLandscapeLeft]; //进来横屏
}
-(BOOL)shouldAutorotate{
    return YES;
}

@end
