//
//  PlayViewController.h
//  NonGPS Project
//
//  Created by Xu pan on 2019/8/14.
//  Copyright © 2019 Xu pan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ControllerView.h"
#import "VisonPlayViewController.h"

@interface PlayViewController : VisonPlayViewController

@property   (nonatomic,strong)          ControllerView          *mControllView;

@end


