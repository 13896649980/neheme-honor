//
//  AppDelegate.h
//  NonGPS Project
//
//  Created by Xu pan on 2019/8/14.
//  Copyright © 2019 Xu pan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


#ifdef  OPENCV_ENABLE

@property (nonatomic,assign,readonly) BOOL isLoad;//图像识别是否加载
@property (nonatomic,assign,readonly) BOOL isLoading;
- (void)loadtrack;

#endif

@end

