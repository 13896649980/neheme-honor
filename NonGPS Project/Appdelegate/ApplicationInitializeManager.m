//
//  ApplicationInitializeManager.m
//  VS GPS PRO
//
//  Created by Xu pan on 2019/7/17.
//  Copyright © 2019 Xu pan. All rights reserved.

#import "ApplicationInitializeManager.h"
#import <SystemConfiguration/CaptiveNetwork.h>

@interface ApplicationInitializeManager ()<CLLocationManagerDelegate>

@property   (nonatomic,strong)          CLLocationManager               *locationManager;

@end

@implementation ApplicationInitializeManager

static ApplicationInitializeManager *_mamager = nil;
+(instancetype)shareInstance{
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _mamager = [[self alloc]init];
    });
    return _mamager;
}



-(void)starInitializeOperation{
    //定位信息
    [self reportLocationServicesAuthorizationStatus:[CLLocationManager authorizationStatus]];
    //创建文件夹
    [self creatDirectory:@"Videos"];
    [self creatDirectory:@"Photos"];
    [self creatDirectory:@"Logs"];
        
#ifdef DEBUG
    NSLog(@"current debug mode!\n");
#else
    UIDevice *device = [UIDevice currentDevice];
    if (![[device model] containsString:@"Simulator"]) {
        //开始保存日志文件
        [Utility redirectNSlogToDocumentFolder];
    }
#endif
    NSLog(@"----------------- app启动 -----------------");
    //打印系统版本手机型号
    NSLog(@"%@,iOSver:%@,APPName:%@,APPver:%@",[Utility hardwareDescription],[UIDevice currentDevice].systemVersion,[Utility getAppCurrentName],[Utility getAppCurrentVersion]);
    
    //开机切换到主镜头
    UserInfoSingle *user = [Utility getUserInfo];
    user.lensType = LensType_Main;
    [Utility saveUserInfo:user];
}

- (void)reportLocationServicesAuthorizationStatus:(CLAuthorizationStatus)status{
    NSString *statusString;
    if(status == kCLAuthorizationStatusNotDetermined){
        //未决定，继续请求授权
        [self requestLocationServicesAuthorization];
        statusString = @"定位未决定，继续请求授权 kCLAuthorizationStatusNotDetermined";
    }else if(status == kCLAuthorizationStatusRestricted){
        //受限制，尝试提示然后进入设置页面进行处理（根据API说明一般不会返回该值）
        [self alertViewWithMessage];
        statusString = @"定位受限制，尝试提示然后进入设置页面进行处理 kCLAuthorizationStatusRestricted";
    }else if(status == kCLAuthorizationStatusDenied){
        //拒绝使用，提示是否进入设置页面进行修改
        [self alertViewWithMessage];
        statusString = @"定位拒绝使用，提示是否进入设置页面进行修改 kCLAuthorizationStatusDenied";
    }else if(status == kCLAuthorizationStatusAuthorizedWhenInUse){
        //授权使用，不做处理
        statusString = @"定位授权使用，不做处理 kCLAuthorizationStatusAuthorizedWhenInUse";
        [self requestLocationServicesAuthorization];
    }else if(status == kCLAuthorizationStatusAuthorizedAlways){
        //始终使用，不做处理
        statusString = @"定位始终使用，不做处理kCLAuthorizationStatusAuthorizedAlways";
        [self requestLocationServicesAuthorization];
    }
    NSLog(@"Application初始化，定位 : %@",statusString);
}

- (void)alertViewWithMessage{
    //提示用户无法进行定位操作
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:NSLocalizedString(@"Please turn on the location sevices",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil];
    [alertView show];
    
    [alertView handlerClickedButton:^(NSInteger btnIndex) {
        NSLog(@"Application初始化，跳转系统化设置页面设置位置权限");
        NSURL * url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        //打开系统设置界面
        if([[UIApplication sharedApplication] canOpenURL:url]) {
            NSURL*url =[NSURL URLWithString:UIApplicationOpenSettingsURLString];
            [[UIApplication sharedApplication] openURL:url];
        }
    }];
}

- (void)requestLocationServicesAuthorization{
    //CLLocationManager的实例对象一定要保持生命周期的存活
    if (!self.locationManager) {
        self.locationManager  = [[CLLocationManager alloc] init];
        //设置代理
        self.locationManager.delegate = self;
        //设置定位精度
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        //定位频率,每隔多少米定位一次
        CLLocationDistance distance = 1;
        self.locationManager.distanceFilter = distance;
    }
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]){
        [self.locationManager requestWhenInUseAuthorization];
    }
    //启动跟踪定位
    [self.locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    //定位服务回调函数
    //此处locations存储了持续更新的位置坐标值，取最后一个值为最新位置，如果不想让其持续更新位置，则在此方法中获取到一个值之后让locationManager stopUpdatingLocation
    CLLocation *currentLocation = [locations lastObject];
    // 获取当前所在的城市名
    UserInfoSingle *userInfo = [Utility getUserInfo];
    userInfo.currentLocation = currentLocation;
    [Utility saveUserInfo:userInfo];
    // 获取当前所在的城市名
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    //根据经纬度反向地理编译出地址信息
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *array, NSError *error){
        if (array.count > 0){
            CLPlacemark *placemark = [array objectAtIndex:0];
            //获取城市
            NSString *city = placemark.locality;
            if (!city) {
                //四大直辖市的城市信息无法通过locality获得，只能通过获取省份的方法来获得（如果city为空，则可知为直辖市）
                city = placemark.administrativeArea;
            }else if (error == nil && [array count] == 0){
                NSLog(@"No results were returned.");
            }else if (error != nil){
                NSLog(@"An error occurred = %@", error);
            }
            //获取城市名，国家码
            UserInfoSingle *userInfo = [Utility getUserInfo];
            userInfo.location = city;
            userInfo.ISOCountryCode = placemark.ISOcountryCode;
            if ([placemark.ISOcountryCode isEqualToString:@"CN"]) {
                userInfo.correctOffsetTag = 2;
            }else{
                userInfo.correctOffsetTag = 1;
            }
            [Utility saveUserInfo:userInfo];
            NSLog(@"手机定位： 用户位置 -- %@， 国家码 -- %@",placemark.name,userInfo.ISOCountryCode);
        }
    }];
    //系统会一直更新数据，直到选择停止更新，因为我们只需要获得一次经纬度即可，所以获取之后就停止更新
    [manager stopUpdatingLocation];
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:LOCATION_NOTI_NAME object:userInfo.currentLocation];
    });
}


- (void)creatDirectory:(NSString*)dir
{
    NSString *document = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *str = [document stringByAppendingPathComponent:dir];
    
    BOOL  isDir = NO;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL existed = [fileManager fileExistsAtPath:str isDirectory:&isDir];
    if ( !(isDir == YES && existed == YES) ){
        [fileManager createDirectoryAtPath:str withIntermediateDirectories:YES attributes:nil error:nil];
    }
}



@end
