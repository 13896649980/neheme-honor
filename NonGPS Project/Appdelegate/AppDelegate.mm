//
//  AppDelegate.m
//  NonGPS Project
//
//  Created by Xu pan on 2019/8/14.
//  Copyright © 2019 Xu pan. All rights reserved.
//

#import "AppDelegate.h"
#import "ApplicationInitializeManager.h"
#import "StarViewController.h"
#import "BaseNavigationViewController.h"

#ifdef OPENCV_ENABLE

#import <opencv2/opencv.hpp>
#import <opencv2/imgproc/types_c.h>
#include "detector.h"

#endif



@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    //查找wifi
    [WIFIConnectManager shareInstance];
    //socket类创建
//    [SocketControlManager shareInstance];
    [DataTransferManager shareInstance];

    //应用初始化
    [[ApplicationInitializeManager shareInstance] starInitializeOperation];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = [[BaseNavigationViewController alloc]initWithRootViewController:[[StarViewController alloc]init]];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    NSLog(@" ----------------- app退出 -----------------");
//    exit(0);
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


#ifdef  OPENCV_ENABLE
- (void)loadtrack{
    _isLoading = YES;
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        int s_videoWidth = 1280; int s_videoHeight = 720;
        if ([WIFIDeviceModel shareInstance].ID == 10010) {
            s_videoWidth = 720;
            s_videoHeight = 576;
        }else if ([WIFIDeviceModel shareInstance].ID == 10101) {
            s_videoWidth = 320;
            s_videoHeight = 240;
        }else if ([WIFIDeviceModel shareInstance].ID == 10102) {
            s_videoWidth = 640;
            s_videoHeight = 480;
        }
        
        int rows = s_videoHeight;
        int cols = s_videoWidth;
        
        
        NSString *haarcascade_frontalface_alt = [[NSBundle mainBundle] pathForResource:@"haarcascade_frontalface_alt" ofType:@"xml"];
        NSString *hand_detect = [[NSBundle mainBundle] pathForResource:@"hand_detect_v2" ofType:@"xml"];
        //NSString *hand_detect1 = [[NSBundle mainBundle] pathForResource:@"hand_detect_v4" ofType:@"xml"];
        
        NSString *svm_v2 = [[NSBundle mainBundle] pathForResource:@"svm_v2" ofType:@"xml"];
        
        NSString *pent = [[NSBundle mainBundle] pathForResource:@"Pnet" ofType:@"txt"];
        NSString *onet = [[NSBundle mainBundle] pathForResource:@"Onet" ofType:@"txt"];
        NSString *rnet = [[NSBundle mainBundle] pathForResource:@"Rnet" ofType:@"txt"];
        NSString *lenet_desc = [[NSBundle mainBundle] pathForResource:@"lenet_desc" ofType:@"prototxt"];
        NSString *lenet_model = [[NSBundle mainBundle] pathForResource:@"lenet_model" ofType:@"caffemodel"];
        
        std::string p_net_path =  [pent UTF8String];
        std::string r_net_path =  [rnet UTF8String];
        std::string o_net_path =  [onet UTF8String];
        std::string hand_detect_path =  [hand_detect UTF8String];
        std::string lenet_desc_path =  [lenet_desc UTF8String];
        std::string lenet_model_path =  [lenet_model UTF8String];
        std::string svm_path =  [svm_v2 UTF8String];
        
        load(p_net_path, r_net_path, o_net_path, hand_detect_path,lenet_desc_path,lenet_model_path,svm_path,0,rows,cols);
        self->_isLoad = YES;
        self->_isLoading = NO;
    });
}
#endif


@end
