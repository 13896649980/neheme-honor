//
//  ApplicationInitializeManager.h
//  VS GPS PRO
//
//  Created by Xu pan on 2019/7/17.
//  Copyright © 2019 Xu pan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIAlertView+Block.h"

NS_ASSUME_NONNULL_BEGIN

@interface ApplicationInitializeManager : NSObject

@property   (nonatomic,strong)      NSString            *oldSSID;

+(instancetype)shareInstance;

-(void)starInitializeOperation;

@end

NS_ASSUME_NONNULL_END
