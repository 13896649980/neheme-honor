//
//  GPUImagePlayView.m
//  博坦创新
//
//  Created by Xu pan on 2019/6/27.
//  Copyright © 2019 Xu pan. All rights reserved.
//

#import "GPUImagePlayView.h"
#import "YUGPUImageCVPixelBufferInput.h"
#import "GPUimageUIImageInput.h"
#import "JJFile.h"
#import "JJMediaManager.h"
#import "UIImage+Extension.h"
#import "WIFIDeviceModel.h"

@interface GPUImagePlayView ()<AVCaptureAudioDataOutputSampleBufferDelegate>{
    //录音
    AVCaptureDeviceInput *audioInput;
    AVCaptureAudioDataOutput *audioOutput;
    AVCaptureDevice *microphone;
    dispatch_queue_t audioProcessingQueue;
    NSString *moviewPath;
}

@property   (nonatomic,strong)          YUGPUImageCVPixelBufferInput        *pixeBufferInput;
@property   (nonatomic,strong)          GPUImageFilter                      *imageFilter;
@property   (nonatomic,strong)          GPUImageMovieWriter                 *movieWriter;
@property   (nonatomic,strong)          GPUimageUIImageInput                *imageInput;


@end

@implementation GPUImagePlayView

@synthesize captureSessionPreset = _captureSessionPreset;
@synthesize captureSession = _captureSession;

-(GPUImageMovieWriter *)movieWriter{
    NSString *name = [Utility getPicName];
    NSString *movieName = [NSString stringWithFormat:@"%@.%@",name,@"mp4"];
    NSString *dic = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES) objectAtIndex:0];
    NSString *str = [dic stringByAppendingPathComponent:@"Videos"];
    
    BOOL  isDir = NO;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL existed = [fileManager fileExistsAtPath:str isDirectory:&isDir];
    if ( !(isDir == YES && existed == YES) )
    {
        [fileManager createDirectoryAtPath:str withIntermediateDirectories:YES attributes:nil error:nil];
    }
    movieName = [str stringByAppendingPathComponent:movieName];
    moviewPath = movieName;
    unlink([movieName UTF8String]);
    

//    NSMutableDictionary *settings = [[NSMutableDictionary alloc] init];
//    if (@available(iOS 11.0, *)) {
//        [settings setObject:AVVideoCodecTypeH264 forKey:AVVideoCodecKey];
//    }
    _movieWriter = [[GPUImageMovieWriter alloc] initWithMovieURL:[NSURL fileURLWithPath:movieName] size:CGSizeMake(1920, 1080) fileType:AVFileTypeMPEG4 outputSettings:nil];
//    _movieWriter.encodingLiveVideo = YES;
//    _movieWriter.assetWriter.movieFragmentInterval = kCMTimeInvalid;
    return _movieWriter;
}


-(UIImage *)currentImage{
    return [self snapShot];
}


#pragma mark ---------      setMethods      ----------
- (void)setVREnable:(BOOL)VREnable{
    _VREnable = VREnable;
    self.pixeBufferInput.VR = VREnable;
}



-(void)setScaleLevel:(CGFloat)scaleLevel{
    _scaleLevel = scaleLevel;
    [UIView animateWithDuration:.1 animations:^{
        self.playImageView.transform = CGAffineTransformMakeScale(scaleLevel, scaleLevel);
    }];
}

-(void)setImageFilterType:(FilterType)imageFilterType{
    if (!self.imageFilter) return;
    _imageFilterType = imageFilterType;
    [self.pixeBufferInput removeAllTargets];
    [self.imageFilter removeAllTargets];
    self.imageFilter = [self getGPUImageFilterWithFilter:imageFilterType];
    [self.pixeBufferInput addTarget:self.imageFilter];
    [self.imageFilter addTarget:self.playImageView];
}

-(void)setRGBFilterWithRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue{
    if ([self.imageFilter isKindOfClass:GPUImageRGBFilter.class]) {
        GPUImageRGBFilter *rgbFilter = (GPUImageRGBFilter *)self.imageFilter;
        rgbFilter.red = red;
        rgbFilter.green = green;
        rgbFilter.blue = blue;
    }
}

-(instancetype)init{
    if (self = [super init]) {
        [self initializeConfig];
    }
    return self;
}

#pragma mark - 初始化设置
-(void)initializeConfig{
    self.userInteractionEnabled = YES;
    self.backgroundColor = [UIColor blackColor];
    self.isRecording = NO;
    self.scaleLevel = 1;
    self.imageFilterType = FilterType_None;
    //outPut
    self.playImageView = [[GPUImageView alloc] init];
    self.playImageView.userInteractionEnabled = YES;
    [self addSubview:self.playImageView];
    [self.playImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
    //滤镜
    self.imageFilter = [self getGPUImageFilterWithFilter:self.imageFilterType];
    [self.imageFilter addTarget:self.playImageView];
    
    [self setImageInput];
}


-(void)setImageInput{
    //input
    if ([WIFIDeviceModel shareInstance].chipModel == ChipModel_QuanZhi_872) {
        
        if (self.pixeBufferInput) {
            [self.pixeBufferInput removeAllTargets];
            self.pixeBufferInput = nil;
        }
        
        
        if (!self.imageInput) {
            self.imageInput = [[GPUimageUIImageInput alloc]init];
            [self.imageInput addTarget:self.imageFilter];
        }
    }else{
        
        if (self.imageInput) {
            [self.imageInput removeAllTargets];
            self.imageInput = nil;
        }
    
        if (!self.pixeBufferInput) {
            self.pixeBufferInput = [[YUGPUImageCVPixelBufferInput alloc] init];
            [self.pixeBufferInput addTarget:self.imageFilter];
        }
    }
}

-(GPUImageFilter *)getGPUImageFilterWithFilter:(FilterType)filterType{
    switch (filterType) {
        case FilterType_None:
        {
            return [[GPUImageRGBFilter alloc]init];
        }
            break;
        case FilterType_Cartoon:
        {
            return [[GPUImageToonFilter alloc]init];
        }
            break;
        case FilterType_Sketch:
        {
            return [[GPUImageSketchFilter alloc]init];
        }
            break;
        case FilterType_Nostalgia:
        {
            return [[GPUImageSepiaFilter alloc]init];
        }
            break;
        default:
            break;
    }
}

-(BOOL)processCVPixelBuffer:(CVPixelBufferRef)pixelBuffer{
    return [self.pixeBufferInput processCVPixelBuffer: pixelBuffer];
}

-(BOOL)processCVPixelBuffer:(CVPixelBufferRef)pixelBuffer frameTime:(CMTime)frameTime{
    return [self.pixeBufferInput processCVPixelBuffer:pixelBuffer frameTime:frameTime];
}

-(BOOL)processImage:(UIImage *)image frameTime:(CMTime)frameTime{
    return [self.imageInput processImage:image frameTime:frameTime];
}

-(BOOL)processImageData:(char *)data width:(int)w height:(int)h len:(int)len frameTime:(CMTime)frameTime{
    return  [self.imageInput processImageData:data width:w height:h len:len frameTime:frameTime];
}


-(UIImage *)snapShot{
    if (self.pixeBufferInput) {
        return [self.pixeBufferInput getCurrentImageFromImageBuffer];
    }else{
        return [self.imageInput getCurrentImageFromImageBuffer];
    }
}


#pragma mark - 开始录制滤镜视频
-(void)starRecord:(void(^)(void))start
      audioEnable:(BOOL)audioEnable{
    [self.imageFilter addTarget:self.movieWriter];
    if (audioEnable) {
        [self startAudioCapture];
    }
    [_movieWriter startRecording];
    self.isRecording = YES;
    start();
}
#pragma mark - 结束录制滤镜视频
-(void)stopRecord:(void (^)(void))stop{
    
    __weak typeof(self) tmp = self;
    [self.imageFilter removeTarget:_movieWriter];
    [_movieWriter finishRecordingWithCompletionHandler:^{
        
        JJFile *file = [[JJFile alloc] init];
        file.videoPath = moviewPath;
        file.name = moviewPath.lastPathComponent;
        
        NSDictionary *attr = [[NSFileManager defaultManager] attributesOfItemAtPath:moviewPath error:nil];
        file.size = [attr[NSFileSize] integerValue];
        file.type = JJ_Video;
        
        [JJMediaManager saveFileToAlbum:@[file] callback:^(int ret) {
            
            UIImage* image = [Utility getMoviewFirstFrameImageWithPath:moviewPath];
            if (!image)
            {
                image = [UIImage imageNamed:@"videopic"];
            }
            [JJMediaManager saveVideoPhoto:image withName:moviewPath.lastPathComponent callback:nil];
            
            image = [image imageScaleAndCropToMaxSize:CGSizeMake(75,75)];
            [JJMediaManager saveVideoThumb:image withName:moviewPath.lastPathComponent callback:nil];
        }];
        
        [self stopAudioCapture];
        _movieWriter = nil;
        tmp.isRecording = NO;
        moviewPath = @"";
        stop();
    }];
}

-(FilterType)getFilterFromIndex:(NSInteger)index{
    switch (index) {
        case 0:
            return FilterType_None;
            break;
        case 1:
            return FilterType_Cartoon;         //卡通            GPUImageToonFilter
            break;
        case 2:
            return FilterType_Sketch;          //素描            GPUImageSketchFilter
            break;
        case 3:
            return FilterType_Nostalgia;
            break;
            
        default:
            break;
    }
    return FilterType_None;
}

-(void)dealloc{
    NSLog(@"dealloc == %@",self);
    [[GPUImageContext sharedImageProcessingContext].framebufferCache purgeAllUnassignedFramebuffers];
}


-(void)configWithAudio{
    audioProcessingQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW,0);
    
    _captureSession = [[AVCaptureSession alloc] init];
    
    [_captureSession beginConfiguration];
    
    //添加麦克风设备
    microphone = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeAudio];
    //音频输入
    audioInput = [AVCaptureDeviceInput deviceInputWithDevice:microphone error:nil];
    if ([_captureSession canAddInput:audioInput])
    {
        [_captureSession addInput:audioInput];
    }
    //音频输出
    audioOutput = [[AVCaptureAudioDataOutput alloc] init];
    if ([_captureSession canAddOutput:audioOutput])
    {
        [_captureSession addOutput:audioOutput];
    }
    else
    {
        NSLog(@"Couldn't add audio output");
    }
    [audioOutput setSampleBufferDelegate:self queue:audioProcessingQueue];
    
    [_captureSession commitConfiguration];

    _movieWriter.hasAudioTrack = YES;
}

-(void)destroyAudio{
    if (!audioOutput)
        return ;
    
    [audioOutput setSampleBufferDelegate:nil queue:dispatch_get_main_queue()];
    [_captureSession beginConfiguration];
    [_captureSession removeInput:audioInput];
    [_captureSession removeOutput:audioOutput];
    audioInput = nil;
    audioOutput = nil;
    microphone = nil;
    [_captureSession commitConfiguration];
}

- (void)startAudioCapture{
    [self configWithAudio];
    if (![_captureSession isRunning])
    {
        [_captureSession startRunning];
    };
}

- (void)stopAudioCapture{
    if ([_captureSession isRunning]){
        [_captureSession stopRunning];
        [self destroyAudio];
    }
}

#pragma mark - AVCaptureAudioDataOutputSampleBufferDelegate
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection{
    if (!self.captureSession.isRunning){
        return;
    }else if (captureOutput == audioOutput){
        [_movieWriter processAudioBuffer:sampleBuffer];
    }

}

@end
