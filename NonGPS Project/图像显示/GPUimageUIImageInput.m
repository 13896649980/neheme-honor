//
//  GPUimageUIImageInput.m
//  GPS Project
//
//  Created by Xu pan on 2020/1/7.
//  Copyright © 2020 Xu pan. All rights reserved.
//

#import "GPUimageUIImageInput.h"
#import <OpenGLES/ES1/gl.h>
//片元着色器
NSString *const kGPUImageImageFragmentShaderString = SHADER_STRING
(
 //纹理坐标
 varying lowp vec2 textureCoordinate;
 //纹理采样器（获取对应的纹理ID）
 uniform sampler2D colorMap;
 
 void main()
 {
    //texture2D(纹理采样器,纹理坐标),获取对应坐标纹素
    gl_FragColor = texture2D(colorMap, textureCoordinate);
 }
);

@interface GPUimageUIImageInput (){
    GLint yuvConversionPositionAttribute, yuvConversionTextureCoordinateAttribute;
    GLint uiimageSamplerUniform;
}
@property (nonatomic) GLProgram *yuvConversionProgram;

@property (nonatomic,strong)        UIImage     *currentImage;

@end

@implementation GPUimageUIImageInput

- (instancetype)init {
    if (self = [super init]) {
        [self yuvConversionSetup];
    }
    return self;
}

- (void)setVR:(BOOL)VR{
    _VR = VR;
}

- (void)dealloc {
    runSynchronouslyOnVideoProcessingQueue(^{
        [GPUImageContext useImageProcessingContext];
    });
}

-(UIImage *)getCurrentImageFromImageBuffer{
    return self.currentImage;
}

- (void)yuvConversionSetup{
    if ([GPUImageContext supportsFastTextureUpload])
    {
        runSynchronouslyOnVideoProcessingQueue(^{
            [GPUImageContext useImageProcessingContext];
            
            self.yuvConversionProgram = [[GPUImageContext sharedImageProcessingContext] programForVertexShaderString:kGPUImageVertexShaderString fragmentShaderString:kGPUImageImageFragmentShaderString];
            
            if (!self.yuvConversionProgram.initialized)
            {
                [self.yuvConversionProgram addAttribute:@"position"];
                [self.yuvConversionProgram addAttribute:@"inputTextureCoordinate"];
                
                if (![self.yuvConversionProgram link])
                {
                    NSString *progLog = [self.yuvConversionProgram programLog];
                    NSLog(@"Program link log: %@", progLog);
                    NSString *fragLog = [self.yuvConversionProgram fragmentShaderLog];
                    NSLog(@"Fragment shader compile log: %@", fragLog);
                    NSString *vertLog = [self.yuvConversionProgram vertexShaderLog];
                    NSLog(@"Vertex shader compile log: %@", vertLog);
                    self.yuvConversionProgram = nil;
                    NSAssert(NO, @"Filter shader link failed");
                }
            }
            
            yuvConversionPositionAttribute = [self.yuvConversionProgram attributeIndex:@"position"];
            yuvConversionTextureCoordinateAttribute = [self.yuvConversionProgram attributeIndex:@"inputTextureCoordinate"];
            uiimageSamplerUniform = [self.yuvConversionProgram uniformIndex:@"colorMap"];
            
            [GPUImageContext setActiveShaderProgram:self.yuvConversionProgram];
            
            glEnableVertexAttribArray(yuvConversionPositionAttribute);
            glEnableVertexAttribArray(yuvConversionTextureCoordinateAttribute);
        });
    }
}

-(BOOL)processImageData:(char *)data width:(int)w height:(int)h len:(int)len frameTime:(CMTime)frameTime{
    [GPUImageContext useImageProcessingContext];
    
    int bufferWidth = w;
    int bufferHeight = h;
    
    GLubyte * spriteData = (GLubyte *) calloc(len, sizeof(GLubyte));
    memcpy(spriteData, data, len * sizeof(GLubyte));
    //绑定纹理到默认的纹理ID（
    glBindTexture(GL_TEXTURE_2D, 0);
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, bufferWidth, bufferHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, spriteData);
    [GPUImageContext setActiveShaderProgram:self.yuvConversionProgram];
    outputFramebuffer = [[GPUImageContext sharedFramebufferCache] fetchFramebufferForSize:CGSizeMake(bufferWidth, bufferHeight) onlyTexture:NO];
    [outputFramebuffer activateFramebuffer];
    
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glRotatef(45, 0, 0, 1);
    
    static const GLfloat squareVertices[] = {
        -1 , -1 ,
        1, -1 ,
        -1 , 1,
        1, 1,
    };
    static const GLfloat   squareVertice2[] = {
        -1.0 ,  0.5 - 1.0,
        0.0,  0.5 - 1.0,
        -1.0 , 1.0 -  0.5,
        0.0, 1.0 - 0.5,
    };
    
    static const GLfloat textureCoordinates[] = {
        0.0f, 0.0f,
        1.0f, 0.0f,
        0.0f, 1.0f,
        1.0f, 1.0f,
    };
    
    
    glUniform1i(uiimageSamplerUniform, 0);
    
    
    
    if (!_VR) {
        glVertexAttribPointer(yuvConversionPositionAttribute, 2, GL_FLOAT, 0, 0, squareVertices);
        glVertexAttribPointer(yuvConversionTextureCoordinateAttribute, 2, GL_FLOAT, 0, 0, textureCoordinates);
        
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    }else{
        glVertexAttribPointer(yuvConversionPositionAttribute, 2, GL_FLOAT, 0, 0, squareVertice2);
        glVertexAttribPointer(yuvConversionTextureCoordinateAttribute, 2, GL_FLOAT, 0, 0, textureCoordinates);
        
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        
        
        static const GLfloat squareVertices1[] = {
            0 , 0.5 - 1.0,
            1, 0.5 - 1.0,
            0 , 1.0 - 0.5,
            1, 1.0 - 0.5,
        };
        
        
        
        glVertexAttribPointer(yuvConversionPositionAttribute, 2, GL_FLOAT, 0, 0, squareVertices1);
        glVertexAttribPointer(yuvConversionTextureCoordinateAttribute, 2, GL_FLOAT, 0, 0, textureCoordinates);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        
    }
    
    
    for (id<GPUImageInput> currentTarget in targets)
    {
        NSInteger indexOfObject = [targets indexOfObject:currentTarget];
        NSInteger targetTextureIndex = [[targetTextureIndices objectAtIndex:indexOfObject] integerValue];
        [currentTarget setInputSize:CGSizeMake(bufferWidth, bufferHeight) atIndex:targetTextureIndex];
        [currentTarget setInputFramebuffer:outputFramebuffer atIndex:targetTextureIndex];
    }
    
    [outputFramebuffer unlock];
    
    for (id<GPUImageInput> currentTarget in targets)
    {
        NSInteger indexOfObject = [targets indexOfObject:currentTarget];
        NSInteger targetTextureIndex = [[targetTextureIndices objectAtIndex:indexOfObject] integerValue];
        [currentTarget newFrameReadyAtTime:frameTime atIndex:targetTextureIndex];
    }
    
    free(spriteData);

    return YES;
}


- (BOOL)processImage:(UIImage *)image frameTime:(CMTime)frameTime{

    self.currentImage = image;
    [GPUImageContext useImageProcessingContext];
    //1、将 UIImage 转换为 CGImageRef
    CGImageRef spriteImage = image.CGImage;
    
    //判断图片是否获取成功
    if (!spriteImage) {
        NSLog(@"Failed to load image");
        return NO;
    }
    
    //2、读取图片的大小，宽和高
    int bufferWidth = (int)CGImageGetWidth(spriteImage);
    int bufferHeight = (int)CGImageGetHeight(spriteImage);
    
    //获取图片字节数 宽*高*4（RGBA）
    GLubyte * spriteData = (GLubyte *) calloc(bufferWidth * bufferHeight * 4, sizeof(GLubyte));
    memset(spriteData, 0, bufferWidth * bufferHeight * 4);
    //6.使用默认方式绘制
    CGContextRef spriteContext = CGBitmapContextCreate(spriteData, bufferWidth, bufferHeight, 8, bufferWidth*4,CGImageGetColorSpace(spriteImage), kCGImageAlphaNoneSkipLast);

    CGRect rect = CGRectMake(0, 0, bufferWidth, bufferHeight);
    CGContextDrawImage(spriteContext, rect, spriteImage);
    //7、画图完毕就释放上下文
    CGContextRelease(spriteContext);
    //绑定纹理到默认的纹理ID（
    glBindTexture(GL_TEXTURE_2D, 0);
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, bufferWidth, bufferHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, spriteData);
    
    
    [GPUImageContext setActiveShaderProgram:self.yuvConversionProgram];
    outputFramebuffer = [[GPUImageContext sharedFramebufferCache] fetchFramebufferForSize:CGSizeMake(bufferWidth, bufferHeight) onlyTexture:NO];
    [outputFramebuffer activateFramebuffer];
    
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glRotatef(45, 0, 0, 1);

    
    static const GLfloat squareVertices[] = {
        -1 , -1 ,
        1, -1 ,
        -1 , 1,
        1, 1,
    };
     static const GLfloat   squareVertice2[] = {
            -1.0 ,  0.5 - 1.0,
            0.0,  0.5 - 1.0,
            -1.0 , 1.0 -  0.5,
            0.0, 1.0 - 0.5,
        };
    
    static const GLfloat textureCoordinates[] = {
        0.0f, 0.0f,
        1.0f, 0.0f,
        0.0f, 1.0f,
        1.0f, 1.0f,
    };
    

    glUniform1i(uiimageSamplerUniform, 0);
    
    
    
    if (!_VR) {
        glVertexAttribPointer(yuvConversionPositionAttribute, 2, GL_FLOAT, 0, 0, squareVertices);
        glVertexAttribPointer(yuvConversionTextureCoordinateAttribute, 2, GL_FLOAT, 0, 0, textureCoordinates);
        
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    }else{
        glVertexAttribPointer(yuvConversionPositionAttribute, 2, GL_FLOAT, 0, 0, squareVertice2);
        glVertexAttribPointer(yuvConversionTextureCoordinateAttribute, 2, GL_FLOAT, 0, 0, textureCoordinates);
        
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        
        
        static const GLfloat squareVertices1[] = {
            0 , 0.5 - 1.0,
            1, 0.5 - 1.0,
            0 , 1.0 - 0.5,
            1, 1.0 - 0.5,
        };
        
        
        
        glVertexAttribPointer(yuvConversionPositionAttribute, 2, GL_FLOAT, 0, 0, squareVertices1);
        glVertexAttribPointer(yuvConversionTextureCoordinateAttribute, 2, GL_FLOAT, 0, 0, textureCoordinates);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    }
    
    
    for (id<GPUImageInput> currentTarget in targets)
    {
        NSInteger indexOfObject = [targets indexOfObject:currentTarget];
        NSInteger targetTextureIndex = [[targetTextureIndices objectAtIndex:indexOfObject] integerValue];
        [currentTarget setInputSize:CGSizeMake(bufferWidth, bufferHeight) atIndex:targetTextureIndex];
        [currentTarget setInputFramebuffer:outputFramebuffer atIndex:targetTextureIndex];
    }
    
    [outputFramebuffer unlock];
    
    for (id<GPUImageInput> currentTarget in targets)
    {
        NSInteger indexOfObject = [targets indexOfObject:currentTarget];
        NSInteger targetTextureIndex = [[targetTextureIndices objectAtIndex:indexOfObject] integerValue];
        [currentTarget newFrameReadyAtTime:frameTime atIndex:targetTextureIndex];
    }
    
    free(spriteData);
    
    return YES;
}



@end
