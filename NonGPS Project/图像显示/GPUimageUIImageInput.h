//
//  GPUimageUIImageInput.h
//  GPS Project
//
//  Created by Xu pan on 2020/1/7.
//  Copyright © 2020 Xu pan. All rights reserved.
//

#import "GPUImageOutput.h"
#import "GPUImage.h"

NS_ASSUME_NONNULL_BEGIN

@interface GPUimageUIImageInput : GPUImageOutput

@property   (nonatomic,assign)      BOOL     VR;

- (BOOL)processImage:(UIImage *)image frameTime:(CMTime)frameTime;

- (BOOL)processImageData:(char *)data width:(int)w height:(int)h len:(int)len frameTime:(CMTime)frameTime;

- (UIImage *)getCurrentImageFromImageBuffer;

@end

NS_ASSUME_NONNULL_END
