//
//  YUGPUImageCVPixelBufferInput.m
//  Pods
//
//  Created by YuAo on 3/28/16.
//
//

#import "YUGPUImageCVPixelBufferInput.h"
#import <OpenGLES/ES1/gl.h>
@interface YUGPUImageCVPixelBufferInput ()
{
    const GLfloat *_preferredConversion;
    BOOL isFullYUVRange;
    GLint yuvConversionPositionAttribute, yuvConversionTextureCoordinateAttribute;
    GLint yuvConversionLuminanceTextureUniform, yuvConversionChrominanceTextureUniform;
    GLint yuvConversionMatrixUniform;
}

@property (nonatomic) GLuint lumaTexture;
@property (nonatomic) GLuint chromaTexture;
@property (nonatomic) GLProgram *yuvConversionProgram;

@property (nonatomic, strong) dispatch_semaphore_t frameRenderingSemaphore;

@end

@implementation YUGPUImageCVPixelBufferInput

- (instancetype)init {
    if (self = [super init]) {
//        self.frameRenderingSemaphore = dispatch_semaphore_create(1);
        [self yuvConversionSetup];
    }
    return self;
}

- (void)setVR:(BOOL)VR{
    _VR = VR;
}

- (void)dealloc {
    runSynchronouslyOnVideoProcessingQueue(^{
        [GPUImageContext useImageProcessingContext];
        /*
        if (self.lumaTextureRef) {
            CFRelease(self.lumaTextureRef);
            self.lumaTextureRef = NULL;
        }
        if (self.chromaTextureRef) {
            CFRelease(self.chromaTextureRef);
            self.chromaTextureRef = NULL;
        }
*/
    });
}

-(UIImage *)getCurrentImageFromImageBuffer{
    
    UIImageOrientation imageOrientation = UIImageOrientationUp;
    GPUImageFramebuffer* framebuffer = outputFramebuffer;
    CGImageRef cgImageFromBytes = [framebuffer newCGImageFromFramebufferContents];
    return [UIImage imageWithCGImage:cgImageFromBytes scale:1.0 orientation:imageOrientation];
}

- (void)yuvConversionSetup;{
    if ([GPUImageContext supportsFastTextureUpload])
    {
        runSynchronouslyOnVideoProcessingQueue(^{
            [GPUImageContext useImageProcessingContext];
            
            _preferredConversion = kColorConversion709;
            isFullYUVRange       = YES;
            self.yuvConversionProgram = [[GPUImageContext sharedImageProcessingContext] programForVertexShaderString:kGPUImageVertexShaderString fragmentShaderString:kGPUImageYUVFullRangeConversionForLAFragmentShaderString];
            
            if (!self.yuvConversionProgram.initialized)
            {
                [self.yuvConversionProgram addAttribute:@"position"];
                [self.yuvConversionProgram addAttribute:@"inputTextureCoordinate"];
                
                if (![self.yuvConversionProgram link])
                {
                    NSString *progLog = [self.yuvConversionProgram programLog];
                    NSLog(@"Program link log: %@", progLog);
                    NSString *fragLog = [self.yuvConversionProgram fragmentShaderLog];
                    NSLog(@"Fragment shader compile log: %@", fragLog);
                    NSString *vertLog = [self.yuvConversionProgram vertexShaderLog];
                    NSLog(@"Vertex shader compile log: %@", vertLog);
                    self.yuvConversionProgram = nil;
                    NSAssert(NO, @"Filter shader link failed");
                }
            }
            
            yuvConversionPositionAttribute = [self.yuvConversionProgram attributeIndex:@"position"];
            yuvConversionTextureCoordinateAttribute = [self.yuvConversionProgram attributeIndex:@"inputTextureCoordinate"];
            yuvConversionLuminanceTextureUniform = [self.yuvConversionProgram uniformIndex:@"luminanceTexture"];
            yuvConversionChrominanceTextureUniform = [self.yuvConversionProgram uniformIndex:@"chrominanceTexture"];
            yuvConversionMatrixUniform = [self.yuvConversionProgram uniformIndex:@"colorConversionMatrix"];
            
            [GPUImageContext setActiveShaderProgram:self.yuvConversionProgram];
            
            glEnableVertexAttribArray(yuvConversionPositionAttribute);
            glEnableVertexAttribArray(yuvConversionTextureCoordinateAttribute);
        });
    }
}


- (BOOL)processCVPixelBuffer:(CVPixelBufferRef)pixelBuffer {
    return [self processCVPixelBuffer:pixelBuffer frameTime:kCMTimeIndefinite];
}

- (BOOL)processCVPixelBuffer:(CVPixelBufferRef)pixelBuffer frameTime:(CMTime)frameTime {
    return [self processCVPixelBuffer:pixelBuffer frameTime:frameTime completion:nil];
}

- (BOOL)processCVPixelBuffer:(CVPixelBufferRef)pixelBuffer frameTime:(CMTime)frameTime completion:(void (^)(void))completion {
    
    /*
    if (dispatch_semaphore_wait(self.frameRenderingSemaphore, DISPATCH_TIME_NOW) != 0) {
        return NO;
    }
    
    size_t bufferWidth = CVPixelBufferGetWidth(pixelBuffer);
    size_t bufferHeight = CVPixelBufferGetHeight(pixelBuffer);

    
    CVPixelBufferLockBaseAddress(pixelBuffer, kCVPixelBufferLock_ReadOnly);
    CFRetain(pixelBuffer);
    
    runAsynchronouslyOnVideoProcessingQueue(^{
        [GPUImageContext useImageProcessingContext];
        
        if (self.lumaTextureRef) {
            CFRelease(self.lumaTextureRef);
            self.lumaTextureRef = NULL;
        }
        if (self.chromaTextureRef) {
            CFRelease(self.chromaTextureRef);
            self.chromaTextureRef = NULL;
        }
        
        CVOpenGLESTextureRef textureRef = NULL;
        CVReturn result = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                                       [[GPUImageContext sharedImageProcessingContext] coreVideoTextureCache],
                                                                       pixelBuffer,
                                                                       NULL,
                                                                       GL_TEXTURE_2D,
                                                                       GL_LUMINANCE,
                                                                       (GLsizei)bufferWidth,
                                                                       (GLsizei)bufferHeight,
                                                                       GL_LUMINANCE,
                                                                       GL_UNSIGNED_BYTE,
                                                                       0,
                                                                       &textureRef);
        
        
        
        if (result == kCVReturnSuccess && textureRef) {
            self.lumaTextureRef = textureRef;
            
            glActiveTexture(GL_TEXTURE4);
            glBindTexture(CVOpenGLESTextureGetTarget(textureRef), CVOpenGLESTextureGetName(textureRef));
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            //初始化帧缓存
            self->outputFramebuffer = [[GPUImageFramebuffer alloc] initWithSize:CGSizeMake(bufferWidth, bufferHeight) overriddenTexture:CVOpenGLESTextureGetName(textureRef)];
            //遍历滤镜
            for (id<GPUImageInput> currentTarget in self->targets) {
                if ([currentTarget enabled]) {
                    NSInteger indexOfObject = [self->targets indexOfObject:currentTarget];
                    NSInteger targetTextureIndex = [[self->targetTextureIndices objectAtIndex:indexOfObject] integerValue];
                    if (currentTarget != self.targetToIgnoreForUpdates) {
                        [currentTarget setInputSize:CGSizeMake(MAX_LEN, MIN_LEN) atIndex:targetTextureIndex];
                        [currentTarget setInputFramebuffer:self->outputFramebuffer atIndex:targetTextureIndex];
                        [currentTarget newFrameReadyAtTime:frameTime atIndex:targetTextureIndex];
                    } else {
                        [currentTarget setInputFramebuffer:self->outputFramebuffer atIndex:targetTextureIndex];
                    }
                }
            }
        }
        
        CVPixelBufferUnlockBaseAddress(pixelBuffer, kCVPixelBufferLock_ReadOnly);
        CFRelease(pixelBuffer);
        
        dispatch_semaphore_signal(self.frameRenderingSemaphore);
    });
    
    return YES;
    
    */
    [GPUImageContext useImageProcessingContext];
    CVOpenGLESTextureRef luminanceTextureRef = NULL;
    CVOpenGLESTextureRef chrominanceTextureRef = NULL;
    
    int bufferWidth = (int)CVPixelBufferGetWidth(pixelBuffer);
    int bufferHeight = (int)CVPixelBufferGetHeight(pixelBuffer);
    self.bufferWidth = bufferWidth;
    self.bufferHeight = bufferHeight;
    

//    CMTime currentTime =  CMSampleBufferGetPresentationTimeStamp(pixelBuffer);
    CVReturn err;
    // Y-plane
    glActiveTexture(GL_TEXTURE4);
    if ([GPUImageContext deviceSupportsRedTextures])
    {
        err = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault, [[GPUImageContext sharedImageProcessingContext] coreVideoTextureCache], pixelBuffer, NULL, GL_TEXTURE_2D, GL_LUMINANCE, bufferWidth, bufferHeight, GL_LUMINANCE, GL_UNSIGNED_BYTE, 0, &luminanceTextureRef);
    }
    else
    {
        err = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault, [[GPUImageContext sharedImageProcessingContext] coreVideoTextureCache], pixelBuffer, NULL, GL_TEXTURE_2D, GL_LUMINANCE, bufferWidth, bufferHeight, GL_LUMINANCE, GL_UNSIGNED_BYTE, 0, &luminanceTextureRef);
    }
    if (err)
    {
        NSLog(@"Error at CVOpenGLESTextureCacheCreateTextureFromImage %d", err);
    }
    
    self.lumaTexture = CVOpenGLESTextureGetName(luminanceTextureRef);
    glBindTexture(GL_TEXTURE_2D, self.lumaTexture);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    // UV-plane
    glActiveTexture(GL_TEXTURE5);
    if ([GPUImageContext deviceSupportsRedTextures])
    {
        err = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault, [[GPUImageContext sharedImageProcessingContext] coreVideoTextureCache], pixelBuffer, NULL, GL_TEXTURE_2D, GL_LUMINANCE_ALPHA, bufferWidth/2, bufferHeight/2, GL_LUMINANCE_ALPHA, GL_UNSIGNED_BYTE, 1, &chrominanceTextureRef);
    }
    else
    {
        err = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault, [[GPUImageContext sharedImageProcessingContext] coreVideoTextureCache], pixelBuffer, NULL, GL_TEXTURE_2D, GL_LUMINANCE_ALPHA, bufferWidth/2, bufferHeight/2, GL_LUMINANCE_ALPHA, GL_UNSIGNED_BYTE, 1, &chrominanceTextureRef);
    }
    if (err)
    {
        NSLog(@"Error at CVOpenGLESTextureCacheCreateTextureFromImage %d", err);
    }
    
    self.chromaTexture = CVOpenGLESTextureGetName(chrominanceTextureRef);
    glBindTexture(GL_TEXTURE_2D, self.chromaTexture);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    
    [GPUImageContext setActiveShaderProgram:self.yuvConversionProgram];
    outputFramebuffer = [[GPUImageContext sharedFramebufferCache] fetchFramebufferForSize:CGSizeMake(bufferWidth, bufferHeight) onlyTexture:NO];
    [outputFramebuffer activateFramebuffer];
    
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glRotatef(45, 0, 0, 1);

    
    static const GLfloat squareVertices[] = {
        -1 , -1 ,
        1, -1 ,
        -1 , 1,
        1, 1,
    };
     static const GLfloat   squareVertice2[] = {
            -1.0 ,  0.5 - 1.0,
            0.0,  0.5 - 1.0,
            -1.0 , 1.0 -  0.5,
            0.0, 1.0 - 0.5,
        };
    
    static const GLfloat textureCoordinates[] = {
        0.0f, 0.0f,
        1.0f, 0.0f,
        0.0f, 1.0f,
        1.0f, 1.0f,
    };
    
    glActiveTexture(GL_TEXTURE4);
    glBindTexture(GL_TEXTURE_2D, self.lumaTexture);
    glUniform1i(yuvConversionLuminanceTextureUniform, 4);
    
    glActiveTexture(GL_TEXTURE5);
    glBindTexture(GL_TEXTURE_2D, self.chromaTexture);
    glUniform1i(yuvConversionChrominanceTextureUniform, 5);
    
    glUniformMatrix3fv(yuvConversionMatrixUniform, 1, GL_FALSE, _preferredConversion);
    
    
    if (!_VR) {
        glVertexAttribPointer(yuvConversionPositionAttribute, 2, GL_FLOAT, 0, 0, squareVertices);
        glVertexAttribPointer(yuvConversionTextureCoordinateAttribute, 2, GL_FLOAT, 0, 0, textureCoordinates);
        
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    }else{
        glVertexAttribPointer(yuvConversionPositionAttribute, 2, GL_FLOAT, 0, 0, squareVertice2);
        glVertexAttribPointer(yuvConversionTextureCoordinateAttribute, 2, GL_FLOAT, 0, 0, textureCoordinates);
        
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        
        
        static const GLfloat squareVertices1[] = {
            0 , 0.5 - 1.0,
            1, 0.5 - 1.0,
            0 , 1.0 - 0.5,
            1, 1.0 - 0.5,
        };
        
        
        
        glVertexAttribPointer(yuvConversionPositionAttribute, 2, GL_FLOAT, 0, 0, squareVertices1);
        glVertexAttribPointer(yuvConversionTextureCoordinateAttribute, 2, GL_FLOAT, 0, 0, textureCoordinates);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    }
    
    
    for (id<GPUImageInput> currentTarget in targets)
    {
        NSInteger indexOfObject = [targets indexOfObject:currentTarget];
        NSInteger targetTextureIndex = [[targetTextureIndices objectAtIndex:indexOfObject] integerValue];
        [currentTarget setInputSize:CGSizeMake(bufferWidth, bufferHeight) atIndex:targetTextureIndex];
        [currentTarget setInputFramebuffer:outputFramebuffer atIndex:targetTextureIndex];
    }
    
    [outputFramebuffer unlock];
    
    for (id<GPUImageInput> currentTarget in targets)
    {
        NSInteger indexOfObject = [targets indexOfObject:currentTarget];
        NSInteger targetTextureIndex = [[targetTextureIndices objectAtIndex:indexOfObject] integerValue];
        [currentTarget newFrameReadyAtTime:frameTime atIndex:targetTextureIndex];
    }
    
    CVPixelBufferUnlockBaseAddress(pixelBuffer, 0);
    CFRelease(luminanceTextureRef);
    CFRelease(chrominanceTextureRef);
    
    return YES;
}



@end
