//
//  UIImage+Extension.h
//  LetingUdisk_V3.0
//
//  Created by mac on 16/9/21.
//  Copyright © 2016年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Extension)
+ (UIImage *)imageWithColor:(UIColor *)color;
+ (UIImage *)boxblurImage:(UIImage *)image withBlurNumber:(CGFloat)blur;
+ (UIImage *)imageNamed:(NSString *)name;      // load from main bundle

- (UIImage *)scaledToSize:(CGSize)newSize;
- (UIImage *)imageScaleAspectToMaxSize:(CGFloat)newSize;
- (UIImage *)imageScaleAndCropToMaxSize:(CGSize)newSize;
- (UIImage *)imageCompress2TargetSize:(CGSize)size;
@end
