/************************************************************
  *  * EaseMob CONFIDENTIAL 
  * __________________ 
  * Copyright (C) 2013-2014 EaseMob Technologies. All rights reserved. 
  *  
  * NOTICE: All information contained herein is, and remains 
  * the property of EaseMob Technologies.
  * Dissemination of this information or reproduction of this material 
  * is strictly forbidden unless prior written permission is obtained
  * from EaseMob Technologies.
  */

#import "UIViewController+ActivityIndicator.h"
#import <objc/runtime.h>



@implementation UIViewController (ActivityIndicator)

- (UIActivityIndicatorView *)activityIndicatorView{
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setActivityIndicatorView:(UIActivityIndicatorView *)activityIndicatorView{
    objc_setAssociatedObject(self, @selector(activityIndicatorView), activityIndicatorView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}



- (void)showActivityIndicatorInView:(UIView *)view
{
    UIActivityIndicatorView *activityIndicatorView = [self activityIndicatorView];
    if (!activityIndicatorView) {
        UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [view addSubview:activityIndicatorView];
        [activityIndicatorView startAnimating];
        [activityIndicatorView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(view.mas_centerY);
            make.centerX.equalTo(view.mas_centerX);
        }];
        
        [self setActivityIndicatorView:activityIndicatorView];
    }else
    {
        [view addSubview:activityIndicatorView];
        [activityIndicatorView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(view.mas_centerY);
            make.centerX.equalTo(view.mas_centerX);
        }];
        [activityIndicatorView startAnimating];
    }
    
}

- (void)hideActivityIndicatorView{
    [[self activityIndicatorView] stopAnimating];
    [[self activityIndicatorView] removeFromSuperview];
}



@end
