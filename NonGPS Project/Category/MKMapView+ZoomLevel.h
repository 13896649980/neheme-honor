//
//  MKMapView+ZoomLevel.h
//  博坦创新
//
//  Created by Xu pan on 2019/5/30.
//  Copyright © 2019 Xu pan. All rights reserved.
//

#import <MapKit/MapKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MKMapView (ZoomLevel)

- (void)setCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate
                  zoomLevel:(NSUInteger)zoomLevel
                   animated:(BOOL)animated;

@end

NS_ASSUME_NONNULL_END
