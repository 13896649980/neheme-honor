//
//  UIAlertView+Block.h
//  VISON_GPS
//
//  Created by hmdmac on 2018/1/17.
//  Copyright © 2018年 huang mindong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (Block)
- (void)handlerClickedButton:(void (^)(NSInteger btnIndex))aBlock;  
@end
