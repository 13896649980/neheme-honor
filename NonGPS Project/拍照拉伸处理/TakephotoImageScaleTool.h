//
//  TakephotoImageScaleTool.h
//  VISON_GPS
//
//  Created by Cc on 2019/5/17.
//  Copyright © 2019 huang mindong. All rights reserved.
//

#import <Foundation/Foundation.h>

#define PhotoSize_4K CGSizeMake(3840, 2160)
#define PhotoSize_2K CGSizeMake(2048, 1152)
#define PhotoSize_720p CGSizeMake(1280, 720)
#define PhotoSize_1080p CGSizeMake(1920, 1080)


#define PhotoSize_Sijie_4K CGSizeMake(4096, 3072)


NS_ASSUME_NONNULL_BEGIN

@interface TakephotoImageScaleTool : NSObject


/**
 拉伸图片

 @param originalImage 原始图片
 @param targetSize 要拉伸的大小
 @return 拉伸后的图片
 */
+(UIImage *)scaleImageWithOriginalImage:(UIImage *)originalImage
                             targetSize:(CGSize)targetSize;

@end

NS_ASSUME_NONNULL_END
