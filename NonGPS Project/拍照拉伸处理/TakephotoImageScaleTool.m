//
//  TakephotoImageScaleTool.m
//  VISON_GPS
//
//  Created by Cc on 2019/5/17.
//  Copyright © 2019 huang mindong. All rights reserved.
//

#import "TakephotoImageScaleTool.h"

@implementation TakephotoImageScaleTool

+(UIImage *)scaleImageWithOriginalImage:(UIImage *)originalImage
                             targetSize:(CGSize)targetSize{
    // Create a graphics image context
    CGSize newSize = targetSize;
    @autoreleasepool {
        UIGraphicsBeginImageContext(newSize);
        // Tell the old image to draw in this new context, with the desired
        // new size
        [originalImage drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
        // Get the new image from the context
        UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
        // End the context
        UIGraphicsEndImageContext();
        // Return the new image.
        return newImage;
    }
}

@end
