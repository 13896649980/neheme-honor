//
//  TakephotoImageHandle.h
//  VISON_GPS
//
//  Created by Cc on 2019/5/17.
//  Copyright © 2019 huang mindong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TakephotoImageScaleTool.h"

typedef void(^complate)(BOOL);

NS_ASSUME_NONNULL_BEGIN

@interface TakephotoImageHandle : NSObject

@property   (nonatomic,strong)      NSString            *imageSavePath;

+(instancetype)shareInstance;


/**
 发送指令从飞机取原图

 @param complate 完成回调 Error类型为获取失败
 */
-(void)getTheSourceImageFromDroneComplate:(void(^)(BOOL isGetSourceImage))complate;


@end

NS_ASSUME_NONNULL_END
