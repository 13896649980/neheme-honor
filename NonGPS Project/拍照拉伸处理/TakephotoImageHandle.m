//
//  TakephotoImageHandle.m
//  VISON_GPS
//
//  Created by Cc on 2019/5/17.
//  Copyright © 2019 huang mindong. All rights reserved.
//

#import "TakephotoImageHandle.h"
#import "JJMediaManager.h"

#define  TIMEOUT_DURTION   4
#define  HI4K_TIMEOUT_DURTION   7

@interface TakephotoImageHandle()<AsyncSocketDelegate>{
    int timeDurtion;
    bool isRecvDataInDurtion;
}


@property(nonatomic,strong)         AsyncSocket         *tcpSocket;
@property(nonatomic,assign)         size_t              totaPiclLen; //图片数据总大小
@property(nonatomic,assign)         size_t              curPicLen; //当前接受的图片数据大小
@property(nonatomic,strong)         NSMutableData       *picData;//接受的图片数据Data

@property(nonatomic,strong)         complate            receiveComplate;
@property(nonatomic,strong)         RACSubject          *receiveCompleteSignal;
@property(nonatomic,strong)         NSTimer             *timeOutTimer;

@end

@implementation TakephotoImageHandle

-(RACSubject *)receiveCompleteSignal{
    if (!_receiveCompleteSignal) {
        _receiveCompleteSignal = [RACSubject subject];
    }
    return _receiveCompleteSignal;
}

static TakephotoImageHandle *_imageHandle = nil;
+(instancetype)shareInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _imageHandle = [[self alloc]init];
    });
    return _imageHandle;
}


-(AsyncSocket *)tcpSocket{
    if (!_tcpSocket) {
        _tcpSocket = [[AsyncSocket alloc]initWithDelegate:self];
        NSError *error = nil;
        if(!([_tcpSocket connectToHost:DEFAULT_HOST_IP onPort:8888 error:&error])){
            NSLog(@"获取图片Socket连接失败:%@",error);
        }
    }
    return _tcpSocket;
} 


-(instancetype)init{
    if (self = [super init]) {
        self.totaPiclLen = 0;
        self.curPicLen = 0;
        
        @weakify(self);
        [self.receiveCompleteSignal subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            NSLog(@"接受图片完成");
            UIImage *picImg = [[UIImage alloc] initWithData:self.picData];
            
            picImg = [TakephotoImageScaleTool scaleImageWithOriginalImage:picImg targetSize:[Utility getPicSize]];
            
            [JJMediaManager savePhoto:picImg withName:[Utility getPicName] addToPhotoAlbum:YES callback:^{
                [self receivedDataOperation];
                if (self.receiveComplate) {
                    self.receiveComplate(YES);
                }
                if (self.timeOutTimer) {
                    [self.timeOutTimer invalidate];
                    self.timeOutTimer = nil;
                    //                      self->timeDurtion = (IS_HI4K || IS_FH8856_8812cu_4000_1280 || IS_Mr100_ssv6x5x_4000_1920 || IS_Mr100_ssv6x5x_4000_1280 || IS_Mr100_8801_4000_1920 || IS_Mr100_8801_4000_1280 || IS_Mr100_ssv6x5x_4000_2048 || IS_Mr100_8801_4000_2048) ? HI4K_TIMEOUT_DURTION : TIMEOUT_DURTION;
                }
            }];
        }];
    }
    return self;
}

#pragma mark - 飞机取原图
-(void)getTheSourceImageFromDroneComplate:(void (^)(BOOL))complate{
    
    isRecvDataInDurtion = NO;
    
    if (complate) {
        self.receiveComplate = complate;
    }
    
    self.tcpSocket = [[AsyncSocket alloc]initWithDelegate:self];
    NSError *error = nil;
    if(!([self.tcpSocket connectToHost:DEFAULT_HOST_IP onPort:8888 error:&error])){
        NSLog(@"获取图片Socket连接失败:%@",error);
        [self receivedDataOperation];
    }else{
        @synchronized (self){
            char str[12] ;
            str[0] = 0x00;
            str[1] = 0x01;
            str[2] = 0x02;
            str[3] = 0x03;
            str[4] = 0x04;
            str[5] = 0x05;
            str[6] = 0x06;
            str[7] = 0x07;
            str[8] = 0x08;
            str[9] = 0x09;
            str[10] = 0x11;
            str[11] = 0x11;
            
            NSData *data = [NSData dataWithBytes:str length:12];
            [self.tcpSocket writeData:data withTimeout:-1 tag:0];
            [self.tcpSocket readDataWithTimeout:-1 tag:0];
        }
        
        if (self.timeOutTimer) {
            [self.timeOutTimer invalidate];
            self.timeOutTimer = nil;
        }
        
        self.timeOutTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(timerOutAction) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:self.timeOutTimer forMode:NSRunLoopCommonModes];
        //self->timeDurtion = (IS_HI4K || IS_FH8856_8812cu_4000_1280 || IS_Mr100_ssv6x5x_4000_1920 || IS_Mr100_ssv6x5x_4000_1280 || IS_Mr100_8801_4000_1920 || IS_Mr100_8801_4000_1280 || IS_Mr100_ssv6x5x_4000_2048 || IS_Mr100_8801_4000_2048) ? HI4K_TIMEOUT_DURTION : TIMEOUT_DURTION;
        
#if 0
        //接受时长为5秒
        __weak typeof(self) weakSelf = self;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(8 * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            if (!weakSelf.totaPiclLen) { //没有收到数据失败
                NSLog(@"图片没有数据传输");
                complate(NO);
            }else{
                if (weakSelf.totaPiclLen == weakSelf.curPicLen) { // 图片数据接受完成
                    UIImage *picImg = [[UIImage alloc] initWithData:self.picData];
                    //是否需要拉伸图片差值
                    if ([Utility isNeedScalePhoto]) {
                        [JJMediaManager savePhoto:[TakephotoImageScaleTool scaleImageWithOriginalImage:picImg targetSize:[Utility getScalePhotoSize]] withName:[[HMDAVControl sharedManager] getPictName] addToPhotoAlbum:YES callback:nil];
                        NSLog(@"接受底层原图完成，拉伸保存完成");
                    }else{
                        [JJMediaManager savePhoto:picImg withName:[[HMDAVControl sharedManager] getPictName] addToPhotoAlbum:YES callback:nil];
                        NSLog(@"接受底层原图完成,保存完成");
                    }
                    complate(YES);
                }else{//图片数据在指定时间未接受完成，取数据失败
                    NSLog(@"图片数据在指定时间未接收完成");
                    complate(NO);
                }
            }
            [weakSelf receivedDataOperation];
        });
#endif
    }
}

#pragma mark - 制定时间接受完数据操作
-(void)receivedDataOperation{
    if (_tcpSocket) {
        [_tcpSocket disconnect];
        _tcpSocket.delegate = nil;
        _tcpSocket = nil;
    }
    self.picData = nil;
    self.totaPiclLen = self.curPicLen = 0;
}

#pragma mark -TcpSocketDelegate
- (void)onSocket:(AsyncSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port{
 if (sock == self.tcpSocket){
        NSLog(@"照片连接 Socket:%p didConnectToHost:%@ port:%hu", sock, host, port);
    }
}

- (void)onSocketDidDisconnect:(AsyncSocket *)sock{
    if(sock == self.tcpSocket){
        self.tcpSocket.delegate = nil;
        self.tcpSocket = nil;
        NSLog(@"照片连接断开 onSocketDidDisconnect!");
    }
}
- (void)onSocket:(AsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag{
    @synchronized (self){
        if (sock == self.tcpSocket){
            [self handlePicTcpSocket:sock data:data withTag:tag];//只发命令不处理
        }
    }
    [sock readDataWithTimeout:-1 tag:tag];
}

#pragma mark - 接受照片的数据
- (void)handlePicTcpSocket:(AsyncSocket *)sock data:(NSData *)data withTag:(long)tag{
    //ff ff 04 03 02 01 data data data data
    isRecvDataInDurtion = YES;
    Byte *picbytes = (Byte *)data.bytes;
    if (picbytes[0] == 0xff && picbytes[1] == 0xff){
        
        uint32_t len = *(uint32_t *)&picbytes[2];
        self.picData = [NSMutableData data];
        [self.picData appendBytes:picbytes+6 length:data.length-6];
        
        self.totaPiclLen = htonl(len);
        self.curPicLen = data.length-6;
    }else{
        [self.picData appendBytes:picbytes length:data.length];
        self.curPicLen += data.length;
    }
    
    if (self.totaPiclLen && self.totaPiclLen == self.curPicLen) { // 接收完成
        [self.receiveCompleteSignal sendNext:nil];
    }
}

-(void)timerOutAction{
    /*
    if (timeDurtion > 0) {
        timeDurtion--;
    }else{ // 规定时间未接收完
        if (self.timeOutTimer) {
            [self.timeOutTimer invalidate];
            self.timeOutTimer = nil;
            timeDurtion = (IS_HI4K || IS_FH8856_8812cu_4000_1280 || IS_Mr100_ssv6x5x_4000_1920 || IS_Mr100_ssv6x5x_4000_1280 || IS_Mr100_8801_4000_1920 || IS_Mr100_8801_4000_1280 || IS_Mr100_ssv6x5x_4000_2048 || IS_Mr100_8801_4000_2048) ? HI4K_TIMEOUT_DURTION : TIMEOUT_DURTION;
        }
        
        if (self.receiveComplate) {
            self.receiveComplate(NO);
        }
        
        [self receivedDataOperation];
        NSLog(@"图片数据在指定时间未接受完成");
    }
    */
    if (isRecvDataInDurtion) {
        isRecvDataInDurtion = NO;
    }else{
        
        [self receivedDataOperation];
        
        if (self.timeOutTimer) {
            [self.timeOutTimer invalidate];
            self.timeOutTimer = nil;
        }
        
        if (self.receiveComplate) {
            self.receiveComplate(NO);
        }
        
        NSLog(@"2秒钟无图片数据传输");
    }
}

@end
