//
//  TiaoJieView.h
//  NonGPS Project
//
//  Created by Sanerly on 2021/7/2.
//  Copyright © 2021 Xu pan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TiaoJieView : UIView
@property   (nonatomic,strong)UIButton  *leftButton;
@property   (nonatomic,strong)UIButton  *middleButton;
@property   (nonatomic,strong)UIButton  *rightButton;
@property   (nonatomic,assign)int type;
@end

NS_ASSUME_NONNULL_END
