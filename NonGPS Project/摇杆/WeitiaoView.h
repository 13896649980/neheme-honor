//
//  WeitiaoView.h
//  AirH264
//
//  Created by huang mindong on 15/12/24.
//  Copyright © 2015年 huang mindong. All rights reserved.
//

#import <UIKit/UIKit.h>

#define TRIMMINGMAXC  32    //64
#define TRIMMINGCOUNT 4.5f //4.1f

@interface WeitiaoView : UIView
{
    char mTrimValue;
    UIImageView *mTrimImgView;
}

@property(nonatomic,strong)UIImageView *mImageView;

- (char)getValue;
- (void)setTrimValue:(char)value;
- (void)resetTrimValue;
@end
