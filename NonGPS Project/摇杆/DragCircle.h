//
//  DragCircle.h
//  Plane
//
//  Created by cong dai on 11-9-27.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DragCircleTouchEndDelegate <NSObject>

@optional
- (void)DragCircleTouchEnded:(UIView *)view touches:(NSSet *)touches withEvent:(UIEvent *)event;
- (void)DragCircleTouchBegan:(UIView *)view touches:(NSSet *)touches withEvent:(UIEvent *)event;
- (void)DragCircleTouchMoved:(UIView *)view touches:(NSSet *)touches withEvent:(UIEvent *)event;
@end

@interface DragCircle : UIView
{
    CGPoint CenterP;
    float   CircleR;
    
    char isL;
    
    unsigned char powerFB;
    unsigned char powerLR;
    
    unsigned char isControl;
}

@property (nonatomic, assign)id<DragCircleTouchEndDelegate>delegate;
@property (nonatomic, retain)UIImageView *myCircle;

/**
 仅支持垂直拖动模式
 */
@property (nonatomic, assign) BOOL      onlyVerticalMode;

/**
 仅支持水平拖动模式
 */
@property (nonatomic, assign) BOOL      onlyhorizontalMode;

- (void)setDragCircleCenter;
- (unsigned char)GetPowerFB;
- (unsigned char)GetPowerLR;

- (unsigned char)GetCircleX;
- (unsigned char)GetCircleY;
- (void)setDragCircle;

- (void)isLeft;
- (void)isRight;

///Circlex,CircleY两个的值，范围必须为0～127之间，且最左为0,最下为0
- (void)setDragCircleXandY : (unsigned char)CircleX
               DragCircleY : (unsigned char)CircleY;

- (void)setTrackCriclePoint:(CGPoint)point;

@end
