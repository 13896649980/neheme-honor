//
//  RightDragBK.h
//  720P_TARGET
//
//  Created by hmdmac on 2018/4/17.
//  Copyright © 2018年 huang mindong. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RightTouchDownDelegate <NSObject>

@optional
- (void)rightTouchDown:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event;
- (void)rightTouchMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event;
- (void)rightTouchEnded:(NSSet *)touches withEvent:(UIEvent *)event;
@end

@interface RightDragBK : UIView
@property(nonatomic,strong)UIView *mRightDragCircle;
@property(nonatomic,strong)UIView *mRightDragBackView;

@property(nonatomic,assign)id<RightTouchDownDelegate>delegate;
@end

