//
//  RightDragBK.m
//  720P_TARGET
//
//  Created by hmdmac on 2018/4/17.
//  Copyright © 2018年 huang mindong. All rights reserved.
//

#import "RightDragBK.h"

@implementation RightDragBK

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    if ([self.delegate respondsToSelector:@selector(rightTouchDown:withEvent:)]) {
        [self.delegate rightTouchDown:touches withEvent:event];
    }
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesMoved:touches withEvent:event];
    if ([self.delegate respondsToSelector:@selector(rightTouchMoved:withEvent:)]) {
        [self.delegate rightTouchMoved:touches withEvent:event];
    }
}
- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    if ([self.delegate respondsToSelector:@selector(rightTouchEnded:withEvent:)]) {
        [self.delegate rightTouchEnded:touches withEvent:event];
    }
}
@end
