//
//  DragCircle.m
//  Plane
//
//  Created by cong dai on 11-9-27.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "DragCircle.h"

#define POWERFB 64
#define POWERLR 64

#define CIRCLER 64

#define CIRCLERLOW 127


@implementation DragCircle

@synthesize myCircle;
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect frame = self.frame;
    CenterP = CGPointMake(frame.size.width/2, frame.size.height/2);
    
    [self setDragCircleCenter];
}

-(id) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        self.userInteractionEnabled = YES;
        
        CenterP = CGPointMake(frame.size.width/2, frame.size.height/2);
        
        CircleR = CIRCLER;
        
        powerFB = POWERFB;
        powerLR = POWERLR;
        
        isControl = 0;
        
        isL = 0;
        
        myCircle = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"遥感小圆"]];
        
        [self addSubview:myCircle];
        [myCircle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.mas_equalTo(self);
            make.width.height.mas_equalTo(LAYOUT_WIDTH(35));
        }];
        [self setDragCircleCenter];
        
        self.alpha = 1;
    }
    return self;
    
}

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if ([self.delegate respondsToSelector:@selector(DragCircleTouchEnded:touches:withEvent:)]) {
        [self.delegate DragCircleTouchBegan:self touches:touches withEvent:event];
    }
    
    CGPoint pt = [[touches anyObject] locationInView:self];
    if (self.onlyVerticalMode) {
        pt = CGPointMake(CenterP.x, pt.y);
    }
    if (self.onlyhorizontalMode) {
        pt = CGPointMake(pt.x, CenterP.y);
    }
    
    float RR2 = (pt.x - CenterP.x)*(pt.x - CenterP.x) + (pt.y - CenterP.y)*(pt.y - CenterP.y);
    if (RR2 <= CIRCLERLOW*CIRCLERLOW) 
    {
        isControl = 1;
        
        if(RR2 <= CircleR*CircleR)
        {
            myCircle.center = pt;
            
            powerLR = POWERLR + (pt.y - CenterP.y);      //(newcenter.y - CenterP.y)/4
            powerFB = POWERFB + (pt.x - CenterP.x);      //(newcenter.x - CenterP.x)/2
            
        }
        else
        {
            CGPoint newcenter;
            if (pt.x - CenterP.x == 0) 
            {
                newcenter.x = pt.x;
                if (pt.y - CenterP.y > 0) 
                {
                    newcenter.y = CenterP.y + POWERLR;
                }
                else
                {
                    newcenter.y = CenterP.y - POWERLR;
                }
                
            }
            else
            {
                float k = (pt.y - CenterP.y)/(pt.x - CenterP.x);
                newcenter.x = CIRCLER / sqrt(1+k*k); //sqrt(1+k*k)
                if (pt.x - CenterP.x < 0) 
                {
                    newcenter.x = -newcenter.x;
                }
                newcenter.y  = k*newcenter.x;
                newcenter.y += CenterP.y;
                
                newcenter.x += CenterP.x;
            }
            
            myCircle.center = newcenter;
            powerLR = POWERLR + (newcenter.y - CenterP.y);
            powerFB = POWERFB + (newcenter.x - CenterP.x);
        }
    }
    else
    {
        isControl = 0;
    }
    
    [super touchesBegan:touches withEvent:event];
}

-(void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if ([self.delegate respondsToSelector:@selector(DragCircleTouchMoved:touches:withEvent:)]) {
        [self.delegate DragCircleTouchMoved:self touches:touches withEvent:event];
    }
    
    if (isControl == 0)
    {
        return;
    }
    
    CGPoint pt = [[touches anyObject ] locationInView:self];
    if (self.onlyVerticalMode) {
        pt = CGPointMake(CenterP.x, pt.y);
    }
    if (self.onlyhorizontalMode) {
        pt = CGPointMake(pt.x, CenterP.y);
    }
    
    float RR2 = (pt.x - CenterP.x)*(pt.x - CenterP.x) + (pt.y - CenterP.y)*(pt.y - CenterP.y);

#ifdef CIRCLERHIG    
    if (RR2 >= CIRCLERHIG*CIRCLERHIG) 
    {
        isControl = 0;
        [self setDragCircleCenter];
        
        return;
    }
#endif
    
    if(RR2 <= CircleR*CircleR)
    {
        myCircle.center = pt;
        
        powerLR = POWERLR + (pt.y - CenterP.y);      //(newcenter.y - CenterP.y)/4
        powerFB = POWERFB + (pt.x - CenterP.x);      //(newcenter.x - CenterP.x)/2
        
//        NSLog(@"LR = %d,FB = %d",powerLR, powerFB);
    }
    else
    {
        CGPoint newcenter;
        if (pt.x - CenterP.x == 0) 
        {
            newcenter.x = pt.x;
            if (pt.y - CenterP.y > 0) 
            {
                newcenter.y = CenterP.y + POWERLR;
            }
            else
            {
                newcenter.y = CenterP.y - POWERLR;
            }
            
        }
        else
        {
           float k = (pt.y - CenterP.y)/(pt.x - CenterP.x);
            newcenter.x = CIRCLER / sqrt(1+k*k); // CIRCLER为固定范围 求 CIRCLER(y) 相对应的  x = y/k
            if (pt.x - CenterP.x < 0) 
            {
                newcenter.x = -newcenter.x;
            }
            newcenter.y  = k*newcenter.x;//根据X 求y     k = y1 - y0 / x1 - x0  CIRCLER = x*k
            newcenter.y += CenterP.y;   //再用中心值 去加减 x y
            
            newcenter.x += CenterP.x;
        }
        
        myCircle.center = newcenter;
        powerLR = POWERLR + (newcenter.y - CenterP.y);
        powerFB = POWERFB + (newcenter.x - CenterP.x);
    }
}

-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (isControl == 0) {
        return;
    }
    isControl = 0;
    [self setDragCircleCenter];
    
    [super touchesEnded:touches withEvent:event];
    
    if ([self.delegate respondsToSelector:@selector(DragCircleTouchEnded:touches:withEvent:)]) {
        [self.delegate DragCircleTouchEnded:self touches:touches withEvent:event];
    }

}

-(void)setDragCircle
{
    powerLR = 127;
    powerFB = 64;
    myCircle.center = CGPointMake(CenterP.x, self.frame.size.height-myCircle.frame.size.height);
}

- (void)setDragCircleCenter
{
    if (isL)
    {
        myCircle.center = CGPointMake(CenterP.x,myCircle.center.y );
        powerFB = POWERFB;
        return;
    }
    
    myCircle.center = CenterP;
    
    powerFB = POWERFB;
    powerLR = POWERLR;
}

- (unsigned char)GetPowerFB
{
    return powerFB;
}

- (unsigned char)GetPowerLR
{
    return powerLR;
}

- (unsigned char)GetCircleX
{
    return (unsigned char)POWERFB;
}

- (unsigned char)GetCircleY
{
    return (unsigned char)POWERLR;
}

- (void)setDragCircleXandY : (unsigned char)CircleX
               DragCircleY : (unsigned char)CircleY
{
    powerFB = CircleX;
    powerLR = CircleY;
    
    CGPoint newcente = CGPointMake(CenterP.x + CircleX - POWERFB, CenterP.y + CircleY - POWERLR);
    
    if (self.onlyVerticalMode) {
        newcente = CGPointMake(CenterP.x, newcente.y);
    }
    if (self.onlyhorizontalMode) {
        newcente = CGPointMake(newcente.x, CenterP.y);
    }
    
    myCircle.center = newcente;
}

- (void)setTrackCriclePoint:(CGPoint)point
{
    CGPoint pt = point;
    
    float RR2 = (pt.x - CenterP.x)*(pt.x - CenterP.x) + (pt.y - CenterP.y)*(pt.y - CenterP.y);
    
#ifdef CIRCLERHIG
    if (RR2 >= CIRCLERHIG*CIRCLERHIG)
    {
        isControl = 0;
        [self setDragCircleCenter];
        
        return;
    }
#endif
    
    if(RR2 <= CircleR*CircleR)
    {
        myCircle.center = pt;
        
        powerLR = POWERLR + (pt.y - CenterP.y);      //(newcenter.y - CenterP.y)/4
        powerFB = POWERFB + (pt.x - CenterP.x);      //(newcenter.x - CenterP.x)/2
        
        //        NSLog(@"LR = %d,FB = %d",powerLR, powerFB);
    }
    else
    {
        CGPoint newcenter;
        if (pt.x - CenterP.x == 0)
        {
            newcenter.x = pt.x;
            if (pt.y - CenterP.y > 0)
            {
                newcenter.y = CenterP.y + POWERLR;
            }
            else
            {
                newcenter.y = CenterP.y - POWERLR;
            }
            
        }
        else
        {
            float k = (pt.y - CenterP.y)/(pt.x - CenterP.x);
            newcenter.x = CIRCLER / sqrt(1+k*k); // CIRCLER为固定范围 求 CIRCLER(y) 相对应的  x = y/k
            if (pt.x - CenterP.x < 0)
            {
                newcenter.x = -newcenter.x;
            }
            newcenter.y  = k*newcenter.x;//根据X 求y     k = y1 - y0 / x1 - x0  CIRCLER = x*k
            newcenter.y += CenterP.y;   //再用中心值 去加减 x y
            
            newcenter.x += CenterP.x;
        }
        
        myCircle.center = newcenter;
        powerLR = POWERLR + (newcenter.y - CenterP.y);
        powerFB = POWERFB + (newcenter.x - CenterP.x);
    }
}


- (void)isLeft
{
    isL = 1;
}

- (void)isRight
{
    isL = 0;
}


@end
