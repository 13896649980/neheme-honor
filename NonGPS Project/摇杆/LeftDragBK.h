//
//  LeftDragBK.h
//  720P_TARGET
//
//  Created by hmdmac on 2018/4/17.
//  Copyright © 2018年 huang mindong. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LeftTouchDownDelegate <NSObject>

@optional
- (void)leftTouchDown:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event;
- (void)leftTouchMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event;
- (void)leftTouchEnded:(NSSet *)touches withEvent:(UIEvent *)event;
@end

@interface LeftDragBK : UIView
@property(nonatomic,strong)UIView *mLeftDragBackView;
@property(nonatomic,strong)UIView *mLeftDragCircle;

@property(nonatomic,assign)id<LeftTouchDownDelegate>delegate;
@end
