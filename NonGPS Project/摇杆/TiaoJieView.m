//
//  TiaoJieView.m
//  NonGPS Project
//
//  Created by Sanerly on 2021/7/2.
//  Copyright © 2021 Xu pan. All rights reserved.
//

#import "TiaoJieView.h"

@implementation TiaoJieView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (id)initWithFrame:(CGRect)frame
{
    self    =   [super initWithFrame:frame];
    if (self)
    {
        [self initSubView];
    }
    return self;
}

- (void)initSubView
{
    self.leftButton =   [UIButton buttonWithType:UIButtonTypeCustom];
    [self.leftButton setImage:[UIImage imageNamed:@"调整箭头"] forState:UIControlStateNormal];
    [self addSubview:_leftButton];
    
    self.middleButton =   [UIButton buttonWithType:UIButtonTypeCustom];
    [self.middleButton setImage:[UIImage imageNamed:@"调整圆矩形"] forState:UIControlStateNormal];
    [self addSubview:_middleButton];
    
    self.rightButton =   [UIButton buttonWithType:UIButtonTypeCustom];
    [self.rightButton setImage:[UIImage imageNamed:@"调整箭头"] forState:UIControlStateNormal];
    [self addSubview:_rightButton];
    
        [_leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self);
            make.centerY.mas_equalTo(self);
        }];
        
        [_rightButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self);
            make.centerY.mas_equalTo(self);
        }];
        
        [_middleButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_leftButton).offset(10);
            make.centerY.mas_equalTo(self);
            make.right.mas_equalTo(_rightButton).offset(-10);
        }];
    
    CGAffineTransform a = CGAffineTransformIdentity;
    [self.rightButton setTransform:CGAffineTransformRotate(a, M_PI)];
}


@end
