//
//  LeftDragBK.m
//  720P_TARGET
//
//  Created by hmdmac on 2018/4/17.
//  Copyright © 2018年 huang mindong. All rights reserved.
//

#import "LeftDragBK.h"

@implementation LeftDragBK

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    if ([self.delegate respondsToSelector:@selector(leftTouchDown:withEvent:)]) {
        [self.delegate leftTouchDown:touches withEvent:event];
    }
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesMoved:touches withEvent:event];
    if ([self.delegate respondsToSelector:@selector(leftTouchMoved:withEvent:)]) {
        [self.delegate leftTouchMoved:touches withEvent:event];
    }
}
- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    if ([self.delegate respondsToSelector:@selector(leftTouchEnded:withEvent:)]) {
        [self.delegate leftTouchEnded:touches withEvent:event];
    }
}
@end
