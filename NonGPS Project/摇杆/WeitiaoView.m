//
//  WeitiaoView.m
//  AirH264
//
//  Created by huang mindong on 15/12/24.
//  Copyright © 2015年 huang mindong. All rights reserved.
//

#import "WeitiaoView.h"
#import <AudioToolbox/AudioToolbox.h>

@implementation WeitiaoView


-(id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        self.userInteractionEnabled = YES;
        
        UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"微调"]];
        imgView.frame = CGRectMake(0, 0, imgView.image.size.width * 0.8, imgView.image.size.height * 0.8);
        [self addSubview:imgView];
        _mImageView = imgView;
        
        UIImageView *imgView1 = [[UIImageView alloc] initWithImage:[self createImageWithColor:[UIColor blueColor]]];
        imgView1.frame = CGRectMake((imgView.frame.size.width-imgView1.image.size.width)/2, 5, 5, 10);
        [imgView addSubview:imgView1];
        mTrimImgView = imgView1;
        
        UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self addSubview:leftBtn];
        [leftBtn addTarget:self action:@selector(trimLeft) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self addSubview:rightBtn];
        [rightBtn addTarget:self action:@selector(trimRight) forControlEvents:UIControlEventTouchUpInside];
        

        // 为了去掉左右微调按钮添加的
        _mImageView.userInteractionEnabled = YES;
        UITapGestureRecognizer *r = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
        [_mImageView addGestureRecognizer:r];
        
        [leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.centerY.mas_equalTo(self->_mImageView.mas_centerY);
            make.left.top.bottom.mas_equalTo(self);
            make.width.mas_equalTo(LAYOUT_WIDTH(25));
        }];
        
        [rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self);
            make.centerY.mas_equalTo(self->_mImageView.mas_centerY);
            make.right.top.bottom.mas_equalTo(self);
            make.width.mas_equalTo(LAYOUT_WIDTH(25));
        }];
        
        
        [_mImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.centerY.mas_equalTo(self.mas_centerY);
            make.left.mas_equalTo(leftBtn.mas_right).offset(LAYOUT_WIDTH(0));
            make.right.mas_equalTo(rightBtn.mas_left).offset(LAYOUT_WIDTH(0));
            make.height.mas_equalTo(LAYOUT_WIDTH(30));
        }];
        
        [mTrimImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_mImageView.mas_centerY);
            make.centerX.equalTo(_mImageView.mas_centerX);
            make.size.mas_equalTo(CGSizeMake(10,20));
        }];
        /*
        
        [leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self->_mImageView.mas_left).offset(LAYOUT_WIDTH(-2));
            make.centerY.mas_equalTo(self->_mImageView.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(15, 20));
        }];
        
        [rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self->_mImageView.mas_right).offset(LAYOUT_WIDTH(2));
            make.centerY.mas_equalTo(self->_mImageView.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(15, 20));
            
        }];
         */
        
        UIImage *leftImg = [UIImage imageNamed:@"微调左箭头"];
        
        UIImageView *lefImageView = [[UIImageView alloc]initWithImage:leftImg];
        [leftBtn addSubview:lefImageView];
        [lefImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.mas_equalTo(leftBtn);
            make.size.mas_equalTo(CGSizeMake(LAYOUT_WIDTH(15), LAYOUT_WIDTH(20)));
        }];
        UIImage *rigImg = [UIImage imageNamed:@"微调右箭头"];
        UIImageView *rightImageView = [[UIImageView alloc]initWithImage:rigImg];
        [rightBtn addSubview:rightImageView];
        [rightImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.mas_equalTo(rightBtn);
            make.size.mas_equalTo(CGSizeMake(LAYOUT_WIDTH(15), LAYOUT_WIDTH(20)));
        }];
        
        mTrimValue = 16;
        
        [self layoutIfNeeded];
        
    }
    return self;
    
}

/*
 将UIColor转换成图片
 */
- (UIImage*)createImageWithColor:(UIColor*)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


- (char)getValue
{
    return mTrimValue;
}


/**
 *  点击事件
 *
 *  @param r UITapGestureRecognizer
 */
- (void)tapGesture:(UITapGestureRecognizer *)r
{
    CGFloat locX = [r locationInView:_mImageView].x;
    CGFloat width2 = _mImageView.bounds.size.width / 2.0;
    if (locX < width2)//左边
    {
        // 判断是否越界
        [self trimLeft];
        
    } else { //右边
        [self trimRight];
    }
}

- (void)trimLeft
{
    if (mTrimValue <= 0) {
        mTrimValue = 0;
        return;
    }
    
    mTrimValue --;
    
    float x = mTrimImgView.center.x - TRIMMINGCOUNT;
    if (x < 0) {
        x = 0;
    }
    [mTrimImgView setCenter:CGPointMake(x, mTrimImgView.center.y)];
    
    NSLog(@"trimLeft = %d",mTrimValue);
    
    if (mTrimValue == TRIMMINGMAXC /2)
    {
        NSString *path = [NSString stringWithFormat:@"%@%@",[[NSBundle mainBundle] resourcePath],@"/heliway_middle.wav"];
        SystemSoundID soundID;
        NSURL *filePath = [NSURL fileURLWithPath:path isDirectory:NO];
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)filePath, &soundID);
        AudioServicesPlaySystemSound(soundID);
    }else{
        NSString *path = [NSString stringWithFormat:@"%@%@",[[NSBundle mainBundle] resourcePath],@"/heliway.wav"];
        SystemSoundID soundID;
        NSURL *filePath = [NSURL fileURLWithPath:path isDirectory:NO];
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)filePath, &soundID);
        AudioServicesPlaySystemSound(soundID);
    }
}

- (void)trimRight
{
    if (mTrimValue >= TRIMMINGMAXC) {
        mTrimValue = TRIMMINGMAXC;
        
        return;
    }
    mTrimValue ++;
    
    float x = mTrimImgView.center.x + TRIMMINGCOUNT;
    if (x > _mImageView.frame.size.width) {
        x = _mImageView.frame.size.width;
    }
    
    [mTrimImgView setCenter:CGPointMake(mTrimImgView.center.x + TRIMMINGCOUNT, mTrimImgView.center.y)];
    
    NSLog(@"trimRight = %d",mTrimValue);
    if (mTrimValue == TRIMMINGMAXC /2){
        NSString *path = [NSString stringWithFormat:@"%@%@",[[NSBundle mainBundle] resourcePath],@"/heliway_middle.wav"];
        SystemSoundID soundID;
        NSURL *filePath = [NSURL fileURLWithPath:path isDirectory:NO];
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)filePath, &soundID);
        AudioServicesPlaySystemSound(soundID);
    }else{
        NSString *path = [NSString stringWithFormat:@"%@%@",[[NSBundle mainBundle] resourcePath],@"/heliway.wav"];
        SystemSoundID soundID;
        NSURL *filePath = [NSURL fileURLWithPath:path isDirectory:NO];
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)filePath, &soundID);
        AudioServicesPlaySystemSound(soundID);
    }
    
}

- (void)setTrimValue:(char)value
{
    if (value >= 16 && value <= TRIMMINGMAXC)
    {
        mTrimValue = value;
        
        float x = _mImageView.frame.size.width/2 + TRIMMINGCOUNT*(value-16);
        if (x > _mImageView.frame.size.width) {
            x = _mImageView.frame.size.width;
        }
        
        NSLog(@"%@",mTrimImgView);
        CGSize size = mTrimImgView.frame.size;
        
        [mTrimImgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self);
            make.centerX.mas_equalTo(self.mImageView.mas_centerX).offset(TRIMMINGCOUNT*(value-16));
            make.size.mas_equalTo(size);
        }];
        
        //        [mTrimImgView setCenter:CGPointMake(x, mTrimImgView.center.y)];
        
        NSLog(@"%@",mTrimImgView);
        
    }else if (value <= 16 && value >= 0)
    {
        mTrimValue = value;
        
        float x = _mImageView.frame.size.width/2 + TRIMMINGCOUNT*(value-16);
        if (x < 0) {
            x = 0;
        }
        //        [mTrimImgView setCenter:CGPointMake(x, mTrimImgView.center.y)];
        
        CGSize size = mTrimImgView.frame.size;
        
        [mTrimImgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self);
            make.centerX.mas_equalTo(self.mImageView.mas_centerX).offset(TRIMMINGCOUNT*(value-16));
            make.size.mas_equalTo(size);
        }];
    }
}

- (void)resetTrimValue
{
    [mTrimImgView setCenter:CGPointMake(_mImageView.frame.size.width/2, _mImageView.frame.size.height/2)];
    mTrimValue = 16;
}

@end
