//
//  Utility.h
//  博坦创新
//
//  Created by Xu pan on 2019/5/22.
//  Copyright © 2019 Xu pan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "UserInfoSingle.h"





@interface Utility : NSObject

+(NSString *)formatTimeInterval:(NSInteger)seconds;
/**
 @return 返回当前设备类型
 */
+ (NSString*)hardwareDescription;

/**
 字符串base64编码输出
 */
+(NSString *)base64EncodeString:(NSString *)string;

/**
  字符串base64a反编码输出
 */
+(NSString *)base64DecodeString:(NSString *)string;

/**
 输出沙盒视频时长
 */
+(NSString *)getVideoInfoWithSourcePath:(NSString *)path;

/**
 获取用户对象
 */
+(UserInfoSingle *)getUserInfo;

/**
 保存用户信息
 */
+(void)saveUserInfo:(UserInfoSingle *)userInfo;
/**
 删除用户信息
 */
+(void)removeUserInfo;

/**
 获取屏幕当前控制器
 */
+ (UIViewController *)topController;
+ (UIViewController *)getCurrentViewController;

/**
 登录，登出替换windowRoot
 */
+(void)reSetWindowRootViewController;


//wgs84转高德
+ (CLLocationCoordinate2D)gcj02Encrypt:(double)ggLat
                                 bdLon:(double)ggLon;

//高德转wgs84
+ (CLLocationCoordinate2D)gcj02Decrypt:(double)gjLat
                                 gjLon:(double)gjLon;


+(NSString *)formattedOutputWithFile:(NSString *)file
                                line:(NSInteger)line;

+(NSString *)getCurrentDataString;

+(NSString *)getAppCurrentName;

+(NSString *)getAppCurrentVersion;

+(NSString *)getAppCurrentSystemVersion;


/**
 *纬盛协议中每次解析出来的提示中，优先级判断，优先级为
 *1. 低电
 *2.（下降2.5   返航2.4    跟随2.3   环绕2.2  航点2.1）飞行状态为互斥，协议每次只会返回一个
 *3. 飞行中
 *4. 遥控器已连接
 *5. 设备已连接
 */
+(NSString *)VisonProtocolGetThePriorityTipFromArr:(NSMutableArray *)tipArr;


/// 获取拍照图片大小（在这里做修改）
+(CGSize)getPicSize;

/// 获取录像分辨率大小（在这里做修改）
+(CGSize)getVideoSize;

+(unsigned long long)getMS;

/// 获取当前图片名字
+(NSString *)getPicName;

/// 拍摄影片，获取存储路径
+(NSString *)getMoviewPath;

/// 获取影片首帧
/// @param moviewPath 影片地址
+(UIImage *)getMoviewFirstFrameImageWithPath:(NSString *)moviewPath;

/// 点击录音声音
+(void)playSoundwithRecordAction;

/// 点击拍照声音
+(void)playSoundwithTakePhotoAction;

/// 点击拍照声音（手势拍照的倒数）
+(void)playSoundwithTakePhotoActionReciprocal;

/// 保存输出日志
+(void)redirectNSlogToDocumentFolder;

/// 获得本机当前语言
+(NSString *)getPreferredLanguage;

/// 获取视频首帧图像
/// @param videoURL 视频地址
+(UIImage *)getThumbImageFromVideoURL:(NSURL *)videoURL;

/// 是否使用硬解码
+(BOOL)isNotUseVTB_DECODE;

/// 保存图片到相册
/// @param image 保存的图片
/// @param complete 完成回调
+(void)saveImage:(UIImage *)image
toPhotoAlbumComplete:(void(^)(void))complete;

/// 保存影片到相册
/// @param moviewPath 保存路径
/// @param complete 完成回调
+(void)saveMoviewToPhotoAlbum:(NSString *)moviewPath
                     complete:(void(^)(void))complete;

/// 视频混合音乐
/// @param sourcePath 视频源文件路径
/// @param musicName 本地音乐名
/// @param block 完成回调
+(void)video:(NSString *)sourcePath
    mixMusic:(NSString *)musicName
complateBlock:(void(^)(void))block;

/// 所有的老版本的硬件类型名称
+(NSArray *)getTotalOldVersionHardwareNames;

/// YUVCVPixelBufferRef拷贝
/// @param pixelBuffer 源CVPixelBufferRef
+(CVPixelBufferRef)YUVBufferCopyWithPixelBuffer:(CVPixelBufferRef)pixelBuffer;

/// GBRAPixelBufferRef拷贝
/// @param pixelBuffer 源CVPixelBufferRef
+(CVPixelBufferRef)BGRABufferCopyWithPixelBuffer:(CVPixelBufferRef)pixelBuffer;

/// image 转换 pixelbuf
/// @param image image
+(CVPixelBufferRef)pixelBufferFromCGImage:(CGImageRef)image;

/// pixelbuf 转换 image
/// @param pixelBuffer pixelbuf
+(UIImage *)imageFromPixelBuffer:(CVPixelBufferRef)pixelBuffer;

@end


