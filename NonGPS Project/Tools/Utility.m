//
//  Utility.m
//  博坦创新
//
//  Created by Xu pan on 2019/5/22.
//  Copyright © 2019 Xu pan. All rights reserved.
//

#import "Utility.h"
#import <sys/utsname.h>
#import <AVFoundation/AVFoundation.h>
#import "JJFile.h"
#import "WIFIDeviceModel.h"
#import "JJMediaManager.h"
#import "UIImage+KTCategory.h"
#import "FfmpegUntils.h"

#define jzA 6378245.0
#define jzEE 0.00669342162296594323

#define RANGE_LON_MAX 137.8347
#define RANGE_LON_MIN 72.004
#define RANGE_LAT_MAX 55.8271
#define RANGE_LAT_MIN 0.8293
#define LAT_OFFSET_0(x,y) -100.0 + 2.0 * x + 3.0 * y + 0.2 * y * y + 0.1 * x * y + 0.2 * sqrt(fabs(x))
#define LAT_OFFSET_1 (20.0 * sin(6.0 * x * M_PI) + 20.0 * sin(2.0 * x * M_PI)) * 2.0 / 3.0
#define LAT_OFFSET_2 (20.0 * sin(y * M_PI) + 40.0 * sin(y / 3.0 * M_PI)) * 2.0 / 3.0
#define LAT_OFFSET_3 (160.0 * sin(y / 12.0 * M_PI) + 320 * sin(y * M_PI / 30.0)) * 2.0 / 3.0
#define LON_OFFSET_0(x,y) 300.0 + x + 2.0 * y + 0.1 * x * x + 0.1 * x * y + 0.1 * sqrt(fabs(x))
#define LON_OFFSET_1 (20.0 * sin(6.0 * x * M_PI) + 20.0 * sin(2.0 * x * M_PI)) * 2.0 / 3.0
#define LON_OFFSET_2 (20.0 * sin(x * M_PI) + 40.0 * sin(x / 3.0 * M_PI)) * 2.0 / 3.0
#define LON_OFFSET_3 (150.0 * sin(x / 12.0 * M_PI) + 300.0 * sin(x / 30.0 * M_PI)) * 2.0 / 3.0

@implementation Utility

+(NSString *)formatTimeInterval:(NSInteger)seconds{
    seconds = MAX(0, seconds);
    
    NSInteger s = seconds;
    NSInteger m = s / 60;
    NSInteger h = m / 60;
    s = s % 60;
    m = m % 60;
    
    if (h > 0) {
        return [NSString stringWithFormat:@"%ld:%0.2ld:%0.2ld", (long)h,(long)m,(long)s];
    }
    return [NSString stringWithFormat:@"%0.2ld:%0.2ld",(long)m,(long)s];
}


+ (NSString*)hardwareDescription{

    NSString *modelIdentifier;
    struct utsname systemInfo;
    uname(&systemInfo);
    modelIdentifier = [NSString stringWithCString:systemInfo.machine
                                    encoding:NSUTF8StringEncoding];
    //iPhone
    if ([modelIdentifier isEqualToString:@"iPhone1,1"])      return @"iPhone";
    else if ([modelIdentifier isEqualToString:@"iPhone1,2"]) return @"iPhone 3G";
    else if ([modelIdentifier isEqualToString:@"iPhone2,1"]) return @"iPhone 3GS";
    else if ([modelIdentifier isEqualToString:@"iPhone3,1"]) return @"iPhone 3GS";
    else if ([modelIdentifier isEqualToString:@"iPhone3,2"]) return @"iPhone 4";
    else if ([modelIdentifier isEqualToString:@"iPhone3,3"]) return @"iPhone 4";
    else if ([modelIdentifier isEqualToString:@"iPhone4,1"]) return @"iPhone 4S";
    else if ([modelIdentifier isEqualToString:@"iPhone5,1"]) return @"iPhone 5";
    else if ([modelIdentifier isEqualToString:@"iPhone5,2"]) return @"iPhone 5";
    else if ([modelIdentifier isEqualToString:@"iPhone5,3"]) return @"iPhone 5c";
    else if ([modelIdentifier isEqualToString:@"iPhone5,4"]) return @"iPhone 5c";
    else if ([modelIdentifier isEqualToString:@"iPhone6,1"]) return @"iPhone 5s";
    else if ([modelIdentifier isEqualToString:@"iPhone6,2"]) return @"iPhone 5s";
    else if ([modelIdentifier isEqualToString:@"iPhone7,2"]) return @"iPhone 6";
    else if ([modelIdentifier isEqualToString:@"iPhone7,1"]) return @"iPhone 6 Plus";
    else if ([modelIdentifier isEqualToString:@"iPhone8,1"]) return @"iPhone 6s";
    else if ([modelIdentifier isEqualToString:@"iPhone8,2"]) return @"iPhone 6s Plus";
    else if ([modelIdentifier isEqualToString:@"iPhone8,4"]) return @"iPhone SE";
    else if ([modelIdentifier isEqualToString:@"iPhone9,1"]) return @"iPhone 7";
    else if ([modelIdentifier isEqualToString:@"iPhone9,2"]) return @"iPhone 7 Plus";
    else if ([modelIdentifier isEqualToString:@"iPhone9,3"]) return @"iPhone 7";
    else if ([modelIdentifier isEqualToString:@"iPhone9,4"]) return @"iPhone 7 Plus";
    else if ([modelIdentifier isEqualToString:@"iPhone10,1"]) return @"iPhone 8";
    else if ([modelIdentifier isEqualToString:@"iPhone10,4"]) return @"iPhone 8";
    else if ([modelIdentifier isEqualToString:@"iPhone10,2"]) return @"iPhone 8 Plus";
    else if ([modelIdentifier isEqualToString:@"iPhone10,5"]) return @"iPhone 8 Plus";
    else if ([modelIdentifier isEqualToString:@"iPhone10,3"]) return @"iPhone X";
    else if ([modelIdentifier isEqualToString:@"iPhone10,6"]) return @"iPhone X";
    else if ([modelIdentifier isEqualToString:@"iPhone11,8"]) return @"iPhone XR";
    else if ([modelIdentifier isEqualToString:@"iPhone11,2"]) return @"iPhone XS";
    else if ([modelIdentifier isEqualToString:@"iPhone11,4"]) return @"iPhone XS Max";
    else if ([modelIdentifier isEqualToString:@"iPhone11,6"]) return @"iPhone XS Max";
    else if ([modelIdentifier isEqualToString:@"iPhone12,1"]) return @"iPhone 11";
    else if ([modelIdentifier isEqualToString:@"iPhone12,3"]) return @"iPhone 11 Pro";
    else if ([modelIdentifier isEqualToString:@"iPhone12,5"]) return @"iPhone 11 Pro Max";
    else if ([modelIdentifier isEqualToString:@"iPhone13,1"]) return @"iPhone 12 mini";
    else if ([modelIdentifier isEqualToString:@"iPhone13,2"]) return @"iPhone 12";
    else if ([modelIdentifier isEqualToString:@"iPhone13,3"]) return @"iPhone 12 Pro";
    else if ([modelIdentifier isEqualToString:@"iPhone13,4"]) return @"iPhone 12 Pro Max";
    
    //iPod touch
    else if ([modelIdentifier isEqualToString:@"iPod1,1"]) return @"iPod touch";
    else if ([modelIdentifier isEqualToString:@"iPod2,1"]) return @"iPod touch 2G";
    else if ([modelIdentifier isEqualToString:@"iPod3,1"]) return @"iPod touch 3G";
    else if ([modelIdentifier isEqualToString:@"iPod4,1"]) return @"iPod touch 4G";
    else if ([modelIdentifier isEqualToString:@"iPod5,1"]) return @"iPod touch 5G";
    else if ([modelIdentifier isEqualToString:@"iPod7,1"]) return @"iPod touch 6G";
    //iPad
    else if ([modelIdentifier isEqualToString:@"iPad1,1"]) return @"iPad";
    else if ([modelIdentifier isEqualToString:@"iPad2,1"]) return @"iPad 2";
    else if ([modelIdentifier isEqualToString:@"iPad2,2"]) return @"iPad 2";
    else if ([modelIdentifier isEqualToString:@"iPad2,3"]) return @"iPad 2";
    else if ([modelIdentifier isEqualToString:@"iPad2,4"]) return @"iPad 2";
    else if ([modelIdentifier isEqualToString:@"iPad3,1"]) return @"iPad 3";
    else if ([modelIdentifier isEqualToString:@"iPad3,2"]) return @"iPad 3";
    else if ([modelIdentifier isEqualToString:@"iPad3,3"]) return @"iPad 3";
    else if ([modelIdentifier isEqualToString:@"iPad3,4"]) return @"iPad 4";
    else if ([modelIdentifier isEqualToString:@"iPad3,5"]) return @"iPad 4";
    else if ([modelIdentifier isEqualToString:@"iPad3,6"]) return @"iPad 4";
    else if ([modelIdentifier isEqualToString:@"iPad4,1"]) return @"iPad Air";
    else if ([modelIdentifier isEqualToString:@"iPad4,2"]) return @"iPad Air";
    else if ([modelIdentifier isEqualToString:@"iPad4,3"]) return @"iPad Air";
    else if ([modelIdentifier isEqualToString:@"iPad5,3"]) return @"iPad Air2";
    else if ([modelIdentifier isEqualToString:@"iPad5,4"]) return @"iPad Air2";
    else if ([modelIdentifier isEqualToString:@"iPad6,7"]) return @"iPad Pro";
    else if ([modelIdentifier isEqualToString:@"iPad6,8"]) return @"iPad Pro";
    else if ([modelIdentifier isEqualToString:@"iPad6,3"]) return @"iPad Pro";
    else if ([modelIdentifier isEqualToString:@"iPad6,4"]) return @"iPad Pro";
    //iPad mini
    else if ([modelIdentifier isEqualToString:@"iPad2,5"]) return @"iPad mini";
    else if ([modelIdentifier isEqualToString:@"iPad2,6"]) return @"iPad mini";
    else if ([modelIdentifier isEqualToString:@"iPad2,7"]) return @"iPad mini";
    else if ([modelIdentifier isEqualToString:@"iPad4,4"]) return @"iPad mini2";
    else if ([modelIdentifier isEqualToString:@"iPad4,5"]) return @"iPad mini2";
    else if ([modelIdentifier isEqualToString:@"iPad4,6"]) return @"iPad mini2";
    else if ([modelIdentifier isEqualToString:@"iPad4,7"]) return @"iPad mini3";
    else if ([modelIdentifier isEqualToString:@"iPad4,8"]) return @"iPad mini3";
    else if ([modelIdentifier isEqualToString:@"iPad4,9"]) return @"iPad mini3";
    else if ([modelIdentifier isEqualToString:@"iPad5,1"]) return @"iPad mini4";
    else if ([modelIdentifier isEqualToString:@"iPad5,2"]) return @"iPad mini4";
    //Apple TV
    else if ([modelIdentifier isEqualToString:@"AppleTV2,1"]) return @"Apple TV 2G";
    else if ([modelIdentifier isEqualToString:@"AppleTV3,1"]) return @"Apple TV 3G";
    else if ([modelIdentifier isEqualToString:@"AppleTV3,2"]) return @"Apple TV 3G";
    else if ([modelIdentifier isEqualToString:@"AppleTV5,3"]) return @"Apple TV 4G";
    //Apple Watch
    else if ([modelIdentifier isEqualToString:@"Watch1,1"]) return @"Apple Watch";
    else if ([modelIdentifier isEqualToString:@"Watch1,2"]) return @"Apple Watch";
    else if ([modelIdentifier isEqualToString:@"Watch2,6"]) return @"Apple Watch Series 1";
    else if ([modelIdentifier isEqualToString:@"Watch2,7"]) return @"Apple Watch Series 1";
    else if ([modelIdentifier isEqualToString:@"Watch2,3"]) return @"Apple Watch Series 2";
    else if ([modelIdentifier isEqualToString:@"Watch2,4"]) return @"Apple Watch Series 2";
    // Simulator
    else if ([modelIdentifier hasSuffix:@"86"] || [modelIdentifier isEqual:@"x86_64"])
    {
        BOOL smallerScreen = ([[UIScreen mainScreen] bounds].size.width < 768.0);
        return (smallerScreen ? @"iPhone Simulator" : @"iPad Simulator");
    }
    
    
    else {
        return @"iOS Device";
    }
    
}

+(NSString *)getAppCurrentName{
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    return [infoDictionary objectForKey:@"CFBundleDisplayName"];;
}

+ (NSString *)getAppCurrentVersion
{
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    return [infoDictionary objectForKey:@"CFBundleShortVersionString"];
}
+(NSString *)getAppCurrentSystemVersion
{
    NSString* phoneVersion = [[UIDevice currentDevice] systemVersion];
    return phoneVersion;
}

#pragma mark -对一个字符串进行base64编码，并返回
+(NSString *)base64EncodeString:(NSString *)string{
    //1、先转换成二进制数据
    NSData *data =[string dataUsingEncoding:NSUTF8StringEncoding];
    //2、对二进制数据进行base64编码，完成后返回字符串
    return [data base64EncodedStringWithOptions:0];
}
+(NSString *)base64DecodeString:(NSString *)string{
    //注意：该字符串是base64编码后的字符串
    //1、转换为二进制数据（完成了解码的过程）
    NSData *data=[[NSData alloc]initWithBase64EncodedString:string options:0];
    //2、把二进制数据转换成字符串
    return [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
}


#pragma mark - 获取沙盒视频时长
+(NSString *)getVideoInfoWithSourcePath:(NSString *)path{
    AVURLAsset * asset = [AVURLAsset assetWithURL:[NSURL fileURLWithPath:path]];
    CMTime   time = [asset duration];
    int seconds = ceil(time.value/time.timescale);
//    NSInteger   fileSize = [[NSFileManager defaultManager] attributesOfItemAtPath:path error:nil].fileSize;
    
//    return @{@"size" : @(fileSize),
//             @"duration" : @(seconds)};
    
    return [Utility formatTimeInterval:seconds];
}

+ (UIViewController *)topController {
    
    UIViewController *topC = [self topViewController:[[UIApplication sharedApplication].keyWindow rootViewController]];
    while (topC.presentedViewController) {
        topC = [self topViewController:topC.presentedViewController];
    }
    return topC;
}
+ (UIViewController *)topViewController:(UIViewController *)controller {
    if ([controller isKindOfClass:[UINavigationController class]]) {
        return [self topViewController:[(UINavigationController *)controller topViewController]];
    } else if ([controller isKindOfClass:[UITabBarController class]]) {
        return [self topViewController:[(UITabBarController *)controller selectedViewController]];
    } else {
        return controller;
    }
}

+(UIViewController *)getCurrentViewController{
    UIViewController *currentViewController = nil;
    
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    NSInteger code = window.windowLevel/1000;
    
    if (window.windowLevel != UIWindowLevelNormal && code != 2 )
    {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows)
        {
            if (tmpWin.windowLevel == UIWindowLevelNormal)
            {
                window = tmpWin;
                break;
            }
        }
    }
    if (window.windowLevel == UIWindowLevelAlert || code == 2) {
        UIWindow * window = [UIApplication sharedApplication].delegate.window;
        UIView *frontView = [window.rootViewController view];
        currentViewController = (UIViewController *)[frontView nextResponder];
    } else {
        UIView *frontView = [[window subviews] objectAtIndex:0];
        id nextResponder = [frontView nextResponder];
        
        if ([nextResponder isKindOfClass:[UIViewController class]])
            currentViewController = nextResponder;
        else
            currentViewController = window.rootViewController;
    }
    
    while ([currentViewController presentedViewController])
    {
        currentViewController = [currentViewController presentedViewController];
        if ([currentViewController isKindOfClass:[UINavigationController class]])
        {
            currentViewController = [(UINavigationController *)currentViewController visibleViewController];
        }
    }
    if ([currentViewController isKindOfClass:[UINavigationController class]])
    {
        currentViewController = [(UINavigationController *)currentViewController visibleViewController];
    }
    return currentViewController;
}

#pragma mark - 图片大于两兆压缩
+(NSData *)imageDataWithImage:(UIImage *)image{
    NSData *data = UIImageJPEGRepresentation(image,1.0);
    CGFloat length = [data length]/(1024.0 * 1024.0) ;
    if (length > 2) {
        data = UIImageJPEGRepresentation(image, 0.6);
    }
    return data;
}


#pragma mark - 坐标转换
+ (BOOL)outOfChina:(double)lat bdLon:(double)lon
{
    if (lon < RANGE_LON_MIN || lon > RANGE_LON_MAX)
        return true;
    if (lat < RANGE_LAT_MIN || lat > RANGE_LAT_MAX)
        return true;
    return false;
}

/*
//高德转wgs84
+ (CLLocationCoordinate2D)gcj02Decrypt:(double)gjLat gjLon:(double)gjLon {
    CLLocationCoordinate2D  gPt = [self gcj02Encrypt:gjLat bdLon:gjLon];
    double dLon = gPt.longitude - gjLon;
    double dLat = gPt.latitude - gjLat;
    CLLocationCoordinate2D pt;
    pt.latitude = gjLat - dLat;
    pt.longitude = gjLon - dLon;
    return pt;
}

//wgs84转高德
+ (CLLocationCoordinate2D)gcj02Encrypt:(double)ggLat bdLon:(double)ggLon
{
    CLLocationCoordinate2D resPoint;
    double mgLat;
    double mgLon;
        
    if (![Utility getUserInfo].correctOffsetTag) { //未知情况的话用不精确的方法判断是否要纠偏
        
        CLLocation *curLocation = [Utility getUserInfo].currentLocation;
        CLLocationCoordinate2D coord = curLocation.coordinate;
        if(![[[APReverseGeocoding defaultGeocoding] geocodeCountryWithCoordinate:coord].shortCode.lowercaseString isEqualToString:@"cn"]){ //不在中国不需纠偏
            resPoint.latitude = ggLat;
            resPoint.longitude = ggLon;
            return resPoint;
        }else{
            double dLat = [Utility transformLat:(ggLon - 105.0)bdLon:(ggLat - 35.0)];
            double dLon = [Utility transformLon:(ggLon - 105.0) bdLon:(ggLat - 35.0)];
            double radLat = ggLat / 180.0 * M_PI;
            double magic = sin(radLat);
            magic = 1 - jzEE * magic * magic;
            double sqrtMagic = sqrt(magic);
            dLat = (dLat * 180.0) / ((jzA * (1 - jzEE)) / (magic * sqrtMagic) * M_PI);
            dLon = (dLon * 180.0) / (jzA / sqrtMagic * cos(radLat) * M_PI);
            mgLat = ggLat + dLat;
            mgLon = ggLon + dLon;
            
            resPoint.latitude = mgLat;
            resPoint.longitude = mgLon;
            return resPoint;
        }
    }else{
        
        if ([Utility getUserInfo].correctOffsetTag == 1) { //不需要纠偏
            resPoint.latitude = ggLat;
            resPoint.longitude = ggLon;
            return resPoint;
        }
        double dLat = [Utility transformLat:(ggLon - 105.0)bdLon:(ggLat - 35.0)];
        double dLon = [Utility transformLon:(ggLon - 105.0) bdLon:(ggLat - 35.0)];
        double radLat = ggLat / 180.0 * M_PI;
        double magic = sin(radLat);
        magic = 1 - jzEE * magic * magic;
        double sqrtMagic = sqrt(magic);
        dLat = (dLat * 180.0) / ((jzA * (1 - jzEE)) / (magic * sqrtMagic) * M_PI);
        dLon = (dLon * 180.0) / (jzA / sqrtMagic * cos(radLat) * M_PI);
        mgLat = ggLat + dLat;
        mgLon = ggLon + dLon;
        
        resPoint.latitude = mgLat;
        resPoint.longitude = mgLon;
        return resPoint;
    }
}

+ (double)transformLat:(double)x bdLon:(double)y{
    double ret = LAT_OFFSET_0(x, y);
    ret += LAT_OFFSET_1;
    ret += LAT_OFFSET_2;
    ret += LAT_OFFSET_3;
    return ret;
}
+ (double)transformLon:(double)x bdLon:(double)y{
    double ret = LON_OFFSET_0(x, y);
    ret += LON_OFFSET_1;
    ret += LON_OFFSET_2;
    ret += LON_OFFSET_3;
    return ret;
}
*/

/*
+(void)reSetWindowRootViewController{
    UserInfoSingle *user = [Utility getUserInfo];
    //是登录的话
    if (user.isLogin) {
        mainTabbarController *tabBarController = [[mainTabbarController alloc]initWithNomalImageArr:@[@"ic_equipment_unselected",
                                                                                                      @"ic_editor_unselected",
                                                                                                      @"ic_me_unselected"]
                                                                                andSelectedImageArr:@[@"ic_equipment_selected",
                                                                                                      @"ic_editor_selected",
                                                                                                      @"ic_me_selected"]
                                                                                        andTitleArr:@[NSLocalizedString(@"Equipment", nil),
                                                                                                      NSLocalizedString(@"Editor", nil),
                                                                                                      NSLocalizedString(@"Me", nil)]];
        
        UIWindow *window = [UIApplication sharedApplication].keyWindow;
        window.rootViewController = tabBarController;
        
    }else{
        UIWindow *window = [UIApplication sharedApplication].keyWindow;
        window.rootViewController = [[UINavigationController alloc]initWithRootViewController:[[LoginViewController alloc]init]];
    }
}
 */

#pragma mark - 获取用户单利
+(UserInfoSingle *)getUserInfo{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingString:@"/userInfo.plist"];
    
    if([NSKeyedUnarchiver unarchiveObjectWithFile:filePath]){
        return [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
    }else{
        return [[UserInfoSingle alloc]init];
    }
}
#pragma mark - 保存用户信息
+(void)saveUserInfo:(UserInfoSingle *)userInfo{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingString:@"/userInfo.plist"];
    //判断之前有无保存
    if([NSKeyedUnarchiver unarchiveObjectWithFile:filePath]){
        //删除之前文件
        NSFileManager *defaultManager = [NSFileManager defaultManager];
        if ([defaultManager isDeletableFileAtPath:filePath]){
            [defaultManager removeItemAtPath:filePath error:nil];
        }
        if ([NSKeyedArchiver archiveRootObject:userInfo toFile:filePath]){
        }
    }else{
        if ([NSKeyedArchiver archiveRootObject:userInfo toFile:filePath]){
        }
    }
}
#pragma mark - 删除用户信息
+(void)removeUserInfo{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingString:@"/userInfo.plist"];
    //判断之前有无保存
    if([NSKeyedUnarchiver unarchiveObjectWithFile:filePath]){
        //删除之前文件
        NSFileManager *defaultManager = [NSFileManager defaultManager];
        if ([defaultManager isDeletableFileAtPath:filePath]){
            [defaultManager removeItemAtPath:filePath error:nil];
        }
    }
}


+(NSString *)formattedOutputWithFile:(NSString *)file line:(NSInteger)line{
    
    NSString *fileStr = [NSString stringWithFormat:@"%ld:%@",(long)line,file];
    if (fileStr.length < 45) {
        NSMutableString *str = [NSMutableString stringWithString:fileStr];
        for (NSInteger i = fileStr.length; i < 45; i++) {
            [str appendString:@"-"];
        }
        fileStr = str;
        
    }else{
        fileStr = [fileStr substringToIndex:45];
    }
    return fileStr;
}

//获取时间
+ (NSString*)getCurrentDataString{
    NSDate* now = [NSDate date];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyy-MM-dd HH:mm:ss"];
    NSString *dateString = [df stringFromDate:now];
    return dateString;
}

#pragma mark - 纬盛协议提示优先级判断
+(NSString *)VisonProtocolGetThePriorityTipFromArr:(NSMutableArray *)tipArr{
    
    NSMutableArray *price = [NSMutableArray array];
    for (id objc in tipArr) {
        if ([objc isKindOfClass:NSString.class]) {
            [price addObject:[NSNumber numberWithFloat:[objc floatValue]]];
        }else if ([objc isKindOfClass:[NSNumber class]]){
            [price addObject:objc];
        }
    }
    
    NSDictionary *dict = @{@"5":@"设备已连接",
                           @"4":@"遥控器已连接",
                           @"3.1":@"等待GPS信号",
                           @"3":@"飞行中",
                           @"2.1":@"航点飞行中",
                           @"2.2":@"环绕飞行中",
                           @"2.3":@"跟随飞行中",
                           @"2.4":@"返航中",
                           @"2.5":@"下降中",
                           @"1":@"飞机低电"};
    
    return dict[[NSString stringWithFormat:@"%@",[price valueForKeyPath:@"@min.floatValue"]]];
}

+(CGSize)getPicSize{
    return [[WIFIDeviceModel shareInstance] getPixelSize:[WIFIDeviceModel shareInstance].mainLensTransmissionPixelSize];
}

+(CGSize)getVideoSize{
    return [[WIFIDeviceModel shareInstance] getPixelSize:[WIFIDeviceModel shareInstance].mainLensTransmissionPixelSize];
}

+(unsigned long long)getMS{
    struct timeval l_tv;
    gettimeofday(&l_tv,NULL);
    unsigned long long l_ret = 0;
    l_ret = (l_tv.tv_sec&0xFFFFFFFF)*1000;
    l_ret += (l_tv.tv_usec/1000);
    return l_ret;
}

#pragma mark - 获取当前图片名字
+(NSString *)getPicName{
    NSDate* now = [NSDate date];
    NSCalendar *cal = [NSCalendar currentCalendar];
    
    unsigned int unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit |NSSecondCalendarUnit;
    NSDateComponents *dd = [cal components:unitFlags fromDate:now];
    NSInteger y1 = [dd year];
    NSInteger m1 = [dd month];
    NSInteger d1 = [dd day];
    
    NSInteger hour1 = [dd hour];
    NSInteger min1 = [dd minute];
    NSInteger sec1 = [dd second];
    
    NSString *PicName = [NSString stringWithFormat:@"%04ld%02ld%02ld%02ld%02ld%02ld",(long)y1,(long)m1,(long)d1,(long)hour1,(long)min1,(long)sec1];
    
    return PicName;
}

#pragma mark - 获取影片路径
+(NSString *)getMoviewPath{
    NSString *name = [Utility getPicName];
    NSString *movieName = [NSString stringWithFormat:@"%@.%@",name,@"mp4"];
    NSString *dic = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES) objectAtIndex:0];
    NSString *str = [dic stringByAppendingPathComponent:@"Videos"];
    
    BOOL  isDir = NO;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL existed = [fileManager fileExistsAtPath:str isDirectory:&isDir];
    if ( !(isDir == YES && existed == YES) ){
        [fileManager createDirectoryAtPath:str withIntermediateDirectories:YES attributes:nil error:nil];
    }
    movieName = [str stringByAppendingPathComponent:movieName];
    
    return movieName;
}

#pragma mark - 获取影片首帧
+(UIImage *)getMoviewFirstFrameImageWithPath:(NSString *)moviewPath{
    
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:[NSURL fileURLWithPath:moviewPath] options:nil];
    AVAssetImageGenerator *gen = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    gen.appliesPreferredTrackTransform = YES;
    //    CMTime time = CMTimeMakeWithSeconds(0.0, 600);
    CMTime time = CMTimeMake(1, 10);
    NSError *error = nil;
    CMTime actualTime;
    CGImageRef image = [gen copyCGImageAtTime:time actualTime:&actualTime error:&error];
    UIImage *thumb = [[UIImage alloc] initWithCGImage:image];
    CGImageRelease(image);
    return thumb;
}

#pragma mark - 点击录音声音
+(void)playSoundwithRecordAction{
    NSError *error;
    AVAudioSession * audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayback error: &error];
    
    NSString *path = [NSString stringWithFormat:@"%@%@",[[NSBundle mainBundle] resourcePath],@"/video_rec.mp3"];
    SystemSoundID soundID;
    NSURL *filePath = [NSURL fileURLWithPath:path isDirectory:NO];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)filePath, &soundID);
    AudioServicesPlaySystemSound(soundID);
}

#pragma mark - 点击拍照声音
+(void)playSoundwithTakePhotoAction{
    NSError *error;
    AVAudioSession * audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayback error: &error];
    
    NSString *path = [NSString stringWithFormat:@"%@%@",[[NSBundle mainBundle] resourcePath],@"/1.wav"];
    SystemSoundID soundID;
    NSURL *filePath = [NSURL fileURLWithPath:path isDirectory:NO];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)filePath, &soundID);
    AudioServicesPlaySystemSound(soundID);
}

+(void)playSoundwithTakePhotoActionReciprocal{
    NSError *error;
    AVAudioSession * audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayback error: &error];
    
    NSString *path = [NSString stringWithFormat:@"%@%@",[[NSBundle mainBundle] resourcePath],@"/countdown.mp3"];
    SystemSoundID soundID;
    NSURL *filePath = [NSURL fileURLWithPath:path isDirectory:NO];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)filePath, &soundID);
    AudioServicesPlaySystemSound(soundID);
}

#pragma mark - 保存输出日志
+(void)redirectNSlogToDocumentFolder{
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    // app名称
    NSString *app_Name = [infoDictionary objectForKey:@"CFBundleDisplayName"];
    // 获取Documents目录路径
    NSString *logDirPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    
    NSDate *curDate = [NSDate date];
    NSDateFormatter *dateformatter = [[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateStr = [dateformatter stringFromDate:curDate];
    
    NSString *fileName = [NSString stringWithFormat:@"/Logs/%@_log_%@.txt",app_Name,dateStr];
    NSString *logFilePath = [logDirPath stringByAppendingPathComponent:fileName];
    
    // 将log输入到文件
    freopen([logFilePath cStringUsingEncoding:NSASCIIStringEncoding], "a+", stdout);
    freopen([logFilePath cStringUsingEncoding:NSASCIIStringEncoding], "a+", stderr);
}

#pragma mark - 获得本机当前语言
+(NSString *)getPreferredLanguage{
    NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
    NSArray* languages = [defs objectForKey:@"AppleLanguages"];
    NSString* preferredLang = [languages objectAtIndex:0];
    return preferredLang;
}

#pragma mark - 获取视频首帧图像
+(UIImage *)getThumbImageFromVideoURL:(NSURL *)videoURL{
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoURL options:nil];
    AVAssetImageGenerator *gen = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    gen.appliesPreferredTrackTransform = YES;
    //    CMTime time = CMTimeMakeWithSeconds(0.0, 600);
    CMTime time = CMTimeMake(1, 10);
    NSError *error = nil;
    CMTime actualTime;
    CGImageRef image = [gen copyCGImageAtTime:time actualTime:&actualTime error:&error];
    UIImage *thumb = [[UIImage alloc] initWithCGImage:image];
    CGImageRelease(image);
    return thumb;
}

#pragma mark - 不使用硬解码
+(BOOL)isNotUseVTB_DECODE{
    if ([WIFIDeviceModel shareInstance].chipModel == ChipModel_QuanZhi_872) {
        return YES;
    }
    return NO;
}

#pragma mark - 保存图片到相册
+(void)saveImage:(UIImage *)image toPhotoAlbumComplete:(void (^)(void))complete{
    NSString *path = [Utility getPicName];
    [JJMediaManager savePhoto:image withName:path addToPhotoAlbum:YES callback:^{
        if (complete) {
            complete();
        }
    }];
}
#pragma mark - 保存影片到相册
+(void)saveMoviewToPhotoAlbum:(NSString *)moviewPath complete:(void (^)(void))complete{
    
    JJFile *file = [[JJFile alloc] init];
    file.videoPath = moviewPath;
    file.name = moviewPath.lastPathComponent;
    
    NSDictionary *attr = [[NSFileManager defaultManager] attributesOfItemAtPath:moviewPath error:nil];
    file.size = [attr[NSFileSize] integerValue];
    file.type = JJ_Video;
    [JJMediaManager saveFileToAlbum:@[file] callback:^(int ret) {
        UIImage* image = [Utility getMoviewFirstFrameImageWithPath:moviewPath];
        if (!image){
            image = [UIImage imageNamed:@"videopic"];
        }
        [JJMediaManager saveVideoPhoto:image withName:moviewPath.lastPathComponent callback:nil];
        image = [image imageScaleAndCropToMaxSize:CGSizeMake(75,75)];
        [JJMediaManager saveVideoThumb:image withName:moviewPath.lastPathComponent callback:nil];
        complete();
    }];
}

#pragma mark - 混合音乐
+(void)video:(NSString *)sourcePath mixMusic:(NSString *)musicName complateBlock:(void (^)(void))block{
    
    NSString *musicPath  = [[NSBundle mainBundle] pathForResource:musicName ofType:@"m4a"];
    NSString *filename = [NSString stringWithFormat:@"mix%@",sourcePath.lastPathComponent];
    NSString *out_filePath = [sourcePath stringByReplacingOccurrencesOfString:sourcePath.lastPathComponent withString:filename];
    [FfmpegUntils ffmpegWithvideo_url:[sourcePath UTF8String] music_url:[musicPath UTF8String] out_video_url:[out_filePath UTF8String]];
    //删除原文件
    JJFile *file = [[JJFile alloc] init];
    file.name = sourcePath.lastPathComponent;
    file.type = JJ_Video;
    [JJMediaManager deleteFile:@[file] callback:nil];
    //写入新文件
    JJFile *newFile = [[JJFile alloc] init];
    newFile.videoPath = out_filePath;
    newFile.name = out_filePath.lastPathComponent;
    NSDictionary *newAttr = [[NSFileManager defaultManager] attributesOfItemAtPath:out_filePath error:nil];
    newFile.size = [newAttr[NSFileSize] integerValue];
    newFile.type = JJ_Video;
    
    [JJMediaManager saveFileToAlbum:@[newFile] callback:^(int ret) {
        UIImage* image = [Utility getMoviewFirstFrameImageWithPath:out_filePath];
        if (!image){
            image = [UIImage imageNamed:@"ico_media_xiangce_nor"];
        }
        [JJMediaManager saveVideoPhoto:image withName:out_filePath.lastPathComponent callback:nil];
        image = [image imageScaleAndCropToMaxSize:CGSizeMake(75,75)];
        [JJMediaManager saveVideoThumb:image withName:out_filePath.lastPathComponent callback:nil];
    }];
    
    if (block) {
        block();
    }
}

#pragma mark - 获取所有老版本版名称
+(NSArray *)getTotalOldVersionHardwareNames{
    return @[@"VGA\0\0",
             @"MJVGA\0",
             @"VSVGA\0",
             @"VGA2",
             @"720P\0",
             @"1080P",
             @"UDP720P",
             @"UDP720PYU",
             @"RTSP1080P",
             @"RTSP720P",
             @"UDP2K",
             @"UDP2K-30",
             @"UDP27K",
             @"UDP4K",
             @"1080P-5G",
             @"1080P-5g",
             @"720P-5G",
             @"720P-5g",
             @"1080P-I",
             @"HI2K",
             @"HI4K",
             @"FH8626_ssv6x5x_24g_1920_1280",
             @"FH8626_ssv6x5x_5g_1920_1280",
             @"8856_8812cu_4000_1280",
             @"Mr100_ssv6x5x_1920_1920",
             @"Mr100_ssv6x5x_1920_1280",
             @"Mr100_8801_1920_1920",
             @"Mr100_8801_1920_1280",
             @"Mr100_ssv6x5x_2048_2048",
             @"Mr100_8801_2048_2048",
             @"Mr100_ssv6x5x_4000_2048",
             @"Mr100_ssv6x5x_4000_1920",
             @"Mr100_ssv6x5x_4000_1280",
             @"Mr100_8801_4000_2048",
             @"Mr100_8801_4000_1920",
             @"Mr100_8801_4000_1280",
             @"872ET_320_320",
             @"872AT_640_640"];
}

#pragma mark - 复制YUV CVPixelBufferRef
+(CVPixelBufferRef)YUVBufferCopyWithPixelBuffer:(CVPixelBufferRef)pixelBuffer{
    
    // Get pixel buffer info
    CVPixelBufferLockBaseAddress(pixelBuffer, 0);
    int bufferWidth = (int)CVPixelBufferGetWidth(pixelBuffer);
    int bufferHeight = (int)CVPixelBufferGetHeight(pixelBuffer);
    OSType pixelFormat = CVPixelBufferGetPixelFormatType(pixelBuffer);
    
    NSDictionary *pixelBufferAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                           @(CVPixelBufferGetBytesPerRowOfPlane(pixelBuffer, 0)), kCVPixelBufferBytesPerRowAlignmentKey,
                                           nil];
    
    // Copy the pixel buffer
    CVPixelBufferRef pixelBufferCopy = NULL;
    CVReturn status = CVPixelBufferCreate(kCFAllocatorDefault,
                                          bufferWidth,
                                          bufferHeight,
                                          pixelFormat,
                                          (__bridge CFDictionaryRef)(pixelBufferAttributes),
                                          &pixelBufferCopy);
    if (status != kCVReturnSuccess) {
        NSLog(@"YUVBufferCopyWithPixelBuffer :: failed");
    }
    
    CVPixelBufferLockBaseAddress(pixelBufferCopy, 0);
    
    
    
    size_t bytesPerRowY = CVPixelBufferGetBytesPerRowOfPlane(pixelBufferCopy, 0);
    long Y_Height = CVPixelBufferGetHeightOfPlane(pixelBufferCopy, 0);
    // Y
    uint8_t *YDestPlane = (uint8_t *) CVPixelBufferGetBaseAddressOfPlane(pixelBufferCopy, 0);
    memset(YDestPlane, 0x80, Y_Height * bytesPerRowY);
    uint8_t *yPlane = CVPixelBufferGetBaseAddressOfPlane(pixelBuffer, 0);
    for (int row = 0; row < Y_Height; ++row) {
        memcpy(YDestPlane + row * bytesPerRowY,
                yPlane + row * bytesPerRowY,
                bytesPerRowY);
    }
 
    size_t bytesPerRowUV = CVPixelBufferGetBytesPerRowOfPlane(pixelBufferCopy, 1);
    long UV_Height = CVPixelBufferGetHeightOfPlane(pixelBufferCopy, 1);

    // Chrominance
    uint8_t *uvDestPlane = (uint8_t *) CVPixelBufferGetBaseAddressOfPlane(pixelBufferCopy, 1);
    memset(uvDestPlane, 0x80, UV_Height * bytesPerRowUV);
    uint8_t *uvPlane = CVPixelBufferGetBaseAddressOfPlane(pixelBuffer, 1);
    for (int row = 0; row < UV_Height; ++row) {
        memcpy(uvDestPlane + row * bytesPerRowUV,
                uvPlane + row * bytesPerRowUV,
                bytesPerRowUV);
    }
    
    /*
    uint8_t *yDestPlane = CVPixelBufferGetBaseAddressOfPlane(pixelBufferCopy, 0);
    //YUV
    uint8_t *yPlane = CVPixelBufferGetBaseAddressOfPlane(pixelBuffer, 0);
    memcpy(yDestPlane, yPlane, bufferWidth * bufferHeight);
    uint8_t *uvDestPlane = CVPixelBufferGetBaseAddressOfPlane(pixelBufferCopy, 1);
    uint8_t *uvPlane = CVPixelBufferGetBaseAddressOfPlane(pixelBuffer, 1);
    memcpy(uvDestPlane, uvPlane, bufferWidth * bufferHeight/2);
    */
    CVPixelBufferUnlockBaseAddress(pixelBuffer, 0);
    CVPixelBufferUnlockBaseAddress(pixelBufferCopy, 0);
    
    return pixelBufferCopy;
}


#pragma mark - 复制BGRA CVPixelBufferRef
+(CVPixelBufferRef)BGRABufferCopyWithPixelBuffer:(CVPixelBufferRef)pixelBuffer{
    // Get pixel buffer info
    CVPixelBufferLockBaseAddress(pixelBuffer, 0);
    int bufferWidth = (int)CVPixelBufferGetWidth(pixelBuffer);
    int bufferHeight = (int)CVPixelBufferGetHeight(pixelBuffer);
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(pixelBuffer);
    uint8_t *baseAddress = CVPixelBufferGetBaseAddress(pixelBuffer);

    // Copy the pixel buffer
    CVPixelBufferRef pixelBufferCopy = NULL;
    CVReturn status = CVPixelBufferCreate(kCFAllocatorDefault, bufferWidth, bufferHeight, kCVPixelFormatType_32ARGB, NULL, &pixelBufferCopy);
    if (status != kCVReturnSuccess) {
        NSLog(@"BGRABufferCopyWithPixelBuffer :: failed");
    }
    CVPixelBufferLockBaseAddress(pixelBufferCopy, 0);
    uint8_t *copyBaseAddress = CVPixelBufferGetBaseAddress(pixelBufferCopy);
    memcpy(copyBaseAddress, baseAddress, bufferHeight * bytesPerRow);
    CVPixelBufferUnlockBaseAddress(pixelBuffer, 0);
    CVPixelBufferUnlockBaseAddress(pixelBufferCopy, 0);
    
    return pixelBufferCopy;
}


#pragma mark - image 转换 PixelBuffer
+(CVPixelBufferRef)pixelBufferFromCGImage:(CGImageRef)image{
    
    if (!image) {
        return NULL;
    }
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], kCVPixelBufferCGImageCompatibilityKey,
                             [NSNumber numberWithBool:YES], kCVPixelBufferCGBitmapContextCompatibilityKey,
                             nil];
    
    CVPixelBufferRef pxbuffer = NULL;
    
    CGFloat frameWidth = CGImageGetWidth(image);
    CGFloat frameHeight = CGImageGetHeight(image);
    CVReturn status = CVPixelBufferCreate(kCFAllocatorDefault,frameWidth,frameHeight,kCVPixelFormatType_32ARGB,(__bridge CFDictionaryRef) options, &pxbuffer);
    
    if ( status != kCVReturnSuccess || !pxbuffer )
        return nil;
    
    CVPixelBufferLockBaseAddress(pxbuffer, 0);
    void *pxdata = CVPixelBufferGetBaseAddress(pxbuffer);
    if (!pxdata)
        return nil;
    
    CGColorSpaceRef rgbColorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(pxdata, frameWidth, frameHeight, 8,CVPixelBufferGetBytesPerRow(pxbuffer),rgbColorSpace,(CGBitmapInfo)kCGImageAlphaNoneSkipFirst);
    CGContextConcatCTM(context, CGAffineTransformIdentity);
    CGContextDrawImage(context, CGRectMake(0, 0,frameWidth,frameHeight),  image);
    CGColorSpaceRelease(rgbColorSpace);
    CGContextRelease(context);
    
    CVPixelBufferUnlockBaseAddress(pxbuffer, 0);
    
    return pxbuffer;
}

+(UIImage *)imageFromPixelBuffer:(CVPixelBufferRef)pixelBuffer {
    CIImage *ciImage = [CIImage imageWithCVPixelBuffer:pixelBuffer];

    CIContext *temporaryContext = [CIContext contextWithOptions:nil];
    CGImageRef videoImage = [temporaryContext
        createCGImage:ciImage
             fromRect:CGRectMake(0, 0, CVPixelBufferGetWidth(pixelBuffer), CVPixelBufferGetHeight(pixelBuffer))];

    UIImage *uiImage = [UIImage imageWithCGImage:videoImage];
    CGImageRelease(videoImage);
    CVPixelBufferRelease(pixelBuffer);
    return uiImage;
}


@end
