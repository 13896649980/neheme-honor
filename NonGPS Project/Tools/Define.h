//
//  Define.h
//  博坦创新
//
//  Created by Xu pan on 2019/5/22.
//  Copyright © 2019 Xu pan. All rights reserved.
//

#ifndef Define_h
#define Define_h

#define IS_IPHONE           (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_iPhoneX          [[Utility hardwareDescription] isEqualToString:@"iPhone X"]
#define IS_iPhoneXR         [[Utility hardwareDescription] isEqualToString:@"iPhone XR"]
#define IS_iPhoneXSMax      [[Utility hardwareDescription] isEqualToString:@"iPhone XS Max"]
#define IS_IPHONE_6         (IS_IPHONE && MAX_LEN == 667.0)
#define IS_IPHONE_6P        (IS_IPHONE && MAX_LEN == 736.0)
#define IS_IPHONE_5         (IS_IPHONE && MAX_LEN == 568.0)
#define IS_iPhoneX_TYPE     ([[Utility hardwareDescription] containsString:@"iPhone X"] || [[Utility hardwareDescription] containsString:@"iPhone 11"] || [[Utility hardwareDescription] containsString:@"iPhone 12"])
/**
 *  RGB颜色宏
 */
#define RGB_COLOR(RGB) [UIColor colorWithRGB:RGB]

#define RGB_COLORWITHRGB(R,G,B) [UIColor colorWithRed:R/255.0 green:G/255.0 blue:B/255.0 alpha:1]

#define MAX_LEN MAX([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)
#define MIN_LEN MIN([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)

#define SCREEN_WIDTH_A ([UIScreen mainScreen].bounds.size.width)
#define SCREEN_HEIGHT_A ([UIScreen mainScreen].bounds.size.height)

#define SCREEN_WIDTH  MAX(SCREEN_WIDTH_A, SCREEN_HEIGHT_A)
#define SCREEN_HEIGHT MIN(SCREEN_WIDTH_A, SCREEN_HEIGHT_A)

#define IS_LOGIN [Utility getUserInfo].isLogin

#define USER_DEFAULT [NSUserDefaults standardUserDefaults]
#define HOST_IP @"host"
#define SSID @"ssid"
#define DEVICE @"device"
#define VGS_WIFI_IP @"vgsIP"
#define DIR_VIDEO @"Videos"
#define DIR_VIDEO_THUMB @"VideoThumbs"
#define DIR_VIDEO_PHOTO @"VideoPhoto"

#define DIR_PHOTO @"Photos"
#define DIR_PHOTO_THUMB @"thumbnails"

#define BACKGROUND_COLOR RGB_COLORWITHRGB(18, 28, 38)
#define IMAGEWITHNAME(r) [UIImage imageWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:r]]
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define kRatio [[UIScreen mainScreen] applicationFrame].size.width/568
#define IOS_VERSION [[[UIDevice currentDevice] systemVersion] floatValue]
#define IS_TFCARD @"no_Tf_card"


#define CURRENT_SSID [USER_DEFAULT objectForKey:SSID]
#define CURRENT_DEVICE [USER_DEFAULT objectForKey:DEVICE]


#define WIFI_SSID_VGS @"VSLCAM"
#define WIFI_SSID_720P  @"VSMCAM"//@"VSMCAM"
#define WIFI_SSID_1080P @"VSHCAM" //   @"VSH"//

#define WIFI_DEVICE_VSVGA WIFI_DEVICE_MJPEG
#define WIFI_DEVICE_VGS @"VGA"
#define WIFI_DEVICE_720P @"720P"
#define WIFI_DEVICE_1080P @"1080P"
#define WIFI_DEVICE_UDP_720P @"UDP720P"
#define WIFI_DEVICE_TCP_VGA @"TCPVGA"
#define WIFI_DEVICE_MJPEG @"MJPEG"
#define WIFI_DEVICE_UDP_720PYU @"UDP720PYU"
#define WIFI_DEVICE_RTSP1080P @"RTSP1080P"
#define WIFI_DEVICE_RTSP720P @"RTSP720P"
#define WIFI_DEVICE_UDP2K @"UDP2K"
#define WIFI_DEVICE_UDP4K @"UDP4K"
#define WIFI_DEVICE_HI4K @"HI4K"
#define WIFI_DEVICE_HI2K @"HI2K"
#define WIFI_DEVICE_UDP2K_30 @"UDP2K-30"
#define WIFI_DEVICE_UDP27K @"UDP27K"
#define WIFI_DEVICE_1080P5G @"1080P-5G"
#define WIFI_DEVICE_1080P5g @"1080P-5g"
#define WIFI_DEVICE_720P5G @"720P-5G"
#define WIFI_DEVICE_720P5g @"720P-5g"
#define WIFI_DEVICE_1080PI @"1080P-I"

#define WIFI_DEVICE_872ET_320_320   @"872ET_320_320"
#define WIFI_DEVICE_872AT_640_640   @"872AT_640_640"

#define WIFI_DEVICE_FH8626_ssv6x5x_24g_1920_1280 @"FH8626_ssv6x5x_24g_1920_1280"
#define WIFI_DEVICE_FH8626_ssv6x5x_5g_1920_1280 @"FH8626_ssv6x5x_5g_1920_1280"
#define WIFI_DEVICE_FH8856_8812cu_4000_1280 @"FH8856_8812cu_4000_1280"

#define WIFI_DEVICE_Mr100_ssv6x5x_2048_2048 @"Mr100_ssv6x5x_2048_2048"
#define WIFI_DEVICE_Mr100_ssv6x5x_1920_1920 @"Mr100_ssv6x5x_1920_1920"
#define WIFI_DEVICE_Mr100_ssv6x5x_1920_1280 @"Mr100_ssv6x5x_1920_1280"
#define WIFI_DEVICE_Mr100_ssv6x5x_4000_2048 @"Mr100_ssv6x5x_4000_2048"
#define WIFI_DEVICE_Mr100_ssv6x5x_4000_1920 @"Mr100_ssv6x5x_4000_1920"
#define WIFI_DEVICE_Mr100_ssv6x5x_4000_1280 @"Mr100_ssv6x5x_4000_1280"
#define WIFI_DEVICE_Mr100_8801_2048_2048 @"Mr100_8801_2048_2048"
#define WIFI_DEVICE_Mr100_8801_1920_1920 @"Mr100_8801_1920_1920"
#define WIFI_DEVICE_Mr100_8801_1920_1280 @"Mr100_8801_1920_1280"
#define WIFI_DEVICE_Mr100_8801_4000_2048 @"Mr100_8801_4000_2048"
#define WIFI_DEVICE_Mr100_8801_4000_1920 @"Mr100_8801_4000_1920"
#define WIFI_DEVICE_Mr100_8801_4000_1280 @"Mr100_8801_4000_1280"


#define IS_VGS  ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_VGS])
#define IS_720P  ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_720P])
#define IS_1080P  ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_1080P])
#define IS_UDP_720P  ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_UDP_720P])
#define IS_MJPEG_VGA  ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_MJPEG])
#define IS_UDP_720PYU  ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_UDP_720PYU])
#define IS_RTSP1080P  ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_RTSP1080P])
#define IS_RTSP720P  ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_RTSP720P])
#define IS_UDP2K  ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_UDP2K])
#define IS_UDP4K  ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_UDP4K])
#define IS_HI4K  ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_HI4K])
#define IS_HI2K  ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_HI2K])
#define IS_UDP2K_30 ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_UDP2K_30])
#define IS_UDP27K  ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_UDP27K])
#define IS_1080P5G  ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_1080P5G])
#define IS_1080P5g  ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_1080P5g])
#define IS_1080PI  ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_1080PI])
#define IS_720P5G  ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_720P5G])
#define IS_720P5g  ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_720P5g])
#define IS_VSVGA  IS_MJPEG_VGA

#define IS_872ET_320_320   ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_872ET_320_320])
#define IS_872AT_640_640   ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_872AT_640_640])

#define IS_FH8626_ssv6x5x_24g_1920_1280  ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_FH8626_ssv6x5x_24g_1920_1280])
#define IS_FH8626_ssv6x5x_5g_1920_1280 ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_FH8626_ssv6x5x_5g_1920_1280])
#define IS_FH8856_8812cu_4000_1280 ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_FH8856_8812cu_4000_1280])

#define IS_Mr100_ssv6x5x_2048_2048  ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_Mr100_ssv6x5x_2048_2048])
#define IS_Mr100_ssv6x5x_1920_1920 ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_Mr100_ssv6x5x_1920_1920])
#define IS_Mr100_ssv6x5x_1920_1280 ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_Mr100_ssv6x5x_1920_1280])
#define IS_Mr100_ssv6x5x_4000_2048 ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_Mr100_ssv6x5x_4000_2048])
#define IS_Mr100_ssv6x5x_4000_1920 ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_Mr100_ssv6x5x_4000_1920])
#define IS_Mr100_ssv6x5x_4000_1280 ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_Mr100_ssv6x5x_4000_1280])

#define IS_Mr100_8801_2048_2048 ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_Mr100_8801_2048_2048])
#define IS_Mr100_8801_1920_1920 ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_Mr100_8801_1920_1920])
#define IS_Mr100_8801_1920_1280 ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_Mr100_8801_1920_1280])
#define IS_Mr100_8801_4000_2048 ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_Mr100_8801_4000_2048])
#define IS_Mr100_8801_4000_1920 ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_Mr100_8801_4000_1920])
#define IS_Mr100_8801_4000_1280 ([CURRENT_DEVICE isEqualToString:WIFI_DEVICE_Mr100_8801_4000_1280])

#define IS_NO_DEVICE ([CURRENT_DEVICE isEqualToString:@"nodevice"])
#define IS_REMOTE_PLAY @"isremote"
#define VTB_DECODE (__IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_8_0)

#define APPLanguage @"appLanguage"

#define MAX_LEN MAX([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)
#define MIN_LEN MIN([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)
#define LAYOUT_WIDTH(width) (MIN_LEN/375 * (width))
#define LAYOUT_HEIGHT(width) (MAX_LEN/667 * (width))

/**
 *  文字适配比例
 */
#define FontScale   (MAX_LEN > 568 ? (MAX_LEN == 667 ? 1.0 : 1.1) : 0.8)
#define FONT_SCALE(fontSize) [UIFont systemFontOfSize:fontSize * FontScale]
#define Font(size)  [UIFont systemFontOfSize:size]

#define PF_LIGHT  @"PingFangSC-Light"

#define FLY_LOG @"FLY_LOG"
#define FLY_PARAM @"FLY_PARAM"

#define TranslationString(string) NSLocalizedString(string, nil)


#endif /* Define_h */
