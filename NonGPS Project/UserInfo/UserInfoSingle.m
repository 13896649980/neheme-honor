//
//  UserInfoSingle.m
//  博坦创新
//
//  Created by Xu pan on 2019/5/27.
//  Copyright © 2019 Xu pan. All rights reserved.
//

#import "UserInfoSingle.h"



@implementation UserInfoSingle

-(instancetype)init{
    if (self = [super init]) {
        self.location = @"";
        self.isAmerHand = YES;
        self.circleRadius = 10;
        self.ISOCountryCode = @"";
        self.customCode = CustomCode_Vison;
        self.correctOffsetTag = 0;
        self.lensType = LensType_Main;
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:_location forKey:@"location"];
    [aCoder encodeBool:_isAmerHand forKey:@"isAmerHand"];
    [aCoder encodeInt:_circleRadius forKey:@"circleRadius"];
    [aCoder encodeObject:_ISOCountryCode forKey:@"ISOCountryCode"];
    [aCoder encodeInteger:_customCode forKey:@"customCode"];
    [aCoder encodeInteger:_correctOffsetTag forKey:@"correctOffsetTag"];
    [aCoder encodeObject:_currentLocation forKey:@"currentLocation"];
    [aCoder encodeInteger:_lensType forKey:@"lensType"];
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super init]) {
        self.location = [aDecoder decodeObjectForKey:@"location"];
        self.isAmerHand = [aDecoder decodeBoolForKey:@"isAmerHand"];
        self.circleRadius = [aDecoder decodeIntForKey:@"circleRadius"];
        self.ISOCountryCode = [aDecoder decodeObjectForKey:@"ISOCountryCode"];
        self.customCode = [aDecoder decodeIntegerForKey:@"customCode"];
        self.correctOffsetTag = [aDecoder decodeIntegerForKey:@"correctOffsetTag"];
        self.currentLocation = [aDecoder decodeObjectForKey:@"currentLocation"];
        self.lensType = [aDecoder decodeIntegerForKey:@"lensType"];
    }
    return self;
}


     
@end
