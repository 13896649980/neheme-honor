//
//  UserInfoSingle.h
//  博坦创新
//
//  Created by Xu pan on 2019/5/27.
//  Copyright © 2019 Xu pan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN





/**
 客户码
 */
typedef NS_ENUM(NSInteger,CustomCode) {
    CustomCode_Vison,
};


/**
 主镜头，光流镜头

 - LensType_Main: 主镜头
 */
typedef NS_ENUM(NSInteger,LensType) {
    LensType_Main,      //主镜头
    LensType_Vice,      //副镜头（光流）
};

@interface UserInfoSingle : NSObject<NSCoding>

/**
 用户位置
 */
@property   (nonatomic,strong)      NSString                *location;
/**
 左右手
 */
@property   (nonatomic,assign)      BOOL                    isAmerHand;
/**
 环绕半径
 */
@property   (nonatomic,assign)      int                     circleRadius;
/**
 国家码
 */
@property   (nonatomic,strong)      NSString                *ISOCountryCode;

/**
 当前的客户吗
 */
@property   (nonatomic,assign)      CustomCode              customCode;

/**
 是否纠偏标记
 0  :   未知
 1  :   不需要
 2  :   需要
 */
@property   (nonatomic,assign)      NSInteger               correctOffsetTag;

/**
 用户当前定位的位置
 */
@property   (nonatomic,strong)      CLLocation              *currentLocation;

/**
 （主副镜头）视频流
 */
@property   (nonatomic,assign)      LensType                lensType;


@end

NS_ASSUME_NONNULL_END
