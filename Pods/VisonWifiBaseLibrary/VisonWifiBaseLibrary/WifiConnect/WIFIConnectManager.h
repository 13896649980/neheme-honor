//
//  WIFIConnectManager.h
//  Version: 1.0
//
//  该文件用于手机实时检测wifi断开，连接以及相应的回调
//
//  Created by Xu pan on 2020/7/31.
//  Copyright © 2020 Xu pan. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface WIFIConnectManager : NSObject
//wifi连接状态
@property   (nonatomic,assign,readonly)     BOOL                            isConnectedWIFI;
//wifi信号强度
@property   (nonatomic,assign,readonly)     int                             wifiSignalQuality;
//当前ssid
@property   (nonatomic,copy,readonly)       NSString                        *currentSSID;

+(instancetype)shareInstance;

@end


