//
//  WIFIConnectManager.m
//  GPS Project
//
//  Created by Xu pan on 2020/7/31.
//  Copyright © 2020 Xu pan. All rights reserved.
//

#import "WIFIConnectManager.h"
#import "VisonWifiBaseLibraryDefine.h"
#import <SystemConfiguration/CaptiveNetwork.h>

@interface WIFIConnectManager ()

@property   (nonatomic,strong)      NSString            *oldSSID;

@end

@implementation WIFIConnectManager

static WIFIConnectManager *_mamager = nil;
+(instancetype)shareInstance{
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _mamager = [[self alloc]init];
    });
    return _mamager;
}
-(instancetype)init{
    if (self = [super init]) {
        [self startCheckSSID];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(wifiSignalQualityAction:) name:@"wifiLevelNoti" object:nil];
    }
    return self;
}

-(void)wifiSignalQualityAction:(NSNotification *)notice{
    _wifiSignalQuality = [notice.object intValue];
}

#pragma mark - 检查wifi
-(void)startCheckSSID{
    [[NSRunLoop currentRunLoop] addTimer:[NSTimer scheduledTimerWithTimeInterval:0.5
                                                                          target:self
                                                                        selector:@selector(checkSSID)
                                                                        userInfo:nil
                                                                         repeats:YES]
                                 forMode:NSRunLoopCommonModes];
}

- (void)checkSSID{
    NSString *newSSID = [self currentWifiSSID];
    if ([newSSID isEqual:[NSNull null]] || !newSSID){
        newSSID = nil;
        //之前有过连接wifi,那这里是断开了WiFi
        if (self.oldSSID) {
            if (_isConnectedWIFI) {
                _isConnectedWIFI = NO;
                [[NSNotificationCenter defaultCenter] postNotificationName:WIFI_NOTI_DISCONNECT object:nil];
            }
            NSLog(@"wifi未连接，oldSSID:%@",self.oldSSID);
            self.oldSSID = nil;
        }else{
            NSLog(@"wifi未连接,请检查网络设置");
        }
    }else{
        //wifi持续连接
        if ([newSSID isEqualToString:self.oldSSID]) {
            
        }
        //wifi变化了
        else{
            //之前有连接过wifi
            NSLog(@"SSID_CHANGE!!!");
            NSLog(@"self.oldSSID = %@, newSSID = %@",self.oldSSID,newSSID);
            NSLog(@"当前WiFi SSID = %@",newSSID);
            self.oldSSID = newSSID;
            if (!_isConnectedWIFI) {
                _isConnectedWIFI = YES;
            }
        }
    }
}

- (NSString *)currentWifiSSID{
    // Does not work on the simulator.
    NSString *ssid = nil;
    NSDictionary *info = [self fetchSSIDInfo];
    if (info) {
        ssid = [info[@"SSID"] lowercaseString];
    }
    return ssid;
}

- (id)fetchSSIDInfo{
    NSArray *ifs = CFBridgingRelease(CNCopySupportedInterfaces());
    id info = nil;
    for (NSString *ifnam in ifs) {
        info = (__bridge_transfer id)CNCopyCurrentNetworkInfo((CFStringRef)ifnam);
        if (info && [info count]) { break; }
    }
    return info;
}


@end
