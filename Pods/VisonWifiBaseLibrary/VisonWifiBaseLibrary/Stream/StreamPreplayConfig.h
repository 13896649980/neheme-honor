//
//  StreamPreplayConfig.h
//  Version: 1.0
//
//  该文件用于拿视频流之前的准备工作：将ip地址，端口号port,devicetype保存下来
//
//  Created by Xu pan on 2020/8/3.
//  Copyright © 2020 Xu pan. All rights reserved.

#import <Foundation/Foundation.h>
#import "WIFIDeviceModel.h"
#include "Includes.h"

NS_ASSUME_NONNULL_BEGIN

@interface StreamPreplayConfig : NSObject

/// 流地址
@property   (nonatomic,assign,readonly)             NSString                *streamIP;
/// 流端口
@property   (nonatomic,assign,readonly)             int                     streamPort;
/// RTSP用
@property   (nonatomic,assign,readonly)             NSString                *streamPath;
/// 设备类型，（用于分配缓存大小）
@property   (nonatomic,assign,readonly)             DeviceType_e            devidetype;

+(instancetype)shareInstance;

/// 接收流数据前的准备工作
/// @param wifiDeviceModel wifi设备类型
-(void)streamPreplayConfigWithWIFIDeviceModel:(WIFIDeviceModel *)wifiDeviceModel;

/// 清空数据
-(void)clearData;

@end

NS_ASSUME_NONNULL_END
