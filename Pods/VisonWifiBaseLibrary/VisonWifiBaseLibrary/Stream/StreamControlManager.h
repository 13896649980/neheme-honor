//
//  StreamControlManager.h
//  Version: 1.0
//
//  该文件用于拿视频流,手势识别处理以及解码的数据回调
//
//  Created by Xu pan on 2020/8/3.
//  Copyright © 2020 Xu pan. All rights reserved.

#import <Foundation/Foundation.h>
#import <CoreMedia/CoreMedia.h>
#import <UIKit/UIKit.h>
#import "Includes.h"
#import "VisonUSBTcpPlay.h"

//是否是用硬解码
#define IS_NOTUSE_VTB_DECODE (([WIFIDeviceModel shareInstance].chipModel == ChipModel_QuanZhi_872) ? YES : NO)
#define VTB_DECODE (__IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_8_0)

@protocol StreamControlManagerDelegate <NSObject>

@optional
/// 用户手势拍照录像的处理回调，若有识别到拍照录像返回 YES
/// @param yuv yuvdata
/// @param bufW bufWidth
/// @param bufH bufHeight
/// @param bufSize bufSize
-(BOOL)gestureDetectWithBuf:(char *)yuv
                       bufW:(int)bufW
                       bufH:(int)bufH
                    bufSize:(float)bufSize;

@optional
/// 用于图像跟随的pbuf数据回调
/// @param pBuf pBuf
/// @param bufCount pBuf大小
-(void)imageFollowDectecWithBuf:(CVPixelBufferRef)pBuf
                       bufCount:(int)bufCount;

@optional
/// 回调解码的CVPixelBufferRef数据
/// @param pixelBuf pBuf
/// @param frameTime pts
/// @param frameWidth 长
/// @param frameHeight 高
-(void)processCVPixelBuffer:(CVPixelBufferRef)pixelBuf
                  frameTime:(CMTime)frameTime
                 frameWidth:(int)frameWidth
                frameHeight:(int)frameHeight;

@optional
/// 回调解码的图像数据
/// @param image 图像
/// @param frameTime pts
/// @param frameWidth 长
/// @param frameHeight 高
-(void)processImage:(UIImage *)image
          frameTime:(CMTime)frameTime
         frameWidth:(int)frameWidth
        frameHeight:(int)frameHeight;

@end


@interface StreamControlManager : NSObject

/// 是否正在接收视频流（可用于判断是否正在播放）
@property   (nonatomic,assign,readonly)     BOOL                                        isReceiving;
@property   (nonatomic,assign)              id<StreamControlManagerDelegate>            delegate;
//是否正在写录像
@property   (nonatomic,assign,readonly)     BOOL                                        isWritingMovie;

/// 初始化
/// @param gestureEnable 启用手势识别
/// @param imageFollowEnable 启用图像识别跟随(只是内部数据回调出来，并未处理)
-(instancetype)initWithGestureEnable:(BOOL)gestureEnable
                   imageFollowEnable:(BOOL)imageFollowEnable;

/// 开始接收视频流
-(void)startReceiveStreamcComplete:(void(^)(void))complete;

/// 停止接收视频流
-(void)stopReceiveStreamComplete:(void(^)(void))complete;

/// 流里拿图片
/// @param complete 完成回调
-(void)asyncGetPhotoFromStream:(void(^)(BOOL isRec,UIImage *image))complete;


/// 取原图和取流截图
/// @param progress 下载进度
/// @param complete 完成回调
-(void)asyncGetStreamPhotoAndSourcePhotoInProgress:(void(^)(float progress))progress
                                          complete:(void(^)(UIImage *streamImage,UIImage *sourceImage))complete;

/// 流中取buff写影片（类似录屏的方式）
/// @param isSaveLocal 是否保存本地
/// @param isSaveTFCard 是否保存tf卡
/// @param filePath 本地保存你路径
/// @param frameRate 帧率
/// @param frameSize 尺寸
/// @param audioEnable 是否录音
/// @param complete 准备完成回调
-(void)starWriteMovieIsSaveLocal:(BOOL)isSaveLocal
                    isSaveTFcard:(BOOL)isSaveTFCard
                      ToFilePath:(NSString *_Nullable)filePath
                      frameRate:(int)frameRate
                      frameSize:(CGSize)frameSize
                    audioEnable:(BOOL)audioEnable
                        complete:(void(^_Nullable)(void))complete;

/// 结束写影片
/// @param complete 完成回调，返回写入的路径
-(void)stopWriteMovieComplete:(nonnull void (^)(NSString * _Nullable moviePath))complete;

@end

