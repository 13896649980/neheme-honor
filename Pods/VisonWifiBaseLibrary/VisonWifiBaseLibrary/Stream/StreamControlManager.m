//
//  StreamControlManager.m
//  GPS Project
//
//  Created by Xu pan on 2020/8/14.
//  Copyright © 2020 Xu pan. All rights reserved.
//

#include "HMDTcpPlay.h"
//#include "RtspPlay.h"
#include "Play.h"
#import "StreamControlManager.h"
#import "VTBDecode.h"
#import "WIFIDeviceModel.h"
#import "PacketList.h"
#import "CodeLock.h"
#import "SocketControlManager.h"
#import <AudioToolbox/AudioToolbox.h>
#import "StreamPreplayConfig.h"
#include <sys/time.h>
#import "MovieWriter.h"
#import "Device_Wifi_Config_Command.h"
#import "VisonPTUSBHandle.h"
#import "DataTransferManager.h"

@interface StreamControlManager(){
    
    /**
        这个是用来装载原始264的链表
     */
    myPacketList_t s_videoFreeList;   // 空闲链
    myPacketList_t s_videoUsedList;   // 已用链
    CODELOCK s_videoFreelistLock;   // 空闲链锁
    CODELOCK s_videoUsedlistLock;   // 已用链锁
    BOOL s_bVDecThreadRunning;
    BOOL s_bVDecThreadIsOver;
    
    /**
        这个是用来装载解码出的YUV的链表
     */
    myPacketList_t s_yuvFreeList;   // 空闲链
    myPacketList_t s_yuvUsedList;   // 已用链
    CODELOCK s_yuvFreelistLock;   // 空闲链锁
    CODELOCK s_yuvUsedlistLock;   // 已用链锁
    BOOL s_bVPlayThreadRunning;
    BOOL s_bVPlayThreadIsOver;
    
    //解码线程
    NSThread *VDecThread;
    //播放线程
    NSThread *VPlayThread;
    CODELOCK s_glLock;
    dispatch_semaphore_t  nodeSemaphore;
    dispatch_semaphore_t  playSemaphore;
    /**
        这个是用来装载解码出的YUV的链表，用来图像识别
     */
    myPacketList_t s_trackYuvFreeList;   // 跟随链
    myPacketList_t s_trackYuvUsedList;   // 已用链
    CODELOCK s_trackYuvFreeListLock;   // 空闲链锁
    CODELOCK s_trackYuvUsedListLock;   // 空闲链锁
    BOOL s_TrackYuvThreadRunning;
    BOOL s_TrackYuvThreadIsOver;
    NSThread *VTrackThread;
    CODELOCK s_detectLock;   // 识别锁
    int lastState;
    int actionState;
    long long lastMS;

    
    
    BOOL wantToExit;
    void* playHandle;
    int forceIframe;
    unsigned char s_currentFrameNum;
    unsigned long long s_lastPts;
    bool lostFrame;
    UIImage *mImg;
    
    dispatch_source_t delayPlayTimer;
    
    
    BOOL    isSaveLocal;
    BOOL    isSaveTFCard;
}

@property   (nonatomic,assign)          BOOL                gestureEnable;
@property   (nonatomic,assign)          BOOL                imageFollowEnable;
@property   (nonatomic,strong)          MovieWriter         *movieWriter;

@property   (nonatomic,strong)          VTBDecode           *s_vtbDecode;

@end


@implementation StreamControlManager


#pragma mark - 环形buff处理NALU为一帧完整264数据回调
static void fDataCB(void* dwPlayHandle, VFrameHead_t* lpFrameHead, char* pBuf, unsigned int dwBufLen, void* lpUser){
    StreamControlManager* managr = (__bridge StreamControlManager*)lpUser;
    //闪退
    int forceIframe = managr->forceIframe;
    myPacketNode_t* pNode = NULL;
    //NSLog(@"pBuf %s  dwBufLen  %u  lpUser  %@ playVCtrl %@",pBuf,dwBufLen,lpUser,playVCtrl);
    if (NULL == pBuf){
        NSLog(@"Error: pBuf is null!");
        return;
    }
    
    if (dwBufLen > 1024*1024){
        NSLog(@"Error:回调的H264数据大于节点内存1M");
        return;
    }
    
    if (0 == myPacketListCount(&managr->s_videoFreeList)){
        if (0 == lpFrameHead->frameType)
        {
            int count = 0;
            while (++count < 10 && 0 == myPacketListCount(&managr->s_videoFreeList)){
                usleep(5000);
            }
            if (0 == myPacketListCount(&managr->s_videoFreeList)){
                return;
            }else{
                NSLog(@"video packet count is 0 ==IwaitFree==");
            }
        }else{
            return;
        }
    }
    // 这块代码主要是防止马赛克
    if(forceIframe){
        if(0 != lpFrameHead->frameType){
            return;
        }else{
            forceIframe = 0;
            managr->forceIframe = forceIframe;
        }
    }
    
    if ((U8)(managr->s_currentFrameNum+1) != lpFrameHead->res2[0]){
        managr->lostFrame = YES;
    }
    
    if (((U8)(managr->s_currentFrameNum+1) != lpFrameHead->res2[0]) && (0 != lpFrameHead->frameType)){
        NSLog(@"wait key frame");
        
        forceIframe = 1;
        managr->forceIframe = forceIframe;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //发送0x27
            char command [5];
            bzero(command,5);
            command[0] = 0x27;
            
            NSData *data = [NSData dataWithBytes:command length:5];
            [[SocketControlManager shareInstance] sendUdpCommand:data tag:0];
        });
        NSLog(@"wait key frame");
        return;
    }
    
    // 从freeList中取一个空闲内存块出来
    CodeLock(&managr->s_videoFreelistLock);
    pNode = myPacketListPop(&managr->s_videoFreeList);
    CodeUnlock(&managr->s_videoFreelistLock);
    
    pNode->lostFrameCount = 1;
    
    memcpy(pNode->pBuf, pBuf, dwBufLen);
    pNode->len = dwBufLen;
    pNode->pts = lpFrameHead->timeStamp;
    pNode->frameType = lpFrameHead->frameType;
    pNode->vHeight = lpFrameHead->height;
    pNode->vWidth = lpFrameHead->width;
    pNode->framerate = lpFrameHead->framerate;
    
    if (((U8)(managr->s_currentFrameNum+1) != lpFrameHead->res2[0]) )
    {

        if (managr->s_lastPts == 0)
        {
            managr->s_lastPts = lpFrameHead->timeStamp;
            pNode->lostFrameCount = 1;
        }
        else
        {
            unsigned long long curms = lpFrameHead->timeStamp/(1000);//ms
            unsigned long long lastms = managr->s_lastPts/(1000);

            float durtion = 1000.0/[WIFIDeviceModel shareInstance].frameRate;
            
            if ((curms - lastms)/durtion < 256)//如果刚好在255个帧率范围内
            {
                if (lpFrameHead->res2[0] < managr->s_currentFrameNum)
                {
                    pNode->lostFrameCount = 255 - managr->s_currentFrameNum + lpFrameHead->res2[0] + 1;
                }else
                {
                    pNode->lostFrameCount = lpFrameHead->res2[0] - managr->s_currentFrameNum + 1;
                }
                
            }else //如果不只255个帧率范围
            {
                pNode->lostFrameCount =  (int)ceilf((curms - lastms) /durtion) + 1 ;
            }
        }
    }
    // 将该内存块压入已用链中
    CodeLock(&managr->s_videoUsedlistLock);
    myPacketListPush(&managr->s_videoUsedList, pNode);
    CodeUnlock(&managr->s_videoUsedlistLock);
    
    managr->s_currentFrameNum = lpFrameHead->res2[0];
    managr->s_lastPts = lpFrameHead->timeStamp;
}


static void failReadFreamCB(void* handle, void* user){
    StreamControlManager* managr = (__bridge StreamControlManager*)user;
    
    managr->wantToExit = YES;
    
    /*
    dispatch_async(dispatch_get_main_queue(), ^{
#ifdef  USER_GESTURE
        UIView *glview = [playVCtrl.view viewWithTag:0xff21];
        UIImage * image = [playVCtrl snapshotScreenWithGL:glview];
#else
//        UIImage * image = [playVCtrl->_eyeView snapshot:CGRectZero];
        UIImage * image = [playVCtrl.playerView snapShot];
#endif
        if (image)
        {
            [playVCtrl->mBackGroundView setAlpha:1];
            [playVCtrl->mBackGroundView setImage:image];
            playVCtrl.playerView.alpha = 0;
        }
    });
    NSLog(@"%s",__FUNCTION__);
     */
}


-(instancetype)initWithGestureEnable:(BOOL)gestureEnable
                   imageFollowEnable:(BOOL)imageFollowEnable{
    if (self = [super init]) {
        _gestureEnable = gestureEnable;
        _imageFollowEnable = imageFollowEnable;
    }
    return self;
}

#pragma mark - 初始化设置
-(void)initializeConfig{
    
    if (!IS_NOTUSE_VTB_DECODE) {
        [self initDecode];
    }
}


#pragma mark - 初始化解码器
-(void)initDecode{
    //264解码器
    if (!self.s_vtbDecode) {
        self.s_vtbDecode = [VTBDecode alloc];
        [self.s_vtbDecode vtb_init];
        NSLog(@"硬解码 VTBDecode 初始化");
    }
}
#pragma mark - 解码器释放
-(void)releaseDecode{
    if (self.s_vtbDecode) {
        [self.s_vtbDecode vtb_dealloc];
        NSLog(@"硬解码 VTBDecode 释放");
    }
}

#pragma mark - 创建视频播放相关链表
-(void)createStreamPacketList{
    NSLog(@"创建视频播放相关链表");
    myPacketListInit(&s_videoFreeList);
    myPacketListInit(&s_videoUsedList);
    myPacketListInit(&s_yuvFreeList);
    myPacketListInit(&s_yuvUsedList);
    
    CreateCodeLock(&s_glLock);
    
    CreateCodeLock(&s_videoFreelistLock);
    CreateCodeLock(&s_videoUsedlistLock);
    CreateCodeLock(&s_yuvFreelistLock);
    CreateCodeLock(&s_yuvUsedlistLock);
    
    int i;
    myPacketNode_t* pNode = NULL;
    //264 申请25个1M的节点
    CodeLock(&s_videoFreelistLock);
    for (i = 0; i < 25; i++)
    {
        pNode = (myPacketNode_t*)malloc(sizeof(myPacketNode_t));
        memset(pNode, 0, sizeof(myPacketNode_t));
        pNode->len = 1024*1024;
        pNode->pBuf = (char *)malloc(pNode->len);
        myPacketListPush(&s_videoFreeList, pNode);
    }
    CodeUnlock(&s_videoFreelistLock);
    
    //YUV
    CodeLock(&s_yuvFreelistLock);
    for (i = 0; i < 25; i++)
    {
        pNode = (myPacketNode_t*)malloc(sizeof(myPacketNode_t));
        memset(pNode, 0, sizeof(myPacketNode_t));
        myPacketListPush(&s_yuvFreeList, pNode);
    }
    CodeUnlock(&s_yuvFreelistLock);
    
    s_bVPlayThreadRunning = 1;
    s_bVPlayThreadIsOver = 0;
    
    if ([VisonWifiBaseLibraryUtility isNeedDelayPlay]) {
        VPlayThread = [[NSThread alloc] initWithTarget:self
                                              selector:@selector(delayPlayThread)
                                                object:nil];
    }else{
        VPlayThread = [[NSThread alloc] initWithTarget:self
                                              selector:@selector(VPlayThread)
                                                object:nil];
    }
    
    [self initializeConfig];
    nodeSemaphore = dispatch_semaphore_create(1);
    playSemaphore = dispatch_semaphore_create(0);
    
    s_bVDecThreadRunning = 1;
    s_bVDecThreadIsOver = 0;
    VDecThread = [[NSThread alloc] initWithTarget:self
                                         selector:@selector(VDecThread)
                                           object:nil];
}

#pragma mark - 销毁视频播放相关链表
-(void)releaseStreamPacketList{
    NSLog(@"销毁视频播放相关链表");
    int i, count;
    myPacketNode_t* pNode = NULL;

    // free video packet list
    CodeLock(&s_videoFreelistLock);
    count = myPacketListCount(&s_videoFreeList);
    for (i = 0; i < count; i++){
        pNode = myPacketListPop(&s_videoFreeList);
        free(pNode->pBuf);
        free(pNode);
    }
    CodeUnlock(&s_videoFreelistLock);
    
    CodeLock(&s_videoUsedlistLock);
    count = myPacketListCount(&s_videoUsedList);
    for (i = 0; i < count; i++){
        pNode = myPacketListPop(&s_videoUsedList);
        free(pNode->pBuf);
        free(pNode);
    }
    CodeUnlock(&s_videoUsedlistLock);
    
    // free yuv packet list
    CodeLock(&s_yuvFreelistLock);
    count = myPacketListCount(&s_yuvFreeList);
    for (i = 0; i < count; i++){
        pNode = myPacketListPop(&s_yuvFreeList);
        free(pNode);
    }
    CodeUnlock(&s_yuvFreelistLock);
    
    CodeLock(&s_yuvUsedlistLock);
    count = myPacketListCount(&s_yuvUsedList);
    for (i = 0; i < count; i++){
        pNode = myPacketListPop(&s_yuvUsedList);
        free(pNode->pBuf);
        free(pNode);
    }
    CodeUnlock(&s_yuvUsedlistLock);
    
    // lock release
    DestoryCodeLock(&s_videoFreelistLock);
    DestoryCodeLock(&s_videoUsedlistLock);
    
    DestoryCodeLock(&s_yuvFreelistLock);
    DestoryCodeLock(&s_yuvUsedlistLock);
    
    DestoryCodeLock(&s_glLock);
}

#pragma mark - 解码线程
-(void)VDecThread{
    myPacketNode_t *pNode = NULL;
    while (s_bVDecThreadRunning){
        if (0 == myPacketListCount(&s_videoUsedList)){
            usleep(1000);
            continue;
        }
        CodeLock(&s_videoUsedlistLock);
        pNode = myPacketListPop(&s_videoUsedList);
        CodeUnlock(&s_videoUsedlistLock);
        
        if (IS_NOTUSE_VTB_DECODE) {
            //空闲链中没有多余的内存块使用
            if (0 == myPacketListCount(&s_yuvFreeList)) {
                NSLog(@"yuv Packet count is 0 ====");
                return ;
            }
            
            // 从freeList中取一个空闲内存块出来
            myPacketNode_t* pNode1 = NULL;
            CodeLock(&s_yuvFreelistLock);
            pNode1 = myPacketListPop(&s_yuvFreeList);
            CodeUnlock(&s_yuvFreelistLock);
            
            uint8_t *buf = (uint8_t *)malloc(pNode->len);
            memcpy(buf, pNode->pBuf, pNode->len);
            
            pNode1->pBuf = (char *)buf;
            pNode1->vWidth = pNode->vWidth;
            pNode1->vHeight = pNode->vHeight;
            pNode1->pts = pNode->pts;
            pNode1->frameType = pNode->frameType;
            pNode1->len = pNode->len;
            pNode1->framerate = pNode->framerate;
            // 将该内存块压入已用链中
            CodeLock(&s_yuvUsedlistLock);
            myPacketListPush(&s_yuvUsedList, pNode1);
            CodeUnlock(&s_yuvUsedlistLock);
        }else{
            if (VTB_DECODE){//硬解码
                [self vtbParseFrame:pNode];
            }
        }
        // 完成后将block压回空闲链, 继续拿来用
        CodeLock(&s_videoFreelistLock);
        myPacketListPush(&s_videoFreeList, pNode);
        CodeUnlock(&s_videoFreelistLock);
    }
    // 线程是否完全退出的标识
    s_bVDecThreadIsOver = 1;
}

#pragma mark - 264视频解码 -> YUV
- (void)vtbParseFrame:(myPacketNode_t *)data{
    void* pixelBuf = nil;
    if ( nil != (pixelBuf = [self.s_vtbDecode vtb_decode:data->frameType frameData:(uint8_t*)data->pBuf frameLen:data->len]) ) {
        
        // 压入yuvList
        myPacketNode_t* pNode = NULL;
        if (0 == myPacketListCount(&s_yuvFreeList)) {
            [self.s_vtbDecode vtb_release:pixelBuf];
            NSLog(@"yuv Packet count is 0 ====");
            return ;
        }
        
        // 从freeList中取一个空闲内存块出来
        CodeLock(&s_yuvFreelistLock);
        pNode = myPacketListPop(&s_yuvFreeList);
        CodeUnlock(&s_yuvFreelistLock);
        
        pNode->pBuf = (char *)pixelBuf;
        pNode->vWidth = (int)CVPixelBufferGetWidth((CVPixelBufferRef)pixelBuf);
        pNode->vHeight = (int)CVPixelBufferGetHeight((CVPixelBufferRef)pixelBuf);
        pNode->pts = data->pts;
        pNode->framerate = data->framerate;
        
        // 将该内存块压入已用链中
        CodeLock(&s_yuvUsedlistLock);
        myPacketListPush(&s_yuvUsedList, pNode);
        CodeUnlock(&s_yuvUsedlistLock);
        
    }else{
#warning 硬解码错误后 输出错误日志
        //        frwite xxxxx.txt
    }
}


#pragma mark - 播放线程
-(void)VPlayThread{

    myPacketNode_t *pNode = NULL;
    while (s_bVPlayThreadRunning){
        dispatch_semaphore_wait(nodeSemaphore, DISPATCH_TIME_FOREVER);
        
        if (0 == myPacketListCount(&s_yuvUsedList)){
            usleep(1000);
            dispatch_semaphore_signal(nodeSemaphore);
            continue;
        }
      
        CodeLock(&s_yuvUsedlistLock);
        pNode = myPacketListPop(&s_yuvUsedList);
        CodeUnlock(&s_yuvUsedlistLock);

        CodeLock(&s_glLock);

        if (IS_NOTUSE_VTB_DECODE) {
            @autoreleasepool {
                UIImage *image = [UIImage imageWithData:[NSData dataWithBytes:pNode->pBuf length:pNode->len]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (self.delegate && [self.delegate respondsToSelector:@selector(processImage:frameTime:frameWidth:frameHeight:)]) {
                        [self.delegate processImage:image
                                          frameTime:CMTimeMake(pNode->pts, 10000)
                                         frameWidth:pNode->vWidth
                                        frameHeight:pNode->vHeight];
                    }
                });
                
                self.movieWriter.writingPixelBuff = [VisonWifiBaseLibraryUtility pixelBufferFromCGImage:image.CGImage];
                
                dispatch_semaphore_signal(self->playSemaphore);
            }
        }else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (self.delegate && [self.delegate respondsToSelector:@selector(processCVPixelBuffer:frameTime:frameWidth:frameHeight:)]) {
                    [self.delegate processCVPixelBuffer:(CVPixelBufferRef)pNode->pBuf
                                              frameTime:CMTimeMakeWithSeconds((Float64)pNode->pts/1000000, pNode->framerate)
                                             frameWidth:(int)CVPixelBufferGetWidth((CVPixelBufferRef)pNode->pBuf)
                                            frameHeight:(int)CVPixelBufferGetHeight((CVPixelBufferRef)pNode->pBuf)];
                }
                
                self.movieWriter.writingPixelBuff = [VisonWifiBaseLibraryUtility YUVBufferCopyWithPixelBuffer:(CVPixelBufferRef)pNode->pBuf];
                
                if (self.gestureEnable) {
                    //图像识别动作 拍照录像。。。
                    [self handImageRecognitionWithBuf:(CVPixelBufferRef)pNode->pBuf];
                }
                
                if (self.imageFollowEnable) {
                    //跟随
                    if (self.delegate && [self.delegate respondsToSelector:@selector(imageFollowDectecWithBuf:bufCount:)]) {
                        [self.delegate imageFollowDectecWithBuf:(CVPixelBufferRef)pNode->pBuf bufCount:pNode->len];
                    }
                }
                
                dispatch_semaphore_signal(self->playSemaphore);
            });
        }

        CodeUnlock(&s_glLock);
        
        dispatch_semaphore_wait(playSemaphore, DISPATCH_TIME_FOREVER);
        
        if (IS_NOTUSE_VTB_DECODE) {
            free(pNode->pBuf);
            pNode->pBuf = NULL;
        }else{
            if (VTB_DECODE){
                [self.s_vtbDecode vtb_release:pNode->pBuf];
                pNode->pBuf = NULL;
            }else{
                free(pNode->pBuf);
                pNode->pBuf = NULL;
            }
        }
        // 完成后将block压回空闲链, 继续拿来用
        CodeLock(&s_yuvFreelistLock);
        myPacketListPush(&s_yuvFreeList, pNode);
        CodeUnlock(&s_yuvFreelistLock);
        
        dispatch_semaphore_signal(nodeSemaphore);
    }
    
    // 线程是否完全退出的标识
    s_bVPlayThreadIsOver = 1;
}

#pragma mark - 延时播放线程
-(void)delayPlayThread{
    
    while (myPacketListCount(&s_yuvUsedList) < ([VisonWifiBaseLibraryUtility delayFrameCount] + 1)) {
        usleep(30000);
    }
    
    dispatch_queue_t cacheFrameQueue = dispatch_queue_create("cacheQueue", DISPATCH_QUEUE_CONCURRENT);
    self->lostFrame = NO;
    self->delayPlayTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, cacheFrameQueue);
    dispatch_source_set_timer(self->delayPlayTimer, DISPATCH_TIME_NOW, (1.0/[VisonWifiBaseLibraryUtility delayFrameRate]) * NSEC_PER_SEC, 0 * NSEC_PER_SEC);
    dispatch_source_set_event_handler(self->delayPlayTimer, ^{
        
       //出现丢帧
       if (self->lostFrame) {
             dispatch_suspend(self->delayPlayTimer);
             while (myPacketListCount(&s_yuvUsedList) < ([VisonWifiBaseLibraryUtility delayFrameCount] + 1)) {
                 usleep(30000);
             }
             dispatch_resume(self->delayPlayTimer);
             self->lostFrame = NO;
       }
       
        if (myPacketListCount(&s_yuvUsedList) > ([VisonWifiBaseLibraryUtility delayFrameCount] + 1)) {
            for (int i = myPacketListCount(&s_yuvUsedList); i >= ([VisonWifiBaseLibraryUtility delayFrameCount] + 1) ; i--) {
                
                myPacketNode_t *pNode = NULL;
                CodeLock(&s_yuvUsedlistLock);
                pNode = myPacketListPop(&s_yuvUsedList);
                CodeUnlock(&s_yuvUsedlistLock);
                CodeLock(&s_glLock);

                if (IS_NOTUSE_VTB_DECODE) {
                    @autoreleasepool {
                        UIImage *image = [UIImage imageWithData:[NSData dataWithBytes:pNode->pBuf length:pNode->len]];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (self.delegate && [self.delegate respondsToSelector:@selector(processImage:frameTime:frameWidth:frameHeight:)]) {
                                [self.delegate processImage:image
                                                  frameTime:CMTimeMake(pNode->pts, 10000)
                                                 frameWidth:pNode->vWidth
                                                frameHeight:pNode->vHeight];
                            }
                        });
                        
                        self.movieWriter.writingPixelBuff = [VisonWifiBaseLibraryUtility pixelBufferFromCGImage:image.CGImage];
                        dispatch_semaphore_signal(self->playSemaphore);
                    }
                }else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (self.delegate && [self.delegate respondsToSelector:@selector(processCVPixelBuffer:frameTime:frameWidth:frameHeight:)]) {
                            [self.delegate processCVPixelBuffer:(CVPixelBufferRef)pNode->pBuf
                                                      frameTime:CMTimeMakeWithSeconds((Float64)pNode->pts/1000000, pNode->framerate)
                                                     frameWidth:(int)CVPixelBufferGetWidth((CVPixelBufferRef)pNode->pBuf)
                                                    frameHeight:(int)CVPixelBufferGetHeight((CVPixelBufferRef)pNode->pBuf)];
                        }
                        
                        self.movieWriter.writingPixelBuff = [VisonWifiBaseLibraryUtility YUVBufferCopyWithPixelBuffer:(CVPixelBufferRef)pNode->pBuf];
                        
                        if (self.gestureEnable) {
                            //图像识别动作 拍照录像。。。
                            [self handImageRecognitionWithBuf:(CVPixelBufferRef)pNode->pBuf];
                        }
                        
                        if (self.imageFollowEnable) {
                            //跟随
                            if (self.delegate && [self.delegate respondsToSelector:@selector(imageFollowDectecWithBuf:bufCount:)]) {
                                [self.delegate imageFollowDectecWithBuf:(CVPixelBufferRef)pNode->pBuf bufCount:pNode->len];
                            }
                        }
                        
                        dispatch_semaphore_signal(self->playSemaphore);
                    });
                }
                
                CodeUnlock(&s_glLock);
                
                dispatch_semaphore_wait(playSemaphore, DISPATCH_TIME_FOREVER);
                
                if (IS_NOTUSE_VTB_DECODE) {
                    free(pNode->pBuf);
                    pNode->pBuf = NULL;
                }else{
                    if (VTB_DECODE){
                        [self.s_vtbDecode vtb_release:pNode->pBuf];
                        pNode->pBuf = NULL;
                    }else{
                        free(pNode->pBuf);
                        pNode->pBuf = NULL;
                    }
                }
                
                // 完成后将block压回空闲链, 继续拿来用
                CodeLock(&s_yuvFreelistLock);
                myPacketListPush(&s_yuvFreeList, pNode);
                CodeUnlock(&s_yuvFreelistLock);
            }
        }else if (myPacketListCount(&s_yuvUsedList) > 0){
            //            NSLog(@"YVU使用播了1张")
            myPacketNode_t *pNode = NULL;
            CodeLock(&s_yuvUsedlistLock);
            pNode = myPacketListPop(&s_yuvUsedList);
            CodeUnlock(&s_yuvUsedlistLock);
            CodeLock(&s_glLock);

            if (IS_NOTUSE_VTB_DECODE) {
                @autoreleasepool {
                    UIImage *image = [UIImage imageWithData:[NSData dataWithBytes:pNode->pBuf length:pNode->len]];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (self.delegate && [self.delegate respondsToSelector:@selector(processImage:frameTime:frameWidth:frameHeight:)]) {
                            [self.delegate processImage:image
                                              frameTime:CMTimeMake(pNode->pts, 10000)
                                             frameWidth:pNode->vWidth
                                            frameHeight:pNode->vHeight];
                        }
                    });
                    
                    self.movieWriter.writingPixelBuff = [VisonWifiBaseLibraryUtility pixelBufferFromCGImage:image.CGImage];
                    dispatch_semaphore_signal(self->playSemaphore);
                }
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (self.delegate && [self.delegate respondsToSelector:@selector(processCVPixelBuffer:frameTime:frameWidth:frameHeight:)]) {
                        [self.delegate processCVPixelBuffer:(CVPixelBufferRef)pNode->pBuf
                                                  frameTime:CMTimeMakeWithSeconds((Float64)pNode->pts/1000000, pNode->framerate)
                                                 frameWidth:(int)CVPixelBufferGetWidth((CVPixelBufferRef)pNode->pBuf)
                                                frameHeight:(int)CVPixelBufferGetHeight((CVPixelBufferRef)pNode->pBuf)];
                    }
                    
                    self.movieWriter.writingPixelBuff = [VisonWifiBaseLibraryUtility YUVBufferCopyWithPixelBuffer:(CVPixelBufferRef)pNode->pBuf];
                    
                    if (self.gestureEnable) {
                        //图像识别动作 拍照录像。。。
                        [self handImageRecognitionWithBuf:(CVPixelBufferRef)pNode->pBuf];
                    }
                    
                    if (self.imageFollowEnable) {
                        //跟随
                        if (self.delegate && [self.delegate respondsToSelector:@selector(imageFollowDectecWithBuf:bufCount:)]) {
                            [self.delegate imageFollowDectecWithBuf:(CVPixelBufferRef)pNode->pBuf bufCount:pNode->len];
                        }
                    }
                    
                    dispatch_semaphore_signal(self->playSemaphore);
                });
            }
            
            CodeUnlock(&s_glLock);
            
            dispatch_semaphore_wait(playSemaphore, DISPATCH_TIME_FOREVER);
            
            if (IS_NOTUSE_VTB_DECODE) {
                free(pNode->pBuf);
                pNode->pBuf = NULL;
            }else{
                if (VTB_DECODE){
                    [self.s_vtbDecode vtb_release:pNode->pBuf];
                    pNode->pBuf = NULL;
                }else{
                    free(pNode->pBuf);
                    pNode->pBuf = NULL;
                }
            }
            
            //         完成后将block压回空闲链, 继续拿来用
            CodeLock(&s_yuvFreelistLock);
            myPacketListPush(&s_yuvFreeList, pNode);
            CodeUnlock(&s_yuvFreelistLock);
        }
    });
    dispatch_resume(self->delayPlayTimer);
}

#pragma mark - 创建图像识别链表
-(void)createTrackPacketList{
    NSLog(@"创建图像识别链表");
    myPacketListInit(&s_trackYuvFreeList);
    myPacketListInit(&s_trackYuvUsedList);
    CreateCodeLock(&s_trackYuvFreeListLock);
    CreateCodeLock(&s_trackYuvUsedListLock);
    CreateCodeLock(&s_detectLock);
    
    lastState = -1;
    actionState = 1;
    
    int i;
    myPacketNode_t* pNode = NULL;
    
    // video packet list init
    CodeLock(&s_trackYuvFreeListLock);
    for (i = 0; i < 25; i++){
        pNode = (myPacketNode_t*)malloc(sizeof(myPacketNode_t));
        memset(pNode, 0, sizeof(myPacketNode_t));
        myPacketListPush(&s_trackYuvFreeList, pNode);
    }
    CodeUnlock(&s_trackYuvFreeListLock);
    
    
    s_TrackYuvThreadRunning = 1;
    s_TrackYuvThreadIsOver = 0;
    VTrackThread = [[NSThread alloc] initWithTarget:self
                                           selector:@selector(VTrackThread)
                                             object:nil];
}


#pragma mark - 销毁图像识别链表
-(void)releaseTrackPakcetList{
    NSLog(@"销毁图像识别链表");
    self->lastState = -1;
    self->actionState = 1;
    
    myPacketNode_t* pNode = NULL;
    int i, count;
    
    CodeLock(&s_trackYuvUsedListLock);
    count = myPacketListCount(&s_trackYuvUsedList);
    for (i = 0; i < count; i++)
    {
        pNode = myPacketListPop(&s_trackYuvUsedList);
        free(pNode->pBuf);
        free(pNode);
    }
    CodeUnlock(&s_trackYuvUsedListLock);
    
    // free video packet list
    CodeLock(&s_trackYuvFreeListLock);
    count = myPacketListCount(&s_trackYuvFreeList);
    for (i = 0; i < count; i++)
    {
        pNode = myPacketListPop(&s_trackYuvFreeList);
        free(pNode->pBuf);
        free(pNode);
    }
    CodeUnlock(&s_trackYuvFreeListLock);
    
    DestoryCodeLock(&s_trackYuvFreeListLock);
    DestoryCodeLock(&s_trackYuvUsedListLock);
    
    DestoryCodeLock(&s_detectLock);
}

#pragma mark - 图像识别线程
-(void)VTrackThread{
    myPacketNode_t *pNode = NULL;
    while (s_TrackYuvThreadRunning){
        if (0 == myPacketListCount(&s_trackYuvUsedList)){
            usleep(1000);
            continue;
        }
        
        CodeLock(&s_trackYuvUsedListLock);
        pNode = myPacketListPop(&s_trackYuvUsedList);
        CodeUnlock(&s_trackYuvUsedListLock);
        
        if (pNode->pBuf != NULL && actionState == 1){
            CodeLock(&s_detectLock);
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(gestureDetectWithBuf:bufW:bufH:bufSize:)]) {
                
                if([self.delegate gestureDetectWithBuf:pNode->pBuf bufW:pNode->vWidth bufH:pNode->vHeight bufSize:pNode->vWidth*pNode->vHeight*3/2]){
                    self->actionState = 0;
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        self->actionState = 1;
                    });
                }
            }
            
            CodeUnlock(&s_detectLock);
            self->lastState = -1;
        }
        
        if (pNode){
            if (pNode->pBuf){
                free(pNode->pBuf);
            }
            pNode->pBuf = NULL;
            
            // 完成后将block压回空闲链, 继续拿来用
            CodeLock(&s_trackYuvFreeListLock);
            myPacketListPush(&s_trackYuvFreeList, pNode);
            CodeUnlock(&s_trackYuvFreeListLock);
        }
    }
    // 线程是否完全退出的标识
    s_TrackYuvThreadIsOver = 1;
}

unsigned  long long   getMS()
{
    struct timeval l_tv;
    gettimeofday(&l_tv,NULL);
    unsigned long long l_ret = 0;
    l_ret = (l_tv.tv_sec&0xFFFFFFFF)*1000;
    l_ret += (l_tv.tv_usec/1000);
    return l_ret;
}

#pragma mark - 将yuv数据放入图像处理的链表中，用于手势拍照录像
-(void)handImageRecognitionWithBuf:(CVPixelBufferRef)pixelBuf{
    
    //图像识别 没有动作时候为 -1 500ms识别一次
    if (lastState == -1){
        unsigned  long long curMS = getMS();
        
        if (curMS - lastMS >= 500){
            
            lastMS = curMS;
            
            int trackCount = myPacketListCount(&s_trackYuvFreeList);
            if (trackCount > 0){
                
                char *YuvData = NULL;
                int width;
                int height;
                
                [self getByteWithCVPixelBuffer:pixelBuf byte:&YuvData width:&width height:&height];
                
                //从空闲链表取node
                CodeLock(&s_trackYuvFreeListLock);
                myPacketNode_t* trackNode  = myPacketListPop(&s_trackYuvFreeList);
                CodeUnlock(&s_trackYuvFreeListLock);
                
                trackNode->pBuf = YuvData;
                trackNode->vWidth = width;
                trackNode->vHeight = height;
                
                //存进要用的链表
                CodeLock(&s_trackYuvUsedListLock);
                myPacketListPush(&s_trackYuvUsedList, trackNode);
                CodeUnlock(&s_trackYuvUsedListLock);
            }
        }
    }else{ //有动作继续识别
        
        int trackCount = myPacketListCount(&s_trackYuvFreeList);
        if (trackCount > 0){
            char *YuvData = NULL;
            int width;
            int height;
            
            [self getByteWithCVPixelBuffer:pixelBuf byte:&YuvData width:&width height:&height];
            
            //从空闲链表取node
            CodeLock(&s_trackYuvFreeListLock);
            myPacketNode_t* trackNode  = myPacketListPop(&s_trackYuvFreeList);
            CodeUnlock(&s_trackYuvFreeListLock);
            
            trackNode->pBuf = YuvData;
            trackNode->vWidth = width;
            trackNode->vHeight = height;
            
            //存进要用的链表
            CodeLock(&s_trackYuvUsedListLock);
            myPacketListPush(&s_trackYuvUsedList, trackNode);
            CodeUnlock(&s_trackYuvUsedListLock);
        }
    }
}

#pragma mark - pixelBuf转YUV
- (void)getByteWithCVPixelBuffer:(CVPixelBufferRef)pixelBuf byte:(char **)yuvData width:(int *)width height:(int *)height{
    CVPixelBufferLockBaseAddress(pixelBuf, 0);
    
    char *Ydata = NULL;
    char *UVdata = NULL;
    
    size_t bytesPerRow;
    
    Ydata = (char *)CVPixelBufferGetBaseAddressOfPlane(pixelBuf, 0);
    UVdata = (char *)CVPixelBufferGetBaseAddressOfPlane(pixelBuf, 1);
    
    *width = (int)CVPixelBufferGetWidthOfPlane(pixelBuf, 0);
    *height = (int)CVPixelBufferGetHeightOfPlane(pixelBuf, 0);
    bytesPerRow = CVPixelBufferGetBytesPerRowOfPlane(pixelBuf, 0);
    
    char *YuvData =(char *) malloc((*width)*(*height)*3/2);
    
    memset(YuvData, 0, (*width)*(*height)*3/2);
    memcpy(YuvData, Ydata, (*width)*(*height));
    memcpy(YuvData+((*width)*(*height)), UVdata, (*width)*(*height)/2);
    
    *yuvData = YuvData;
    CVPixelBufferUnlockBaseAddress(pixelBuf, 0);
}



#pragma mark - 开始接收视频流
-(void)startReceiveStreamcComplete:(void (^)())complete{
    
    if ([WIFIDeviceModel shareInstance].playType == PlayType_None || self.isReceiving){
        if ([WIFIDeviceModel shareInstance].playType == PlayType_None) NSLog(@"startReceiveStream Error：playType = None");
        if (self.isReceiving) NSLog(@"startReceiveStream Error：视频流已在接收中");
        return;
    }
    
    //创建链表
    [self createStreamPacketList];
    //初始图像跟随化链表
    if (self.gestureEnable) {
        [self createTrackPacketList];
    }
    //开启解码线程
    [VDecThread start];
    //开始播放线程
    [VPlayThread start];
    //开启图像识别线程
    if (self.gestureEnable) {
        [VTrackThread start];
    }
    
    switch ([WIFIDeviceModel shareInstance].playType) {
        case PlayType_TCP:
        {
            NSLog(@"startTcpPlay %@:%d",[StreamPreplayConfig shareInstance].streamIP,[StreamPreplayConfig shareInstance].streamPort);
            if([VisonPTUSBHandle shareInstance].isConnected){
                playHandle = StartUSBHMDTcpPlay([StreamPreplayConfig shareInstance].devidetype, fDataCB, (__bridge void *)(self));
            }else{
                playHandle = StartHMDTcpPlay((char *)[StreamPreplayConfig shareInstance].streamIP.UTF8String, [StreamPreplayConfig shareInstance].streamPort, [StreamPreplayConfig shareInstance].devidetype, fDataCB, (__bridge void *)(self));
            }
        }
            break;
        case PlayType_UDP:
        {
            NSLog(@"startUdpPlay %@:%d",[StreamPreplayConfig shareInstance].streamIP,[StreamPreplayConfig shareInstance].streamPort);
          
            self->playHandle = StartVodPlay((char *)[StreamPreplayConfig shareInstance].streamIP.UTF8String, [StreamPreplayConfig shareInstance].streamPort, 0, [StreamPreplayConfig shareInstance].devidetype, fDataCB, (__bridge void *)(self));
            [[DataTransferManager shareInstance] starUDPPlaySendHeartBeatPacket];
        }
            break;
        case PlayType_RTSP:
        {
            __weak typeof(self)weakSelf = self;
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                NSLog(@"startRtspPlay %@", [StreamPreplayConfig shareInstance].streamPath);
                self->wantToExit = NO;
                while ((self->playHandle = StartRtspPlay((char *)[StreamPreplayConfig shareInstance].streamPath.UTF8String, [StreamPreplayConfig shareInstance].devidetype, fDataCB,failReadFreamCB, (__bridge void *)(weakSelf),1) ) == NULL
                       && self->wantToExit == NO){
                    [NSThread sleepForTimeInterval:0.5];    // try to connect every 500ms if failed
                }
            });
        }
            break;
        default:
            break;
    }
    
    if (!_isReceiving) _isReceiving = YES;
    
    if (complete) {
        complete();
    }
}

#pragma mark - 停止接收视频流
-(void)stopReceiveStreamComplete:(void (^)())complete{
    
    if (!self.isReceiving){
        NSLog(@"stopReceiveStream Error: 视频流本不在接收状态");
        if (complete) {
            complete();
        }
        return;
    }
    
    switch ([WIFIDeviceModel shareInstance].playType) {
        case PlayType_TCP:
        {
            NSLog(@"stopTcpPlay %@:%d",[StreamPreplayConfig shareInstance].streamIP,[StreamPreplayConfig shareInstance].streamPort);
            if (playHandle) {
                if ([VisonPTUSBHandle shareInstance].isConnected) {
                    StopUSBHMDTcpPlay(playHandle);
                }else{
                    StopHMDTcpPlay(playHandle);
                }
            }
        }
            break;
        case PlayType_UDP:
        {
            [[DataTransferManager shareInstance]stopSendUDPStream];
            NSLog(@"stopUdpPlay %@:%d",[StreamPreplayConfig shareInstance].streamIP,[StreamPreplayConfig shareInstance].streamPort);
            if (playHandle) {
                StopVodPlay(playHandle);
            }
        }
            break;
        case PlayType_RTSP:
        {
            NSLog(@"startRtspPlay %@", [StreamPreplayConfig shareInstance].streamPath);
            if (playHandle) {
                StopRtspPlay(playHandle);
            }
        }
            break;
        default:
            break;
    }
    playHandle = 0;
    
    // stop thread
    s_bVDecThreadRunning = 0;
    while (!s_bVDecThreadIsOver)
        usleep(1000);

    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        self->s_bVPlayThreadRunning = 0;
        while (!self->s_bVPlayThreadIsOver) {
            usleep(1000);
        }
        self->playSemaphore = 0;
        self->nodeSemaphore = 0;
    });
    
    if (self.gestureEnable) {
            // stop thread
        s_TrackYuvThreadRunning = 0;
        while (!s_TrackYuvThreadIsOver)
            usleep(1000);
    }
    
    [self releaseStreamPacketList];
    if (self.gestureEnable) {
        [self releaseTrackPakcetList];
    }
    
    if (_isReceiving) _isReceiving = NO;
    
    if (!IS_NOTUSE_VTB_DECODE) {
        [self initDecode];
    }
    
    if (complete) {
        complete();
    }
    
}

#pragma mark - 流里拿图片
-(void)asyncGetPhotoFromStream:(void (^)(BOOL, UIImage *))complete{
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        while (self->s_bVPlayThreadRunning){
            dispatch_semaphore_wait(self->nodeSemaphore, DISPATCH_TIME_FOREVER);
            
            myPacketNode_t *pNode = NULL;
            if (0 == myPacketListCount(&self->s_yuvUsedList)){
                usleep(1000);
                dispatch_semaphore_signal(self->nodeSemaphore);
                continue;
            }
            
            CodeLock(&self->s_yuvUsedlistLock);
            pNode = myPacketListPop(&self->s_yuvUsedList);
            CodeUnlock(&self->s_yuvUsedlistLock);
            
            if (IS_NOTUSE_VTB_DECODE) {
                self->mImg = [UIImage imageWithData:[NSData dataWithBytes:pNode->pBuf length:pNode->len]];
            }else{
                self->mImg = [self vtbShotImage:pNode];
                NSLog(@"image - %@",self->mImg);
            }
            
            if (self->mImg){
                
                
                
                if (complete) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        complete(YES,self->mImg);
                    });
                }
            }else{
                if (complete) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        complete(NO,nil);
                    });
                }
            }
            
            if (IS_NOTUSE_VTB_DECODE) {
                free(pNode->pBuf);
                pNode->pBuf = NULL;
            }else{
                if (VTB_DECODE){
                    [self.s_vtbDecode vtb_release:pNode->pBuf];
                    pNode->pBuf = NULL;
                }else{
                    free(pNode->pBuf);
                    pNode->pBuf = NULL;
                }
            }
            
            // 完成后将block压回空闲链, 继续拿来用
            CodeLock(&self->s_yuvFreelistLock);
            myPacketListPush(&self->s_yuvFreeList, pNode);
            CodeUnlock(&self->s_yuvFreelistLock);
            
            dispatch_semaphore_signal(self->nodeSemaphore);
            break;
        }
    });
}

#pragma mark - 取原图
-(void)asyncGetStreamPhotoAndSourcePhotoInProgress:(void (^)(float))progress
                                          complete:(void (^)(UIImage *, UIImage *))complete{

    dispatch_group_t group = dispatch_group_create();
    
    dispatch_group_enter(group);
    
    __block UIImage *streamPhoto;
    __block UIImage *sourceTmpImage;
    //拿截图
    [self asyncGetPhotoFromStream:^(BOOL isRec, UIImage *image) {
        if (isRec) {
            streamPhoto = image;
        }
        dispatch_group_leave(group);
    }];
    //拿源图
    
    dispatch_group_enter(group);
    
    [VisonWifiBaseLibraryUtility getTheSourceImageInProgress:progress complete:^(BOOL isRec, UIImage * _Nonnull sourceImage) {
        if (isRec) {
            sourceTmpImage = sourceImage;
        }
        dispatch_group_leave(group);
    }];
    
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        if (complete) {
            complete(streamPhoto,sourceTmpImage);
        }
    });
}

- (UIImage *)vtbShotImage:(myPacketNode_t*)pNode{
#define clamp(a) (a>255?255:(a<0?0:a))
    
    CVPixelBufferRef imageBuffer = (CVPixelBufferRef)pNode->pBuf;
    CVPixelBufferLockBaseAddress(imageBuffer,0);    // lock
    
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    uint8_t *yBuffer = (uint8_t *)CVPixelBufferGetBaseAddressOfPlane(imageBuffer, 0);
    size_t yPitch = CVPixelBufferGetBytesPerRowOfPlane(imageBuffer, 0);
    uint8_t *cbCrBuffer = (uint8_t *)CVPixelBufferGetBaseAddressOfPlane(imageBuffer, 1);
    size_t cbCrPitch = CVPixelBufferGetBytesPerRowOfPlane(imageBuffer, 1);
    
    int bytesPerPixel = 4;
    uint8_t *rgbBuffer = (uint8_t *)malloc(width * height * bytesPerPixel);
    
    for(int y = 0; y < height; y++) {
        uint8_t *rgbBufferLine = &rgbBuffer[y * width * bytesPerPixel];
        uint8_t *yBufferLine = &yBuffer[y * yPitch];
        uint8_t *cbCrBufferLine = &cbCrBuffer[(y >> 1) * cbCrPitch];
        
        for(int x = 0; x < width; x++) {
            int16_t y = yBufferLine[x];
            int16_t cb = cbCrBufferLine[x & ~1] - 128;
            int16_t cr = cbCrBufferLine[x | 1] - 128;
            
            uint8_t *rgbOutput = &rgbBufferLine[x*bytesPerPixel];
            
            int16_t r = (int16_t)roundf( y + cr *  1.4 );
            int16_t g = (int16_t)roundf( y + cb * -0.343 + cr * -0.711 );
            int16_t b = (int16_t)roundf( y + cb *  1.765);
            
            rgbOutput[0] = 0xff;
            rgbOutput[1] = clamp(b);
            rgbOutput[2] = clamp(g);
            rgbOutput[3] = clamp(r);
        }
    }
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(rgbBuffer, width, height, 8, width * bytesPerPixel, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaNoneSkipLast);
    CGImageRef quartzImage = CGBitmapContextCreateImage(context);
    UIImage *image = [UIImage imageWithCGImage:quartzImage];
    
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    CGImageRelease(quartzImage);
    free(rgbBuffer);
    
    CVPixelBufferUnlockBaseAddress(imageBuffer, 0);     // unlock
    return image;
}

#pragma mark - MoviewWrite

-(MovieWriter *)movieWriter{
    if (!_movieWriter) {
        _movieWriter = [[MovieWriter alloc]init];
    }
    return _movieWriter;
}

-(BOOL)isWritingMovie{
    return self.movieWriter.isWriting;
}

-(void)starWriteMovieIsSaveLocal:(BOOL)isSaveLocal
                    isSaveTFcard:(BOOL)isSaveTFCard
                      ToFilePath:(NSString *)filePath
                       frameRate:(int)frameRate
                       frameSize:(CGSize)frameSize
                     audioEnable:(BOOL)audioEnable
                        complete:(void (^)(void))complete{
    
    self->isSaveLocal = isSaveLocal;
    self->isSaveTFCard = isSaveTFCard;
    
    if (isSaveLocal) {
        [self.movieWriter starWriteMovieWithType:Source_PixelBuff_Type_YUV
                                        FilePath:filePath
                                       frameRate:frameRate
                                       frameSize:frameSize
                                     audioEnable:audioEnable complete:^{
            
            if (isSaveTFCard) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[SocketControlManager shareInstance] sendTcpCommand:[Device_Wifi_Config_Command getStartRecordCommand] tag:9090];
                });
            }
            
            if (complete) {
                complete();
            }
            
        }];
    }else{
        
        if (isSaveTFCard) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[SocketControlManager shareInstance] sendTcpCommand:[Device_Wifi_Config_Command getStartRecordCommand] tag:9090];
            });
        }
        
        if (complete) {
            complete();
        }
    }
}

-(void)stopWriteMovieComplete:(void (^)(NSString *))complete{
    
    
    if (isSaveLocal) {
        
        [self.movieWriter stopWriteMovieComplete:^(NSString * _Nonnull moviePath) {
            
            if (self->isSaveTFCard) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[SocketControlManager shareInstance] sendTcpCommand:[Device_Wifi_Config_Command getStopRecordCommand] tag:9191];
                });
            }
            
            if (complete) {
                complete(moviePath);
            }
        }];
        
    }else{
        
        if (isSaveTFCard) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[SocketControlManager shareInstance] sendTcpCommand:[Device_Wifi_Config_Command getStopRecordCommand] tag:9191];
            });
        }
        
        if (complete) {
            complete(@"");
        }
    }
}



@end
