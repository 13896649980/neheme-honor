#include "LoopBuf.h"
#include "CodeLock.h"

#ifdef WIN32
#   include <windows.h>
#else
#   include <stdlib.h>
#   include <string.h>
#endif

typedef struct 
{
    CODELOCK mutex;
    unsigned int dwCurRead;
	unsigned int dwCurWrite;
	unsigned int dwSizeInUse;
	unsigned int dwBufSize;
	char *pBuf;
}LBUF_t;

LBUFHANDLE LBUF_Create(unsigned int dwMemSize)
{
    LBUF_t* pHandle = (LBUF_t*)malloc(sizeof(LBUF_t));
    if (!pHandle || 0 == dwMemSize)
        return 0;

    memset(pHandle, 0, sizeof(LBUF_t));

    pHandle->pBuf = (char*)malloc(dwMemSize);
    if (!pHandle->pBuf)
    {
        free(pHandle);
        return 0;
    }

    pHandle->dwCurRead = 0;
    pHandle->dwCurWrite = 0;
    pHandle->dwSizeInUse = 0;
    pHandle->dwBufSize = dwMemSize;
    CreateCodeLock(&pHandle->mutex);

    return (LBUFHANDLE)pHandle;
}

unsigned char LBUF_Destory(LBUFHANDLE dwLBUFHandle)
{
    LBUF_t* pHandle = (LBUF_t*)dwLBUFHandle;
    if (!pHandle)
        return 0;

    if (pHandle->pBuf)
        free(pHandle->pBuf);
    DestoryCodeLock(&pHandle->mutex);
    free(pHandle);
    
    return 0;
}

unsigned char LBUF_Write(LBUFHANDLE dwLBUFHandle, char* pSrcBuf, unsigned int dwWriteLen)
{
    unsigned int tmpSize = 0;
    LBUF_t* pHandle = (LBUF_t*)dwLBUFHandle;
    if (!pHandle)
        return 0;  

    if (!pSrcBuf || 0 == dwWriteLen)
        return 0;

	CodeLock(&pHandle->mutex);

    if (dwWriteLen > pHandle->dwBufSize - pHandle->dwSizeInUse)
	{
		LBUF_Clear(dwLBUFHandle);

        CodeUnlock(&pHandle->mutex);
		return 0;
	}

	if (pHandle->dwCurWrite + dwWriteLen <= pHandle->dwBufSize)
	{
		memcpy(pHandle->pBuf + pHandle->dwCurWrite, pSrcBuf, dwWriteLen);
		pHandle->dwCurWrite  += dwWriteLen;
		pHandle->dwSizeInUse += dwWriteLen;
		if (pHandle->dwCurWrite == pHandle->dwBufSize)
		{
			pHandle->dwCurWrite = 0;
		}
	}
	else
	{
		tmpSize = pHandle->dwBufSize - pHandle->dwCurWrite;
		memcpy(pHandle->pBuf + pHandle->dwCurWrite, pSrcBuf, tmpSize);
		pHandle->dwCurWrite = dwWriteLen - tmpSize;
		memcpy(pHandle->pBuf, pSrcBuf + tmpSize, dwWriteLen - tmpSize);		
		pHandle->dwSizeInUse += dwWriteLen;
	}

    CodeUnlock(&pHandle->mutex);

    return 1;
}

unsigned char LBUF_Read(LBUFHANDLE dwLBUFHandle, char* pDstBuf, unsigned int* pReadLen)
{
    unsigned int tmpSize = 0;
    LBUF_t* pHandle = (LBUF_t*)dwLBUFHandle;
    if (!pHandle)
        return 0;

	if (!pDstBuf || 0 == *pReadLen)
		return 0;

    CodeLock(&pHandle->mutex);

	if (*pReadLen > pHandle->dwSizeInUse)
	{
		*pReadLen = pHandle->dwSizeInUse;
        CodeUnlock(&pHandle->mutex);
		return 0;
	}

	if (pHandle->dwCurRead + *pReadLen <= pHandle->dwBufSize)
	{
		memcpy(pDstBuf, pHandle->pBuf + pHandle->dwCurRead, *pReadLen);
		pHandle->dwCurRead	 += *pReadLen;
		pHandle->dwSizeInUse -= *pReadLen;
		if (pHandle->dwCurRead == pHandle->dwBufSize)
		{
			pHandle->dwCurRead = 0;
		}
	}
	else
	{
		tmpSize = pHandle->dwBufSize - pHandle->dwCurRead;
		memcpy(pDstBuf, pHandle->pBuf + pHandle->dwCurRead, tmpSize);
		pHandle->dwCurRead    = *pReadLen - tmpSize;
		pHandle->dwSizeInUse -= *pReadLen;
		memcpy(pDstBuf + tmpSize, pHandle->pBuf, pHandle->dwCurRead);
	}

    CodeUnlock(&pHandle->mutex);
    
	return 1;
}

unsigned char LBUF_PreRead(LBUFHANDLE dwLBUFHandle, char* pDstBuf, unsigned int* pReadLen, unsigned int dwOffset, unsigned char bLock)
{
    unsigned int tmpSize, readPos;
    LBUF_t* pHandle = (LBUF_t*)dwLBUFHandle;
    if (!pHandle)
        return 0;

    if (bLock)
        CodeLock(&pHandle->mutex);

	if (!pDstBuf || pHandle->dwSizeInUse == 0 || *pReadLen == 0 || pHandle->dwSizeInUse <= dwOffset)
	{
		*pReadLen = 0;
        if (bLock)
            CodeUnlock(&pHandle->mutex);

		return 0;
	}

	if (*pReadLen > pHandle->dwSizeInUse - dwOffset)
	{
		*pReadLen = pHandle->dwSizeInUse - dwOffset;
	}

	if (pHandle->dwCurRead + dwOffset < pHandle->dwBufSize)
	{
		readPos = pHandle->dwCurRead + dwOffset;
	}
	else
	{
		readPos = pHandle->dwCurRead + dwOffset - pHandle->dwBufSize;
	}
	
	if (readPos + *pReadLen <= pHandle->dwBufSize)
	{
		memcpy(pDstBuf, pHandle->pBuf + readPos, *pReadLen);
	}
	else
	{
		tmpSize = pHandle->dwBufSize - readPos;
		memcpy(pDstBuf, pHandle->pBuf + readPos, tmpSize);
		memcpy(pDstBuf + tmpSize, pHandle->pBuf, *pReadLen - tmpSize);
	}

	if (bLock)
	    CodeUnlock(&pHandle->mutex);

	return 1;
}

unsigned char LBUF_SetReadPos(LBUFHANDLE dwLBUFHandle, unsigned int dwRead, unsigned char bLock)
{
    LBUF_t* pHandle = (LBUF_t*)dwLBUFHandle;
    if (!pHandle || 0 == dwRead)
        return 0;

    if (bLock)
        CodeLock(&pHandle->mutex);

	if (dwRead > pHandle->dwSizeInUse)
	{
		LBUF_Clear(dwLBUFHandle);
		return 1;
	}

	if (pHandle->dwCurRead + dwRead <= pHandle->dwBufSize)
	{
		pHandle->dwCurRead	 += dwRead;
		pHandle->dwSizeInUse -= dwRead;
		if (pHandle->dwCurRead == pHandle->dwBufSize)
		{
			pHandle->dwCurRead = 0;
		}
	}
	else
	{
		pHandle->dwCurRead    = dwRead - (pHandle->dwBufSize - pHandle->dwCurRead);
		pHandle->dwSizeInUse -= dwRead;
	}
	
	if (bLock)
        CodeUnlock(&pHandle->mutex);
    
    return 1;
}

unsigned char LBUF_GetBufStatus(LBUFHANDLE dwLBUFHandle)
{
    LBUF_t* pHandle = (LBUF_t*)dwLBUFHandle;
    if (!pHandle)
        return 0;

    CodeLock(&pHandle->mutex);

	if (3*pHandle->dwSizeInUse > 2*pHandle->dwBufSize)
	{
        CodeUnlock(&pHandle->mutex);
		return 0;
	}

    CodeUnlock(&pHandle->mutex);

	return 1;
}

unsigned char LBUF_Clear(LBUFHANDLE dwLBUFHandle)
{
    LBUF_t* pHandle = (LBUF_t*)dwLBUFHandle;
    if (!pHandle)
        return 0;

    CodeLock(&pHandle->mutex);

    pHandle->dwCurRead = 0;
    pHandle->dwCurWrite = 0;
    pHandle->dwSizeInUse = 0;

    CodeUnlock(&pHandle->mutex);

    return 1;
}

unsigned int LBUF_GetUsedSize(LBUFHANDLE dwLBUFHandle)
{
    unsigned int dwSizeInUse = 0;
    LBUF_t* pHandle = (LBUF_t*)dwLBUFHandle;
    if (!pHandle)
        return 0;

    CodeLock(&pHandle->mutex);
	dwSizeInUse = pHandle->dwSizeInUse;
    CodeUnlock(&pHandle->mutex);

    return dwSizeInUse;
}
unsigned int LBUF_GetNoUsedSize(LBUFHANDLE dwLBUFHandle)
{
    unsigned int dwRet = 0;
    LBUF_t* pHandle = (LBUF_t*)dwLBUFHandle;
    if (!pHandle)
        return 0;

    CodeLock(&pHandle->mutex);
	dwRet = pHandle->dwBufSize - pHandle->dwSizeInUse;
    CodeUnlock(&pHandle->mutex);

    return dwRet;
}

char* LBUF_GetPtr(LBUFHANDLE dwLBUFHandle)
{
    LBUF_t* pHandle = (LBUF_t*)dwLBUFHandle;
    if (!pHandle)
        return 0;
    
    return pHandle->pBuf;
}

unsigned char LBUF_Lock(LBUFHANDLE dwLBUFHandle)
{
    LBUF_t* pHandle = (LBUF_t*)dwLBUFHandle;
    if (!pHandle)
        return 0;

    CodeLock(&pHandle->mutex);
    return 1;
}

unsigned char LBUF_Unlock(LBUFHANDLE dwLBUFHandle)
{
    LBUF_t* pHandle = (LBUF_t*)dwLBUFHandle;
    if (!pHandle)
        return 0;

    CodeUnlock(&pHandle->mutex);
    return 1;
}

unsigned int LBUF_MallocBuf(LBUFHANDLE dwLBUFHandle, char** ppBuf)
{
    unsigned int dwRet = 0;
    LBUF_t* pHandle = (LBUF_t*)dwLBUFHandle;
    if (!pHandle)
        return 0;

    if (!ppBuf)
        return 0;

    CodeLock(&pHandle->mutex);

	*ppBuf = pHandle->pBuf + pHandle->dwCurRead;
	if (pHandle->dwSizeInUse + pHandle->dwCurRead < pHandle->dwBufSize)
	{
        dwRet = pHandle->dwSizeInUse;
	}
	else
	{
		dwRet = pHandle->dwBufSize - pHandle->dwCurRead;
	}

    CodeUnlock(&pHandle->mutex);

    return dwRet;
}

unsigned char LBUF_AdvGetWritePtr(LBUFHANDLE dwLBUFHandle, char** ppWritePtr1, unsigned int* pWriteLen1, char** ppWritePtr2, unsigned int* pWriteLen2)
{
    unsigned int dwSizeNoUse = 0;
    LBUF_t* pHandle = (LBUF_t*)dwLBUFHandle;
    if (!pHandle)
        return 0;

    CodeLock(&pHandle->mutex);

    dwSizeNoUse = pHandle->dwBufSize - pHandle->dwSizeInUse;

	if (0 == dwSizeNoUse)
	{
		CodeUnlock(&pHandle->mutex);
		return 0;
	}

    if (pHandle->dwCurWrite + dwSizeNoUse <= pHandle->dwBufSize)
    {
        *ppWritePtr1 = pHandle->pBuf + pHandle->dwCurWrite;
        *pWriteLen1 = dwSizeNoUse;
        
        *ppWritePtr2 = NULL;
        *pWriteLen2 = 0;
    }
    else
    {
        *ppWritePtr1 = pHandle->pBuf + pHandle->dwCurWrite;
        *pWriteLen1 = pHandle->dwBufSize - pHandle->dwCurWrite;

        *ppWritePtr2 = pHandle->pBuf;
        *pWriteLen2 = dwSizeNoUse - *pWriteLen1;
    }    

    CodeUnlock(&pHandle->mutex);

    return 1;
}

unsigned char LBUF_AdvSetWritePos(LBUFHANDLE dwLBUFHandle, unsigned int dwWriteLen)
{
    unsigned int tmpSize = 0;
    LBUF_t* pHandle = (LBUF_t*)dwLBUFHandle;
    if (!pHandle)
        return 0;

    CodeLock(&pHandle->mutex);

	if (dwWriteLen > (pHandle->dwBufSize-pHandle->dwSizeInUse))
	{
		CodeUnlock(&pHandle->mutex);
		return 0;
	}

	if (pHandle->dwCurWrite + dwWriteLen <= pHandle->dwBufSize)
	{
		pHandle->dwCurWrite  += dwWriteLen;
		pHandle->dwSizeInUse += dwWriteLen;
		if (pHandle->dwCurWrite == pHandle->dwBufSize)
		{
			pHandle->dwCurWrite = 0;
		}
	}
	else
	{
		tmpSize = pHandle->dwBufSize - pHandle->dwCurWrite;
		pHandle->dwCurWrite = dwWriteLen - tmpSize;
		pHandle->dwSizeInUse += dwWriteLen;
	}

    CodeUnlock(&pHandle->mutex);

    return 1;
}

unsigned char LBUF_AdvGetReadPtr(LBUFHANDLE dwLBUFHandle, char** ppReadPtr1, unsigned int* pReadLen1, char** ppReadPtr2, unsigned int* pReadLen2)
{
    LBUF_t* pHandle = (LBUF_t*)dwLBUFHandle;
    if (!pHandle)
        return 0;

    CodeLock(&pHandle->mutex);

    if (pHandle->dwCurRead + pHandle->dwSizeInUse <= pHandle->dwBufSize)
    {
        *ppReadPtr1 = pHandle->pBuf + pHandle->dwCurRead;
        *pReadLen1 = pHandle->dwSizeInUse;

        *ppReadPtr2 = NULL;
        *pReadLen2 = 0;
    }
    else
    {
        *ppReadPtr1 = pHandle->pBuf + pHandle->dwCurRead;
        *pReadLen1 = pHandle->dwBufSize - pHandle->dwCurRead;

        *ppReadPtr2 = pHandle->pBuf;
        *pReadLen2 = pHandle->dwSizeInUse - *pReadLen1;
    }

    CodeUnlock(&pHandle->mutex);

    return 1;
}
