#ifndef _INCLUDES_H_
#define _INCLUDES_H_

#include <stdio.h>

#if defined(WIN32)
#   include <WINSOCK2.H>
#   include <ws2tcpip.h>
#   pragma comment(lib, "ws2_32.lib")
#   include <windows.h>
#   define SLEEPMS(_ms_)     Sleep(_ms_);
#else
#   include <memory.h>
#   include <stdlib.h>
#   include <sys/types.h>
#   include <sys/socket.h>
#   include <netinet/in.h>
#   include <arpa/inet.h>
#   include <unistd.h>
#   include <pthread.h>
#   include <fcntl.h>
#   define SLEEPMS(_ms_)     usleep(_ms_*1000);
#   define closesocket      close
#endif

#if defined(_MSC_VER)
#   define U8      unsigned __int8
#   define U16     unsigned __int16
#   define U32     unsigned __int32
#   define U64     unsigned __int64
#else
#   define U8      unsigned char
#   define U16     unsigned short
#   define U32     unsigned int
#   define U64     unsigned long long
#endif


typedef struct
{
    U8  frameType;      // 0-IFrame, 1-PFrame, 2-BFrame
    U8  videoFormat;    // 0-H264
    U8  restartFlag;
    U8  res;
    U16 width;
    U16 height;
    U64 timeStamp;
    U8  res2[4];
    U8  framerate;
}VFrameHead_t;

typedef enum
{
    EN_DTYPE_FH8610 = 0x01,//VGS
    EN_DTYPE_FH8620 = 0x02,//IS_UDP_720P IS_UDP_720PYU
    EN_DTYPE_FH8810 = 0x03,
    EN_DTYPE_720P = 0x04,//720P
    EN_DTYPE_1080P = 0x05,//1080P
    EN_DTYPE_TCP_VGA = 0x06,
    EN_DTYPE_RTSP_720P = 0x07,
    EN_DTYPE_RTSP_1080P = 0x08,
    EN_DTYPE_2K = 0x09,
    EN_DTYPE_UNKNOWN = 0xFF,
}DeviceType_e;

#endif

