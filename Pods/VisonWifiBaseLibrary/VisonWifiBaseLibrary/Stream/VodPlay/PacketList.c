//
//  PacketList.c
//  jltClient
//
//  Created by sz.fullhan on 14-7-10.
//  Copyright (c) 2014年 jlt. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#include "PacketList.h"

//#define PL_HEAD     node.next
//#define PL_TAIL     node.prev
void myPacketListInit(myPacketList_t* pList)
{
    memset(pList, 0, sizeof(myPacketList_t));
}

int myPacketListCount(myPacketList_t* pList)
{
    return pList->count;
}

void myPacketListPush(myPacketList_t* pList, myPacketNode_t* pNode)
{
    myPacketNode_t *pNext;
    myPacketNode_t *pPrev = pList->node.prev;
    
    if (pPrev == NULL)
    {
        pNext = pList->node.next;
        pList->node.next = pNode;
    }
    else
    {
        pNext = pPrev->next;
        pPrev->next = pNode;
    }
    if (pNext == NULL)
        pList->node.prev = pNode;
    else
        pNext->prev = pNode;
 
    pNode->next = pNext;
    pNode->prev = pPrev;
    pList->count++;
}
myPacketNode_t* myPacketListPop(myPacketList_t* pList)
{
    myPacketNode_t* pNode = pList->node.next;

    if (pNode->prev == NULL)
        pList->node.next = pNode->next;
    else
        pNode->prev->next = pNode->next;
    
    if (pNode->next == NULL)
        pList->node.prev = pNode->prev;
    else
        pNode->next->prev = pNode->prev;
    
    pList->count--;
    
    return pNode;
}

myPacketNode_t* myPacketListPop_try(myPacketList_t* pList)
{
    return pList->node.next;
}

