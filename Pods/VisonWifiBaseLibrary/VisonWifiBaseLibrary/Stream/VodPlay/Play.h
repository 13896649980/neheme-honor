
#ifndef _VOD_PLAY_H_
#define _VOD_PLAY_H_

#include "Includes.h"



#ifdef __cplusplus
extern "C" {
#endif

typedef void (*fDataCallBack)(void* handle, VFrameHead_t* head, char* buf, U32 bufLen, void* user);
typedef void (*fFailReadFreamCallBack)(void* handle, void* user);
// bindIP: mcast bind local ip
// dtype: fh8610/fh8620/fh8810
// return: null-error, other-handle
void* StartVodPlay(char* ip, U16 port, char* bindIP, DeviceType_e dtype, fDataCallBack fun, void* user);
void StopVodPlay(void* handle);
void* CreateSendHandle(char* ip, U16 port, U8 protocol, char* destIP, U16 destPort);
// return: sendto()
int SendData(void* sendHandle, char* buf, U32 bufLen);
void DestorySendHandle(void* sendHandle);
void* StartRtspPlay(char* url, DeviceType_e dtype, fDataCallBack fun,fFailReadFreamCallBack failFun, void* user,int useTCP);
void StopRtspPlay(void* handle);

#ifdef __cplusplus
}
#endif


#endif
