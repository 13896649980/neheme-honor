//
//  CodeLock.c
//  jltClient
//
//  Created by sz.fullhan on 14-8-20.
//  Copyright (c) 2014年 jlt. All rights reserved.
//

#include <stdio.h>
#include "CodeLock.h"

#define PTHREAD_MUTEX_FAST_NP       PTHREAD_MUTEX_NORMAL
#define PTHREAD_MUTEX_RECURSIVE_NP  PTHREAD_MUTEX_RECURSIVE
#define PTHREAD_MUTEX_ERRORCHECK_NP PTHREAD_MUTEX_ERRORCHECK



    
void CreateCodeLock(CODELOCK* pLock)
{
    pthread_mutexattr_t attr;
    pthread_mutexattr_init(&attr);
    pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE_NP);
    pthread_mutex_init(pLock, &attr);
    pthread_mutexattr_destroy(&attr);
}

void DestoryCodeLock(CODELOCK* pLock)
{
    pthread_mutex_unlock(pLock);
    pthread_mutex_destroy(pLock);
}

void CodeLock(CODELOCK* pLock)
{
    pthread_mutex_lock(pLock);
}

void CodeUnlock(CODELOCK* pLock)
{
    pthread_mutex_unlock(pLock);
}
