//
//  VTBDecode.h
//  jltClient
//
//  Created by sz.fullhan on 16/1/26.
//  Copyright © 2016年 jlt. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "libavcodec/avcodec.h"

typedef void (*vtb_write_video_fream)(AVPacket *pkt);

@interface VTBDecode : NSObject

- (void)vtb_init;
- (void)vtb_dealloc;

// frameType: 0-i frame, 1-p frame, 2-b frame
- (void*)vtb_decode:(int)frameType frameData:(uint8_t*)buf frameLen:(uint32_t)bufLen;

- (void)vtb_release:(void*)pixelBuf;

@end
