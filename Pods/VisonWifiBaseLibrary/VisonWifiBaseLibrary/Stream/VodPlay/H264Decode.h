//
//  H264Decode.m
//  H264Demo
//
//  Created by iimgal on 13-6-18.
//  Copyright (c) 2013年 iimgal. All rights reserved.
//

#ifndef _H264DECODE_H_
#define _H264DECODE_H_

#include "libavcodec/avcodec.h"

typedef void (*write_video_fream)(AVPacket pkt);

typedef unsigned char byte_t;
typedef unsigned int uint_t;

struct AVCodec *fCodec = NULL;              // Codec
struct AVCodecContext *fCodecContext = NULL;          // Codec Context
struct AVFrame *fVideoFrame = NULL;          // Frame

int fDisplayWidth    = 0;
int fDisplayHeight    = 0;
int *fColorTable = NULL;


int avcodec_decode_video(AVCodecContext *avctx, AVFrame *picture,
                         int *got_picture_ptr,
                         const uint8_t *buf, int buf_size,write_video_fream fun)
{
    AVPacket avpkt;
    av_init_packet(&avpkt);
    avpkt.data = (uint8_t*)buf;
    avpkt.size = buf_size;
    // HACK for CorePNG to decode as normal PNG by default
    avpkt.flags = AV_PKT_FLAG_KEY;
    fun(avpkt); //回调写文件
    int  i = avcodec_decode_video2(avctx, picture, got_picture_ptr, &avpkt);
    av_free_packet(&avpkt);
    return i;
}

#define RGB_V(v) ((v < 0) ? 0 : ((v > 255) ? 255 : v))

void DeleteYUVTable()
{
    av_free(fColorTable);
}

void CreateYUVTable()
{
    int i;
    int u, v;
    int *u_b_tab = NULL;
    int *u_g_tab = NULL;
    int *v_g_tab = NULL;
    int *v_r_tab = NULL;
    
    fColorTable = (int *)av_malloc(4 * 256 * sizeof(int));
    u_b_tab = &fColorTable[0 * 256];
    u_g_tab = &fColorTable[1 * 256];
    v_g_tab = &fColorTable[2 * 256];
    v_r_tab = &fColorTable[3 * 256];
    
    for (i = 0; i < 256; i++) {
        u = v = (i - 128);
        u_b_tab[i] = (int) ( 1.772   * u);
        u_g_tab[i] = (int) ( 0.34414 * u);
        v_g_tab[i] = (int) ( 0.71414 * v);
        v_r_tab[i] = (int) ( 1.402   * v);
    }
}

/** YV12 To RGB888 */
void DisplayYUV_32(uint_t *displayBuffer, int videoWidth, int videoHeight, int outPitch)
{
    int *u_b_tab = &fColorTable[0 * 256];
    int *u_g_tab = &fColorTable[1 * 256];
    int *v_g_tab = &fColorTable[2 * 256];
    int *v_r_tab = &fColorTable[3 * 256];
    
    // YV12: [Y:MxN] [U:M/2xN/2] [V:M/2xN/2]
    byte_t* y = fVideoFrame->data[0];
    byte_t* u = fVideoFrame->data[1];
    byte_t* v = fVideoFrame->data[2];
    
    int src_ystride  = fVideoFrame->linesize[0];
    int src_uvstride = fVideoFrame->linesize[1];
    
    int i, line;
    int r, g, b;
    
    int ub, ug, vg, vr;
    
    int width  = videoWidth;
    int height = videoHeight;
    
    // 剪切边框
    if (width > fDisplayWidth) {
        width = fDisplayWidth;
        y += (videoWidth - fDisplayWidth) / 2;
        u += (videoWidth - fDisplayWidth) / 4;
        v += (videoWidth - fDisplayWidth) / 4;
    }
    
    if (height > fDisplayHeight) {
        height = fDisplayHeight;
    }
    
    for (line = 0; line < height; line++) {
        byte_t* yoff = y + line * src_ystride;
        byte_t* uoff = u + (line / 2) * src_uvstride;
        byte_t* voff = v + (line / 2) * src_uvstride;
        //uint_t* buffer = displayBuffer + (height - line - 1) * outPitch;
        uint_t* buffer = displayBuffer + line * outPitch;
        
        for (i = 0; i < width; i++) {
            ub = u_b_tab[*uoff];
            ug = u_g_tab[*uoff];
            vg = v_g_tab[*voff];
            vr = v_r_tab[*voff];
            
            b = RGB_V(*yoff + ub);
            g = RGB_V(*yoff - ug - vg);
            r = RGB_V(*yoff + vr);
            
            *buffer = 0xff000000 | b << 16 | g << 8 | r;
            
            buffer++;
            yoff ++;
            
            if ((i % 2) == 1) {
                uoff++;
                voff++;
            }
        }
    }
}

int avc_decode_init(int width, int height)
{
    if (fCodecContext != NULL) {
        return 0;
    }
    //    avcodec_init();
    avcodec_register_all();
    fCodec = avcodec_find_decoder(CODEC_ID_H264);
    
    fDisplayWidth  = width;
    fDisplayHeight = height;
    
    CreateYUVTable();
    
    fCodecContext = avcodec_alloc_context3(fCodec);
    avcodec_open2(fCodecContext, fCodec, nil);
    fVideoFrame  = avcodec_alloc_frame();
    
    return 1;
}

int avc_decode_release()
{
    if (fCodecContext) {
        avcodec_close(fCodecContext);
        free(fCodecContext->priv_data);
        free(fCodecContext);
        fCodecContext = NULL;
    }
    
    if (fVideoFrame) {
        free(fVideoFrame);
        fVideoFrame = NULL;
    }
    
    DeleteYUVTable();
    return 1;
}

int avc_decode(char* buf, int nalLen, char* out1,write_video_fream fun)
{
    byte_t* data = (byte_t*)buf;
    int frameSize = 0;
    
    int ret = avcodec_decode_video(fCodecContext, fVideoFrame, &frameSize, data, nalLen,fun);
    
//    sps
//    pps
    
    if (ret <= 0) {
        return ret;
    }
    
//    int width  = fCodecContext->width;
//    int height = fCodecContext->height;
//    DisplayYUV_32((uint32_t*)out1, width, height, fDisplayWidth);
    
    return ret;
}

#endif
