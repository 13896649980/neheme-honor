//
//  Udp720PTcpPlay.m
//  720P_TARGET
//
//  Created by mac on 17/3/16.
//  Copyright © 2017年 huang mindong. All rights reserved.
//

#import "HMDTcpPlay.h"
#import "AsyncSocket.h"
#include "BLoopBufData.h"
#include "ThreadPlatform.h"
#include "NetProtocol.h"
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
#include "libavutil/log.h"
#include "CodeLock.h"
#import "WIFIDeviceModel.h"
#import "VisonWifiBaseLibraryDefine.h"

#ifdef WIN32
#   define PSOCKET_LEN(x)   ((int*)(x))
#else
#   define PSOCKET_LEN(x)   ((socklen_t*)(x))
#endif

typedef struct
{
    char* 			tcpURL;
    unsigned int    port;
    DeviceType_e    dtype;
    int             sockfd;
    fDataCallBack   fun;
    void*           user;
    BLHANDLE        loopBufDataHandle;
    
    U8              sppThreadRunning;
    THREADID        sppThreadID;
    U8              srThreadRunning;
    THREADID        srThreadID;
    AVFormatContext *pFormatCtx;
    AVCodecContext *pCodecCtx;
    AVCodec *pCodec;
    AVFrame *pFrame;
    AVPacket packet;
    AVPicture picture;
    struct SwsContext * pSwsCtx;
    int videoStream;
    FILE* savefp;
    unsigned char recFirstFlag;      // ±£÷§ ◊÷°Œ™I÷°
    unsigned char recSavePreFNum;    // ±£¥Ê…œ“ª¥Œµƒ÷°∫≈(”√”⁄÷°¡¨–¯–‘≈–∂œ)
    unsigned char recIsDropFrame;    //  «∑Ò∂™÷°¡À
    unsigned char recType;
    char saveFilePath[1024];
    
    CODELOCK mutex;
    __unsafe_unretained HMDTcpPlay *tcpPlay;

}VodPlayHandle_t;

typedef struct
{
    unsigned char       FrmHd[4];
    unsigned char       chan;
    unsigned char       Vmsflag;
    unsigned char       AVOption;
    unsigned char       VideoFormat;
    unsigned char       AudioFormat;
    unsigned char       restart_flag;
    unsigned char       frmnum;
    unsigned char       Framerate;
    unsigned int		Bitrate;
    int                 framelen;
    unsigned int        HideAlarmStatus;
    unsigned int        alarmstatus;
    unsigned int        mdstatus;
    unsigned long long  timestamp;
    char                reserve[4];
}TCP_FrameHead_t;

static U8 _ParseFrameType(FHNP_Dev_FrameHead_t* pHead)
{
    if (   pHead->FrmHd[0] != 0x00
        || pHead->FrmHd[1] != 0x00
        || pHead->FrmHd[2] != 0x01
        || (pHead->FrmHd[3] != 0xa0 && pHead->FrmHd[3] != 0xa1 && pHead->FrmHd[3] != 0xa2
            && pHead->FrmHd[3] != 0xa3 && pHead->FrmHd[3] != 0xa4 && pHead->FrmHd[3] != 0xa5)
        )
    {
        return 255;
    }
    
    if (pHead->FrmHd[3] == 0xa5)
        return 4;
    else if (pHead->FrmHd[3] == 0xa4)
        return 3;
    else if (pHead->FrmHd[3] == 0xa1)
        return 0;
    else if (pHead->FrmHd[3] == 0xa2)
        return 2;
    
    return 1;
}

static U8 _StreamDataProc(VodPlayHandle_t* pNode, FHNP_Dev_FrameHead_t* pHead, char* pFrame)
{
    U8 btFrameType = 0;
    VFrameHead_t stVFrameHead;
    
    memset(&stVFrameHead, 0, sizeof(VFrameHead_t));
    btFrameType = _ParseFrameType(pHead);
    if (0 != btFrameType && 1 != btFrameType && 2 != btFrameType)
        return 0;
    
    stVFrameHead.frameType = btFrameType;
    stVFrameHead.videoFormat = pHead->VideoFormat;
    stVFrameHead.restartFlag = pHead->restart_flag;
    stVFrameHead.width = (unsigned short)(pHead->alarmstatus >> 16);
    stVFrameHead.height = (unsigned short)(pHead->alarmstatus);
    stVFrameHead.timeStamp = pHead->timestamp;
    stVFrameHead.res2[0] = pHead->reserve[3];
    stVFrameHead.framerate =  pHead->Framerate;
    
    if (pNode->fun)
        (*pNode->fun)(pNode,&stVFrameHead, pFrame, pHead->framelen, pNode->user);
    
    return 1;
}

static void _StreamPreProcThreadBody(void* lpUser)
{
    U8 bGetFrameRet = 0;
    FHNP_Dev_FrameHead_t stHead;
    char* pFrame = 0;
    
    VodPlayHandle_t* handle = (VodPlayHandle_t*)lpUser;
    if (!handle)
        return;
    
    if (!(pFrame = (char*)malloc(0x80000))) // 512KB
    {
        handle->sppThreadID = 0;
        return;
    }
    
    while (handle->sppThreadRunning)
    {
        bGetFrameRet = 0;
        while (1)
        {
            if (!BLBDATA_GetOneFrame(handle->loopBufDataHandle, (char*)&stHead, pFrame, 0))
                break;
            _StreamDataProc(handle, &stHead, pFrame);
            bGetFrameRet = 1;
        }
        
        if (bGetFrameRet)
        {
            SLEEPMS(0);
        }
        else
        {
            SLEEPMS(1);
        }
    }
    
    handle->sppThreadID = 0;
    free(pFrame);
}

static void _StreamRecvThreadBody(void* lpUser,uint8_t *buffer, int bufferLen)
{
    VodPlayHandle_t* handle = (VodPlayHandle_t*)lpUser;
    if (handle->srThreadRunning == 1)
    {
        int iRecvLen = bufferLen;
        char* pLoopBuf1 = NULL; int iLoopBufLen1 = 0;
        char* pLoopBuf2 = NULL; int iLoopBufLen2 = 0;
        if (!handle)
            return;
        
        if (iRecvLen > 0)
        {
            BLBDATA_Lock(handle->loopBufDataHandle);   // lock
            if (BLBDATA_AdvGetWritePtr(handle->loopBufDataHandle, &pLoopBuf1, (unsigned int*)&iLoopBufLen1, &pLoopBuf2, (unsigned int*)&iLoopBufLen2))
            {
                if (iLoopBufLen1 >= iRecvLen)
                {
                    memcpy(pLoopBuf1, buffer, iRecvLen);
                }
                else
                {
                    memcpy(pLoopBuf1, buffer, iLoopBufLen1);
                    memcpy(pLoopBuf2, buffer+iLoopBufLen1, iRecvLen-iLoopBufLen1);
                }
                BLBDATA_AdvSetWritePos(handle->loopBufDataHandle, iRecvLen);
            }
            BLBDATA_Unlock(handle->loopBufDataHandle); // unlock
        }
    }
}



void* StartHMDTcpPlay(char* url, unsigned int port,DeviceType_e dtype, fDataCallBack fun, void* user)
{
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);           //允许退出线程
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS,   NULL);   //设置立即取消
    
    
    
    VodPlayHandle_t* handle = 0;
    handle = (VodPlayHandle_t*)malloc(sizeof(VodPlayHandle_t));
    if (!handle)
    {
        return 0;
    }
    memset(handle, 0, sizeof(VodPlayHandle_t));
    handle->tcpURL = url;
    handle->port = port;
    handle->dtype = dtype;
    handle->fun = fun;
    handle->user = user;

    if (EN_DTYPE_FH8610 == dtype)//vgs
        handle->loopBufDataHandle = BLBDATA_Create(BLBDATA_TYPE_61_FRAME, 1024*500);
    else if (EN_DTYPE_FH8620 == dtype) //udp720p udp720pyu
        handle->loopBufDataHandle = BLBDATA_Create(BLBDATA_TYPE_62_FRAME, 1024*1024*10);
    else if (EN_DTYPE_720P == dtype) //udp720p
        handle->loopBufDataHandle = BLBDATA_Create(BLBDATA_TYPE_62_FRAME, 1024*1024*10);
    else if (EN_DTYPE_FH8810 == dtype)
        handle->loopBufDataHandle = BLBDATA_Create(BLBDATA_TYPE_81_FRAME, 1024*1024*10);
    else if (EN_DTYPE_1080P == dtype) //1080p
        handle->loopBufDataHandle = BLBDATA_Create(BLBDATA_TYPE_81_FRAME, 1024*1024*20);
    else if (EN_DTYPE_2K == dtype)
        handle->loopBufDataHandle = BLBDATA_Create(BLBDATA_TYPE_62_FRAME, 1024*1024*10);
    
    if (0 == handle->loopBufDataHandle)
    {
        free(handle);
        return 0;
    }
    
    HMDTcpPlay *play = [[HMDTcpPlay alloc] init];
    
    //设置handle
    play->handle = handle;
    
    [play startHMDTcpPlay];
    
    handle->tcpPlay = play;
    
    handle->sppThreadRunning = 1;
    
    
    
    if (!StartThread(_StreamPreProcThreadBody, handle, &handle->sppThreadID))
    {
        handle->sppThreadRunning = 0;
        BLBDATA_Destory(handle->loopBufDataHandle);
        free(handle);
        return 0;
    }
    
    handle->srThreadRunning = 1;
    return handle;
}



void StopHMDTcpPlay(void* handle)
{
    U32 dwCount = 0;
    VodPlayHandle_t* playHandle = (VodPlayHandle_t*)handle;
    if (!handle){
        return;
    }
    
    dwCount = 0;
    playHandle->srThreadRunning = 0;
    playHandle->fun = NULL;
    
    while (playHandle->srThreadID && dwCount < 300)
    {
        dwCount++;
        SLEEPMS(10);
    }
    dwCount = 0;
    playHandle->sppThreadRunning = 0;
    while (playHandle->sppThreadID && dwCount < 300)
    {
        dwCount++;
        SLEEPMS(10);
    }
    BLBDATA_Destory(playHandle->loopBufDataHandle);
    
    
    [NSObject cancelPreviousPerformRequestsWithTarget:playHandle->tcpPlay];
    playHandle->tcpPlay.socket1 = nil;
    playHandle->tcpPlay = NULL;
    

    if (playHandle->pFrame) {
        av_free(playHandle->pFrame);
        playHandle->pFrame = NULL;
    }
    if (playHandle->pCodecCtx) {
        avcodec_close(playHandle->pCodecCtx);
        playHandle->pCodecCtx = NULL;
    }
    
    if (playHandle->pFormatCtx) {
        avformat_close_input(&playHandle->pFormatCtx);
        playHandle->pFormatCtx = NULL;
    }
    
//    pthread_cancel(playHandle->srThreadID);//取消线程
//    pthread_cancel(playHandle->sppThreadID);//取消线程
    
    free(playHandle);
}


AsyncSocket *createHMDTcpSocket(void *pUser,const char *host,int port)
{
    AsyncSocket *socket = [[AsyncSocket alloc] initWithDelegate:(__bridge id)(pUser)];
    NSError *error;
    [socket connectToHost:[NSString stringWithUTF8String:host] onPort:port error:&error];
    return socket;
}


@implementation HMDTcpPlay
{
    int timeout;
    NSString *url;
    uint32_t port;
}

- (void)dealloc
{
    NSLog(@"dealloc ==> %@",self);
    if (_socket1.isConnected) {
        [_socket1 disconnect];
    }
    NSLog(@"dealloc1 ==> %@",self);
}

- (void)startHMDTcpPlay
{
    VodPlayHandle_t *handle_t = self->handle;
    dispatch_async(dispatch_get_main_queue(), ^{
        _socket1 = [[AsyncSocket alloc] initWithDelegate:self];
        _isConnetting = YES;
        NSLog(@"AsyncSocket alloc ==>%@",_socket1);
        url = [[NSString alloc] initWithUTF8String:handle_t->tcpURL];//闪退handle_t->tcpURL空时
        port = handle_t->port;
        NSError *error;
        [_socket1 connectToHost:url onPort:port error:&error];
        
        [self recvTCPVideoData];
    });
}



- (void)recvTCPVideoData
{
    if (_socket1.isConnected)
    {
        timeout++;
        if (timeout == 5)
        {
            timeout = 0;
            [_socket1 disconnect];
        }else
        {

            char command [12];
            bzero(command,12);
            command[0] = 0x00; //发送Ip地址
            command[1] = 0x01;
            command[2] = 0x02;
            command[3] = 0x03;
            command[4] = 0x04;
            command[5] = 0x05;
            command[6] = 0x06;
            command[7] = 0x07;
            command[8] = 0x08;
            command[9] = 0x09;
            command[10] = 0x28;
            command[11] = 0x28;
            
            
            if ([WIFIDeviceModel shareInstance].isNewVersion) {
                if ([WIFIDeviceModel shareInstance].isDoubleCamera == 2) {
                    //光流镜头
                    if ([USER_DEFAULT integerForKey:LENS_STATUS] == 1) {
                        command[10] = 0x30;
                        command[11] = 0x30;
                    }
                }
            }else{
                //光流镜头
                if ([USER_DEFAULT integerForKey:LENS_STATUS] == 1) {
                    command[10] = 0x30;
                    command[11] = 0x30;
                }
            }
            
            NSData *data = [NSData dataWithBytes:command length:12];
            [_socket1 writeData:data withTimeout:-1 tag:0];
        }
    }else
    {
        if (!_isConnetting)
        {
            NSLog(@"reconnectToHost");
            NSError *error;
            _isConnetting = YES;
            [_socket1 connectToHost:url onPort:port error:&error];
        }
    }
    
    [self performSelector:@selector(recvTCPVideoData) withObject:nil afterDelay:1];
}


#pragma mark -TcpSocketDelegate
- (void)onSocket:(AsyncSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port
{
    NSLog(@"视频 onSocket:%p didConnectToHost:%@ port:%hu", sock, host, port);
    [sock readDataWithTimeout:-1 tag:0];

}

- (void)onSocketDidDisconnect:(AsyncSocket *)sock
{
    NSLog(@"视频 %@ onSocketDidDisconnect",sock);
    _isConnetting = NO;
}

- (void)onSocket:(AsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag
{
    
    if (sock == _socket1)
    {
        timeout = 0;
        //收到环形buffer中处理
        if (data.length > 0)
        {
            VodPlayHandle_t *handle_t = self->handle;
            if (handle_t->srThreadRunning) {
                uint8_t *buffer = (uint8_t *)data.bytes;
                _StreamRecvThreadBody(self->handle,buffer,(int)data.length);
            }
        }
    }
    [sock readDataWithTimeout:-1 tag:tag];
}

- (void)onSocket:(AsyncSocket *)sock didWriteDataWithTag:(long)tag
{
    
}

- (void)onSocket:(AsyncSocket *)sock willDisconnectWithError:(NSError *)err
{
}


@end
