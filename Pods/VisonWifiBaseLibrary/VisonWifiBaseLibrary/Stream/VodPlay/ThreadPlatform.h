#ifndef _THREAD_PLATFORM_H_
#define _THREAD_PLATFORM_H_

#if defined(WIN32)
#   include <windows.h>
#   define THREADID        unsigned int
#elif defined(_ECOS_)
#   include <cyg/kernel/kapi.h>
#   define THREADID        unsigned int
#elif defined(_MIPSEL_)
#   include <pthread.h>
#   define THREADID        pthread_t
#else
#   include <pthread.h>
#   define THREADID        pthread_t
#endif

extern int StartThread(void* pThreadBody, void* pUser, THREADID* pThreadID); 

#endif
