//
//  AsyncTcpPlay.h
//  VS FPV_as
//
//  Created by huangmindong on 2018/9/14.
//  Copyright © 2018年 huang mindong. All rights reserved.
//

#ifndef AsyncTcpPlay_h
#define AsyncTcpPlay_h

#include <stdio.h>
#include "Includes.h"


#ifdef __cplusplus
extern "C" {
#endif
    
    typedef void (*fDataCallBack)(void* handle, VFrameHead_t* head, char* buf, U32 bufLen, void* user);
    typedef void (*fFailReadFreamCallBack)(void* handle, void* user);
    // bindIP: mcast bind local ip
    // dtype: fh8610/fh8620/fh8810
    // return: null-error, other-handle
    void* StartTCPPlay(char* ip, U16 port, char* bindIP, DeviceType_e dtype, fDataCallBack fun, void* user);
    void StopTCPPlay(void* handle);
    void DestorySendHandle(void* sendHandle);
    
#ifdef __cplusplus
}
#endif

#endif /* AsyncTcpPlay_h */
