#include "Play.h"
#include "RtspPlay.h"
#include "BLoopBufData.h"
#include "ThreadPlatform.h"
#include "NetProtocol.h"

#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
#include "libavutil/log.h"
#include "AVIConvert.h"
#include "CodeLock.h"


#ifdef WIN32
#   define PSOCKET_LEN(x)   ((int*)(x))
#else
#   define PSOCKET_LEN(x)   ((socklen_t*)(x))
#endif
int openInput(char *path, void *handle);
typedef struct 
{
	char* 			rtspURL;
    DeviceType_e    dtype;
    int             sockfd;
    fDataCallBack   fun;
    fFailReadFreamCallBack failFun;
    void*           user;
    BLHANDLE        loopBufDataHandle;

    U8              sppThreadRunning;
    THREADID        sppThreadID;
    U8              srThreadRunning;
    THREADID        srThreadID;
    AVFormatContext *pFormatCtx;
	AVCodecContext *pCodecCtx;
    AVCodec *pCodec;
	AVFrame *pFrame;
	AVPacket packet;
	AVPicture picture;
	struct SwsContext * pSwsCtx;
	int videoStream;
	FILE* savefp;
	unsigned char recFirstFlag;      // ±£÷§ ◊÷°Œ™I÷°
	unsigned char recSavePreFNum;    // ±£¥Ê…œ“ª¥Œµƒ÷°∫≈(”√”⁄÷°¡¨–¯–‘≈–∂œ)
	unsigned char recIsDropFrame;    //  «∑Ò∂™÷°¡À
	unsigned char recType;
	char saveFilePath[1024];

    CODELOCK mutex;
	// avi
	AVIHANDLE aviHandle;
	AVIMakeCfg_t aviMakeCfg;
	char saveAviIdxPath[1024];
    
    int useTcp;
}VodPlayHandle_t;


typedef FHNP_Dev_FrameHead_t RTSP_FrameHead_t;

VodPlayHandle_t myHandle;

static U8 _ParseFrameType(FHNP_Dev_FrameHead_t* pHead)
{
    if (   pHead->FrmHd[0] != 0x00
        || pHead->FrmHd[1] != 0x00
        || pHead->FrmHd[2] != 0x01
        || (pHead->FrmHd[3] != 0xa0 && pHead->FrmHd[3] != 0xa1 && pHead->FrmHd[3] != 0xa2
            && pHead->FrmHd[3] != 0xa3 && pHead->FrmHd[3] != 0xa4 && pHead->FrmHd[3] != 0xa5)
       )
    {
        return 255;
    }

    if (pHead->FrmHd[3] == 0xa5)
        return 4;
    else if (pHead->FrmHd[3] == 0xa4)
        return 3;
    else if (pHead->FrmHd[3] == 0xa1)
        return 0;
    else if (pHead->FrmHd[3] == 0xa2)
        return 2;

    return 1;
}

static U8 _StreamDataProc(VodPlayHandle_t* pNode, FHNP_Dev_FrameHead_t* pHead, char* pFrame)
{
    U8 btFrameType = 0;
    VFrameHead_t stVFrameHead;

    memset(&stVFrameHead, 0, sizeof(VFrameHead_t));
    btFrameType = _ParseFrameType(pHead);
    if (0 != btFrameType && 1 != btFrameType && 2 != btFrameType)
        return 0;
    
    stVFrameHead.frameType = btFrameType;
    stVFrameHead.videoFormat = pHead->VideoFormat;
    stVFrameHead.restartFlag = pHead->restart_flag;
    stVFrameHead.width = (unsigned short)(pHead->alarmstatus >> 16);
    stVFrameHead.height = (unsigned short)(pHead->alarmstatus);
    if(stVFrameHead.width==0||stVFrameHead.height==0)
	{
		if(pNode->dtype == EN_DTYPE_720P)
		{
			stVFrameHead.width = 1280;
			stVFrameHead.height = 720;
		}else if (pNode->dtype == EN_DTYPE_1080P)
        {
            stVFrameHead.width = 1920;
            stVFrameHead.height = 1080;
        }
	}
    stVFrameHead.timeStamp = pHead->timestamp;
	stVFrameHead.res2[0] = pHead->reserve[3];
    if (pNode->fun)
         (*pNode->fun)(pNode,&stVFrameHead, pFrame, pHead->framelen, pNode->user);
    
    return 1;
}

static void _StreamPreProcThreadBody(void* lpUser)
{
    U8 bGetFrameRet = 0;
    FHNP_Dev_FrameHead_t stHead;
    char* pFrame = 0;

    VodPlayHandle_t* handle = (VodPlayHandle_t*)lpUser;
    if (!handle)
        return;

    if (!(pFrame = (char*)malloc(0x80000))) // 512KB
    {
        handle->sppThreadID = 0;
        return;
    }

    while (handle->sppThreadRunning)
    {
        bGetFrameRet = 0;
        while (1)
        {
            if (!BLBDATA_GetOneFrame(handle->loopBufDataHandle, (char*)&stHead, pFrame, 0))
                break;
            _StreamDataProc(handle, &stHead, pFrame);
            bGetFrameRet = 1;
        }

        if (bGetFrameRet)
        {
            SLEEPMS(0);
        }
        else
        {
            SLEEPMS(1);
        }
    }

    handle->sppThreadID = 0;
    free(pFrame);
}

static void _StreamRecvThreadBody(void* lpUser)
{
    int iRecvLen = 0;
    char* pLoopBuf1 = NULL; int iLoopBufLen1 = 0;
    char* pLoopBuf2 = NULL; int iLoopBufLen2 = 0;
    
    RTSP_FrameHead_t* fHead = 0;
    fHead = (RTSP_FrameHead_t*)malloc(sizeof(RTSP_FrameHead_t));
    memset(fHead, 0, sizeof(RTSP_FrameHead_t));
    
    long iUdpBufLen = 1024*1024;
    char *pUdpBuf = malloc(iUdpBufLen);
    bzero(pUdpBuf, iUdpBufLen);
    
    VodPlayHandle_t* handle = (VodPlayHandle_t*)lpUser;
    
    if (!handle)  return;
    if (!handle->pFormatCtx) return;
    
    while (handle->srThreadRunning)
    {
        if (NULL == pUdpBuf)
            break;
        
        AVPacket packet;
        if(av_read_frame(handle->pFormatCtx, &packet) >= 0)
        {
            if (packet.stream_index == handle->videoStream)
            {
                fHead->FrmHd[2] = 0x01;
                
                if((packet.data[4]&0x1f) == 1)
                {
                    fHead->FrmHd[3] = 0xa0;
                }else if((packet.data[4]&0x1f) == 5)
                {
                    fHead->FrmHd[3] = 0xa1;
                }else if((packet.data[4]&0x1f) == 7)
                {
                    fHead->FrmHd[3] = 0xa1;
                }else if((packet.data[4]&0x1f) == 8)
                {
                    fHead->FrmHd[3] = 0xa1;
                }
                
                
                fHead->frmnum++;
                fHead->framelen = packet.size;
                
                iRecvLen = sizeof(RTSP_FrameHead_t)+packet.size;
                
                memcpy(pUdpBuf,fHead,sizeof(RTSP_FrameHead_t));
                memcpy(pUdpBuf+sizeof(RTSP_FrameHead_t),packet.data,packet.size);
                
                BLBDATA_Lock(handle->loopBufDataHandle);   // lock
                if (BLBDATA_AdvGetWritePtr(handle->loopBufDataHandle, &pLoopBuf1, (unsigned int*)&iLoopBufLen1, &pLoopBuf2, (unsigned int*)&iLoopBufLen2))
                {
                    if (iLoopBufLen1 >= iRecvLen)
                    {
                        memcpy(pLoopBuf1, pUdpBuf, iRecvLen);
                    }
                    else
                    {
                        memcpy(pLoopBuf1, pUdpBuf, iLoopBufLen1);
                        memcpy(pLoopBuf2, pUdpBuf+iLoopBufLen1, iRecvLen-iLoopBufLen1);
                    }
                    BLBDATA_AdvSetWritePos(handle->loopBufDataHandle, iRecvLen);
                }
                bzero(pUdpBuf, iUdpBufLen);
                BLBDATA_Unlock(handle->loopBufDataHandle); // unlock
            }
            
            av_free_packet(&packet);
        
        }else
        {
            if (handle->failFun)
                (*handle->failFun)(handle, handle->user);
            
            if (handle->pCodecCtx) {
                avcodec_close(handle->pCodecCtx);
                handle->pCodecCtx = NULL;
            }
            
            if (handle->pFormatCtx) {
                avformat_close_input(&handle->pFormatCtx);
                handle->pFormatCtx = NULL;
            }
            
            
            handle->pFormatCtx = avformat_alloc_context();
            
            AVDictionary *opts = NULL;
            if (handle->useTcp)
                av_dict_set(&opts, "rtsp_transport", "tcp", 0);
            else
                av_dict_set(&opts, "rtsp_transport", "udp", 0); // 作提示
            
            // 设置超时
            av_dict_set(&opts, "stimeout", "500000", 0); // actually is us, not ms. 0.5s
            
            
            while(handle->srThreadRunning)
            {
                if (avformat_open_input(&handle->pFormatCtx, handle->rtspURL, NULL, &opts) < 0) {
                    //LOGE("Can not open this file");
                    usleep(500000);
                    printf("av_read_frame\n");
                    continue;
                }
                break;
            }
        }
    }
    
    if (pUdpBuf)
        free(pUdpBuf);
    
    handle->srThreadID = 0;

}


void* StartRtspPlay(char* url, DeviceType_e dtype, fDataCallBack fun, fFailReadFreamCallBack failFun,void* user,int useTCP)
{
    printf("StartRtspPlay FFMPEG2\n");
    
    av_register_all();
    avcodec_register_all();
    avformat_network_init();
    
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);           //允许退出线程
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS,   NULL);   //设置立即取消
    
    VodPlayHandle_t* handle = 0;
    handle = (VodPlayHandle_t*)malloc(sizeof(VodPlayHandle_t));
    if (!handle)
    {
        return 0;
    }
    
    memset(handle, 0, sizeof(VodPlayHandle_t));
    handle->rtspURL = url;
    handle->dtype = dtype;
    handle->fun = fun;
    handle->failFun = failFun;
    handle->user = user;
    handle->useTcp = useTCP;
    
    handle->pFrame = av_frame_alloc();
    
    printf("avformat_open_input\n");
    
    
    AVDictionary *opts = NULL;
    if (useTCP)
        av_dict_set(&opts, "rtsp_transport", "tcp", 0);
    else
        av_dict_set(&opts, "rtsp_transport", "udp", 0); // 作提示
    
    // 设置超时
    av_dict_set(&opts, "stimeout", "500000", 0); // actually is us, not ms. 0.5s
    
    if (avformat_open_input(&handle->pFormatCtx, handle->rtspURL, NULL,&opts) < 0) {
        return 0;
    }
    
    int i = 0;
    handle->videoStream = -1;
    for (i = 0; i < handle->pFormatCtx->nb_streams; i++) {
        if (handle->pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) {
            handle->videoStream = i;
            break;
        }
    }
    if (handle->videoStream == -1) {
        return 0;
    }
    handle->pCodecCtx = handle->pFormatCtx->streams[handle->videoStream]->codec;
    
    //	av_dict_set(handle->pCodecCtx->priv_data, "superfast", "fast", 0);
    //	av_dict_set(handle->pCodecCtx->priv_data, "tune", "zerolatency", 0);
    
    printf("avcodec_find_decoder\n");
    handle->pCodec = avcodec_find_decoder(handle->pCodecCtx->codec_id);
    
    if (handle->pCodec == NULL) {
        return 0;
    }
    if (avcodec_open2(handle->pCodecCtx, handle->pCodec, NULL) < 0) {
        return 0;
    }
    
    if (EN_DTYPE_FH8610 == dtype)//vgs
        handle->loopBufDataHandle = BLBDATA_Create(BLBDATA_TYPE_61_FRAME, 1024*500);
    else if (EN_DTYPE_FH8620 == dtype) //udp720p udp720pyu
        handle->loopBufDataHandle = BLBDATA_Create(BLBDATA_TYPE_62_FRAME, 1024*1024*10);
    else if (EN_DTYPE_FH8810 == dtype)
        handle->loopBufDataHandle = BLBDATA_Create(BLBDATA_TYPE_81_FRAME, 1024*1024*10);
    else if (EN_DTYPE_720P == dtype || EN_DTYPE_RTSP_720P == dtype) //720P RSTP720P
        handle->loopBufDataHandle = BLBDATA_Create(BLBDATA_TYPE_81_FRAME, 1024*1024*10);
    else if (EN_DTYPE_1080P == dtype ||EN_DTYPE_RTSP_1080P == dtype) //1080p RTSP1080P
        handle->loopBufDataHandle = BLBDATA_Create(BLBDATA_TYPE_81_FRAME, 1024*1024*20);
    else if (EN_DTYPE_2K == dtype)
        handle->loopBufDataHandle = BLBDATA_Create(BLBDATA_TYPE_81_FRAME, 1024*1024*10);
    
    if (0 == handle->loopBufDataHandle)
    {
        free(handle);
        return 0;
    }
    
    printf("StartThread _StreamPreProcThreadBody\n");
    handle->sppThreadRunning = 1;
    if (!StartThread(_StreamPreProcThreadBody, handle, &handle->sppThreadID))
    {
        handle->sppThreadRunning = 0;
        BLBDATA_Destory(handle->loopBufDataHandle);
        free(handle);
        return 0;
    }
    
    printf("StartThread _StreamRecvThreadBody\n");
    handle->srThreadRunning = 1;
    if (!StartThread(_StreamRecvThreadBody, handle, &handle->srThreadID))
    {
        handle->srThreadRunning = 0;
        BLBDATA_Destory(handle->loopBufDataHandle);
        free(handle);
        return 0;
    }
    printf("StartRtspPlay FFMPEG2\n");
    return handle;
}

void StopRtspPlay(void* handle)
{
  
    
    U32 dwCount = 0;
    VodPlayHandle_t* playHandle = (VodPlayHandle_t*)handle;
    if (!handle)
        return;
    dwCount = 0;
    playHandle->srThreadRunning = 0;


    
    while (playHandle->srThreadID && dwCount < 300)
    {
        dwCount++;
        SLEEPMS(10);
    }
    dwCount = 0;
    playHandle->sppThreadRunning = 0;
    while (playHandle->sppThreadID && dwCount < 300)
    {
        dwCount++;
        SLEEPMS(10);
    }
    BLBDATA_Destory(playHandle->loopBufDataHandle);
    
    
    pthread_cancel(playHandle->srThreadID);//取消线程
    pthread_cancel(playHandle->sppThreadID);//取消线程
#if 1
    if (playHandle->pFrame) {
        av_free(playHandle->pFrame);
        playHandle->pFrame = NULL;
    }
    if (playHandle->pCodecCtx) {
        avcodec_close(playHandle->pCodecCtx);
        playHandle->pCodecCtx = NULL;
    }
    
    if (playHandle->pFormatCtx) {
        avformat_close_input(&playHandle->pFormatCtx);
        playHandle->pFormatCtx = NULL;
    }
    

#endif
    
    free(playHandle);
    
    printf("%s\n",__FUNCTION__);
}


// recType: 2-avi, 1-h264
unsigned char StartRtspRecord(void* handle, unsigned char recType, const char* recPath)
{
    VodPlayHandle_t* playHandle = (VodPlayHandle_t*)handle;
    if (!handle || !recPath || strlen(recPath) <= 0)
    {
    	return 0;
    }


    BLBDATA_Lock(playHandle->loopBufDataHandle);

    playHandle->recFirstFlag = 0;
    playHandle->recType = recType;
    strcpy(playHandle->saveFilePath, recPath);

    playHandle->savefp = fopen(recPath, "wb+");
    if (!playHandle->savefp)
    {
        BLBDATA_Unlock(playHandle->loopBufDataHandle);
        return 0;
    }

    if (2 == recType)
    {
        // avi
        playHandle->aviHandle = 0;
        memset(&playHandle->aviMakeCfg, 0, sizeof(AVIMakeCfg_t));
        playHandle->aviMakeCfg.pAVIFile = playHandle->savefp;
        sprintf(playHandle->saveAviIdxPath, "%s.idx", recPath);
        playHandle->aviMakeCfg.pAVIIdxFile = fopen(playHandle->saveAviIdxPath, "wb+");
        if (!playHandle->aviMakeCfg.pAVIIdxFile)
        {
            fclose(playHandle->savefp);
            playHandle->savefp = NULL;
            BLBDATA_Unlock(playHandle->loopBufDataHandle);
            return 0;
        }
    }

    BLBDATA_Unlock(playHandle->loopBufDataHandle);
    return 1;
}

unsigned char StopRtspRecord(void* handle)
{
    
    VodPlayHandle_t* playHandle = (VodPlayHandle_t*)handle;
    
    if (!handle)
        return 0;

    BLBDATA_Lock(playHandle->loopBufDataHandle);

    if (2 == playHandle->recType)
    {
        if (playHandle->aviHandle)
            AVIStop(playHandle->aviHandle, &playHandle->aviMakeCfg);
        playHandle->aviHandle = 0;
        if (playHandle->aviMakeCfg.pAVIIdxFile)
            fclose(playHandle->aviMakeCfg.pAVIIdxFile);
        if (strlen(playHandle->saveAviIdxPath) > 0)
            remove(playHandle->saveAviIdxPath);
        memset(&playHandle->aviMakeCfg, 0, sizeof(AVIMakeCfg_t));
    }

    if (playHandle->savefp)
        fclose(playHandle->savefp);
    playHandle->savefp = NULL;
    memset(playHandle->saveFilePath, 0, sizeof(playHandle->saveFilePath));
    memset(playHandle->saveAviIdxPath, 0, sizeof(playHandle->saveAviIdxPath));

    BLBDATA_Unlock(playHandle->loopBufDataHandle);
    
    return 1;
}

int openInput(char *path, void *handle)
{
    VodPlayHandle_t* playHandle = (VodPlayHandle_t*)handle;
    
    AVFormatContext *formatCtx = NULL;
    formatCtx = avformat_alloc_context();
    
    AVDictionary* opts = NULL;

    int err_code = avformat_open_input(&formatCtx, path, NULL,&opts);
    if ( err_code < 0) {
        
        if (formatCtx)
            avformat_free_context(formatCtx);
        return -1;
    }
    

    av_dump_format(formatCtx, 0, path, 0);
    
    playHandle->pFormatCtx = formatCtx;
    
    
    int i = 0;
    playHandle->videoStream = -1;
    for (i = 0; i < playHandle->pFormatCtx->nb_streams; i++) {
        if (playHandle->pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) {
            playHandle->videoStream = i;
            break;
        }
    }
    if (playHandle->videoStream == -1) {
        return 0;
    }

    AVCodecContext *codecCtx = playHandle->pFormatCtx ->streams[playHandle->videoStream]->codec;
    AVCodec *codec = avcodec_find_decoder(codecCtx->codec_id);
    if (!codec)
        return -1;
    
    // open codec
    if (avcodec_open2(codecCtx, codec, NULL) < 0)
        return -1;

    AVFrame *videoFrame = av_frame_alloc();
    playHandle->pFrame = videoFrame;
    return 0;
}




