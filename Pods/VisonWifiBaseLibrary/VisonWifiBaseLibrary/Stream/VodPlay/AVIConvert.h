#ifndef _AVI_CONVERT_H_
#define _AVI_CONVERT_H_

typedef struct 
{
    void* pAVIFile;
    void* pAVIIdxFile;
    unsigned int dwWidth;
    unsigned int dwHeight;
    unsigned int dwRate;               // 帧率x100
    unsigned int dwAudioSamplerate;
    unsigned int dwAudioBitWidth;      // 8/16
    unsigned int dwMaxVideoBitrate;    // bps
    unsigned int dwMaxAudioBitrate;
    unsigned char byAudioTrack;         // 声道, 1单声道, 2立体声
    unsigned char byIsAudioFrame;       // 音频信息是否填入
	unsigned char byIsVideoFrame;		// 视频信息是否填入
}AVIMakeCfg_t;

typedef struct 
{
    unsigned int  dwFrameLen;
    unsigned char byIsAudio;
    unsigned char byIsKeyFrame;
}AVIFrameCfg_t;

typedef struct
{
    unsigned int dwFileSize;
    unsigned int dwMediaSize;          // 媒体块大小包含 movi 4个字节 TBD_WAIT ...
    unsigned int dwIndexSize;          // ...
    unsigned int dwBaseMediaOffset;
    unsigned int dwMediaOffset;
    unsigned int dwVideoCount;
    unsigned int dwAudioCount;
    unsigned int dwVideoLenFillPos;     // AVIWriteDWORD(pFile, pstAVICfg->dwVideoCount + pstAVICfg->dwAudioCount); // wait upt...
    unsigned int dwAudioLenFillPos;     // AVIWriteDWORD(pFile, pstAVICfg->dwVideoCount + pstAVICfg->dwAudioCount); // wait upt...
    AVIMakeCfg_t stAVIMakeCfg;

    unsigned int dwRateUpdatePos;       // AVIWriteDWORD(pFile, AVI_MS_ASIST_VALUE * 100 / pstMakeCfg->dwRate);
    unsigned int dwAVBitrateUpdatePos;  // AVIWriteDWORD(pFile, (pstMakeCfg->dwMaxVideoBitrate+pstMakeCfg->dwMaxAudioBitrate) / 8);
    unsigned int dwWHUpdatePos;         // AVIWriteDWORD(pFile, pstMakeCfg->dwWidth);
    unsigned int dwRateUpdatePos2;      // AVIWriteDWORD(pFile, AVI_MS_ASIST_VALUE * 100 / pstMakeCfg->dwRate); // scale
    unsigned int wWHUpdatePos;          // AVIWriteWORD(pFile, (WORD)pstMakeCfg->dwWidth); AVIWriteWORD(pFile, (WORD)pstMakeCfg->dwHeight);
    unsigned int dwWHUpdatePos2;        // AVIWriteDWORD(pFile, pstMakeCfg->dwWidth); AVIWriteDWORD(pFile, pstMakeCfg->dwHeight);
    unsigned int dwSamplerateUpdatePos; // AVIWriteDWORD(pFile, AVI_MS_ASIST_VALUE / pstMakeCfg->dwAudioSamplerate); // scale TBD_WAIT ...
    unsigned int dwSampBitWUpdatePos;   // AVIWriteDWORD(pFile, pstMakeCfg->dwAudioSamplerate * pstMakeCfg->dwAudioBitWidth / 8); // RM 用的是平均帧大小 TBD_WAIT...
    unsigned int dwAudioParamUpdatePos; // AVIWriteWORD(pFile, pstMakeCfg->byAudioTrack);// 指出波形数据的声道数; 单声道为 1, 立体声为 2 (??)
                                        // AVIWriteDWORD(pFile, pstMakeCfg->dwAudioSamplerate); // 指定采样频率(每秒的样本数)  
                                        // AVIWriteDWORD(pFile, pstMakeCfg->dwAudioSamplerate * pstMakeCfg->dwAudioBitWidth / 8);// 指定数据传输的传输速率(每秒的字节数)  
                                        // AVIWriteWORD(pFile, (WORD)(pstMakeCfg->dwAudioBitWidth / 8));// 指定块对齐块对齐是数据的最小单位 [RM 2 ...]
                                        // AVIWriteWORD(pFile, (WORD)(pstMakeCfg->dwAudioBitWidth));
}AVIMakeCB_t;

typedef void*   AVIHANDLE;

AVIHANDLE AVIStart(AVIMakeCfg_t* pstMakeCfg);
unsigned char AVIStop(AVIHANDLE aviHandle, AVIMakeCfg_t* pstMakeCfg);
unsigned char AVICommit(AVIHANDLE aviHandle, unsigned char* pData, AVIFrameCfg_t* pstFrameCfg);
unsigned char AVICheckSize(AVIHANDLE aviHandle, long maxFileSize);

#endif
