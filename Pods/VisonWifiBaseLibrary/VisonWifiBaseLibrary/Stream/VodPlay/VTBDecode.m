//
//  VTBDecode.m
//  jltClient
//
//  Created by sz.fullhan on 16/1/26.
//  Copyright © 2016年 jlt. All rights reserved.
//

#import "VTBDecode.h"
#import <VideoToolbox/VideoToolbox.h>

@interface VTBDecode()
{
    uint8_t *s_sps;
    NSInteger s_spsSize;
    uint8_t *s_pps;
    NSInteger s_ppsSize;
    VTDecompressionSessionRef s_decoderSession;
    CMVideoFormatDescriptionRef s_decoderFormatDescription;
}
@end



static void vtb_didDecompress( void *decompressionOutputRefCon, void *sourceFrameRefCon, OSStatus status, VTDecodeInfoFlags infoFlags, CVImageBufferRef pixelBuffer, CMTime presentationTimeStamp, CMTime presentationDuration ){
    
    CVPixelBufferRef *outputPixelBuffer = (CVPixelBufferRef *)sourceFrameRefCon;
    *outputPixelBuffer = CVPixelBufferRetain(pixelBuffer);
}

@implementation VTBDecode

- (void)vtb_init{
    
}

- (void)vtb_dealloc{
    if (s_sps)
        free(s_sps);
    s_sps = 0;
    s_spsSize = 0;
    
    if (s_pps)
        free(s_pps);
    s_pps = 0;
    s_ppsSize = 0;
    
    if (s_decoderSession) {
        VTDecompressionSessionInvalidate(s_decoderSession);
        CFRelease(s_decoderSession);
        s_decoderSession = 0;
    }
    
    if (s_decoderFormatDescription) {
        CFRelease(s_decoderFormatDescription);
        s_decoderFormatDescription = 0;
    }
}

// rlen: =nil时不找下个startCode
uint8_t* vtb_findStartCode(uint8_t* buf, uint32_t len, uint32_t* rlen)
{
    uint8_t* retBuf = buf;
    const uint8_t kStartCode[4] = {0, 0, 0, 1};
    
    while (1) {
        if (*retBuf == 0x01 && 0 == memcmp(retBuf-3, kStartCode, 4))
            break;
        retBuf++;
        if ( buf + len <= retBuf )
            return 0;
    }

    if ( rlen ) {
        uint8_t* nextBuf = vtb_findStartCode((retBuf+1), (len-1-(uint32_t)(retBuf-buf)), 0);
        if (nil == nextBuf)
            return 0;
        *rlen = (uint32_t)(nextBuf - retBuf) + 3;
    }
    
    return retBuf - 3;
}

-(BOOL)vtb_initH264Decoder {
    if (s_decoderSession) {
        return YES;
    }
    
    int i, spsSize = 0, ppsSize = 0;
    for (i = (int)s_spsSize-1; i >= 0; i--){
        if (0 != s_sps[i])
            break;
    }
    spsSize = i+1;
    for (i = (int)s_ppsSize-1; i>= 0; i--) {
        if (0 != s_pps[i])
            break;
    }
    ppsSize = i+1;
    
    const uint8_t* const parameterSetPointers[2] = { s_sps, s_pps };
    const size_t parameterSetSizes[2] = { spsSize, ppsSize }; // {s_spsSize, s_ppsSize }
    
    
    OSStatus status = CMVideoFormatDescriptionCreateFromH264ParameterSets(kCFAllocatorDefault,
                                                                          2, //param count
                                                                          parameterSetPointers,
                                                                          parameterSetSizes,
                                                                          4, //nal start code size
                                                                          &s_decoderFormatDescription);
    
    if (status == noErr) {
        CFDictionaryRef attrs = NULL;
        const void *keys[] = { kCVPixelBufferPixelFormatTypeKey };
        //      kCVPixelFormatType_420YpCbCr8Planar is YUV420
        //      kCVPixelFormatType_420YpCbCr8BiPlanarFullRange is NV12
        uint32_t v = kCVPixelFormatType_420YpCbCr8BiPlanarFullRange;
        const void *values[] = { CFNumberCreate(NULL, kCFNumberSInt32Type, &v) };
        attrs = CFDictionaryCreate(NULL, keys, values, 1, NULL, NULL);
        
        VTDecompressionOutputCallbackRecord callBackRecord;
        callBackRecord.decompressionOutputCallback = vtb_didDecompress;
        callBackRecord.decompressionOutputRefCon = NULL;
        
        status = VTDecompressionSessionCreate(kCFAllocatorDefault,
                                              s_decoderFormatDescription,
                                              NULL, attrs,
                                              &callBackRecord,
                                              &s_decoderSession);
        CFRelease(attrs);
    } else {
        NSLog(@"IOS8VT: reset decoder session failed status=%d", (int)status);
        return NO;
    }
    
    return YES;
}

-(CVPixelBufferRef)vtb_decodeFrame:(uint8_t*)buf frameLen:(uint32_t)bufLen {
    CVPixelBufferRef outputPixelBuffer = NULL;
    
    CMBlockBufferRef blockBuffer = NULL;
    OSStatus status  = CMBlockBufferCreateWithMemoryBlock(kCFAllocatorDefault,
                                                          (void*)buf, bufLen,
                                                          kCFAllocatorNull,
                                                          NULL, 0, bufLen,
                                                          0, &blockBuffer);
    if (status == kCMBlockBufferNoErr) {
        CMSampleBufferRef sampleBuffer = NULL;
        const size_t sampleSizeArray[] = { bufLen };
        status = CMSampleBufferCreateReady(kCFAllocatorDefault,
                                           blockBuffer,
                                           s_decoderFormatDescription ,
                                           1, 0, NULL, 1, sampleSizeArray,
                                           &sampleBuffer);
        if (status == kCMBlockBufferNoErr && sampleBuffer) {
            VTDecodeFrameFlags flags = 0;//0;kVTDecodeFrame_1xRealTimePlayback
            VTDecodeInfoFlags flagOut = 0;
            OSStatus decodeStatus = VTDecompressionSessionDecodeFrame(s_decoderSession,
                                                                      sampleBuffer,
                                                                      flags,
                                                                      &outputPixelBuffer,
                                                                      &flagOut);
            
            if(decodeStatus == kVTInvalidSessionErr) {
                NSLog(@"IOS8VT: Invalid session, reset decoder session");
            } else if(decodeStatus == kVTVideoDecoderBadDataErr) {
                NSLog(@"IOS8VT: decode failed status=%d(Bad data)", (int)decodeStatus);
            } else if(decodeStatus != noErr) {
                NSLog(@"IOS8VT: decode failed status=%d", (int)decodeStatus);
            }
            
            CFRelease(sampleBuffer);
        }
        CFRelease(blockBuffer);
    }
    
    return outputPixelBuffer;
}

- (void*)vtb_decode:(int)frameType frameData:(uint8_t*)buf frameLen:(uint32_t)bufLen
{
    if (nil == buf || 0 == bufLen || frameType > 2)
        return nil;
    
    uint8_t* dBuf = nil;
    
    if ( 0 == frameType )
    {
        // is i frame
        // first
        uint32_t fLen = 0;
        uint8_t* fBuf = vtb_findStartCode(buf, bufLen, &fLen);
        if (nil == fBuf || fLen <= 4)
            return nil;
        
        // second
        uint32_t sLen = 0;
        uint8_t* nBuf = fBuf + fLen;
        uint8_t* sBuf = vtb_findStartCode(nBuf, (bufLen-(uint32_t)(nBuf-buf)), &sLen);
        if (nil == sBuf || sLen <= 4)
            return nil;
        
        //three
        uint32_t eLen = 0;
        uint8_t* eBuf = sBuf + sLen;
        uint8_t* eeBuf = vtb_findStartCode(eBuf, (bufLen-(uint32_t)(eBuf-buf)), &eLen);
        
        if ( s_decoderSession && s_sps && s_pps )
        {
            uint8_t cFlag = 0;
            if ( 0x07 == fBuf[4] ) {
                if (s_spsSize != fLen-4 || 0 != memcmp(s_sps, fBuf+4, s_spsSize))
                    cFlag = 1;
                if (s_ppsSize != sLen-4 || 0 != memcmp(s_pps, sBuf+4, s_ppsSize))
                    cFlag = 1;
            } else {
                if (s_ppsSize != fLen-4 || 0 != memcmp(s_pps, fBuf+4, s_ppsSize))
                    cFlag = 1;
                if (s_spsSize != sLen-4 || 0 != memcmp(s_sps, sBuf+4, s_spsSize))
                    cFlag = 1;
            }
            if ( cFlag ) {
                NSLog(@"reset vtb");
                
                
                if (s_decoderSession) {
                    VTDecompressionSessionInvalidate(s_decoderSession);
                    CFRelease(s_decoderSession);
                    s_decoderSession = 0;
                }
                
                if (s_decoderFormatDescription) {
                    CFRelease(s_decoderFormatDescription);
                    s_decoderFormatDescription = 0;
                }
            }
        }
        
        if ( !s_decoderSession )
        {
            if ( s_sps )
                free(s_sps);
            s_spsSize = 0;
            if ( s_pps )
                free(s_pps);
            s_ppsSize = 0;
            
            if ( 0x07 == fBuf[4] ) {
                s_spsSize = fLen - 4;
                s_sps = malloc(s_spsSize);
                memcpy(s_sps, fBuf + 4, s_spsSize);
                
                s_ppsSize = sLen - 4;
                s_pps = malloc(s_ppsSize);
                memcpy(s_pps, sBuf + 4, s_ppsSize);
            } else {
                s_ppsSize = fLen - 4;
                s_pps = malloc(s_ppsSize);
                memcpy(s_pps, fBuf + 4, s_ppsSize);
                
                s_spsSize = sLen - 4;
                s_sps = malloc(s_spsSize);
                memcpy(s_sps, sBuf + 4, s_spsSize);
            }
            if ( ![self vtb_initH264Decoder] || !s_decoderSession )
                return nil;
        }
        
        // frameData
        if (nil == eeBuf || sLen <= 4)
        {
            
        }else
        {
            sLen += eLen;
        }
        dBuf = sBuf + sLen;

        if (0x05 != (dBuf[4] & 0x0F))
        {
            
            dBuf += 4;
            dBuf = vtb_findStartCode(dBuf, bufLen - (uint32_t)(dBuf - buf), nil);
            if (!dBuf)
            {
                NSLog(@"vtb not find startCode");
                return nil;
            }
        }
        
    } else if (!s_decoderSession) {
        return nil;
    } else {
        // is p/b frame
        dBuf = vtb_findStartCode(buf, bufLen, nil);
    }
    
    if (!dBuf)
        return nil;
    
    uint32_t dLen = 0;
    dLen = (bufLen - (uint32_t)(dBuf - buf));
    uint32_t nalLen = dLen - 4;
    uint8_t* pNalLen = (uint8_t*)(&nalLen);
    
    dBuf[0] = *(pNalLen + 3);
    dBuf[1] = *(pNalLen + 2);
    dBuf[2] = *(pNalLen + 1);
    dBuf[3] = *(pNalLen);
    
    
    void* pixelBuffer = NULL;
    
    //    NSData *dData1 = [NSData dataWithBytes:dBuf length:dLen];
    
    pixelBuffer = (void*)[self vtb_decodeFrame:dBuf frameLen:dLen];
    
    // 恢复原样
    dBuf[0] = 0x00;
    dBuf[1] = 0x00;
    dBuf[2] = 0x00;
    dBuf[3] = 0x01;
    
    return pixelBuffer;
}

- (void)vtb_release:(void*)pixelBuf{
    if (!pixelBuf)
        return;
    CVPixelBufferRelease((CVPixelBufferRef)pixelBuf);
}


@end
