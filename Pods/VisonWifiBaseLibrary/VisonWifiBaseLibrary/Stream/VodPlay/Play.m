#include "Play.h"
#include "BLoopBufData.h"
#include "ThreadPlatform.h"
#include "NetProtocol.h"
#include <sys/time.h>
#import "WIFIDeviceModel.h"

#ifdef WIN32
#   define PSOCKET_LEN(x)   ((int*)(x))
#else
#   define PSOCKET_LEN(x)   ((socklen_t*)(x))
#endif



int CreateMcastSocket(char* sMcastIP, unsigned short wPort, char* sLocalIP)
{
	int val = 1;
    int ret = 0;
    int sockfd = 0;
	struct sockaddr_in addr;
	struct ip_mreq mreq;
	unsigned long opt;
    
#ifdef WIN32
    static U8 s_SockInitFlag = 0;
    if (!s_SockInitFlag)
    {
        WSADATA wsa;
        WSAStartup(MAKEWORD(2, 0), &wsa);
        s_SockInitFlag = 1;
    }
#endif

    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if (sockfd < 0)
	{
		return -1;
	}

    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (char*)&val, sizeof(val));

	memset((void *)&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(wPort);
    if (!sLocalIP || strlen(sLocalIP) <= 0)
        addr.sin_addr.s_addr = INADDR_ANY;
    else
        addr.sin_addr.s_addr = inet_addr(sLocalIP);

	if (bind(sockfd, (struct sockaddr *)&addr, sizeof(addr)) < 0)
	{
		closesocket(sockfd);
		return -1;
	}

	mreq.imr_multiaddr.s_addr = inet_addr(sMcastIP); 
    if (!sLocalIP || strlen(sLocalIP) <= 0)
        mreq.imr_interface.s_addr = INADDR_ANY;
    else
        mreq.imr_interface.s_addr = inet_addr(sLocalIP);

	ret = setsockopt(sockfd, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char *)&mreq, sizeof(mreq));
	if (ret < 0)
	{
		closesocket(sockfd);
		return -1;
	}

	opt = 1;
#ifdef WIN32
	if (ioctlsocket(sockfd, FIONBIO, &opt) == SOCKET_ERROR)
#else
    if ((val = fcntl(sockfd, F_GETFL, 0)) < 0 || fcntl(sockfd, F_SETFL, val|O_NONBLOCK) < 0)
#endif
	{
		closesocket(sockfd);
		return -1;
	}

	return sockfd;
}

int CreateUDPSocket(char* ip, U16 port)
{
    int val = 1;
    int sockfd = 0;
	struct sockaddr_in addr;
	unsigned long opt;

#ifdef WIN32
    static U8 s_SockInitFlag = 0;
    if (!s_SockInitFlag)
    {
        WSADATA wsa;
        WSAStartup(MAKEWORD(2, 0), &wsa);
        s_SockInitFlag = 1;
    }
#endif

	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if(sockfd < 0)
        return -1;

	setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (char*)&val, sizeof(val));

	memset((void *)&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
    if (!ip || 0 == strcmp(ip, ""))
        addr.sin_addr.s_addr = INADDR_ANY;
    else
        addr.sin_addr.s_addr = inet_addr(ip);    

	if (bind(sockfd, (struct sockaddr *)&addr, sizeof(addr)) < 0)
	{
		closesocket(sockfd);
		return -1;
	}

	opt = 1;
#ifdef WIN32
	if (ioctlsocket(sockfd, FIONBIO, &opt) == SOCKET_ERROR)
#else
    if ((val = fcntl(sockfd, F_GETFL, 0)) < 0 || fcntl(sockfd, F_SETFL, val|O_NONBLOCK) < 0)
#endif
	{
		closesocket(sockfd);
		return -1;
	}

    return sockfd;
}

int UDPSocketRecv(int sockfd, char* pData, int iDataLen)
{
    struct sockaddr_in inaddr;
	int len;
	long recvbytes = 0;
	long totalbytes = 0;

	if(sockfd <= 0 || !pData || iDataLen <= 0)
        return 0;

	len = sizeof(inaddr);
	while(totalbytes < iDataLen)
	{
		recvbytes = recvfrom(sockfd, (char*)(pData + totalbytes), iDataLen - totalbytes, 0, (struct sockaddr*)&inaddr, PSOCKET_LEN(&len));
		if (recvbytes <= 0)
			break;
		totalbytes += recvbytes;
	}
    

    /*
    NSData *data = [NSData dataWithBytes:pData+44 length:totalbytes-44];
    UIImage *image = [UIImage imageWithData:data];
    NSString *picName = [NSString stringWithFormat:@"%d.%@",rand()%1000,@"jpeg"];
    NSString *dic = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES) objectAtIndex:0];
    NSString *str = [dic stringByAppendingPathComponent:@"Videos"];
    picName = [str stringByAppendingPathComponent:picName];
    
    [data writeToFile:picName atomically:NO];
    NSData *data = [NSData dataWithBytes:pData+44 length:totalbytes-44];
    NSLog(@"UDP接收 长度：%ld   %@",totalbytes,data);
    */
	return (int)totalbytes;
}

#define IMAGE_BUFF_MAX             (256 * 1024)
#define IMAGE_INDEX_MAX            (2)
#define IMAGE_FRAME_BUF_MAX        (1500)
#define IMAGE_FRAME_PACKAGE_MAX    (100)
#define IMAGE_FRAME_HEAD           "@@"
#define IMAGE_FRAME_HEAD_LEN       (8)    // "@@"(2B) + image_index(2B) + frame_total(1B) + frame_inedx(1B) + image_length(2B) + data
#define IMAGE_FRAME_TAIL_LEN       (2)    // "##"
#define IMAGE_FRAME_DATA_LENGTH    (1400)

typedef struct{
    int image_index;
    unsigned char frame_total;
    unsigned char frame_index;
    long image_total_length;
    int package_length;
}udp_image_t;

static udp_image_t image_data[IMAGE_INDEX_MAX] = {0};
static unsigned char image_buffer[IMAGE_INDEX_MAX][IMAGE_BUFF_MAX];
static unsigned char image_temp_buffer[IMAGE_FRAME_BUF_MAX];
static char package_flag[IMAGE_INDEX_MAX][IMAGE_FRAME_PACKAGE_MAX];


int _872UDPSocketRecv(int sockfd, char* pData, int iDataLen){
    struct sockaddr_in inaddr;

    int i;
    int len;
    char *p_image_current;
    long totalbytes = 0;
    long recvbytes = 0;
    int iamge_index = 0;  //recv current picture index
    
    udp_image_t udp_pack;
    memset(&udp_pack, 0, sizeof(udp_image_t));

    if(sockfd <= 0 || !pData || iDataLen <= 0)
        return 0;

    len = sizeof(inaddr);
    while(1)
    {
        recvbytes = recvfrom(sockfd, (char*)image_temp_buffer, IMAGE_FRAME_BUF_MAX, 0, (struct sockaddr*)&inaddr, PSOCKET_LEN(&len));
        if (recvbytes <= IMAGE_FRAME_HEAD_LEN){ // "@@"(2B) + image_index(2B) + frame_total(1B) + frame_inedx(1B) + image_length(2B) + data
            continue;
        }

        if (memcmp(image_temp_buffer, IMAGE_FRAME_HEAD, 2))    //check "@@"
        {
            continue;
        }

        udp_pack.image_index = image_temp_buffer[2] | ((unsigned int)(image_temp_buffer[3]) << 8);      //get image index
        udp_pack.frame_total = image_temp_buffer[4];
        udp_pack.frame_index = image_temp_buffer[5];                                                   //get currentImage framepack index
        udp_pack.package_length = image_temp_buffer[6] | ((unsigned int)(image_temp_buffer[7]) << 8);   //current framepack length
        if ((recvbytes != udp_pack.package_length) || (udp_pack.frame_total == 0) || (udp_pack.frame_index > IMAGE_FRAME_PACKAGE_MAX))
        {
            continue;
        }
        
        i = 0;
        for (i=0; i<IMAGE_INDEX_MAX; i++) 
        {
            if (udp_pack.image_index == image_data[i].image_index)
            {
                iamge_index = i;      //search (n) picture in index buffer
                break;
            }
        }
        
        if (i == IMAGE_INDEX_MAX)    //index buffer empty, recv a new picture
        {
            if (image_data[0].image_index <= image_data[1].image_index)
            {
                iamge_index = 0;
            }
            else
            {
                iamge_index = 1;
            }
            
            memset(&package_flag[iamge_index], 0, IMAGE_FRAME_PACKAGE_MAX);
            memset(&image_data[iamge_index], 0, sizeof(udp_image_t));
            image_data[iamge_index].image_total_length = 0;
            image_data[iamge_index].image_index = udp_pack.image_index;
            
        }
        image_data[iamge_index].frame_total = udp_pack.frame_total;
        image_data[iamge_index].frame_index = udp_pack.frame_index;
        image_data[iamge_index].package_length = udp_pack.package_length;
        if (image_data[iamge_index].frame_index > image_data[iamge_index].frame_total)
        {
            continue;   //protocol err
        }
        
        i = image_data[iamge_index].frame_index;
        if (package_flag[iamge_index][i] == 0)
        {
            p_image_current = (char *)(&image_buffer[iamge_index]);
            memcpy(p_image_current+image_data[iamge_index].frame_index*(IMAGE_FRAME_DATA_LENGTH - (IMAGE_FRAME_HEAD_LEN + IMAGE_FRAME_TAIL_LEN)), image_temp_buffer+IMAGE_FRAME_HEAD_LEN, image_data[iamge_index].package_length - (IMAGE_FRAME_HEAD_LEN + IMAGE_FRAME_TAIL_LEN));
            image_data[iamge_index].image_total_length += (image_data[iamge_index].package_length - (IMAGE_FRAME_HEAD_LEN + IMAGE_FRAME_TAIL_LEN));
        }
        package_flag[iamge_index][i] = 1;
        
        i = 0;
        int count = 0;
        for (i=0; i<image_data[iamge_index].frame_total; i++) //check a picture data had receive complete
        {
            if (package_flag[iamge_index][i] == 0)
            {
                break;
            }
            else
            {
                count += 1;
            }
        }
        
        if (count == image_data[iamge_index].frame_total)
        {
            memcpy(pData, p_image_current, image_data[iamge_index].image_total_length);
            totalbytes = image_data[iamge_index].image_total_length;
            memset(&image_data[iamge_index], 0, sizeof(udp_image_t));
            memset(&package_flag[iamge_index], 0, IMAGE_FRAME_PACKAGE_MAX);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"STREAMNOTI" object:@(totalbytes)];
            break;
        }
        else 
        {
            continue;
        }
    }

    return (int)totalbytes;
}



typedef struct 
{
    char            ip[32];
    U16             port;
    U8              protocol;
    DeviceType_e    dtype;
    int             sockfd;
    fDataCallBack   fun;
    void*           user;
    BLHANDLE        loopBufDataHandle;

    U8              sppThreadRunning;
    THREADID        sppThreadID;
    U8              srThreadRunning;
    THREADID        srThreadID;
}VodPlayHandle_t;


static U8 _ParseFrameType(FHNP_Dev_FrameHead_t* pHead)
{
    if (   pHead->FrmHd[0] != 0x00 
        || pHead->FrmHd[1] != 0x00 
        || pHead->FrmHd[2] != 0x01 
        || (pHead->FrmHd[3] != 0xa0 && pHead->FrmHd[3] != 0xa1 && pHead->FrmHd[3] != 0xa2 
            && pHead->FrmHd[3] != 0xa3 && pHead->FrmHd[3] != 0xa4 && pHead->FrmHd[3] != 0xa5)
       )
    {
        return 255;
    }

    if (pHead->FrmHd[3] == 0xa5)
        return 4;
    else if (pHead->FrmHd[3] == 0xa4)
        return 3;
    else if (pHead->FrmHd[3] == 0xa1)
        return 0;
    else if (pHead->FrmHd[3] == 0xa2)
        return 2;

    return 1;
}

static U8 _StreamDataProc(VodPlayHandle_t* pNode, FHNP_Dev_FrameHead_t* pHead, char* pFrame)
{
    U8 btFrameType = 0;
    VFrameHead_t stVFrameHead;

    memset(&stVFrameHead, 0, sizeof(VFrameHead_t));
    btFrameType = _ParseFrameType(pHead);
    if (0 != btFrameType && 1 != btFrameType && 2 != btFrameType)
        return 0;

    stVFrameHead.frameType = btFrameType;
    stVFrameHead.videoFormat = 0;
    stVFrameHead.restartFlag = pHead->restart_flag;
    stVFrameHead.width = (unsigned short)(pHead->alarmstatus >> 16);
    stVFrameHead.height = (unsigned short)(pHead->alarmstatus);
    stVFrameHead.timeStamp = pHead->timestamp;
	stVFrameHead.res2[0] = pHead->reserve[3];
    stVFrameHead.framerate = pHead->Framerate;

    if (pNode->fun)
        (*pNode->fun)(pNode, &stVFrameHead, pFrame, pHead->framelen, pNode->user);

    return 1;
}

static void _StreamPreProcThreadBody(void* lpUser)
{
    U8 bGetFrameRet = 0;
    FHNP_Dev_FrameHead_t stHead;
    char* pFrame = 0;

    VodPlayHandle_t* handle = (VodPlayHandle_t*)lpUser;
    if (!handle)
        return;

    if (!(pFrame = (char*)malloc(0x80000))) // 512KB
    {
        handle->sppThreadID = 0;
        return;   
    }

    while (handle->sppThreadRunning)
    {
        bGetFrameRet = 0;
        while (1)
        {
            if (!BLBDATA_GetOneFrame(handle->loopBufDataHandle, (char*)&stHead, pFrame, 0))
                break;
            _StreamDataProc(handle, &stHead, pFrame);
            bGetFrameRet = 1;
        }

        if (bGetFrameRet)
        {
            SLEEPMS(0);
        }
        else
        {
            SLEEPMS(1);
        }
    }

    handle->sppThreadID = 0;
    free(pFrame);
}

static void _StreamRecvThreadBody(void* lpUser)
{

    int iRet = 0;
    fd_set fd_set_read;
	fd_set fd_set_error;
    struct timeval tv;
    int iRecvLen = 0;
    char* pLoopBuf1 = NULL; int iLoopBufLen1 = 0;
    char* pLoopBuf2 = NULL; int iLoopBufLen2 = 0;

    char* pUdpBuf = 0;
	int iUdpBufLen = 0;

    VodPlayHandle_t* handle = (VodPlayHandle_t*)lpUser;
    if (!handle)
        return;

    while (handle->srThreadRunning)
    {
        FD_ZERO(&fd_set_read);
		FD_ZERO(&fd_set_error);

        FD_SET((unsigned int)handle->sockfd, &fd_set_read);
		FD_SET((unsigned int)handle->sockfd, &fd_set_error);
        
        tv.tv_sec = 0;
        tv.tv_usec = 500000;
        iRet = select(handle->sockfd+1, &fd_set_read, NULL, &fd_set_error, &tv);
        if (-1 == iRet)
        {
            SLEEPMS(10);
            continue;
        }
        else if (0 == iRet)
        {
            continue;
        }

        //////////////////////////////////////////////////////////////////////////
        if (FD_ISSET(handle->sockfd, &fd_set_read))
        {
			if (!pUdpBuf)
			{
				iUdpBufLen = 1024*1024;
				pUdpBuf = malloc(iUdpBufLen);
			}
			
			if (NULL == pUdpBuf)
				break;
            
            if ([WIFIDeviceModel shareInstance].chipModel == ChipModel_QuanZhi_872) {
                iRecvLen = _872UDPSocketRecv(handle->sockfd, pUdpBuf, iUdpBufLen);
            }else{
                iRecvLen = UDPSocketRecv(handle->sockfd, pUdpBuf, iUdpBufLen);
            }
            
			if (iRecvLen > 0)
			{
				BLBDATA_Lock(handle->loopBufDataHandle);   // lock
				if (BLBDATA_AdvGetWritePtr(handle->loopBufDataHandle, &pLoopBuf1, (unsigned int*)&iLoopBufLen1, &pLoopBuf2, (unsigned int*)&iLoopBufLen2))
				{
					if (iLoopBufLen1 >= iRecvLen)
					{
						memcpy(pLoopBuf1, pUdpBuf, iRecvLen);
					}
					else
					{
						memcpy(pLoopBuf1, pUdpBuf, iLoopBufLen1);
						memcpy(pLoopBuf2, pUdpBuf+iLoopBufLen1, iRecvLen-iLoopBufLen1);
					}
					BLBDATA_AdvSetWritePos(handle->loopBufDataHandle, iRecvLen);
				}						
				BLBDATA_Unlock(handle->loopBufDataHandle); // unlock
			}
        }
    }

	if (pUdpBuf)
		free(pUdpBuf);

    handle->srThreadID = 0;
}

void* StartVodPlay(char* ip, U16 port, char* bindIP, DeviceType_e dtype, fDataCallBack fun, void* user)
{
    VodPlayHandle_t* handle = 0;
    handle = (VodPlayHandle_t*)malloc(sizeof(VodPlayHandle_t));
    if (!handle)
    {
    	return 0;
    }

    memset(handle, 0, sizeof(VodPlayHandle_t));
    if (ip && strlen(ip) > 0)
        strcpy(handle->ip, ip);
    handle->port = port;
    handle->dtype = dtype;
    handle->fun = fun;
    handle->user = user;

#ifdef WIN32
    if (stricmp(ip, "224.0.0.0") > 0 && stricmp(ip, "239.255.255.255") < 0)
#else
    if (strcasecmp(ip, "224.0.0.0") > 0 && strcasecmp(ip, "239.255.255.255") < 0)
#endif
        handle->sockfd = CreateMcastSocket(ip, port, bindIP);
    else
        handle->sockfd = CreateUDPSocket(ip, port);

    if (handle->sockfd < 0)
    {
        free(handle);
        return 0;
    }

    if (EN_DTYPE_FH8610 == dtype)//vgs
        handle->loopBufDataHandle = BLBDATA_Create(BLBDATA_TYPE_61_FRAME, 1024*500);
    else if (EN_DTYPE_FH8620 == dtype) //udp720p udp720pyu
        handle->loopBufDataHandle = BLBDATA_Create(BLBDATA_TYPE_62_FRAME, 1024*1024*10);
    else if (EN_DTYPE_720P == dtype) //udp720p
        handle->loopBufDataHandle = BLBDATA_Create(BLBDATA_TYPE_62_FRAME, 1024*1024*10);
    else if (EN_DTYPE_FH8810 == dtype)
        handle->loopBufDataHandle = BLBDATA_Create(BLBDATA_TYPE_81_FRAME, 1024*1024*10);
    else if (EN_DTYPE_1080P == dtype) //1080p
        handle->loopBufDataHandle = BLBDATA_Create(BLBDATA_TYPE_81_FRAME, 1024*1024*20);
    else if (EN_DTYPE_2K == dtype)
        handle->loopBufDataHandle = BLBDATA_Create(BLBDATA_TYPE_62_FRAME, 1024*1024*10);
    
    if (0 == handle->loopBufDataHandle)
    {
        closesocket(handle->sockfd);
        free(handle);
        return 0;
    }

    handle->sppThreadRunning = 1;
    if (!StartThread(_StreamPreProcThreadBody, handle, &handle->sppThreadID))
    {
        handle->sppThreadRunning = 0;
        closesocket(handle->sockfd);
        BLBDATA_Destory(handle->loopBufDataHandle);
        free(handle);
        return 0;
    }

    handle->srThreadRunning = 1;
    if (!StartThread(_StreamRecvThreadBody, handle, &handle->srThreadID))
    {
        handle->srThreadRunning = 0;
        closesocket(handle->sockfd);
        BLBDATA_Destory(handle->loopBufDataHandle);
        free(handle);
        return 0;
    }
    return handle;
}

void StopVodPlay(void* handle)
{
    U32 dwCount = 0;
    VodPlayHandle_t* playHandle = (VodPlayHandle_t*)handle;

    if (!handle)
        return;

    dwCount = 0;
    playHandle->srThreadRunning = 0;
    while (playHandle->srThreadID && dwCount < 300)
    {
        dwCount++;
        SLEEPMS(10);
    }

    dwCount = 0;
    playHandle->sppThreadRunning = 0;
    while (playHandle->sppThreadID && dwCount < 300)
    {
        dwCount++;
        SLEEPMS(10);
    }

    closesocket(playHandle->sockfd);
    BLBDATA_Destory(playHandle->loopBufDataHandle);
    free(playHandle);
}

//////////////////////////////////////////////////////////////////////////
typedef struct 
{
    char    ip[32];
    U16     port;
    U8      protocol;
    char    destIP[32];
    U16     destPort;
    int     sockfd;
}SendHandle_t;

void* CreateSendHandle(char* ip, U16 port, U8 protocol, char* destIP, U16 destPort)
{
    SendHandle_t* handle = 0;
    if (!(handle = (SendHandle_t*)malloc(sizeof(SendHandle_t))))
        return 0;
    
    memset(handle, 0, sizeof(SendHandle_t));
    if (ip && strlen(ip) > 0)
        strcpy(handle->ip, ip);
    handle->port = port;
    handle->protocol = protocol;
    if (destIP && strlen(destIP) > 0)
        strcpy(handle->destIP, destIP);
    handle->destPort = destPort;

    if ((handle->sockfd = CreateUDPSocket(ip, port)) < 0)
    {
        free(handle);
        return 0;
    }

    return handle;
}

int SendData(void* sendHandle, char* buf, U32 bufLen)
{
    struct sockaddr_in devAddr;
    int devAddrLen = sizeof(devAddr);

    SendHandle_t* handle = (SendHandle_t*)sendHandle;
    if (!handle || !buf || 0 == bufLen)
        return 0;

    memset((void *)&devAddr, 0, sizeof(devAddr));
    devAddr.sin_family = AF_INET;
    devAddr.sin_port = htons(handle->destPort);
    if (strlen(handle->destIP) > 0)
        devAddr.sin_addr.s_addr = inet_addr(handle->destIP);
    else
        devAddr.sin_addr.s_addr = INADDR_ANY;

    return sendto(handle->sockfd, buf, bufLen, 0, (struct sockaddr*)&devAddr, devAddrLen);
}

void DestorySendHandle(void* sendHandle)
{
    SendHandle_t* handle = (SendHandle_t*)sendHandle;
    if (!handle)
        return;

    closesocket(handle->sockfd);
    free(handle);
}
