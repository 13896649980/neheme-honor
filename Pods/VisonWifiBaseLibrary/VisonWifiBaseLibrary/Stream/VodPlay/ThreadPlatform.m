#include "ThreadPlatform.h"

int StartThread(void* pThreadBody, void* pUser, THREADID* pThreadID)
{
#if defined(WIN32)
    HANDLE hThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)pThreadBody, pUser, 0, (unsigned int*)pThreadID);
    if (!hThread)
        return 0;
#elif defined(_ECOS_)
    static unsigned char thread_stack[0x1000]; // 4KB
    static cyg_handle_t thread_handle;
    static cyg_thread thread_obj;
    cyg_thread_create(5, pThreadBody, (cyg_addrword_t)pUser, "sdk_thread", &thread_stack, 0x1000, &thread_handle, thread_obj);
    cyg_thread_resume(thread_handle);
    *pThreadID = thread_handle;
#elif defined(_MIPSEL_)
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);				
    if(0 != pthread_create(pThreadID, &attr, (void* (*)(void *))pThreadBody, pUser))
    {
        pthread_attr_destroy(&attr);
        return 0;
    }
	pthread_attr_destroy(&attr);
#else
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);				
    if(0 != pthread_create(pThreadID, &attr, (void* (*)(void *))pThreadBody, pUser))
    {
        pthread_attr_destroy(&attr);
        return 0;
    }
	pthread_attr_destroy(&attr);
#endif
    return 1;
}
