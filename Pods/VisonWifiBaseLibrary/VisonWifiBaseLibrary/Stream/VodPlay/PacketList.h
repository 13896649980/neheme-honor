//
//  PacketList.h
//  jltClient
//
//  Created by sz.fullhan on 14-7-10.
//  Copyright (c) 2014年 jlt. All rights reserved.
//

#ifndef jltClient_PacketList_h
#define jltClient_PacketList_h

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _myPacketNode_t
{
    struct _myPacketNode_t *next;
    struct _myPacketNode_t *prev;
    char* pBuf;
    unsigned char frameType;
    int len;
    unsigned long long pts;
    int sampleRate;
    int audioFmt;
    int audioTrack;
    int audioBitWidth;
    int vWidth;
    int vHeight;
    uint8_t *data[8];
    int linesize[8];
    unsigned int lostFrameCount;
    int framerate;
//    
//    char *luma;
//    char *chromaB;
//    char *chromaR;
    
}myPacketNode_t;

typedef struct
{
    myPacketNode_t node;
    int count;
}myPacketList_t;

void myPacketListInit(myPacketList_t* pList);

int myPacketListCount(myPacketList_t* pList);

void myPacketListPush(myPacketList_t* pList, myPacketNode_t* pNode);

myPacketNode_t* myPacketListPop(myPacketList_t* pList);

myPacketNode_t* myPacketListPop_try(myPacketList_t* pList);

#ifdef __cplusplus
}
#endif
#endif
