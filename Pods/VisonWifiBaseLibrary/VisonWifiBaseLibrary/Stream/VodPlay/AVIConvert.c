#include <stdlib.h>
#include <errno.h>
#include "AVIConvert.h"

#if defined(WIN32)    // WIN32
#   ifndef STRUCT_ATTR 
#       define STRUCT_ATTR 
#   endif
#   define PSOCKET_LEN(x)   ((int*)(x))
#   include <WINSOCK2.H>
#   include <ws2tcpip.h>
#   pragma comment(lib, "ws2_32.lib")
#   include <windows.h>
#   include <stdio.h>
#   define SOCKET_LASTERRID     WSAGetLastError()
#   define SOCKET_LASTSTRERR    ""
    typedef int socklen_t;
#elif defined(_ECOS_)   // ecos
#   ifndef STRUCT_ATTR 
#       define STRUCT_ATTR      __attribute__ ((packed))
#   endif
#   define SOCKET_ERROR     (-1)
#   define PSOCKET_LEN(x)   ((socklen_t*)(x))
#   define CALLBACK 
#   include <stdio.h>
#   include <stdlib.h>
#   include <sys/types.h>
#   include <unistd.h>
#   include <pthread.h>
#   include <fcntl.h>
#   define SOCKET_LASTERRID     errno
#   define SOCKET_LASTSTRERR    strerror(errno)
#else           // other
#   ifndef STRUCT_ATTR 
#       define STRUCT_ATTR      __attribute__ ((packed))
#   endif
#   define SOCKET_ERROR     (-1)
#   define PSOCKET_LEN(x)   ((socklen_t*)(x))
#   define CALLBACK 
#   include <stdio.h>
#   include <memory.h>
#   include <stdlib.h>
#   include <sys/types.h>
#   include <sys/socket.h>
#   include <netinet/in.h>
#   include <arpa/inet.h>
#   include <unistd.h>
#   include <pthread.h>
#   include <fcntl.h>
#   define SOCKET_LASTERRID     errno
#   define SOCKET_LASTSTRERR    strerror(errno)
#endif          // endif

#ifndef MY_PRINTF
#define MY_PRINTF  printf
#else 
extern int MY_PRINTF(const char *fmt, ...);
#endif

#define AVIF_TRUSTCKTYPE        0x00000800
#define AVIF_HASINDEX           0x00000010      // 标明该AVI文件有"idx1"块
#define AVIF_ISINTERLEAVED      0x00000100      // 标明该AVI文件是interleaved格式的
#define AVI_MAX_RIFF_SIZE       0x70000000UL    // 1.75GB 0x40000000UL TBD_WAIT ...

#define AVI_MS_ASIST_VALUE      1000000UL
#define AVI_SUGGEST_BUF_SIZE    0x100000        // 1M 当高清时此值可适当改大 RM use 平均值 TBD_WAIT ...
#define AVI_MID_BUF_SIZE        (256 * 1024)


#define AVI_DWORD   unsigned int
#define AVI_WORD    unsigned short

typedef struct                                  //AVI文件的开始
{
    char        riff[4];                        //四个字符RIFF
    AVI_DWORD   file_size;                      //文件大小，不包括riff,file_size字段，小字节顺序 ...
    char        file_type[4];                   //文件类型，四个字符：AVI [RM AVI ]
} RIFF_HEADER_T;

typedef struct                                  //AVI头部信息块的开始
{
    char        list[4];                        //四个字符LIST，表示列表 
    AVI_DWORD   list_size;                      //列表大小，不包括list,size_list字段，小字节顺序  [v 192 a 102  v+a 0x0126(294) ]
    char        list_type[4];                   //四个字符，列表类型，如hdrl
} HDR_HEADER_LIST_T;


typedef struct                                  //avi头部列表开始
{
    char        avih[4];                        //四个字符avih，表示avi的头部信息
    AVI_DWORD   avih_size;                      //avi的头部信息的大小  sizeof(MAIN_AVI_HEADER)  [56] ...
   
    /*MainAVIHeader*/
    AVI_DWORD   microsec_per_frame;             //显示每帧所需的时间ns,定义avi的显示速率  [1000000 / fr] ...
    AVI_DWORD   max_bytes_per_sec;              //最大的数据传输率  [码率 ...]
    AVI_DWORD   padding_granularity;            //记录块的长度需为此值的倍数，通常是2048 [RM 0...]
    AVI_DWORD   flags;                          //AVI文件的特殊属性，如是否包含索引块，音视频数据是否交叉存储 [RM 16(0x10)...]
    AVI_DWORD   total_frame;                    //文件中的总帧数
    AVI_DWORD   initial_frames;                 //说明在开始播放前需要多少帧  [RM 0...]
    AVI_DWORD   streams;                        //文件中包含的数据流种类   [RM av一起时 2 ...]
    AVI_DWORD   suggested_buffer_size;          //建议使用的缓冲区大小；通常为存储一帧图像以及同步声音所需要的数据之和 [RM 使用的是平均值 ...]
    AVI_DWORD   width;                          //图像宽  [RM 352]
    AVI_DWORD   height;                         //图像高  [RM 240]
    AVI_DWORD   reserved[4];                    //保留
} AVI_HEADER_T;

typedef struct                                  //(video)AVI流头部列表
{
    char        list[4];                        //"LIST"
    AVI_DWORD   list_size;                      // [RM V 116   Audio  94...]   
    char        list_type[4];                   //"strl"数据流类型
} AVISTREAM_HEADER_LIST_T;

typedef struct
{
    char        block[4];                       //"strh",表示流头部
    AVI_DWORD   list_size;                      //流头部大小  [56 无论v audio... ...]

    /*AVIStreamHeader*/
    char          fcc_type[4];                  //4字节，表示数据流的种类,vids表示视频数据流，auds音频数据流 [RM vids ...auds ...]
    char          fcc_handler[4];               //4字节，表示数据流解压缩的驱动程序代号  [RM v H264  audio 值0...]
    AVI_DWORD     flags;                        //数据流属性   [RM 0 ...]
    AVI_WORD      priotity;                     //此数据流的播放优先级  [RM 0 ...]
    AVI_WORD      language;                     //音频的语言代号  [RM 0 ...]
    AVI_DWORD     inital_frames;                //说明在开始播放前需要多少帧  [RM v 0 ，audio 0]
    AVI_DWORD     scale;                        //数据量，视频每帧的大小或者音频的采样大小  [RM v填入帧间隔微秒 同microsec_per_frame; audio 0x7d(125)...] TBD_WAIT ... 
    AVI_DWORD     rate;                         //rate/scale = fps;   [RM v audio 均 1000000] 
    AVI_DWORD     start;                        //数据流开始播放的位置，以dwScale为单位   [RM 0 ... ...] 
    AVI_DWORD     length;                       //数据流的数据量，以dwScale为单位(总帧数) [RM v audio 均填的是 total_frame ...]
    AVI_DWORD     suggested_buffer_size;        //建议缓冲区的大小   [RM 使用的是平均值  audio用的是16000 ...]
    AVI_DWORD     quality;                      //解压缩质量参数，值越大，质量越好 [RM 使用的 0xffffffff]
    AVI_DWORD     sample_size;                  //音频的采样大小        [RM v 0 audio 0  TBD_WAIT ...]         
    AVI_WORD      left;                         //RECT 视频图像所占的矩形, 指定这个流（视频流或文字流）在视频主窗口中的显示位置
    AVI_WORD      top;
    AVI_WORD      right;                        // [RM width...]
    AVI_WORD      bottom;                       // [RM height...]
} AVISTREAM_HEADER_T;

typedef struct                                  //流格式块
{
    char        block[4];                       //"strf"
    AVI_DWORD   block_size;                     // [RM v: 40 audio : 18 ...]
} AVISTREAM_FORMAT_T;

typedef struct  
{
    /*bitmapInfoHeader*/
    AVI_DWORD    size;                          //位图信息头部大小 [RM v: 40  ...]
    AVI_DWORD    width;                         //图像宽度
    AVI_DWORD    height;                        //图像高度
    AVI_WORD     planes;                        //目标设备位面熟，设为1
    AVI_WORD     bit_count;                     //单位像素的位数，即图像的位深度 [RM 24 ...]
    char         compression[4];                //图像的压缩类型  [RM H264 ...]
    AVI_DWORD    size_image;                    //图像的大小，以字节为单位 [RM 以下全0 ...]
    AVI_DWORD    xpels_per_meter;               //水平方向每米像素数
    AVI_DWORD    ypels_per_meter;               //垂直方向每米像素数
    AVI_DWORD    clr_used;                      //实际使用的色彩表中的颜色索引数
    AVI_DWORD    clr_important;                 //重要的颜色数，此值为0时所有颜色都重要  
    //  RGBQUAD     bmiColors[1];                          //颜色表
} AVI_BIT_MAP_INFO_T;

typedef struct  
{
    AVI_WORD    format_tag;                     // 指定格式类型, 默认 WAVE_FORMAT_PCM = 1   [RM 1 ...]
    AVI_WORD    channels;                       // 指出波形数据的声道数; 单声道为 1, 立体声为 2  [RM 1...]
    AVI_DWORD   samples_per_sec;                // 指定采样频率(每秒的样本数)                [RM 8000 采样率]
    AVI_DWORD   bytes_per_sec;                  // 指定数据传输的传输速率(每秒的字节数)  [RM 16000 ...]
    AVI_WORD    block_align;                    // 指定块对齐块对齐是数据的最小单位 [RM 2 ...]
    AVI_WORD    bits_per_sample;                // 采样大小  [RM 16 ...]
    AVI_WORD    cb_size;                        // 附加信息的字节大小 [RM 18 TBD_WAIT...]
} AVI_WAVE_FORMATEX_T;

typedef struct                                  //option 可选
{
    char        list[4];                        //四个字符LIST，表示列表    
    AVI_DWORD   list_size;                      //列表大小，不包括list,size_list字段，小字节顺序
    char        list_type[4];                   //四个字符，列表类型，如movi
} AVI_JUNK_T;

typedef struct                                  //媒体数据整体数据的开始
{
    char        list[4];                        //四个字符"LIST"
    AVI_DWORD   list_size;                      //整个媒体块的大小，字节数  从movi开始计 TBD_WAIT...
    char        movi[4];                        //"movi"表示视频媒体块的开始    
} AVI_MOVIE_LIST_T;

typedef struct                                  //一帧媒体数据的开始
{
    char        flag[4];                        //"00dc"表示视频"00wb"表示音频 [RM v 00dc  audio 01wb]
    AVI_DWORD   data_size;                      //一帧视频或音频大小
} AVI_FRAME_STORE_T;

typedef struct  
{
    char        idx[4];                         //必须为idx1
    AVI_DWORD   cb;                             //本数据结构的大小，不包括最初的8字节(fcc和cb两个域)
} AVI_OLD_INDEX_T;

typedef struct                                  //表征每一帧的属性
{
    char        chunk_id[4];                    //表征本数据块的四字符码 [RM v 00dc  audio 01wb]
    AVI_DWORD   flags;                          //说明本数据块是不是关键帧，是不是rec列表等信息[(0x10 标示 I 帧)]
    AVI_DWORD   offset;                         //本数据块在文件中的偏移量,可以相对于‘movi’列表，也可以相对于AVI文件开头
    AVI_DWORD   size;                           //本数据块的大小 帧的大小
} AVI_OLD_INDEX_ENTY_T;                         //只是一个数组，为每个媒体数据块（一帧）定义一个索引值


// 暂用此种分拆方式 (TBD_WAIT ...)
static void AVIWriteWORD(FILE *pFile, AVI_WORD wValue)
{
    fputc(( wValue      ) & 0xff, pFile);
    fputc(( wValue >> 8 ) & 0xff, pFile);
}

static void AVIWriteDWORD(FILE *pFile, AVI_DWORD dwValue)
{
    fputc(( dwValue      ) & 0xff, pFile);
    fputc(( dwValue >> 8 ) & 0xff, pFile);
    fputc(( dwValue >> 16) & 0xff, pFile);
    fputc(( dwValue >> 24) & 0xff, pFile);
}


// 部分信息(如size 等)需要在结束时 再次填充
// phAvi会填充句柄
AVIHANDLE AVIStart(AVIMakeCfg_t* pstMakeCfg)
{
    AVIMakeCB_t* pstAVICfg = NULL;
    FILE *pFile = NULL;
    FILE *pIdxFile = NULL;

    if (NULL == pstMakeCfg || NULL == pstMakeCfg->pAVIFile || NULL == pstMakeCfg->pAVIIdxFile)
        return 0;

    pstAVICfg = (AVIMakeCB_t*)malloc(sizeof(AVIMakeCB_t));
    if (NULL == pstAVICfg)
        return 0;
    memset(pstAVICfg, 0, sizeof(AVIMakeCB_t));

    pFile = pstMakeCfg->pAVIFile;
    pIdxFile = pstMakeCfg->pAVIIdxFile;
    pstAVICfg->stAVIMakeCfg = (*pstMakeCfg);
    rewind(pFile);

/*
typedef struct                                  //AVI文件的开始
{
    char        riff[4];                        //四个字符RIFF
    AVI_DWORD   file_size;                      //文件大小，不包括riff,file_size字段，小字节顺序 ...
    char        file_type[4];                   //文件类型，四个字符：AVI [RM AVI ]
} RIFF_HEADER_T;
*/
    fwrite("RIFF", 1, 4, pFile);
    AVIWriteDWORD(pFile, pstAVICfg->dwFileSize);//finish时更新
    fwrite("AVI ", 1, 4, pFile);

/*
typedef struct                                  //AVI头部信息块的开始
{
    char        list[4];                        //四个字符LIST，表示列表 
    AVI_DWORD   list_size;                      //列表大小，不包括list,size_list字段，小字节顺序  [v 192 a 102  v+a 0x0126(294) ]
    char        list_type[4];                   //四个字符，列表类型，如hdrl
} HDR_HEADER_LIST_T;
*/
    fwrite("LIST", 1, 4, pFile);
    AVIWriteDWORD(pFile, 294);                  //audio+video
    fwrite("hdrl", 1, 4, pFile);

/*
typedef struct                                  //avi头部列表开始
{
    char        avih[4];                        //四个字符avih，表示avi的头部信息
    AVI_DWORD   avih_size;                      //avi的头部信息的大小  sizeof(MAIN_AVI_HEADER)  [56] ...
   
    //MainAVIHeader
    AVI_DWORD   microsec_per_frame;             //显示每帧所需的时间ns,定义avi的显示速率  [1000000 / fr] ...
    AVI_DWORD   max_bytes_per_sec;              //最大的数据传输率  [码率 ...]
    AVI_DWORD   padding_granularity;            //记录块的长度需为此值的倍数，通常是2048 [RM 0...]
    AVI_DWORD   flags;                          //AVI文件的特殊属性，如是否包含索引块，音视频数据是否交叉存储 [RM 16(0x10)...]
    AVI_DWORD   total_frame;                    //文件中的总帧数
    AVI_DWORD   initial_frames;                 //说明在开始播放前需要多少帧  [RM 0...]
    AVI_DWORD   streams;                        //文件中包含的数据流种类   [RM av一起时 2 ...]
    AVI_DWORD   suggested_buffer_size;          //建议使用的缓冲区大小；通常为存储一帧图像以及同步声音所需要的数据之和 [RM 使用的是平均值 ...]
    AVI_DWORD   width;                          //图像宽  [RM 352]
    AVI_DWORD   height;                         //图像高  [RM 240]
    AVI_DWORD   reserved[4];                    //保留
} AVI_HEADER_T;
*/
    fwrite("avih", 1, 4, pFile);
    AVIWriteDWORD(pFile, 56);

    // MainAVIHeader
    pstAVICfg->dwRateUpdatePos = ftell(pFile);
    AVIWriteDWORD(pFile, AVI_MS_ASIST_VALUE * 100 / pstMakeCfg->dwRate);
    pstAVICfg->dwAVBitrateUpdatePos = ftell(pFile);
    AVIWriteDWORD(pFile, (pstMakeCfg->dwMaxVideoBitrate+pstMakeCfg->dwMaxAudioBitrate) / 8);
    AVIWriteDWORD(pFile, 0);
    
    AVIWriteDWORD(pFile, AVIF_HASINDEX);
    AVIWriteDWORD(pFile, pstAVICfg->dwVideoCount + pstAVICfg->dwAudioCount);
    AVIWriteDWORD(pFile, 0);
    AVIWriteDWORD(pFile, 2);                    //audio+video
    AVIWriteDWORD(pFile, AVI_SUGGEST_BUF_SIZE);
    pstAVICfg->dwWHUpdatePos = ftell(pFile);    //注意是AVI_DWORD(4字节)
    AVIWriteDWORD(pFile, pstMakeCfg->dwWidth);
    AVIWriteDWORD(pFile, pstMakeCfg->dwHeight);
    AVIWriteDWORD(pFile, 0);
    AVIWriteDWORD(pFile, 0);
    AVIWriteDWORD(pFile, 0);
    AVIWriteDWORD(pFile, 0);

/*
typedef struct                                  //(video)AVI流头部列表
{
    char        list[4];                        //"LIST"
    AVI_DWORD   list_size;                      // [RM V 116   Audio  94...]   
    char        list_type[4];                   //"strl"数据流类型
} AVISTREAM_HEADER_LIST_T;
*/
    // video
    fwrite("LIST", 1, 4, pFile);
    AVIWriteDWORD(pFile, 116);
    fwrite("strl", 1, 4, pFile);

/*
typedef struct
{
    char        block[4];                       //"strh",表示流头部
    AVI_DWORD   list_size;                      //流头部大小  [56 无论v audio... ...]

    //AVIStreamHeader
    char          fcc_type[4];                  //4字节，表示数据流的种类,vids表示视频数据流，auds音频数据流 [RM vids ...auds ...]
    char          fcc_handler[4];               //4字节，表示数据流解压缩的驱动程序代号  [RM v H264  audio 值0...]
    AVI_DWORD     flags;                        //数据流属性   [RM 0 ...]
    AVI_WORD      priotity;                     //此数据流的播放优先级  [RM 0 ...]
    AVI_WORD      language;                     //音频的语言代号  [RM 0 ...]
    AVI_DWORD     inital_frames;                //说明在开始播放前需要多少帧  [RM v 0 ，audio 0]
    AVI_DWORD     scale;                        //数据量，视频每帧的大小或者音频的采样大小  [RM v填入帧间隔微秒 同microsec_per_frame; audio 0x7d(125)...] TBD_WAIT ... 
    AVI_DWORD     rate;                         //rate/scale = fps;   [RM v audio 均 1000000] 
    AVI_DWORD     start;                        //数据流开始播放的位置，以dwScale为单位   [RM 0 ... ...] 
    AVI_DWORD     length;                       //数据流的数据量，以dwScale为单位(总帧数) [RM v audio 均填的是 total_frame ...]
    AVI_DWORD     suggested_buffer_size;        //建议缓冲区的大小   [RM 使用的是平均值  audio用的是16000 ...]
    AVI_DWORD     quality;                      //解压缩质量参数，值越大，质量越好 [RM 使用的 0xffffffff]
    AVI_DWORD     sample_size;                  //音频的采样大小        [RM v 0 audio 0  TBD_WAIT ...]         
    AVI_WORD      left;                         //RECT 视频图像所占的矩形, 指定这个流（视频流或文字流）在视频主窗口中的显示位置
    AVI_WORD      top;
    AVI_WORD      right;                        // [RM width...]
    AVI_WORD      bottom;                       // [RM height...]
} AVISTREAM_HEADER_T;
*/    
    fwrite("strh", 1, 4, pFile);
    AVIWriteDWORD(pFile, 56);

    // AVIStreamHeader
    fwrite("vids", 1, 4, pFile);
    fwrite("H264", 1, 4, pFile);
    AVIWriteDWORD(pFile, 0);
    AVIWriteWORD(pFile, 0); // priotity
    AVIWriteWORD(pFile, 0); // language
    AVIWriteDWORD(pFile, 0); // inital_frames
    pstAVICfg->dwRateUpdatePos2 = ftell(pFile);
    AVIWriteDWORD(pFile, AVI_MS_ASIST_VALUE * 100 / pstMakeCfg->dwRate); // scale
    AVIWriteDWORD(pFile, AVI_MS_ASIST_VALUE); // rate 
    AVIWriteDWORD(pFile, 0);
    pstAVICfg->dwVideoLenFillPos = ftell(pFile);
    AVIWriteDWORD(pFile, pstAVICfg->dwVideoCount/* + pstAVICfg->dwAudioCount*/); // wait upt...
    AVIWriteDWORD(pFile, AVI_SUGGEST_BUF_SIZE); // suggested_buffer_size
    AVIWriteDWORD(pFile, 0xffffffff); // 解压缩质量参数，值越大，质量越好
    AVIWriteDWORD(pFile, 0);
    AVIWriteWORD(pFile, 0);
    AVIWriteWORD(pFile, 0);
    pstAVICfg->wWHUpdatePos = ftell(pFile);   // 注意是AVI_WORD(2字节)
    AVIWriteWORD(pFile, (AVI_WORD)pstMakeCfg->dwWidth);
    AVIWriteWORD(pFile, (AVI_WORD)pstMakeCfg->dwHeight);

/*
typedef struct                                  //流格式块
{
    char        block[4];                       //"strf"
    AVI_DWORD   block_size;                     // [RM v: 40 audio : 18 ...]
} AVISTREAM_FORMAT_T;
*/
    fwrite("strf", 1, 4, pFile);
    AVIWriteDWORD(pFile, 40);

/*
typedef struct  
{
    //bitmapInfoHeader
    AVI_DWORD    size;                          //位图信息头部大小 [RM v: 40  ...]
    AVI_DWORD    width;                         //图像宽度
    AVI_DWORD    height;                        //图像高度
    AVI_WORD     planes;                        //目标设备位面熟，设为1
    AVI_WORD     bit_count;                     //单位像素的位数，即图像的位深度 [RM 24 ...]
    char         compression[4];                //图像的压缩类型  [RM H264 ...]
    AVI_DWORD    size_image;                    //图像的大小，以字节为单位 [RM 以下全0 ...]
    AVI_DWORD    xpels_per_meter;               //水平方向每米像素数
    AVI_DWORD    ypels_per_meter;               //垂直方向每米像素数
    AVI_DWORD    clr_used;                      //实际使用的色彩表中的颜色索引数
    AVI_DWORD    clr_important;                 //重要的颜色数，此值为0时所有颜色都重要  
    //  RGBQUAD     bmiColors[1];                          //颜色表
} AVI_BIT_MAP_INFO_T;
*/
    AVIWriteDWORD(pFile, 40);
    pstAVICfg->dwWHUpdatePos2 = ftell(pFile);
    AVIWriteDWORD(pFile, pstMakeCfg->dwWidth);
    AVIWriteDWORD(pFile, pstMakeCfg->dwHeight);
    AVIWriteWORD(pFile, 1);//目标设备位 planes，设为1
    AVIWriteWORD(pFile, 24); // 色深 
    fwrite("H264", 1, 4, pFile);
    AVIWriteDWORD(pFile, 0);
    AVIWriteDWORD(pFile, 0);
    AVIWriteDWORD(pFile, 0);
    AVIWriteDWORD(pFile, 0);
    AVIWriteDWORD(pFile, 0);

    //if (pstMakeCfg->byIsAudioFrame)
    {
        /*
        typedef struct                                  //(video)AVI流头部列表
        {
            char        list[4];                        //"LIST"
            AVI_DWORD   list_size;                      // [RM V 116   Audio  94...]   
            char        list_type[4];                   //"strl"数据流类型
        } AVISTREAM_HEADER_LIST_T;
        */
        fwrite("LIST", 1, 4, pFile);
        AVIWriteDWORD(pFile, 94);
        fwrite("strl", 1, 4, pFile);

        /*
        typedef struct
        {
            char        block[4];                       //"strh",表示流头部
            AVI_DWORD   list_size;                      //流头部大小  [56 无论v audio... ...]

            //AVIStreamHeader
            char          fcc_type[4];                  //4字节，表示数据流的种类,vids表示视频数据流，auds音频数据流 [RM vids ...auds ...]
            char          fcc_handler[4];               //4字节，表示数据流解压缩的驱动程序代号  [RM v H264  audio 值0...]
            AVI_DWORD     flags;                        //数据流属性   [RM 0 ...]
            AVI_WORD      priotity;                     //此数据流的播放优先级  [RM 0 ...]
            AVI_WORD      language;                     //音频的语言代号  [RM 0 ...]
            AVI_DWORD     inital_frames;                //说明在开始播放前需要多少帧  [RM v 0 ，audio 0]
            AVI_DWORD     scale;                        //数据量，视频每帧的大小或者音频的采样大小  [RM v填入帧间隔微秒 同microsec_per_frame; audio 0x7d(125)...] TBD_WAIT ... 
            AVI_DWORD     rate;                         //rate/scale = fps;   [RM v audio 均 1000000] 
            AVI_DWORD     start;                        //数据流开始播放的位置，以dwScale为单位   [RM 0 ... ...] 
            AVI_DWORD     length;                       //数据流的数据量，以dwScale为单位(总帧数) [RM v audio 均填的是 total_frame ...]
            AVI_DWORD     suggested_buffer_size;        //建议缓冲区的大小   [RM 使用的是平均值  audio用的是16000 ...]
            AVI_DWORD     quality;                      //解压缩质量参数，值越大，质量越好 [RM 使用的 0xffffffff]
            AVI_DWORD     sample_size;                  //音频的采样大小        [RM v 0 audio 0  TBD_WAIT ...]         
            AVI_WORD      left;                         //RECT 视频图像所占的矩形, 指定这个流（视频流或文字流）在视频主窗口中的显示位置
            AVI_WORD      top;
            AVI_WORD      right;                        // [RM width...]
            AVI_WORD      bottom;                       // [RM height...]
        } AVISTREAM_HEADER_T;
        */    
        fwrite("strh", 1, 4, pFile);
        AVIWriteDWORD(pFile, 56);
        fwrite("auds", 1, 4, pFile);
        AVIWriteDWORD(pFile, 0); // audio format  TBD_WAIT ...
        AVIWriteDWORD(pFile, 0);
        AVIWriteWORD(pFile, 0); // priotity
        AVIWriteWORD(pFile, 0); // language
        AVIWriteDWORD(pFile, 0); //
        pstAVICfg->dwSamplerateUpdatePos = ftell(pFile);
        AVIWriteDWORD(pFile, AVI_MS_ASIST_VALUE / pstMakeCfg->dwAudioSamplerate); // scale TBD_WAIT ...
        AVIWriteDWORD(pFile, AVI_MS_ASIST_VALUE); // rate 
        AVIWriteDWORD(pFile, 0);
        pstAVICfg->dwAudioLenFillPos = ftell(pFile);
        AVIWriteDWORD(pFile, /*pstAVICfg->dwVideoCount + */pstAVICfg->dwAudioCount); // wait upt...
        pstAVICfg->dwSampBitWUpdatePos = ftell(pFile);
        AVIWriteDWORD(pFile, pstMakeCfg->dwAudioSamplerate * pstMakeCfg->dwAudioBitWidth / 8); // RM 用的是平均帧大小 TBD_WAIT...
        AVIWriteDWORD(pFile, 0xffffffff); // 解压缩质量参数，值越大，质量越好 [RM 使用的 全fffffffff]
        AVIWriteDWORD(pFile, 0);
        AVIWriteWORD(pFile, 0);
        AVIWriteWORD(pFile, 0);
        AVIWriteWORD(pFile, 0);
        AVIWriteWORD(pFile, 0);
    
        /*
        typedef struct                                  //流格式块
        {
            char        block[4];                       //"strf"
            AVI_DWORD   block_size;                     // [RM v: 40 audio : 18 ...]
        } AVISTREAM_FORMAT_T;
        */
        fwrite("strf", 1, 4, pFile);
        AVIWriteDWORD(pFile, 18);

        /*
        typedef struct  
        {
            AVI_WORD    format_tag;                     // 指定格式类型, 默认 WAVE_FORMAT_PCM = 1   [RM 1 ...]
            AVI_WORD    channels;                       // 指出波形数据的声道数; 单声道为 1, 立体声为 2  [RM 1...]
            AVI_DWORD   samples_per_sec;                // 指定采样频率(每秒的样本数)                [RM 8000 采样率]
            AVI_DWORD   bytes_per_sec;                  // 指定数据传输的传输速率(每秒的字节数)  [RM 16000 ...]
            AVI_WORD    block_align;                    // 指定块对齐块对齐是数据的最小单位 [RM 2 ...]
            AVI_WORD    bits_per_sample;                // 采样大小  [RM 16 ...]
            AVI_WORD    cb_size;                        // 附加信息的字节大小 [RM 18 TBD_WAIT...]
        } AVI_WAVE_FORMATEX_T;
        */
        AVIWriteWORD(pFile, 1);// 指定格式类型, 默认 WAVE_FORMAT_PCM = 1
        pstAVICfg->dwAudioParamUpdatePos = ftell(pFile);
        AVIWriteWORD(pFile, pstMakeCfg->byAudioTrack);// 指出波形数据的声道数; 单声道为 1, 立体声为 2 (??)
        AVIWriteDWORD(pFile, pstMakeCfg->dwAudioSamplerate); // 指定采样频率(每秒的样本数)  
        AVIWriteDWORD(pFile, pstMakeCfg->dwAudioSamplerate * pstMakeCfg->dwAudioBitWidth / 8);// 指定数据传输的传输速率(每秒的字节数)  
        AVIWriteWORD(pFile, (AVI_WORD)(pstMakeCfg->dwAudioBitWidth / 8));// 指定块对齐块对齐是数据的最小单位 [RM 2 ...]
        AVIWriteWORD(pFile, (AVI_WORD)(pstMakeCfg->dwAudioBitWidth));
        AVIWriteWORD(pFile, 18);
    }

/*
typedef struct                                  //媒体数据整体数据的开始
{
    char        list[4];                        //四个字符"LIST"
    AVI_DWORD   list_size;                      //整个媒体块的大小，字节数  从movi开始计 TBD_WAIT...
    char        movi[4];                        //"movi"表示视频媒体块的开始    
} AVI_MOVIE_LIST_T;
*/
    fwrite("LIST", 1, 4, pFile);
    pstAVICfg->dwMediaSize = 4; // 包含movi ...
    AVIWriteDWORD(pFile, pstAVICfg->dwMediaSize);
    fwrite("movi", 1, 4, pFile);

    pstAVICfg->dwBaseMediaOffset = ftell(pFile);
    MY_PRINTF("AVI media offset is %u (AIM: v + a: 326, v: 224)\n", pstAVICfg->dwBaseMediaOffset);  // v + a 326, v 224
    pstAVICfg->dwMediaOffset = pstAVICfg->dwBaseMediaOffset;

/*
typedef struct  
{
    char        idx[4];                         //必须为idx1
    AVI_DWORD   cb;                             //本数据结构的大小，不包括最初的8字节(fcc和cb两个域)
} AVI_OLD_INDEX_T;
*/
    // rewine index file
    rewind(pIdxFile);
    pstAVICfg->dwIndexSize = 0;
    fwrite("idx1", 1, 4, pIdxFile);
    AVIWriteDWORD(pIdxFile, pstAVICfg->dwIndexSize); // wait upt...

    pstAVICfg->dwAudioCount = 0;
    pstAVICfg->dwVideoCount = 0;    
    return (AVIHANDLE)pstAVICfg;
}

unsigned char AVICheckSize(AVIHANDLE aviHandle, long maxFileSize)
{
    AVIMakeCB_t* pstAVICfg = (AVIMakeCB_t*)aviHandle;
    if (NULL == pstAVICfg)
        return 0;

    if(!pstAVICfg->stAVIMakeCfg.pAVIFile)
        return 1;
    if (ftell(pstAVICfg->stAVIMakeCfg.pAVIFile) >= maxFileSize)
        return 0;
    return 1;
}

unsigned char AVICommit(AVIHANDLE aviHandle, unsigned char* pData, AVIFrameCfg_t* pstFrameCfg)
{
    AVI_DWORD nowpos;
    FILE* pFile = NULL;
    FILE* pIdxFile = NULL;
    AVIMakeCB_t* pstAVICfg = (AVIMakeCB_t*)aviHandle;
    if (NULL == pstAVICfg)
        return 0;
    
    if(!pstFrameCfg || !pData)
        return 0;

    pFile = (FILE*)pstAVICfg->stAVIMakeCfg.pAVIFile;
    pIdxFile = (FILE*)pstAVICfg->stAVIMakeCfg.pAVIIdxFile;
    if(!pFile || !pIdxFile)
        return 0;

/*
typedef struct                                  //表征每一帧的属性
{
    char        chunk_id[4];                    //表征本数据块的四字符码 [RM v 00dc  audio 01wb]
    AVI_DWORD   flags;                          //说明本数据块是不是关键帧，是不是rec列表等信息[(0x10 标示 I 帧)]
    AVI_DWORD   offset;                         //本数据块在文件中的偏移量,可以相对于‘movi’列表，也可以相对于AVI文件开头
    AVI_DWORD   size;                           //本数据块的大小 帧的大小
} AVI_OLD_INDEX_ENTY_T;                       //只是一个数组，为每个媒体数据块（一帧）定义一个索引值
*/

    // write index  16byte per index
    if(!pstFrameCfg->byIsAudio)
    {
        fwrite("00dc", 1, 4, pIdxFile);
        if(pstFrameCfg->byIsKeyFrame)
            AVIWriteDWORD(pIdxFile, 0x00000010);
        else
            AVIWriteDWORD(pIdxFile, 0);
        pstAVICfg->dwVideoCount++;              // videoCount++
    }
    else
    {
        fwrite("01wb", 1, 4, pIdxFile);
        AVIWriteDWORD(pIdxFile, 0);
        pstAVICfg->dwAudioCount++;              // audioCount++
    }
    nowpos = ftell(pFile); // ...
    AVIWriteDWORD(pIdxFile, nowpos); // AVIWriteDWORD(pIdxFile, pstAVICfg->dwMediaOffset);
    //pstAVICfg->dwMediaOffset += (8 + pstFrameCfg->dwFrameLen); // (RM is)TBD_WAIT ...
    AVIWriteDWORD(pIdxFile, pstFrameCfg->dwFrameLen);

    pstAVICfg->dwIndexSize += 16; // ...
    pstAVICfg->dwMediaSize += (8 + pstFrameCfg->dwFrameLen); // ...

    if(!pstFrameCfg->byIsAudio)
        fwrite("00dc", 1, 4, pFile);
    else
        fwrite("01wb", 1, 4, pFile);
    AVIWriteDWORD(pFile, pstFrameCfg->dwFrameLen); // 一帧的大小 ...
    fwrite(pData, 1, pstFrameCfg->dwFrameLen, pFile);// write data ...

    // 填补 TBD_WAIT ...
    if(pstFrameCfg->dwFrameLen & 0x01)
    {
        fputc(0, pFile);
        pstAVICfg->dwMediaSize += 1;
    }

    if (pstAVICfg->dwMediaSize >= AVI_MAX_RIFF_SIZE)
    {
        MY_PRINTF("avi file is full,must finish the file!!!\n");
        return 0;
    }
    
    return 1;
}

// 以pstMakeCfg回去更新avi文件头
unsigned char AVIStop(AVIHANDLE aviHandle, AVIMakeCfg_t* pstMakeCfg)
{
    unsigned char *pMidBuf = NULL;
    AVI_DWORD indexFSize, restlen, oncerlen;
    FILE* pFile;
    FILE* pIdxFile;

    AVIMakeCB_t* pstAVICfg = (AVIMakeCB_t*)aviHandle;
    if (NULL == pstAVICfg)
        return 0;
    
	pstAVICfg->stAVIMakeCfg = (*pstMakeCfg);
    pFile = pstAVICfg->stAVIMakeCfg.pAVIFile;
    pIdxFile = pstAVICfg->stAVIMakeCfg.pAVIIdxFile;
    if (NULL == pFile || NULL == pIdxFile)
        return 0;
    
    pMidBuf = (unsigned char *)malloc(AVI_MID_BUF_SIZE);
    if (NULL == pMidBuf)
    {
        MY_PRINTF("Avi Export Finish: but malloc failed...\n");
        return 0;
    }
    
    indexFSize = ftell(pIdxFile);
    
    MY_PRINTF("avi index file size %u, index len %u, frcnt %u(vcnt %u,acnt %u)\n", 
        indexFSize, pstAVICfg->dwIndexSize, pstAVICfg->dwVideoCount + pstAVICfg->dwAudioCount,
        pstAVICfg->dwVideoCount, pstAVICfg->dwAudioCount);

    if (indexFSize != pstAVICfg->dwIndexSize + 8)
    {
        MY_PRINTF("Avi Export Finish: but index size abnormal...\n");
        free(pMidBuf); // free ...
        free(pstAVICfg);
        return 0;
    }

    // 写入index 的size 
    fseek(pIdxFile, 4, SEEK_SET);
    AVIWriteDWORD(pIdxFile, pstAVICfg->dwIndexSize);
    fflush(pIdxFile); // flush ...
    
    rewind(pIdxFile); // 置到开始
    restlen = indexFSize;
    while (restlen > 0)
    {
        oncerlen = (restlen > AVI_MID_BUF_SIZE) ? AVI_MID_BUF_SIZE : restlen;
        fread(pMidBuf, 1, oncerlen, pIdxFile);
        fwrite(pMidBuf, 1, oncerlen, pFile);  // pInx 是否需要刷一些 TBD_WAIT ...
        restlen -= oncerlen;
    }
    
    pstAVICfg->dwFileSize = ftell(pFile); // ...
    fseek(pFile, 4, SEEK_SET);
    AVIWriteDWORD(pFile, pstAVICfg->dwFileSize - 8); // ...

    fseek(pFile, 48, SEEK_SET); // 在write(AVIF_HASINDEX)后
    AVIWriteDWORD(pFile, pstAVICfg->dwVideoCount + pstAVICfg->dwAudioCount); // ...

    // print
    MY_PRINTF("avi index file size %u, index len %u, frcnt %u(vcnt %u,acnt %u)\n", 
        indexFSize, pstAVICfg->dwIndexSize, pstAVICfg->dwVideoCount + pstAVICfg->dwAudioCount,
        pstAVICfg->dwVideoCount, pstAVICfg->dwAudioCount);

    MY_PRINTF("video count fill pos is %u <aim is 140>\n", pstAVICfg->dwVideoLenFillPos);

    MY_PRINTF("audio count fill pos is %u <aim is 264>, auEnable is %d\n",
        pstAVICfg->dwAudioLenFillPos, pstAVICfg->stAVIMakeCfg.byIsAudioFrame);

    fseek(pFile, pstAVICfg->dwVideoLenFillPos, SEEK_SET); // CHECK ...
    AVIWriteDWORD(pFile, pstAVICfg->dwVideoCount/* + pstAVICfg->dwAudioCount*/); // TBD_WAIT ...
    
    fseek(pFile, pstAVICfg->dwAudioLenFillPos, SEEK_SET); // CHECK ...
    AVIWriteDWORD(pFile, /*pstAVICfg->dwVideoCount + */pstAVICfg->dwAudioCount); // TBD_WAIT ...

    fseek(pFile, pstAVICfg->dwBaseMediaOffset - 8, SEEK_SET); // CHECK ...
    AVIWriteDWORD(pFile, pstAVICfg->dwMediaSize); // TBD_WAIT ...

    fseek(pFile, pstAVICfg->dwRateUpdatePos, SEEK_SET);
    AVIWriteDWORD(pFile, AVI_MS_ASIST_VALUE * 100 / pstMakeCfg->dwRate);

    fseek(pFile, pstAVICfg->dwAVBitrateUpdatePos, SEEK_SET);
    AVIWriteDWORD(pFile, (pstMakeCfg->dwMaxVideoBitrate+pstMakeCfg->dwMaxAudioBitrate) / 8);
    
    fseek(pFile, pstAVICfg->dwWHUpdatePos, SEEK_SET);
    AVIWriteDWORD(pFile, pstMakeCfg->dwWidth); AVIWriteDWORD(pFile, pstMakeCfg->dwHeight);

    fseek(pFile, pstAVICfg->dwRateUpdatePos2, SEEK_SET);
    AVIWriteDWORD(pFile, AVI_MS_ASIST_VALUE * 100 / pstMakeCfg->dwRate); // scale
    
    fseek(pFile, pstAVICfg->wWHUpdatePos, SEEK_SET);
    AVIWriteWORD(pFile, (AVI_WORD)pstMakeCfg->dwWidth); AVIWriteWORD(pFile, (AVI_WORD)pstMakeCfg->dwHeight);
    
    fseek(pFile, pstAVICfg->dwWHUpdatePos2, SEEK_SET);
    AVIWriteDWORD(pFile, pstMakeCfg->dwWidth); AVIWriteDWORD(pFile, pstMakeCfg->dwHeight);

    fseek(pFile, pstAVICfg->dwSamplerateUpdatePos, SEEK_SET);
    AVIWriteDWORD(pFile, AVI_MS_ASIST_VALUE / pstMakeCfg->dwAudioSamplerate); // scale TBD_WAIT ...
    
    fseek(pFile, pstAVICfg->dwSampBitWUpdatePos, SEEK_SET);
    AVIWriteDWORD(pFile, pstMakeCfg->dwAudioSamplerate * pstMakeCfg->dwAudioBitWidth / 8); // RM 用的是平均帧大小 TBD_WAIT...
    
    fseek(pFile, pstAVICfg->dwAudioParamUpdatePos, SEEK_SET);
    AVIWriteWORD(pFile, pstMakeCfg->byAudioTrack);// 指出波形数据的声道数; 单声道为 1, 立体声为 2 (??)
    AVIWriteDWORD(pFile, pstMakeCfg->dwAudioSamplerate); // 指定采样频率(每秒的样本数)  
    AVIWriteDWORD(pFile, pstMakeCfg->dwAudioSamplerate * pstMakeCfg->dwAudioBitWidth / 8);// 指定数据传输的传输速率(每秒的字节数)  
    AVIWriteWORD(pFile, (AVI_WORD)(pstMakeCfg->dwAudioBitWidth / 8));// 指定块对齐块对齐是数据的最小单位 [RM 2 ...]
    AVIWriteWORD(pFile, (AVI_WORD)(pstMakeCfg->dwAudioBitWidth));

    fflush(pFile);
    
    free(pMidBuf); // free ...
    pMidBuf = NULL;

    free(pstAVICfg);
    pstAVICfg = NULL;
    MY_PRINTF("avi file finish!!!\n");
    return 1;
}
