//
//  CodeLock.h
//  jltClient
//
//  Created by sz.fullhan on 14-8-20.
//  Copyright (c) 2014年 jlt. All rights reserved.
//

#ifndef jltClient_CodeLock_h
#define jltClient_CodeLock_h

#ifdef __cplusplus
extern "C" {
#endif
    
#include <pthread.h>
#define CODELOCK        pthread_mutex_t

void CreateCodeLock(CODELOCK* pLock);

void DestoryCodeLock(CODELOCK* pLock);

void CodeLock(CODELOCK* pLock);

void CodeUnlock(CODELOCK* pLock);

#ifdef __cplusplus
}
#endif

#endif
