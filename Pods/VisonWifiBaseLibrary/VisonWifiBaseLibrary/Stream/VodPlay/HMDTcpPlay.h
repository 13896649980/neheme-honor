//
//  Udp720PTcpPlay.h
//  720P_TARGET
//
//  Created by mac on 17/3/16.
//  Copyright © 2017年 huang mindong. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "Includes.h"
#import "AsyncSocket.h"


#ifdef __cplusplus
extern "C" {
#endif
    
    typedef void (*fDataCallBack)(void* handle, VFrameHead_t* head, char* buf, U32 bufLen, void* user);
    void* StartHMDTcpPlay(char* url,unsigned int port, DeviceType_e dtype, fDataCallBack fun, void* user);
    void StopHMDTcpPlay(void* handle);
    
    AsyncSocket *createHMDTcpSocket(void *pUser,const char *host,int port);
    
#ifdef __cplusplus
}
#endif

@interface HMDTcpPlay : NSObject<AsyncSocketDelegate>
{
@public
    void *handle;
    
    
    NSTimer *timer;
    NSTimer *reconnectTimer;
   
    int disconnetCount;
}
- (void)startHMDTcpPlay;

@property(atomic,strong)AsyncSocket *socket1;
@property(atomic,assign) BOOL isConnetting;
@end
