//
//  TcpPlay.h
//  720P_TARGET
//
//  Created by huang mindong on 16/7/31.
//  Copyright © 2016年 huang mindong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AsyncSocket.h"
#include "Includes.h"
#ifdef __cplusplus
extern "C" {
#endif
    
    typedef void (*fDataCallBack)(void* handle, VFrameHead_t* head, char* buf, U32 bufLen, void* user);
    void* StartTcpPlay(char* url, DeviceType_e dtype, fDataCallBack fun, void* user);
    void StopTcpPlay(void* handle);
    
    AsyncSocket *createAsyncTcpSocket(void *pUser,const char *host,int port);
    
#ifdef __cplusplus
}
#endif

@interface TcpPlay: NSObject<AsyncSocketDelegate>
{
    @public
    void *handle;
    AsyncSocket *socket1;
    AsyncSocket *socket2;
    AsyncSocket *socket3;
    AsyncSocket *socket4;
    
    NSTimer *timer;
    int isDisconnet;
    int disconnetCount;
}
- (void)startTCPPlay;
- (void)setVodPlayHandle_t:(void *)handle;

@end
