//
//  AsyncTcpPlay.c
//  VS FPV_as
//
//  Created by huangmindong on 2018/9/14.
//  Copyright © 2018年 huang mindong. All rights reserved.
//

#include "AsyncTcpPlay.h"
#include "BLoopBufData.h"
#include "ThreadPlatform.h"
#include "NetProtocol.h"
#include <sys/time.h>


#ifdef WIN32
#   define PSOCKET_LEN(x)   ((int*)(x))
#else
#   define PSOCKET_LEN(x)   ((socklen_t*)(x))
#endif


int CreateTCPSocket(char* ip, U16 port)
{
    int val = 1;
    int sockfd = 0;
    struct sockaddr_in addr;
    unsigned long opt;
    
    
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sockfd < 0)
        return -1;
    
    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (char*)&val, sizeof(val));
    
    memset((void *)&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = inet_addr(ip);
    
    int ret = connect(sockfd,(struct sockaddr *)&addr, sizeof(struct sockaddr_in));
    if (ret < 0) {
        closesocket(sockfd);
        return -1;
    }
    
    opt = 1;
    
    if ((val = fcntl(sockfd, F_GETFL, 0)) < 0 || fcntl(sockfd, F_SETFL, val|O_NONBLOCK) < 0)
    {
        closesocket(sockfd);
        return -1;
    }
    
    return sockfd;
}

int TCPSocketRecv(int sockfd, char* pData, int iDataLen)
{
    struct sockaddr_in inaddr;
    int len;
    long recvbytes = 0;
    long totalbytes = 0;
    
    if(sockfd <= 0 || !pData || iDataLen <= 0)
        return 0;
    
    len = sizeof(inaddr);
    
    while(totalbytes < iDataLen)
    {
        recvbytes = recv(sockfd, (char*)(pData + totalbytes), iDataLen - totalbytes, 0);
        if (recvbytes <= 0)
            break;
        totalbytes += recvbytes;
    }

    return (int)totalbytes;
}

typedef struct
{
    char            ip[32];
    U16             port;
    U8              protocol;
    DeviceType_e    dtype;
    int             sockfd;
    fDataCallBack   fun;
    void*           user;
    BLHANDLE        loopBufDataHandle;
    
    U8              sppThreadRunning;
    THREADID        sppThreadID;
    U8              srThreadRunning;
    THREADID        srThreadID;
}VodPlayHandle_t;


static U8 _ParseFrameType(FHNP_Dev_FrameHead_t* pHead)
{
    if (   pHead->FrmHd[0] != 0x00
        || pHead->FrmHd[1] != 0x00
        || pHead->FrmHd[2] != 0x01
        || (pHead->FrmHd[3] != 0xa0 && pHead->FrmHd[3] != 0xa1 && pHead->FrmHd[3] != 0xa2
            && pHead->FrmHd[3] != 0xa3 && pHead->FrmHd[3] != 0xa4 && pHead->FrmHd[3] != 0xa5)
        )
    {
        return 255;
    }
    
    if (pHead->FrmHd[3] == 0xa5)
        return 4;
    else if (pHead->FrmHd[3] == 0xa4)
        return 3;
    else if (pHead->FrmHd[3] == 0xa1)
        return 0;
    else if (pHead->FrmHd[3] == 0xa2)
        return 2;
    
    return 1;
}

static U8 _StreamDataProc(VodPlayHandle_t* pNode, FHNP_Dev_FrameHead_t* pHead, char* pFrame)
{
    U8 btFrameType = 0;
    VFrameHead_t stVFrameHead;
    
    memset(&stVFrameHead, 0, sizeof(VFrameHead_t));
    btFrameType = _ParseFrameType(pHead);
    if (0 != btFrameType && 1 != btFrameType && 2 != btFrameType)
        return 0;
    
    stVFrameHead.frameType = btFrameType;
    stVFrameHead.videoFormat = 0;
    stVFrameHead.restartFlag = pHead->restart_flag;
    stVFrameHead.width = (unsigned short)(pHead->alarmstatus >> 16);
    stVFrameHead.height = (unsigned short)(pHead->alarmstatus);
    stVFrameHead.timeStamp = pHead->timestamp;
    stVFrameHead.res2[0] = pHead->reserve[3];
    
    if (pNode->fun)
        (*pNode->fun)(pNode, &stVFrameHead, pFrame, pHead->framelen, pNode->user);
    
    return 1;
}

static void _StreamPreProcThreadBody(void* lpUser)
{
    U8 bGetFrameRet = 0;
    FHNP_Dev_FrameHead_t stHead;
    char* pFrame = 0;
    
    VodPlayHandle_t* handle = (VodPlayHandle_t*)lpUser;
    if (!handle)
        return;
    
    if (!(pFrame = (char*)malloc(0x80000))) // 512KB
    {
        handle->sppThreadID = 0;
        return;
    }
    
    while (handle->sppThreadRunning)
    {
        bGetFrameRet = 0;
        while (1)
        {
            if (!BLBDATA_GetOneFrame(handle->loopBufDataHandle, (char*)&stHead, pFrame, 0))
                break;
            _StreamDataProc(handle, &stHead, pFrame);
            bGetFrameRet = 1;
        }
        
        if (bGetFrameRet)
        {
            SLEEPMS(0);
        }
        else
        {
            SLEEPMS(1);
        }
    }
    
    handle->sppThreadID = 0;
    free(pFrame);
}

static void _StreamSendThreadBody(void* lpUser)
{
    VodPlayHandle_t* handle = (VodPlayHandle_t*)lpUser;
    if (!handle)
        return;
    
    char command [12];
    bzero(command,12);
    command[0] = 0x00; //发送Ip地址
    command[1] = 0x01;
    command[2] = 0x02;
    command[3] = 0x03;
    command[4] = 0x04;
    command[5] = 0x05;
    command[6] = 0x06;
    command[7] = 0x07;
    command[8] = 0x08;
    command[9] = 0x09;
    command[10] = 0x28;
    command[11] = 0x28;
    
    while (handle->srThreadRunning)
    {
       
        send(handle->sockfd, command, 12, 0);
        SLEEPMS(1000);
    }
}

static void _StreamRecvThreadBody(void* lpUser)
{
    int iRet = 0;
    fd_set fd_set_read;
    fd_set fd_set_error;
    struct timeval tv;
    int iRecvLen = 0;
    char* pLoopBuf1 = NULL; int iLoopBufLen1 = 0;
    char* pLoopBuf2 = NULL; int iLoopBufLen2 = 0;
    
    char* pUdpBuf = 0;
    int iUdpBufLen = 0;
    
    VodPlayHandle_t* handle = (VodPlayHandle_t*)lpUser;
    if (!handle)
        return;
    
    while (handle->srThreadRunning)
    {
        FD_ZERO(&fd_set_read);
        FD_ZERO(&fd_set_error);
        
        FD_SET((unsigned int)handle->sockfd, &fd_set_read);
        FD_SET((unsigned int)handle->sockfd, &fd_set_error);
        
        tv.tv_sec = 0;
        tv.tv_usec = 500000;
        iRet = select(handle->sockfd+1, &fd_set_read, NULL, &fd_set_error, &tv);
        if (-1 == iRet)
        {
            SLEEPMS(10);
            continue;
        }
        else if (0 == iRet)
        {
            continue;
        }
        
        //////////////////////////////////////////////////////////////////////////
        
        if (FD_ISSET(handle->sockfd, &fd_set_read))
        {
            if (!pUdpBuf)
            {
                iUdpBufLen = 1024*1024;
                pUdpBuf = malloc(iUdpBufLen);
            }
            
            if (NULL == pUdpBuf)
                break;
            iRecvLen = TCPSocketRecv(handle->sockfd, pUdpBuf, iUdpBufLen);
            if (iRecvLen > 0)
            {
                BLBDATA_Lock(handle->loopBufDataHandle);   // lock
                if (BLBDATA_AdvGetWritePtr(handle->loopBufDataHandle, &pLoopBuf1, (unsigned int*)&iLoopBufLen1, &pLoopBuf2, (unsigned int*)&iLoopBufLen2))
                {
                    if (iLoopBufLen1 >= iRecvLen)
                    {
                        memcpy(pLoopBuf1, pUdpBuf, iRecvLen);
                    }
                    else
                    {
                        memcpy(pLoopBuf1, pUdpBuf, iLoopBufLen1);
                        memcpy(pLoopBuf2, pUdpBuf+iLoopBufLen1, iRecvLen-iLoopBufLen1);
                    }
                    BLBDATA_AdvSetWritePos(handle->loopBufDataHandle, iRecvLen);
                }
                BLBDATA_Unlock(handle->loopBufDataHandle); // unlock
            }
        }
    }
    
    if (pUdpBuf)
        free(pUdpBuf);
    
    handle->srThreadID = 0;
}

void* StartTCPPlay(char* ip, U16 port, char* bindIP, DeviceType_e dtype, fDataCallBack fun, void* user)
{
    VodPlayHandle_t* handle = 0;
    handle = (VodPlayHandle_t*)malloc(sizeof(VodPlayHandle_t));
    if (!handle)
    {
        return 0;
    }
    
    memset(handle, 0, sizeof(VodPlayHandle_t));
    if (ip && strlen(ip) > 0)
        strcpy(handle->ip, ip);
    handle->port = port;
    handle->dtype = dtype;
    handle->fun = fun;
    handle->user = user;
    
    handle->sockfd = CreateTCPSocket(ip, port);
    
    if (handle->sockfd < 0)
    {
        free(handle);
        return 0;
    }
    
    if (EN_DTYPE_FH8610 == dtype)
        handle->loopBufDataHandle = BLBDATA_Create(BLBDATA_TYPE_61_FRAME, 1024*500);
    else if (EN_DTYPE_FH8620 == dtype)
        handle->loopBufDataHandle = BLBDATA_Create(BLBDATA_TYPE_62_FRAME, 1024*1024*10);
    else if (EN_DTYPE_FH8810 == dtype)
        handle->loopBufDataHandle = BLBDATA_Create(BLBDATA_TYPE_81_FRAME, 1024*1024*10);
    
    if (0 == handle->loopBufDataHandle)
    {
        closesocket(handle->sockfd);
        free(handle);
        return 0;
    }
    
    handle->sppThreadRunning = 1;
    if (!StartThread(_StreamPreProcThreadBody, handle, &handle->sppThreadID))
    {
        handle->sppThreadRunning = 0;
        closesocket(handle->sockfd);
        BLBDATA_Destory(handle->loopBufDataHandle);
        free(handle);
        return 0;
    }
    
    handle->srThreadRunning = 1;
    if (!StartThread(_StreamRecvThreadBody, handle, &handle->srThreadID))
    {
        handle->srThreadRunning = 0;
        closesocket(handle->sockfd);
        BLBDATA_Destory(handle->loopBufDataHandle);
        free(handle);
        return 0;
    }
    
    if (!StartThread(_StreamSendThreadBody, handle, &handle->srThreadID))
    {
//        handle->srThreadRunning = 0;
//        closesocket(handle->sockfd);
//        BLBDATA_Destory(handle->loopBufDataHandle);
        free(handle);
        return 0;
    }
    return handle;
}

void StopTCPPlay(void* handle)
{
    U32 dwCount = 0;
    VodPlayHandle_t* playHandle = (VodPlayHandle_t*)handle;
    
    if (!handle)
        return;
    
    dwCount = 0;
    playHandle->srThreadRunning = 0;
    while (playHandle->srThreadID && dwCount < 300)
    {
        dwCount++;
        SLEEPMS(10);
    }
    
    dwCount = 0;
    playHandle->sppThreadRunning = 0;
    while (playHandle->sppThreadID && dwCount < 300)
    {
        dwCount++;
        SLEEPMS(10);
    }
    
    closesocket(playHandle->sockfd);
    BLBDATA_Destory(playHandle->loopBufDataHandle);
    free(playHandle);
}


