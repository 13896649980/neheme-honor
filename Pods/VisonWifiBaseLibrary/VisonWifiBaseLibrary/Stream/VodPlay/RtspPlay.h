#ifndef _VOD_PLAY_H_
#define _VOD_PLAY_H_


#include "Includes.h"


#ifdef __cplusplus
extern "C" {
#endif


typedef void (*fDataCallBack)(void* handle, VFrameHead_t* head, char* buf, U32 bufLen, void* user);

    // recType: 2-avi, 1-h264
unsigned char StartRtspRecord(void* handle, unsigned char recType, const char* recPath);
unsigned char StopRtspRecord(void* handle);

#ifdef __cplusplus
}
#endif

#endif
