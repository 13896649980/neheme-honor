//
//  StreamPreplayConfig.m
//  GPS Project
//
//  Created by Xu pan on 2020/8/11.
//  Copyright © 2020 Xu pan. All rights reserved.
//

#import "StreamPreplayConfig.h"
#import "VisonWifiBaseLibraryDefine.h"

#define Stream_Port 8888

@implementation StreamPreplayConfig

static StreamPreplayConfig *_mamager = nil;
+(instancetype)shareInstance{
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _mamager = [[self alloc]init];
    });
    return _mamager;
}

-(instancetype)init{
    if (self = [super init]) {
        _streamPort = Stream_Port;
    }
    return self;
}

#pragma mark - 接收流数据前的准备工作(区分播放方式，播放地址)
-(void)streamPreplayConfigWithWIFIDeviceModel:(WIFIDeviceModel *)wifiDeviceModel{
    
    switch (wifiDeviceModel.playType) {
        case PlayType_TCP:
        {
            _streamIP = @"172.16.10.1";
        }
            break;
        case PlayType_UDP:
        {
            _streamIP = [USER_DEFAULT objectForKey:VGS_WIFI_IP];
        }
            break;
        case PlayType_RTSP:
        {
            _streamPath = [NSString stringWithFormat:@"rtsp://%@/0",[USER_DEFAULT objectForKey:HOST_IP]];
        }
            break;
            
        default:
            break;
    }
    
    switch (wifiDeviceModel.mainLensTransmissionPixelSize) {
        case PixelSize_320_240:
        case PixelSize_640_360:
        case PixelSize_640_480:
        case PixelSize_1280_720:
        case PixelSize_1024_576:
        {
            _devidetype = EN_DTYPE_720P;
        }
            break;
        case PixelSize_1920_1080:
        case PixelSize_1632_928:
        {
            _devidetype = EN_DTYPE_1080P;
        }
            break;
        case PixelSize_2048_1152:
        case PixelSize_2592_1520:
        case PixelSize_2976_1680:
        case PixelSize_3840_2160:
        case PixelSize_4000_3000:
        {
            _devidetype = EN_DTYPE_2K;
        }
            break;
        default:
        {
            _devidetype = EN_DTYPE_1080P;
        }
            break;
    }
    
    NSLog(@"配置播放参数streamIP:%@ streamPort:%d streamPath:%@ deviceType:%d",_streamIP,_streamPort,_streamPath,_devidetype);
}

#pragma mark - 清空数据
-(void)clearData{
    NSLog(@"StreamPreplayConfig - 清空数据");
    _streamIP = @"";
    _streamPath = @"";
    _devidetype = 0;
}

@end
