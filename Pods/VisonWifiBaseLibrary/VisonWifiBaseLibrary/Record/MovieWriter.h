//
//  MovieWriter.h
//  录像类，可以录制 pixelBuffer（YUV/RGB） 类型 并能录制音频 
//  Version:1.0
//
//  Created by Xu pan on 2020/9/8.
//  Copyright © 2020 Xu pan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

typedef NS_ENUM(NSInteger,Source_PixelBuff_Type){
    Source_PixelBuff_Type_YUV,
    Source_PixelBuff_Type_RGB,
};

NS_ASSUME_NONNULL_BEGIN

@interface MovieWriter : NSObject

@property   (nonatomic,assign,readonly)             BOOL                            isWriting;
@property   (nonatomic,assign)                      CVPixelBufferRef                writingPixelBuff;


/// 开始写录像文件（录屏）
/// @param pixelBuff_Type 类型
/// @param filePath 写入的沙河地址
/// @param frameRate 帧率
/// @param frameSize 尺寸
/// @param audioEnable 是否录制声音
/// @param complete 准备完成
-(void)starWriteMovieWithType:(Source_PixelBuff_Type)pixelBuff_Type
                     FilePath:(NSString *)filePath
                    frameRate:(int)frameRate
                    frameSize:(CGSize)frameSize
                  audioEnable:(BOOL)audioEnable
                     complete:(void(^)(void))complete;

/// 结束录制（录屏）
/// @param complete 回调
-(void)stopWriteMovieComplete:(nonnull void (^)(NSString *moviePath))complete;

NS_ASSUME_NONNULL_END

@end
