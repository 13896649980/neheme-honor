//
//  MovieWriter.m
//  GPS Project
//
//  Created by Xu pan on 2020/9/8.
//  Copyright © 2020 Xu pan. All rights reserved.
//

#import "MovieWriter.h"
#import <sys/time.h>

@interface MovieWriter()<AVCaptureAudioDataOutputSampleBufferDelegate>{
    CMTime      firstFrameTime;
    unsigned  long long starWriteTime;
    dispatch_source_t   writeFrameTimer;
    
    //录音
    AVCaptureDeviceInput *audioInput;
    AVCaptureAudioDataOutput *audioOutput;
    AVCaptureDevice *microphone;
    dispatch_queue_t audioProcessingQueue;
    AVCaptureSession *captureSession;
    dispatch_queue_t writeFrameQueue;
    
    BOOL    isWritingFrame;
}

@property   (nonatomic,assign)          Source_PixelBuff_Type                       pixelBuff_Type;
@property   (nonatomic,strong)          NSString                                    *moviePath;
@property   (nonatomic,assign)          int                                         frameRate;
@property   (nonatomic,assign)          CGSize                                      frameSize;
@property   (nonatomic,assign)          long                                        writeFrameCount;

@property   (nonatomic,strong)          AVAssetWriter                               *videoWriter;
@property   (nonatomic,strong)          AVAssetWriterInput                          *videoWriterInput;
@property   (nonatomic,strong)          AVAssetWriterInput                          *audioWriterInput;
@property   (nonatomic,strong)          AVAssetWriterInputPixelBufferAdaptor        *adaptor;
@property   (nonatomic,assign)          BOOL                                        audioEnable;

@end

@implementation MovieWriter

-(unsigned long long)getMS{
    struct timeval l_tv;
    gettimeofday(&l_tv,NULL);
    unsigned long long l_ret = 0;
    l_ret = (l_tv.tv_sec&0xFFFFFFFF)*1000;
    l_ret += (l_tv.tv_usec/1000);
    return l_ret;
}

-(void)dealloc{
    NSLog(@"dealloc %@",self);
}

-(void)setWritingPixelBuff:(CVPixelBufferRef)writingPixelBuff{
    
    if (!self->writeFrameQueue) {
        self->writeFrameQueue = dispatch_queue_create("WriteFrameQueue", DISPATCH_QUEUE_SERIAL);
    }

    dispatch_async(self->writeFrameQueue, ^{
        if (self.isWriting) {
            
//            if (!self->isWritingFrame) {
                if (self.writingPixelBuff) {
                    CVPixelBufferRelease(self.writingPixelBuff);
                }
                
                switch (self.pixelBuff_Type) {
                    case Source_PixelBuff_Type_RGB:
                    {
                        self->_writingPixelBuff = writingPixelBuff;
                    }
                        break;
                    case Source_PixelBuff_Type_YUV:
                    {
                        self->_writingPixelBuff = writingPixelBuff;
                    }
                        break;
                    default:
                        break;
                }
//            }
        }else{
            CVPixelBufferRelease(writingPixelBuff);
        }
    });
}


#pragma mark - 开始写文件
-(void)starWriteMovieWithType:(Source_PixelBuff_Type)pixelBuff_Type
                     FilePath:(NSString *)filePath
                    frameRate:(int)frameRate
                    frameSize:(CGSize)frameSize
                  audioEnable:(BOOL)audioEnable
                     complete:(void (^)(void))complete{
    if (self.isWriting) {
        NSLog(@"开始写录像文件出错: 正在录像中，无法开始！！！");
        return;
    }

    self.pixelBuff_Type = pixelBuff_Type;
    self.writeFrameCount = 0;
    self.moviePath = filePath;
    self.frameRate = frameRate;
    self.frameSize = frameSize;
    self.audioEnable = audioEnable;
    self->firstFrameTime = kCMTimeInvalid;

#pragma mark - 设置movieWriter属性
    NSError *error;
    // Configure videoWriter
    NSURL *fileUrl = [NSURL fileURLWithPath:filePath];
    self.videoWriter = [[AVAssetWriter alloc] initWithURL:fileUrl fileType:AVFileTypeMPEG4 error:&error];
    NSParameterAssert(self.videoWriter);
    
    //设置比特率
    NSDictionary *videoCompressionProps = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithDouble:frameSize.width * frameSize.height * 3], AVVideoAverageBitRateKey,nil];
    //参数设置
    if (@available(iOS 11.0, *)) {
        NSDictionary *videoSettings = @{AVVideoCodecKey: AVVideoCodecTypeH264,
                                        AVVideoWidthKey: @(frameSize.width),
                                        AVVideoHeightKey: @(frameSize.height),
                                        AVVideoCompressionPropertiesKey:videoCompressionProps
        };
        self.videoWriterInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeVideo outputSettings:videoSettings];
    } else {
        // Fallback on earlier versions
        NSDictionary *videoSettings = @{AVVideoCodecKey: AVVideoCodecH264,
                                        AVVideoWidthKey: @(frameSize.width),
                                        AVVideoHeightKey: @(frameSize.height),
                                        AVVideoCompressionPropertiesKey:videoCompressionProps
        };
        self.videoWriterInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeVideo outputSettings:videoSettings];
    }
    
    
    NSParameterAssert(self.videoWriterInput);
    self.videoWriterInput.expectsMediaDataInRealTime = YES;
    NSDictionary *bufferAttributes = [NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithInt:kCVPixelFormatType_32ARGB], kCVPixelBufferPixelFormatTypeKey, nil];
    self.adaptor = [AVAssetWriterInputPixelBufferAdaptor assetWriterInputPixelBufferAdaptorWithAssetWriterInput:self.videoWriterInput sourcePixelBufferAttributes:bufferAttributes];
    // add input
    [self.videoWriter addInput:self.videoWriterInput];
    
    if (self.audioEnable) {
        //设置录音
        audioProcessingQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW,0);
        captureSession = [[AVCaptureSession alloc] init];
        [captureSession beginConfiguration];
        //添加麦克风设备
        microphone = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeAudio];
        //音频输入
        audioInput = [AVCaptureDeviceInput deviceInputWithDevice:microphone error:nil];
        if ([captureSession canAddInput:audioInput]){
            [captureSession addInput:audioInput];
        }
        //音频输出
        audioOutput = [[AVCaptureAudioDataOutput alloc] init];
        if ([captureSession canAddOutput:audioOutput]){
            [captureSession addOutput:audioOutput];
        }else{
            NSLog(@"Couldn't add audio output");
        }
        [audioOutput setSampleBufferDelegate:self queue:audioProcessingQueue];
        [captureSession commitConfiguration];
        if (![captureSession isRunning]){
            [captureSession startRunning];
        };
        
        //设置写入参数
        AVAudioSession *sharedAudioSession = [AVAudioSession sharedInstance];
        double preferredHardwareSampleRate;
        
        if ([sharedAudioSession respondsToSelector:@selector(sampleRate)])
        {
            preferredHardwareSampleRate = [sharedAudioSession sampleRate];
        }
        else
        {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
            preferredHardwareSampleRate = [[AVAudioSession sharedInstance] currentHardwareSampleRate];
#pragma clang diagnostic pop
        }
        
        AudioChannelLayout acl;
        bzero( &acl, sizeof(acl));
        acl.mChannelLayoutTag = kAudioChannelLayoutTag_Mono;
        
        NSDictionary *audioOutputSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                             [ NSNumber numberWithInt: kAudioFormatMPEG4AAC], AVFormatIDKey,
                                             [ NSNumber numberWithInt: 1 ], AVNumberOfChannelsKey,
                                             [ NSNumber numberWithFloat: preferredHardwareSampleRate], AVSampleRateKey,
                                             [ NSData dataWithBytes: &acl length: sizeof( acl ) ], AVChannelLayoutKey,
                                             //[ NSNumber numberWithInt:AVAudioQualityLow], AVEncoderAudioQualityKey,
                                             [ NSNumber numberWithInt: 64000 ], AVEncoderBitRateKey,
                                             nil];
        
        self.audioWriterInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeAudio outputSettings:audioOutputSettings];
        self.audioWriterInput.expectsMediaDataInRealTime = NO;
        [self.videoWriter addInput:self.audioWriterInput];
    }
    
    [self.videoWriter startWriting];
    [self.videoWriter startSessionAtSourceTime:kCMTimeZero];
    
    writeFrameTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_global_queue(0, 0));
    dispatch_source_set_timer(writeFrameTimer, DISPATCH_TIME_NOW, 1.0/self.frameRate * NSEC_PER_SEC, 0 * NSEC_PER_SEC);
    dispatch_source_set_event_handler(writeFrameTimer, ^{
        
        dispatch_async(self->writeFrameQueue, ^{
//            self->isWritingFrame = YES;
            if (!self.writingPixelBuff) {
//                self->isWritingFrame = NO;
                return ;
            }
            [self appendPixelBuffer:self.writingPixelBuff];
//            self->isWritingFrame = NO;
        });
    });
    dispatch_resume(writeFrameTimer);
    
    if (complete) {
        
        complete();
    }
    _isWriting = YES;
    NSLog(@"开始写录像文件");
}


#pragma mark - 停止写文件
-(void)stopWriteMovieComplete:(void (^)(NSString *))complete{
    
    NSLog(@"停止写录像文件");
    _isWriting = NO;
//    self->isWritingFrame = NO;
    dispatch_source_cancel(writeFrameTimer);
    writeFrameTimer = nil;
    
    
    if (captureSession && [captureSession isRunning]) {
        
        [self.audioWriterInput markAsFinished];
        [captureSession stopRunning];
        
        [audioOutput setSampleBufferDelegate:nil queue:dispatch_get_main_queue()];
        [captureSession beginConfiguration];
        [captureSession removeInput:audioInput];
        [captureSession removeOutput:audioOutput];
        audioInput = nil;
        audioOutput = nil;
        microphone = nil;
        audioProcessingQueue = 0;
        [captureSession commitConfiguration];
        captureSession = nil;
        self.audioWriterInput = nil;
    }
    
    [self.videoWriterInput markAsFinished];
    [self.videoWriter finishWritingWithCompletionHandler:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if (complete) {
                complete(self.moviePath);
            }
            self.moviePath = @"";
        });
        
        self.adaptor = nil;
        self.videoWriterInput = nil;
        self.videoWriter = nil;
    }];
    
    self.frameRate = 0;
    self.frameSize = CGSizeZero;
    self.writeFrameCount = 0;
}


#pragma mark - 写录像文件
-(void)appendPixelBuffer:(CVPixelBufferRef)pixelbuffer{
    
    if (!pixelbuffer) {
        NSLog(@"写入的源文件为空");
        return;
    }
    
    if (!self.isWriting) {
//        NSLog(@"录像还未开始");
        return;
    }
    
    CMTime frameTime = CMTimeMake(self.writeFrameCount, (int32_t)self.frameRate);
    
    if (CMTIME_IS_INVALID(firstFrameTime)) {
        firstFrameTime = frameTime;
        starWriteTime = [self getMS];
    }
    
    if (!self.videoWriterInput.readyForMoreMediaData) {
        NSLog(@"写入影片videoWriterInput未准备好");
    }else{
        if (self.adaptor.assetWriterInput.readyForMoreMediaData) {
            if(![self.adaptor appendPixelBuffer:pixelbuffer withPresentationTime:frameTime]){
                NSError *error = self.videoWriter.error;
                if(error) {
                    NSLog(@"写入影片文件错误 %@,%@.", error, [error userInfo]);
                }
            }else{
                self.writeFrameCount++;
#ifdef DEBUG
//                NSLog(@"影片写入中 %ld/%d",self.writeFrameCount,self.frameRate);
#endif
            }
        }else{
            NSLog(@"写入影片adaptor 缓存区未准备好");
        }
    }
}


#pragma mark - AVCaptureAudioDataOutputSampleBufferDelegate
- (void)captureOutput:(AVCaptureOutput *)captureOutput
didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer
       fromConnection:(AVCaptureConnection *)connection{
    if (!captureSession.isRunning){
        return;
    }else if (captureOutput == audioOutput){
        //先录制视频
        if (CMTIME_IS_INVALID(firstFrameTime)) {
            return;
        }
        
        @synchronized(self){
            [self appendAudioSampleBuffer:sampleBuffer];
        }
    }
}

-(void)appendAudioSampleBuffer:(CMSampleBufferRef)sampleBuffer{
    //给音频重新写pts
    CFRetain(sampleBuffer);
    long time = (long)([self getMS] - starWriteTime);
    CMTime newTimeStamp = CMTimeMakeWithSeconds(time/1000.0, 44100);
    CMItemCount count;
    CMSampleBufferGetSampleTimingInfoArray(sampleBuffer, 0, nil, &count);
    CMSampleTimingInfo* pInfo = malloc(sizeof(CMSampleTimingInfo) * count);
    CMSampleBufferGetSampleTimingInfoArray(sampleBuffer, count, pInfo, &count);
    for (CMItemCount i = 0; i < count; i++){
        pInfo[i].decodeTimeStamp = newTimeStamp;
        pInfo[i].presentationTimeStamp = newTimeStamp;
    }
    CMSampleBufferRef sout;
    CMSampleBufferCreateCopyWithNewTiming(kCFAllocatorDefault, sampleBuffer, count, pInfo, &sout);
    free(pInfo);
    CFRelease(sampleBuffer);
    //音频写入
    if([self.audioWriterInput isReadyForMoreMediaData]){
        if (sout && self.videoWriter.status == AVAssetWriterStatusWriting){
            if([self.audioWriterInput appendSampleBuffer:sout]){
                
            }else{
                printf("failed to append Audio buffer\n");
            }
            CFRelease(sout);
        }
    }
}




@end
