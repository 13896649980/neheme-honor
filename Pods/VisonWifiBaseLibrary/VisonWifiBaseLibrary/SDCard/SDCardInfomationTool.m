//
//  SDCardInfomationTool.m
//  VISON_GPS
//
//  Created by Xu pan on 2019/10/22.
//  Copyright © 2019 huang mindong. All rights reserved.
//

#import "SDCardInfomationTool.h"
#import "SocketControlManager.h"


@interface SDCardInfomationTool(){
    int     formattCount;
}

@property   (nonatomic,strong)      NSTimer                     *getSDCardfInfoTimer;

@end

@implementation SDCardInfomationTool

static SDCardInfomationTool *tool = nil;
static dispatch_once_t onceToken;

+(instancetype)shareInstance{
    
    dispatch_once(&onceToken, ^{
        tool = [[self alloc]init];
    });
    return tool;
}

-(instancetype)init{
    if (self = [super init]) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotificationData:) name:SDCARD_NOTI_INFO object:nil];
    }
    return self;
}

#pragma mark - 获取当前sd卡状态
-(void)getSdcardCurrentInfomationComplete:(TFCardInfoBlock)complete{
    self.formatResultBlock = nil;
    self.infoBlock = complete;
    unsigned char cardData[5] = {0};
    cardData[0] = 0xff;
    cardData[1] = 0x53;
    cardData[2] = 0x54;
    cardData[3] = 0x00;
    cardData[4] = 0x01;
    
    NSData *data = [NSData dataWithBytes:cardData length:5];
    [[SocketControlManager shareInstance] sendUdpCommand:data tag:9090];
}

#pragma mark - 格式化sd卡
-(void)formatSDCardActionComplete:(TFCardFormatResultBlock)complete{
    self.infoBlock = nil;
    self.formatResultBlock = complete;
    formattCount += 1;
    
    unsigned char cardData[5] = {0};
    cardData[0] = 0xff;
    cardData[1] = 0x53;
    cardData[2] = 0x54;
    cardData[3] = 0x00;
    cardData[4] = 0x02;
    
    NSData *data = [NSData dataWithBytes:cardData length:5];
    [[SocketControlManager shareInstance] sendUdpCommand:data tag:9090];
}

-(void)formatSDCardAction{
    formattCount += 1;
    
    unsigned char cardData[5] = {0};
    cardData[0] = 0xff;
    cardData[1] = 0x53;
    cardData[2] = 0x54;
    cardData[3] = 0x00;
    cardData[4] = 0x02;
    
    NSData *data = [NSData dataWithBytes:cardData length:5];
    [[SocketControlManager shareInstance] sendUdpCommand:data tag:9090];
}

#pragma mark - 接收SD卡信息通知
-(void)receiveNotificationData:(NSNotification *)noti{
    
    NSData *data = noti.object;
    Byte *bytes = (Byte *)data.bytes;

    NSDictionary *dict;
    /*
     {
        @"existTFCard"          : value     (1:检测到TF卡,      0:未检测到TF卡)
        @"needFormat"           : value     (1:卡需要格式化,     0:卡不需要格式化)
        @"tfCardTotalSpec"      : value     卡最大容量 nsvalue @(float)
        @"tfCardRemainSpace"    : value     卡剩余容量 nsvalue @(float)
        @"needRePlug"           : value     (1:TF卡需要重新插拔    0:TF卡不需要重新插拔)
        @"Formatting"           : value     (1:TF卡正在格式化     0:TF卡未正在格式化)
        @"FormatResult"         : value     (1:格式化成功        2:格式化失败)
     }
     **/
    
    if (bytes[0] == 0xff && bytes[1] == 0x53 && bytes[2] == 0x54 && bytes[3] == 0x00 && bytes[4] == 0x01 && bytes[5] == 0x00) { //没有SD卡
        
        dict =      @{
            @"existTFCard"          : @(0),
            @"needFormat"           : @(0),
            @"tfCardTotalSpec"      : @(0),
            @"tfCardRemainSpace"    : @(0),
            @"needRePlug"           : @(0),
            @"formatting"           : @(0),
            @"formatResult"         : @(0)
        };
        
        
    }else if (bytes[0] == 0xff && bytes[1] == 0x53 && bytes[2] == 0x54 && bytes[3] == 0x00 && bytes[4] == 0x01 && (bytes[5] == 0x01 || bytes[5] == 0x02)) { //sd卡需要格式化

        dict =      @{
            @"existTFCard"          : @(1),
            @"needFormat"           : @(1),
            @"tfCardTotalSpec"      : @(0),
            @"tfCardRemainSpace"    : @(0),
            @"needRePlug"           : @(0),
            @"formatting"           : @(0),
            @"formatResult"         : @(0)
        };
        
        
    }else if (bytes[0] == 0xff && bytes[1] == 0x53 && bytes[2] == 0x54 && bytes[3] == 0x00 && bytes[4] == 0x01 && bytes[5] == 0x03) { //sd卡ok
        int len = (bytes[6] << 16) | (bytes[7] << 8) | bytes[8];
        int len2 = (bytes[9] << 16) | (bytes[10] << 8) | bytes[11];
        
        dict =      @{
            @"existTFCard"          : @(1),
            @"needFormat"           : @(0),
            @"tfCardTotalSpec"      : @(len2/1024.0),
            @"tfCardRemainSpace"    : @(len/1024.0),
            @"needRePlug"           : @(0),
            @"formatting"           : @(0),
            @"formatResult"         : @(0)
        };
        
        
    }else if (bytes[0] == 0xff && bytes[1] == 0x53 && bytes[2] == 0x54 && bytes[3] == 0x00 && bytes[4] == 0x01 && bytes[5] == 0x04) {
        
        dict =      @{
            @"existTFCard"          : @(1),
            @"needFormat"           : @(0),
            @"tfCardTotalSpec"      : @(0),
            @"tfCardRemainSpace"    : @(0),
            @"needRePlug"           : @(1),
            @"formatting"           : @(0),
            @"formatResult"         : @(0)
        };
        
    }else if (bytes[0] == 0xff && bytes[1] == 0x53 && bytes[2] == 0x54 && bytes[3] == 0x00 && bytes[4] == 0x02 && bytes[5] == 0x00) {
        
        dict =      @{
            @"existTFCard"          : @(1),
            @"needFormat"           : @(0),
            @"tfCardTotalSpec"      : @(0),
            @"tfCardRemainSpace"    : @(0),
            @"needRePlug"           : @(0),
            @"formatting"           : @(1),
            @"formatResult"         : @(0)
        };
        
    }else if (bytes[0] == 0xff && bytes[1] == 0x53 && bytes[2] == 0x54 && bytes[3] == 0x00 && bytes[4] == 0x02 && bytes[5] == 0x01) {
        
        dict =      @{
            @"existTFCard"          : @(1),
            @"needFormat"           : @(0),
            @"tfCardTotalSpec"      : @(0),
            @"tfCardRemainSpace"    : @(0),
            @"needRePlug"           : @(0),
            @"formatting"           : @(0),
            @"formatResult"         : @(2)
        };
        
        if (formattCount < 2) {
            [self formatSDCardAction];
        }else{
            formattCount = 0;
        }
        
        if (self.formatResultBlock) {
            self.formatResultBlock(NO);
        }
        
    }else if (bytes[0] == 0xff && bytes[1] == 0x53 && bytes[2] == 0x54 && bytes[3] == 0x00 && bytes[4] == 0x02 && bytes[5] == 0x02) {
        
        dict =      @{
            @"existTFCard"          : @(1),
            @"needFormat"           : @(0),
            @"tfCardTotalSpec"      : @(0),
            @"tfCardRemainSpace"    : @(0),
            @"needRePlug"           : @(0),
            @"formatting"           : @(0),
            @"formatResult"         : @(1)
        };
        
        formattCount = 0;
        
        if (self.formatResultBlock) {
            self.formatResultBlock(YES);
        }
        //格式化成功后3秒钟再读容量
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self getSdcardCurrentInfomationComplete:nil];
        });
    }
    self.infoDic = dict;
    if (self.infoBlock) {
        self.infoBlock(dict);
    }
}

@end
