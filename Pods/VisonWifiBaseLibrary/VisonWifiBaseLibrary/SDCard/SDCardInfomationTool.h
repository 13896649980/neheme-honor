//
//  SDCardInfomationTool.h
//  VISON_GPS
//
//  Created by Xu pan on 2019/10/22.
//  Copyright © 2019 huang mindong. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^TFCardInfoBlock)(NSDictionary *dataDic);
typedef void(^TFCardFormatResultBlock)(BOOL result);

@interface SDCardInfomationTool : NSObject

@property   (nonatomic,copy)      TFCardInfoBlock             infoBlock;
@property   (nonatomic,copy)      TFCardFormatResultBlock     formatResultBlock;
@property   (nonatomic,strong)      NSDictionary     *infoDic;

+(instancetype)shareInstance;

/// 获取当前sd卡状态
/// @param complete 完成回调
-(void)getSdcardCurrentInfomationComplete:(TFCardInfoBlock)complete;

/// 格式化sd卡
/// @param complete 完成回调
-(void)formatSDCardActionComplete:(TFCardFormatResultBlock)complete;

@end


