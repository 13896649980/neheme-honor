//
//  VisonWifiBaseLibraryMethods.h
//  FuncCode
//
//  Created by Xu pan on 2021/4/14.
//  Copyright © 2021 Xu pan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StreamControlManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface VisonWifiBaseLibraryMethods : NSObject


/// 开始wifi检测
+(void)starWifiMonitoring;

/// 是否有连接wifi
+(BOOL)isConnectedWifi;

/// 当前连接的SSID
+(NSString *)getCurrentSSID;

/// 获取wifi信号强度
+(int)getWifiSignalQuality;

/// 是否连接上纬盛wifi设备
+(BOOL)isConnectedVisonWifiDevice;

/// 初始化纬盛wifi板，并建立连接(在连接纬盛wifi下)
+(void)initializeVisonWifiDevice;

/// 镜头画面翻转
+(void)imageFileAction;

/// 镜头切换动作
/// @param streamManager 流管理对象
+(void)switchLensActionFromStream:(StreamControlManager *_Nullable)streamManager;

/// 发送UDP指令
/// @param command 指令data
/// @param tag 指令标记
+(BOOL)sendUdpCommand:(NSData *_Nullable)command
                  tag:(int)tag;

/// 发送TCP指令
/// @param command 指令
/// @param tag 指令标记
+(void)sendTcpCommand:(NSData *_Nullable)command
                  tag:(int)tag;

/// 开始接收视频流
/// @param streamManager 流管理对象
/// @param complete 完成回调
+(void)startReceiveStreamFromStreamManager:(StreamControlManager *_Nullable)streamManager
                                  complete:(void(^_Nullable)(void))complete;

/// 停止接收视频流
/// @param streamManager 流管理对象
/// @param complete 完成回调
+(void)stopReceiveStreamFromStreamManager:(StreamControlManager *_Nullable)streamManager
                                 complete:(void(^_Nullable)(void))complete;

/// 拍照动作(视频流中截图和板端取原图)
/// @param streamManager 流管理对象
/// @param need 是否需要板端回传原图
/// @param complete 回调(image有可能nil)
+(void)asyncTakePhotoFromStream:(StreamControlManager *_Nullable)streamManager
                needSourceImage:(BOOL)need
                       complete:(void(^_Nullable)(UIImage * _Nullable streamImage,float progress,UIImage * _Nullable sourceImage))complete;


/// 是否正在录像
+(BOOL)isRecordingInStream:(StreamControlManager *_Nullable)streamManager;

/// 开始写录像文件动作(类似录屏的方式)
/// @param isSaveLocal 是否保存本地
/// @param isSaveTFCard 是否保存tf卡
/// @param streamManager 流管理对象
/// @param filePath 写入路径
/// @param frameRate 帧率
/// @param frameSize 尺寸
/// @param audioEnable 是否录制声音
/// @param complete 准备完成回调
+(void)starRecordScreenIsSaveLocal:(BOOL)isSaveLocal
                      isSaveTFcard:(BOOL)isSaveTFCard
                        FromStream:(StreamControlManager *_Nullable)streamManager
                        ToFilePath:(NSString *_Nullable)filePath
                         frameRate:(int)frameRate
                         frameSize:(CGSize)frameSize
                       audioEnable:(BOOL)audioEnable
                          complete:(void(^_Nullable)(void))complete;


/// 停止影片写入(类似录屏的方式)
/// @param complete 完成回调，返回写入的地址
+(void)stopRecordScreenFromStream:(StreamControlManager *_Nullable)streamManager
                         complete:(nonnull void (^)(NSString * _Nullable moviePath))complete;


/// 读取TF卡信息
/// @param complete 读取完成
/*
 回调的键值对
 {
    @"existTFCard"          : value     (1:检测到TF卡,      0:未检测到TF卡)
    @"needFormat"           : value     (1:卡需要格式化,     0:卡不需要格式化)
    @"tfCardTotalSpec"      : value     卡最大容量 nsvalue @(float)
    @"tfCardRemainSpace"    : value     卡剩余容量 nsvalue @(float)
    @"needRePlug"           : value     (1:TF卡需要重新插拔    0:TF卡不需要重新插拔)
    @"formatting"           : value     (1:TF卡正在格式化     0:TF卡未正在格式化)
    @"formatResult"         : value     (1:格式化成功        2:格式化失败)
 }
 **/
+(void)getTFCardInfomation:(void(^_Nullable)(NSDictionary * _Nullable infoDict))complete;

/// 格式化TF卡动作
/// @param complete 结果回调 result YES：格式化成功  NO：格式化失败
+(void)formatTFCardActionComplete:(void(^_Nullable)(BOOL result))complete;





@end

NS_ASSUME_NONNULL_END
