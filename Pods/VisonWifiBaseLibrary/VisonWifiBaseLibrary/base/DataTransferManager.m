//
//  DataTransferManager.m
//  GPS Project
//
//  Created by pyz on 2021/6/24.
//  Copyright © 2021 Xu pan. All rights reserved.
//

#import "DataTransferManager.h"
#import "StreamPreplayConfig.h"
#import "Device_Wifi_Config_Command.h"
#import "APReverseGeocoding.h"
#import <ifaddrs.h>
#import "getgateway.h"
#include <net/if.h>
#import <arpa/inet.h>
#import "SendCommandManager.h"
#import "WIFIConnectManager.h"

@interface DataTransferManager ()
@property   (nonatomic,strong)      NSTimer                 *TCPHeartBeatTimer;//tcp心跳包计时器
@property   (nonatomic,strong)      NSTimer                 *checkWifiTimer;//wifi检查计时器
@property   (nonatomic,strong)      NSTimer                 *getWifiVisonTimer;//udp获取版本号计时器
@property   (nonatomic,strong)      NSTimer                 *UDP_Play_HeartBeatTimer; //UDP播放时需要持续发送ip地址
@property   (nonatomic,strong)      NSString                *countryCode;
@property   (nonatomic,assign)      BOOL                    setCountryCodeSuccess;
@end


@implementation DataTransferManager
static DataTransferManager *_mamager = nil;
+(instancetype)shareInstance{
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _mamager = [[self alloc]init];
    });
    return _mamager;
}

-(instancetype)init{
    if (self = [super init]) {
        [USER_DEFAULT setObject:@"nodevice" forKey:DEVICE];
        [USER_DEFAULT setInteger:1 forKey:FLIP_STATE_VGS];
        [USER_DEFAULT setInteger:0 forKey:LENS_STATUS];
        [USER_DEFAULT synchronize];
        
        //检查WIFI是否连接
        self.checkWifiTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(checkWifiAction) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:self.checkWifiTimer forMode:NSRunLoopCommonModes];
        //读设备版本号
        self.getWifiVisonTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(getWifiVisonAction) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:self.getWifiVisonTimer forMode:NSRunLoopCommonModes];
        
        //定位到后要设置国家码
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationsAction:) name:LOCATION_NOTI_NAME object:nil];
        //监听TCP数据
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveTcpData:) name:TransferTCPData object:nil];
        //监听UDP数据
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveUdpData:) name:TransferUDPData object:nil];
        //监听872 UDP数据
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceive872UdpData:) name:TransferUDPComData object:nil];
        
        //监听WIFI断开
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(DisConnected) name:WIFI_NOTI_DISCONNECT object:nil];
        
        //监听USB断开
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(DisConnected) name:USBDISCONNECT object:nil];
    }
    return self;
}


#pragma mark - WIFI/USB断开
-(void)DisConnected{
    //0.5防止后清空数据，防止销毁播放控制器时没有数据判断
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (![WIFIConnectManager shareInstance].isConnectedWIFI) {
            [self clearData];
        }
        [[WIFIDeviceModel shareInstance] clearData];
        [[StreamPreplayConfig shareInstance] clearData];
    });
}


#pragma mark ------------------------------------- TCP数据 -------------------------------------
- (void)didReceiveTcpData:(NSNotification*)ns{
    NSData * data = ns.object;
    if (self.delegate && [self.delegate respondsToSelector:@selector(didReceiveTcpData:)]) {
        [self.delegate didReceiveTcpData:data];
    }
}
#pragma mark ------------------------------------- 872UDP数据 -------------------------------------
- (void)didReceive872UdpData:(NSNotification*)ns{
    NSData * data = ns.object;
    if (self.delegate && [self.delegate respondsToSelector:@selector(theUdpCommunicationDidReceiveData:)]) {
        [self.delegate theUdpCommunicationDidReceiveData:data];
    }
}

#pragma mark ------------------------------------- UDP数据 -------------------------------------
- (void)didReceiveUdpData:(NSNotification*)ns{
    NSData * data = ns.object;
    Byte *bytes = (Byte *)data.bytes;
    NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    //----------- 老版本的版本号 -----------
    if ([[VisonWifiBaseLibraryUtility getTotalOldVersionHardwareNames] containsObject:string]) {
        //----------- 读wifi型号 -----------
        [[WIFIDeviceModel shareInstance] identifyOldVersionWifiModelFromData:string complete:^(BOOL isIdentified, NSString * _Nonnull model) {
            if(isIdentified){
                if (!self->_isConnectedDevice) {
                    self->_isConnectedDevice = YES;
                }
                //----------- 读到设备型号 -----------
                NSLog(@"旧版本——读到设备型号：%@",model);
                self->_deviceModel = model;
                [[StreamPreplayConfig shareInstance] streamPreplayConfigWithWIFIDeviceModel:[WIFIDeviceModel shareInstance]];
                
                //可以开始出图
                [[NSNotificationCenter defaultCenter] postNotificationName:STREAM_NOTI_READYTOPLAY object:nil];
                
                NSString *verStr;
                if (self->_deviceVersion.length) {
                    verStr = [NSString stringWithFormat:@"FirmwareVer:%@_%@",self.deviceVersion,self.deviceModel];
                }else{
                    verStr = [NSString stringWithFormat:@"FirmwareVer:%@",self.deviceModel];
                }
                [[NSNotificationCenter defaultCenter] postNotificationName:VERSON_NOTI_NAME object:verStr];
            }
        }];
        return;
    }
    
    //----------- 返回的 新的wifi型号 -----------
    if (bytes[0] == 0xaa && bytes[1] == 0xff) {
        //----------- 读wifi型号 -----------
        [[WIFIDeviceModel shareInstance] identifyWifiModelFromData:data
                                             complete:^(BOOL isIdentified, NSString * _Nonnull model) {
            if (isIdentified) {
                if (!self.isConnectedDevice) {
                    self->_isConnectedDevice = YES;
                }
                //----------- 读到设备型号 -----------
                NSLog(@"新版本——读到设备型号：%@",model);
                self->_deviceModel = model;
                [[StreamPreplayConfig shareInstance] streamPreplayConfigWithWIFIDeviceModel:[WIFIDeviceModel shareInstance]];
                
                //可以开始出图
                [[NSNotificationCenter defaultCenter] postNotificationName:STREAM_NOTI_READYTOPLAY object:nil];
                
                NSString *verStr;
                if (self.deviceVersion.length) {
                    verStr = [NSString stringWithFormat:@"FirmwareVer:%@_%@",self.deviceVersion,self.deviceModel];
                }else{
                    verStr = [NSString stringWithFormat:@"FirmwareVer:%@",self.deviceModel];
                }
                [[NSNotificationCenter defaultCenter] postNotificationName:VERSON_NOTI_NAME object:verStr];
            }
        }];
        
        return;
    }
    
    //----------- 读到了wifi版本号 -----------
    if ([string hasPrefix:@"V2."] || [string hasPrefix:@"V3."] || [string hasPrefix:@"V4."] || [string hasPrefix:@"V5."]){
        
        if (!self.deviceVersion) {
            //----------- 读到设备版本号 -----------
            _deviceVersion = string;
            
            NSString *verStr;
            if (self.deviceModel) {
                verStr = [NSString stringWithFormat:@"FirmwareVer:%@_%@",self.deviceVersion,self.deviceModel];
            }else{
                verStr = [NSString stringWithFormat:@"FirmwareVer:%@",self.deviceVersion];
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:VERSON_NOTI_NAME object:verStr];
            
            NSLog(@"设备信息(旧版本的分辨率\n帧率信息不可信)\n{\n   型号：%@\n   版本号:%@\n   出图方式:%@\n   回传分辨率:%d*%d\n   板端分辨率:%d*%d\n   帧率:%dfps\n   是否双镜头:%@\n}",_deviceModel,_deviceVersion,([WIFIDeviceModel shareInstance].playType == PlayType_TCP ? @"TCP" : ([WIFIDeviceModel shareInstance].playType == PlayType_UDP ? @"UDP" : @"RTSP")),(int)[[WIFIDeviceModel shareInstance] getPixelSize:[WIFIDeviceModel shareInstance].mainLensTransmissionPixelSize].width,(int)[[WIFIDeviceModel shareInstance] getPixelSize:[WIFIDeviceModel shareInstance].mainLensTransmissionPixelSize].height,(int)[[WIFIDeviceModel shareInstance] getPixelSize:[WIFIDeviceModel shareInstance].mainLensRealPixelSize].width,(int)[[WIFIDeviceModel shareInstance] getPixelSize:[WIFIDeviceModel shareInstance].mainLensRealPixelSize].height,[WIFIDeviceModel shareInstance].frameRate,[WIFIDeviceModel shareInstance].isDoubleCamera ? @"是" : @"否");
            
            //发送系统时间给wifi
            [self sendSystemTimeToWIFIDevice];
            //获取飞机跟随码
            [self getDroneType];
            //读镜头方向
            [self getVideoOrientation];
            
            [self creatTCPHeartBeatTimer];
            [[NSNotificationCenter defaultCenter]postNotificationName:StartDroneConnect object:nil];

        }
        return;
    }
    
    //----------- 国家码相关 -----------
    if ([string rangeOfString:@"dist_code="].length > 0){
        NSArray *strings = [string componentsSeparatedByString:@"="];
        self.countryCode = [strings.lastObject stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        NSLog(@"读到设备国家码:%@",self.countryCode);
        return ;
    }
    
    if ([string rangeOfString:@"set CTR ok"].length > 0){
        NSLog(@"设置设备国家码OK");
        self.setCountryCodeSuccess = YES;
        return;
    }
    
    //----------- 镜头方向 -----------
    if (data.length == 3) {
        if (bytes[0] == 0x6f && bytes[1] == 0x6b && bytes[2] == 0x01){ //正
            [USER_DEFAULT setInteger:1 forKey:FLIP_STATE_VGS];
            [USER_DEFAULT synchronize];
            return ;
        }else if (bytes[0] == 0x6f && bytes[1] == 0x6b && bytes[2] == 0x02){//反
            [USER_DEFAULT setInteger:2 forKey:FLIP_STATE_VGS];
            [USER_DEFAULT synchronize];
            return ;
        }else if (bytes[0] == 0x6f && bytes[1] == 0x6b && bytes[2] == 0x31){ //正
            [USER_DEFAULT setInteger:1 forKey:FLIP_STATE_VGS];
            [USER_DEFAULT synchronize];
            return ;
        }else if (bytes[0] == 0x6f && bytes[1] == 0x6b && bytes[2] == 0x32){//反
            [USER_DEFAULT setInteger:2 forKey:FLIP_STATE_VGS];
            [USER_DEFAULT synchronize];
            return ;
        }
    }
    
    //----------- 主副镜头切换回调 -----------
    if (bytes[0] == 0x0b && bytes[1] == 0x00) {
        [USER_DEFAULT setInteger:0 forKey:LENS_STATUS];
        [USER_DEFAULT synchronize];
        return ;
    }else if (bytes[0] == 0x0b && bytes[1] == 0x01){
        [USER_DEFAULT setInteger:1 forKey:LENS_STATUS];
        [USER_DEFAULT synchronize];
        return ;
    }
    
    //----------- wifi信号质量 -----------
    if (bytes[0] == 0xff && bytes[1] == 0x53 && bytes[2] == 0x54 && bytes[3] == 0x08) {
        [[NSNotificationCenter defaultCenter] postNotificationName:WIFI_NOTI_QUALITY object:@(bytes[4])];
        return ;
    }

    //sd卡信息
    if (bytes[0] == 0xff && bytes[1] == 0x53 && bytes[2] == 0x54 && bytes[3] == 0x00) {
        [[NSNotificationCenter defaultCenter] postNotificationName:SDCARD_NOTI_INFO object:data];
        return ;
    }
    
    //消息回调
    if (self.delegate && [self.delegate respondsToSelector:@selector(didReceiveUdpData:)]) {
        [self.delegate didReceiveUdpData:data];
    }
 
}



#pragma mark ----------- 定时检查wifi,获取wifi类型（1s/次) -----------
-(void)checkWifiAction{
    if(!IS_NO_DEVICE){
        return;
    }
    NSLog(@"发送读设备型号命令");
    
    if ([VisonPTUSBHandle shareInstance].isConnected) {
        [[SendCommandManager shareInstance]sendUdpCommand:[Device_Wifi_Config_Command getUSBWifiModelCommand] tag:1];
    }else{
        //没有设备型号的话就一直读
        NSString *ip = [self localWiFiIPAddress];
        [USER_DEFAULT setObject:ip forKey:VGS_WIFI_IP];
        
        NSString *host = [self getGateWayIP];
        if ([ip hasPrefix:@"192.168.0"]) {
            host = @"192.168.0.1";
        }
        
        [USER_DEFAULT setObject:host?host:@"unknow" forKey:HOST_IP];
        [USER_DEFAULT synchronize];
        
        if (host && ([host isEqualToString:@"192.168.0.1"] || [host isEqualToString:@"172.16.10.1"] || [host isEqualToString:@"192.168.1.1"])){
            NSLog(@"发送读设备型号命令");
            //获取wifi型号
            [[SendCommandManager shareInstance]sendUdpCommand:[Device_Wifi_Config_Command getWifiModelCommandWithIP:host] tag:1];
            usleep(10000);
            [[SendCommandManager shareInstance]sendUdpCommand:[Device_Wifi_Config_Command getWifiModelCommandWithIP:host] tag:1];
        }
    }
}

#pragma mark ----------- 定时获取wifi版本号（1s/次）-----------
-(void)getWifiVisonAction{
    if (self.deviceVersion) {
        return;
    }
    NSLog(@"发送读设备版本号命令");
    
    if ([VisonPTUSBHandle shareInstance].isConnected) {
        [[SendCommandManager shareInstance]sendUdpCommand:[Device_Wifi_Config_Command getUSBWifiVisonCommand] tag:1];
    }else{
        [[SendCommandManager shareInstance]sendUdpCommand:[Device_Wifi_Config_Command getWifiVisonCommand] tag:1];
    }
    
}

#pragma mark ----------- 创建TCP心跳包 -----------
-(void)creatTCPHeartBeatTimer{
    if (self.TCPHeartBeatTimer) {
        [self.TCPHeartBeatTimer invalidate];
        self.TCPHeartBeatTimer = nil;
    }
    //先每0.1s发1次共发10次后再每1.5秒发一次
    __block int timeout = 10; //倒计时时间
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue());
    dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, 0.1f * NSEC_PER_SEC, 0);
    dispatch_source_set_event_handler(timer, ^{
        if(timeout <= 0){ //倒计时结束，关闭
            self.TCPHeartBeatTimer = [NSTimer scheduledTimerWithTimeInterval:1.5f target:self selector:@selector(tcpHeartBeatAction) userInfo:nil repeats:YES];
            dispatch_source_cancel(timer);
        }else{
            [[SendCommandManager shareInstance]sendTcpCommand:[Device_Wifi_Config_Command getTcpHeartbeatCommand] tag:1];
        }
        timeout--;
    });
    dispatch_resume(timer);
}

-(void)tcpHeartBeatAction{
    [[SendCommandManager shareInstance]sendTcpCommand:[Device_Wifi_Config_Command getTcpHeartbeatCommand] tag:1];
}

#pragma mark ----------- 创建TCP心跳包 -----------
-(void)stopTCPHeartBeatTimer{
    if (self.TCPHeartBeatTimer) {
        [self.TCPHeartBeatTimer invalidate];
        self.TCPHeartBeatTimer = nil;
    }
}


#pragma mark ----------- udp播放方式 发送心跳包 -----------
-(void)starUDPPlaySendHeartBeatPacket{
    if (self.UDP_Play_HeartBeatTimer) {
        [self.UDP_Play_HeartBeatTimer invalidate];
        self.UDP_Play_HeartBeatTimer = nil;
    }
    
    self.UDP_Play_HeartBeatTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(UDPPlaySendHeartBeatPacketAction) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:self.UDP_Play_HeartBeatTimer forMode:NSRunLoopCommonModes];
}

-(void)UDPPlaySendHeartBeatPacketAction{
    if (!self.isConnectedDevice) return;
    [[SendCommandManager shareInstance]sendUdpCommand:[Device_Wifi_Config_Command getUDPNeedSendIpAddressCommandWithIP:[USER_DEFAULT objectForKey:VGS_WIFI_IP]] tag:1];
}

#pragma mark ----------- 停止UDP视频流 -----------
-(void)stopSendUDPStream{
    if (!self.isConnectedDevice) return;
    for (int i = 0;  i < 2; i++) {
        [[SendCommandManager shareInstance]sendUdpCommand:[Device_Wifi_Config_Command getUDPStopSendIPAddressCommandWithIP:[USER_DEFAULT objectForKey:VGS_WIFI_IP]] tag:1];
    }
    
    if (self.UDP_Play_HeartBeatTimer) {
        [self.UDP_Play_HeartBeatTimer invalidate];
        self.UDP_Play_HeartBeatTimer = nil;
    }
}




#pragma mark - 定位通知，设置国家码
-(void)locationsAction:(NSNotification *)noti{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        //获取手机当前国家码
        CLLocation *curLocation = noti.object;
        CLLocationCoordinate2D coord = curLocation.coordinate;
        NSString *code = [[APReverseGeocoding defaultGeocoding] geocodeCountryWithCoordinate:coord].shortCode.lowercaseString;
        //获取wifi板当前国家码
        [self getDeviceCountryCodeComplete:^(NSString *countryCode) {
            //不相同的话就设置手机国家码给wifi板
            if (![code isEqualToString:countryCode]){
                NSLog(@"手机国家码与WIFI设备国家码不同,设置国家码...");
                [self setCountryCodeToDevice:code];
            }else{
                NSLog(@"手机国家码与WIFI设备国家码相同,跳过设置国家码步骤");
            }
        }];
    });
}

#pragma mark ----------- 发送系统时间给wifi -----------
-(void)sendSystemTimeToWIFIDevice{
    NSLog(@"发送系统时间给设备");
    __block int timeout = 5; //倒计时时间
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue());
    dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, 0.2f * NSEC_PER_SEC, 0);
    dispatch_source_set_event_handler(timer, ^{
        if(timeout <= 0){ //倒计时结束，关闭
            dispatch_source_cancel(timer);
        }else{
            //发送时间命令
            [[SendCommandManager shareInstance]sendUdpCommand:[Device_Wifi_Config_Command getTimeCommandRTSP] tag:1];
            [[SendCommandManager shareInstance]sendUdpCommand:[Device_Wifi_Config_Command getTimeCommand] tag:1];
        }
        timeout--;
    });
    dispatch_resume(timer);
}

#pragma mark ----------- 获取飞机跟随码 -----------
-(void)getDroneType{
    NSLog(@"发送获取跟随码");
  //循环发送八次B返回飞机型号
    __block int count = 0;
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue());
    dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, 0.3f* NSEC_PER_SEC, 0 * NSEC_PER_SEC);
    dispatch_source_set_event_handler(timer, ^{
        [[SendCommandManager shareInstance]sendUdpCommand:[Device_Wifi_Config_Command getDroneTypeCommand] tag:1];
    if (count >= 5){
            dispatch_source_cancel(timer);
        }
        count++;
    });
    dispatch_resume(timer);
}


#pragma mark ----------- 获取wifi板国家码 -----------
-(void)getDeviceCountryCodeComplete:(void(^)(NSString *countryCode))complete{
    NSLog(@"获取wifi板国家码");
    [[SendCommandManager shareInstance]sendUdpCommand:[Device_Wifi_Config_Command getWifiDeviceCountryCodeCommand] tag:1];

    __block int timeout = 5;
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue());
    dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, 0.1f * NSEC_PER_SEC, 0 * NSEC_PER_SEC);
    dispatch_source_set_event_handler(timer, ^{
        
        if (timeout <= 0){
            dispatch_source_cancel(timer);
        }else{
            if(!self.countryCode){
                dispatch_source_cancel(timer);
                timeout = 0;
            }else{
                [[SendCommandManager shareInstance]sendUdpCommand:[Device_Wifi_Config_Command getWifiDeviceCountryCodeCommand] tag:1];
            }
        }
        timeout --;
    });
    dispatch_resume(timer);
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (complete) {
            if (!self.countryCode) {
                NSLog(@"3秒内未读到国家码，发送手机国家码给wifi板");
            }
            complete(self.countryCode);
        }
    });
}

#pragma mark ----------- 设置wifi板国家码 -----------
-(void)setCountryCodeToDevice:(NSString *)countryCode{
    NSLog(@"发送设置国家码命令动作：%@",countryCode);
    dispatch_async(dispatch_get_main_queue(), ^{
        //用udp发命令
        [[SendCommandManager shareInstance]sendUdpCommand:[Device_Wifi_Config_Command setWifiDeviceCountryCodeCommandWithCountryCode:countryCode] tag:1];

        __block int timeout = 5;
        dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue());
        dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, 0.1f * NSEC_PER_SEC, 0 * NSEC_PER_SEC);
        dispatch_source_set_event_handler(timer, ^{
            
            if (timeout <= 0){
                dispatch_source_cancel(timer);
            }else{
                if(!self.setCountryCodeSuccess){
                    [[SendCommandManager shareInstance]sendUdpCommand:[Device_Wifi_Config_Command setWifiDeviceCountryCodeCommandWithCountryCode:countryCode] tag:1];

                }else{
                    dispatch_source_cancel(timer);
                    timeout = 0;
                }
            }
            timeout --;
        });
        dispatch_resume(timer);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self.setCountryCodeSuccess = NO;
            self.countryCode = nil;
        });
    });
}

#pragma mark ----------- 读取画面方向 -----------
-(void)getVideoOrientation{
    NSLog(@"读取画面方向");
 //循环发送八次B返回飞机型号
    __block int count = 0;
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue());
    dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, 0.3f* NSEC_PER_SEC, 0 * NSEC_PER_SEC);
    dispatch_source_set_event_handler(timer, ^{
        [[SendCommandManager shareInstance]sendUdpCommand:[Device_Wifi_Config_Command getVideoOrientationCommand] tag:1];

    if (count >= 5){
            dispatch_source_cancel(timer);
        }
        count++;
    });
    dispatch_resume(timer);
    
}


#pragma mark ----------- 清除数据/timer -----------
-(void)clearData{
    NSLog(@"SocketManager - 清空数据");
    
    if (self.getWifiVisonTimer) {
        [self.getWifiVisonTimer invalidate];
        self.getWifiVisonTimer = nil;
    }

    if (self.checkWifiTimer) {
        [self.checkWifiTimer invalidate];
        self.checkWifiTimer = nil;
    }
    
    if (self.UDP_Play_HeartBeatTimer) {
        [self.UDP_Play_HeartBeatTimer invalidate];
        self.UDP_Play_HeartBeatTimer = nil;
    }
    
    if (self.TCPHeartBeatTimer) {
        [self.TCPHeartBeatTimer invalidate];
        self.TCPHeartBeatTimer = nil;
    }
    
    _isConnectedDevice = NO;
    _deviceVersion = @"";
    _deviceModel = @"";
    
    [USER_DEFAULT setObject:@"nodevice" forKey:DEVICE];
    [USER_DEFAULT setInteger:0 forKey:LENS_STATUS];
    [USER_DEFAULT synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:VERSON_NOTI_NAME object:@" "];
}




#pragma mark ---------- 获取wifi-IP地址 ----------
- (NSString *) localWiFiIPAddress{
    BOOL success;
    struct ifaddrs * addrs;
    const struct ifaddrs * cursor;
    
    success = getifaddrs(&addrs) == 0;
    if (success) {
        cursor = addrs;
        while (cursor != NULL) {
            // the second test keeps from picking up the loopback address
            if (cursor->ifa_addr->sa_family == AF_INET && (cursor->ifa_flags & IFF_LOOPBACK) == 0){
                NSString *name = [NSString stringWithUTF8String:cursor->ifa_name];
                if ([name isEqualToString:@"en0"])  // Wi-Fi adapter
                    return [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)cursor->ifa_addr)->sin_addr)];
            }
            cursor = cursor->ifa_next;
        }
        freeifaddrs(addrs);
    }
    return nil;
}

#pragma mark ---------- 获取路由网关 ----------
- (NSString *) getGateWayIP{
    struct in_addr addr;
    getdefaultgateway(&(addr.s_addr));
    return [NSString stringWithUTF8String:inet_ntoa(addr)];
}


@end
