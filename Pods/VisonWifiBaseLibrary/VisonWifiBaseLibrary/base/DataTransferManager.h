//
//  DataTransferManager.h
//  GPS Project
//
//  Created by pyz on 2021/6/24.
//  Copyright © 2021 Xu pan. All rights reserved.
//

#import <Foundation/Foundation.h>

#define TransferTCPData @"TransferTCPData"
#define TransferUDPData @"TransferUDPData"
#define TransferUDPComData @"TransferUDPComData"
#define StartDroneConnect @"StartDroneConnect"


@protocol DataTransferManagerDelegate <NSObject>

@optional
/// udp接收数据回调
/// @param data 数据data
-(void)didReceiveUdpData:(NSData *)data;

@optional
/// tcp接收数据回调
/// @param data 数据data
-(void)didReceiveTcpData:(NSData *)data;

/// UDP协议通信方式数据通信（现是872udp协议通信）
/// @param data 数据data
-(void)theUdpCommunicationDidReceiveData:(NSData *)data;

@end

@interface DataTransferManager : NSObject
//读到板类型和版本号为连上设备
@property   (nonatomic,assign,readonly)     BOOL                                    isConnectedDevice;
//设备型号
@property   (nonatomic,assign,readonly)     NSString                                *deviceModel;
//设备版本号
@property   (nonatomic,assign,readonly)     NSString                                *deviceVersion;

+(instancetype)shareInstance;

@property   (nonatomic,assign)              id<DataTransferManagerDelegate>        delegate;


/// udp播放方式 发送心跳包
-(void)starUDPPlaySendHeartBeatPacket;

/// udp播放方式停止接收udp视频流
-(void)stopSendUDPStream;

/// 创建TCP心跳包 -----------
-(void)creatTCPHeartBeatTimer;
/// 停止TCP心跳包 -----------
-(void)stopTCPHeartBeatTimer;
@end


