//
//  SendCommandManger.h
//  GPS Project
//
//  Created by pyz on 2021/6/24.
//  Copyright © 2021 Xu pan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SocketControlManager.h"
#import "VisonPTUSBHandle.h"

@interface SendCommandManager : NSObject
+(instancetype)shareInstance;

/// 发送UDP指令
/// @param command 指令data
/// @param tag 指令标记
-(void)sendUdpCommand:(NSData *)command tag:(int)tag;

/// 发送TCP指令
/// @param command 指令
/// @param tag 指令标记
-(void)sendTcpCommand:(NSData *)command tag:(int)tag;
@end

