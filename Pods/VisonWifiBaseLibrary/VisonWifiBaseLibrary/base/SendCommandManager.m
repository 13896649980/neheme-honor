//
//  SendCommandManger.m
//  GPS Project
//
//  Created by pyz on 2021/6/24.
//  Copyright © 2021 Xu pan. All rights reserved.
//

#import "SendCommandManager.h"
#import "PTProtocol.h"

@implementation SendCommandManager
static SendCommandManager *_mamager = nil;
+(instancetype)shareInstance{
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _mamager = [[self alloc]init];
    });
    return _mamager;
}

-(instancetype)init{
    if (self = [super init]) {
        

    }
    return self;
}



/// 发送UDP指令
/// @param command 指令data
/// @param tag 指令标记
-(void)sendUdpCommand:(NSData *)command tag:(int)tag{
    if ([VisonPTUSBHandle shareInstance].isConnected) {
        [VisonPTUSBHandle sendData:command frameType:PTFrameTypeUdpData callback:nil];
    }else{
        [[SocketControlManager shareInstance]sendUdpCommand:command tag:tag];
    }
}

/// 发送TCP指令
/// @param command 指令
/// @param tag 指令标记
-(void)sendTcpCommand:(NSData *)command tag:(int)tag{
    if ([VisonPTUSBHandle shareInstance].isConnected) {
        [VisonPTUSBHandle sendData:command frameType:PTFrameTypeTcpData callback:nil];
    }else{
        [[SocketControlManager shareInstance]sendTcpCommand:command tag:tag];
    }
}

@end
