#import "PTProtocol.h"
#import "PTPrivate.h"
#import <objc/runtime.h>
#include <pthread/pthread.h>

static const uint32_t PTProtocolVersion1 = 1;

NSString * const PTProtocolErrorDomain = @"PTProtocolError";





@interface PTProtocol () {
    uint32_t nextFrameTag_;
@public
    dispatch_queue_t queue_;
    uint16_t frameIndex;
    pthread_mutex_t writeLock;
}
- (dispatch_data_t)createDispatchDataWithFrameOfType:(uint32_t)type frameTag:(uint32_t)frameTag payload:(dispatch_data_t)payload;
@end


static void _release_queue_local_protocol(void *objcobj) {
    if (objcobj) {
        PTProtocol *protocol = (__bridge_transfer id)objcobj;
        protocol->queue_ = NULL;
    }
}


@interface RQueueLocalIOFrameProtocol : PTProtocol
@end
@implementation RQueueLocalIOFrameProtocol
- (void)setQueue:(dispatch_queue_t)queue {
}
@end


@implementation PTProtocol


+ (PTProtocol*)sharedProtocolForQueue:(dispatch_queue_t)queue {
    static const char currentQueueFrameProtocolKey;
    //dispatch_queue_t queue = dispatch_get_current_queue();
    PTProtocol *currentQueueFrameProtocol = (__bridge PTProtocol*)dispatch_queue_get_specific(queue, &currentQueueFrameProtocolKey);
    if (!currentQueueFrameProtocol) {
        currentQueueFrameProtocol = [[RQueueLocalIOFrameProtocol alloc] initWithDispatchQueue:NULL];
        currentQueueFrameProtocol->queue_ = queue; // reference, no retain, since we would create cyclic references
        dispatch_queue_set_specific(queue, &currentQueueFrameProtocolKey, (__bridge_retained void*)currentQueueFrameProtocol, &_release_queue_local_protocol);
        return (__bridge PTProtocol*)dispatch_queue_get_specific(queue, &currentQueueFrameProtocolKey); // to avoid race conds
    } else {
        return currentQueueFrameProtocol;
    }
}


- (id)initWithDispatchQueue:(dispatch_queue_t)queue {
    if (!(self = [super init])) return nil;
    queue_ = queue;
#if PT_DISPATCH_RETAIN_RELEASE
    if (queue_) dispatch_retain(queue_);
#endif
    frameIndex = 1;
    
    //创建写操作自旋锁
    pthread_mutexattr_t attr;
    pthread_mutexattr_init(&attr);
    pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_NORMAL);
    pthread_mutex_init(&writeLock, &attr);
    pthread_mutexattr_destroy(&attr);
    
    return self;
}

- (id)init {
    return [self initWithDispatchQueue:dispatch_get_main_queue()];
}

- (void)dealloc {
    if (queue_) {
#if PT_DISPATCH_RETAIN_RELEASE
        dispatch_release(queue_);
#endif
    }
    
    //销毁锁
    pthread_mutex_unlock(&writeLock);
    pthread_mutex_destroy(&writeLock);
}

- (dispatch_queue_t)queue {
    return queue_;
}

- (void)setQueue:(dispatch_queue_t)queue {
#if PT_DISPATCH_RETAIN_RELEASE
    dispatch_queue_t prev_queue = queue_;
    queue_ = queue;
    if (queue_) dispatch_retain(queue_);
    if (prev_queue) dispatch_release(prev_queue);
#else
    queue_ = queue;
#endif
}


- (uint32_t)newTag {
    return ++nextFrameTag_;
}


#pragma mark -
#pragma mark Creating frames



 /*
- (dispatch_data_t)createDispatchDataWithFrameOfType:(uint32_t)type frameTag:(uint32_t)frameTag payload:(dispatch_data_t)payload {
    PTFrame *frame = CFAllocatorAllocate(kCFAllocatorDefault, sizeof(PTFrame), 0);
    frame->heard.length = htonl(frame->heard.length);
    frame->heard.num = htonl(type);
    frame->tag = htonl(frameTag);
    
    if (payload) {
        size_t payloadSize = dispatch_data_get_size(payload);
        assert(payloadSize <= UINT32_MAX);
        frame->payloadSize = htonl((uint32_t)payloadSize);
    } else {
        frame->payloadSize = 0;
    }
    
    dispatch_data_t frameData = dispatch_data_create((const void*)frame, sizeof(PTFrame), queue_, ^{
        CFAllocatorDeallocate(kCFAllocatorDefault, (void*)frame);
    });
    
    if (payload && frame->payloadSize != 0) {
        // chain frame + payload
        dispatch_data_t data = dispatch_data_create_concat(frameData, payload);
#if PT_DISPATCH_RETAIN_RELEASE
        dispatch_release(frameData);
#endif
        frameData = data;
    }
    
    return frameData;
}
  */
 

-(dispatch_data_t)createDispatchDataWithFrameOfType:(uint16_t)type data:(NSData *)data{
    
    Byte *bytes = (Byte *)data.bytes;
    uint16_t ckValue = 0;
    for (int i = 0; i < data.length; i++) {
        ckValue ^= bytes[i];
    }
    
    PTFrame *frame = CFAllocatorAllocate(kCFAllocatorDefault, sizeof(PTFrameHeard), 0);
    frame->heard.length = data.length;
    frame->heard.num = frameIndex;
    frame->heard.check = ckValue;
    frame->heard.type = type;
    
    char headLength = sizeof(PTFrameHeard);
        
    uint8_t *sendData = malloc(headLength + data.length + VISON_USBDATA_HEAD_LEN);
    memcpy(sendData, &VISON_USBDATA_HEAD, VISON_USBDATA_HEAD_LEN);
    memcpy(sendData + VISON_USBDATA_HEAD_LEN, &frame->heard, headLength);
    memcpy(sendData + VISON_USBDATA_HEAD_LEN + headLength, bytes, data.length);

    free(frame);
    
    dispatch_data_t frameData = dispatch_data_create((const void*)sendData, VISON_USBDATA_HEAD_LEN+headLength + data.length, queue_, ^{
        CFAllocatorDeallocate(kCFAllocatorDefault, (void*)sendData);
    });
    
    NSLog(@"发送数据%@",frameData);
    return frameData;
}

#pragma mark -
#pragma mark Sending frames


- (void)sendFrameOfType:(uint32_t)frameType tag:(uint32_t)tag withPayload:(dispatch_data_t)payload overChannel:(dispatch_io_t)channel callback:(void(^)(NSError*))callback {
    dispatch_data_t frame = [self createDispatchDataWithFrameOfType:frameType frameTag:tag payload:payload];
    dispatch_io_write(channel, 0, frame, queue_, ^(bool done, dispatch_data_t data, int _errno) {
        if (done && callback) {
            callback(_errno == 0 ? nil : [[NSError alloc] initWithDomain:NSPOSIXErrorDomain code:_errno userInfo:nil]);
        }
    });
#if PT_DISPATCH_RETAIN_RELEASE
    dispatch_release(frame);
#endif
}


-(void)sendFrameOfType:(uint16_t)frameType data:(NSData *)data overChannel:(dispatch_io_t)channel callback:(void (^)(NSError *))callback{
    
    //加锁
    pthread_mutex_lock(&writeLock);
    dispatch_data_t frame = [self createDispatchDataWithFrameOfType:frameType data:data];

    dispatch_io_write(channel, 0, frame, queue_, ^(bool done, dispatch_data_t data, int _errno) {
        if (done) {
            self->frameIndex++;
        }else{
            NSLog(@"发送失败");
        }
        if (done && callback) {
            callback(_errno == 0 ? nil : [[NSError alloc] initWithDomain:NSPOSIXErrorDomain code:_errno userInfo:nil]);
        }
        //解锁
        pthread_mutex_unlock(&self->writeLock);
    });
#if PT_DISPATCH_RETAIN_RELEASE
    dispatch_release(frame);
#endif
}


#pragma mark -
#pragma mark Receiving frames


- (void)readFrameOverChannel:(dispatch_io_t)channel callback:(void (^)(NSError *, PTFrameHeard *))callback {
    
    
    dispatch_semaphore_t headLock = dispatch_semaphore_create(0);
    __block BOOL findHead = NO;
    __block BOOL disConnectedTag = NO;
    while (1) {
        __block dispatch_data_t headData = NULL;
        dispatch_io_read(channel, 0, 3, queue_, ^(bool done, dispatch_data_t data, int error) {
            size_t headDataSize = data ? dispatch_data_get_size(data) : 0;
            
            if (headDataSize) {
                if (!headData) {
                    headData = data;
#if PT_DISPATCH_RETAIN_RELEASE
                    dispatch_retain(allData);
#endif
                } else {
#if PT_DISPATCH_RETAIN_RELEASE
                    dispatch_data_t allDataPrev = headData;
                    allData = dispatch_data_create_concat(headData, data);
                    dispatch_release(allDataPrev);
#else
                    headData = dispatch_data_create_concat(headData, data);
#endif
                }
            }
            
            if (done) {
                
                if (error != 0) {
                    disConnectedTag = YES;
                    dispatch_semaphore_signal(headLock);
                    return;
                }
                
                if (headDataSize == 0) {
                    disConnectedTag = YES;
                    dispatch_semaphore_signal(headLock);
                    return;
                }
                
                if (!headData || dispatch_data_get_size(headData) < 3) {
#if PT_DISPATCH_RETAIN_RELEASE
                    if (allData) dispatch_release(allData);
#endif
                    disConnectedTag = YES;
                    dispatch_semaphore_signal(headLock);
                    return;
                }
                
                
                uint8_t *heard = NULL;
                size_t size = 0;
                
                PT_PRECISE_LIFETIME dispatch_data_t contiguousData = dispatch_data_create_map(headData, (const void **)&heard, &size); // precise lifetime guarantees bytes in frame will stay valid till the end of scope
#if PT_DISPATCH_RETAIN_RELEASE
                dispatch_release(allData);
#endif
                if (*heard == 0xff &&
                    *(heard + 1) == 0x56 &&
                    *(heard + 2) == 0x53) {
                    findHead = YES;
                }
                
                
#if PT_DISPATCH_RETAIN_RELEASE
                dispatch_release(contiguousData);
#endif
            }
            dispatch_semaphore_signal(headLock);
        });
        
        dispatch_semaphore_wait(headLock, DISPATCH_TIME_FOREVER);
        
        
        if (findHead ||
            disConnectedTag) {
            headLock = 0;
            break;
        }
    }
    
    if (disConnectedTag) {
        callback(nil,NULL);
        return;
    }
    
    __block dispatch_data_t allData = NULL;
    
    dispatch_io_read(channel, 0, sizeof(PTFrameHeard), queue_, ^(bool done, dispatch_data_t data, int error) {
        //NSLog(@"dispatch_io_read: done=%d data=%p error=%d", done, data, error);
        size_t dataSize = data ? dispatch_data_get_size(data) : 0;
        
        if (dataSize) {
            if (!allData) {
                allData = data;
#if PT_DISPATCH_RETAIN_RELEASE
                dispatch_retain(allData);
#endif
            } else {
#if PT_DISPATCH_RETAIN_RELEASE
                dispatch_data_t allDataPrev = allData;
                allData = dispatch_data_create_concat(allData, data);
                dispatch_release(allDataPrev);
#else
                allData = dispatch_data_create_concat(allData, data);
#endif
            }
        }
        
        if (done) {
            if (error != 0) {
                callback([[NSError alloc] initWithDomain:NSPOSIXErrorDomain code:error userInfo:nil], NULL);
                return;
            }
            
            if (dataSize == 0) {
                callback(nil, NULL);
                return;
            }
            
            if (!allData || dispatch_data_get_size(allData) < sizeof(PTFrameHeard)) {
#if PT_DISPATCH_RETAIN_RELEASE
                if (allData) dispatch_release(allData);
#endif
                callback([[NSError alloc] initWithDomain:PTProtocolErrorDomain code:0 userInfo:nil], NULL);
                return;
            }
            
            PTFrameHeard *heard = NULL;
            size_t size = 0;
            
            PT_PRECISE_LIFETIME dispatch_data_t contiguousData = dispatch_data_create_map(allData, (const void **)&heard, &size); // precise lifetime guarantees bytes in frame will stay valid till the end of scope
#if PT_DISPATCH_RETAIN_RELEASE
            dispatch_release(allData);
#endif
            if (!contiguousData) {
                callback([[NSError alloc] initWithDomain:NSPOSIXErrorDomain code:ENOMEM userInfo:nil], NULL);
                return;
            }
//            heard->length = ntohl(heard->length);
//            heard->num = ntohl(heard->num);
//            heard->check = ntohl(heard->check);
//            heard->type = ntohl(heard->type);
            callback(nil, heard);
            
#if PT_DISPATCH_RETAIN_RELEASE
            dispatch_release(contiguousData);
#endif
        }
    });
}


- (void)readPayloadOfSize:(size_t)payloadSize overChannel:(dispatch_io_t)channel callback:(void(^)(NSError *error, dispatch_data_t contiguousData, const uint8_t *buffer, size_t bufferSize))callback {
    __block dispatch_data_t allData = NULL;
    dispatch_io_read(channel, 0, payloadSize, queue_, ^(bool done, dispatch_data_t data, int error) {
        //NSLog(@"dispatch_io_read: done=%d data=%p error=%d", done, data, error);
        size_t dataSize = dispatch_data_get_size(data);
        
        if (dataSize) {
            if (!allData) {
                allData = data;
#if PT_DISPATCH_RETAIN_RELEASE
                dispatch_retain(allData);
#endif
            } else {
#if PT_DISPATCH_RETAIN_RELEASE
                dispatch_data_t allDataPrev = allData;
                allData = dispatch_data_create_concat(allData, data);
                dispatch_release(allDataPrev);
#else
                allData = dispatch_data_create_concat(allData, data);
#endif
            }
        }
        
        if (done) {
            if (error != 0) {
#if PT_DISPATCH_RETAIN_RELEASE
                if (allData) dispatch_release(allData);
#endif
                callback([[NSError alloc] initWithDomain:NSPOSIXErrorDomain code:error userInfo:nil], NULL, NULL, 0);
                return;
            }
            
            if (dataSize == 0) {
#if PT_DISPATCH_RETAIN_RELEASE
                if (allData) dispatch_release(allData);
#endif
                callback(nil, NULL, NULL, 0);
                return;
            }
            
            uint8_t *buffer = NULL;
            size_t bufferSize = 0;
            PT_PRECISE_LIFETIME dispatch_data_t contiguousData = NULL;
            
            if (allData) {
                contiguousData = dispatch_data_create_map(allData, (const void **)&buffer, &bufferSize);
#if PT_DISPATCH_RETAIN_RELEASE
                dispatch_release(allData); allData = NULL;
#endif
                if (!contiguousData) {
                    callback([[NSError alloc] initWithDomain:NSPOSIXErrorDomain code:ENOMEM userInfo:nil], NULL, NULL, 0);
                    return;
                }
            }
            
            
            callback(nil, contiguousData, buffer, bufferSize);
#if PT_DISPATCH_RETAIN_RELEASE
            if (contiguousData) dispatch_release(contiguousData);
#endif
        }
    });
}


- (void)readAndDiscardDataOfSize:(size_t)size overChannel:(dispatch_io_t)channel callback:(void(^)(NSError*, BOOL))callback {
    dispatch_io_read(channel, 0, size, queue_, ^(bool done, dispatch_data_t data, int error) {
        if (done && callback) {
            size_t dataSize = data ? dispatch_data_get_size(data) : 0;
            callback(error == 0 ? nil : [[NSError alloc] initWithDomain:NSPOSIXErrorDomain code:error userInfo:nil], dataSize == 0);
        }
    });
}


- (void)readFramesOverChannel:(dispatch_io_t)channel onFrame:(void (^)(NSError *, PTFrameHeard *, dispatch_block_t))onFrame {
    
    [self readFrameOverChannel:channel callback:^(NSError *error, PTFrameHeard *heard) {
        onFrame(error, heard, ^{
            [self readFramesOverChannel:channel onFrame:onFrame];
        });
    }];
}


@end


@interface _PTDispatchData : NSObject {
    dispatch_data_t dispatchData_;
}
@end
@implementation _PTDispatchData
- (id)initWithDispatchData:(dispatch_data_t)dispatchData {
    if (!(self = [super init])) return nil;
    dispatchData_ = dispatchData;
#if PT_DISPATCH_RETAIN_RELEASE
    dispatch_retain(dispatchData_);
#endif
    return self;
}
- (void)dealloc {
#if PT_DISPATCH_RETAIN_RELEASE
    if (dispatchData_) dispatch_release(dispatchData_);
#endif
}
@end

@implementation NSData (PTProtocol)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-getter-return-value"

- (dispatch_data_t)createReferencingDispatchData {
    // Note: The queue is used to submit the destructor. Since we only perform an
    // atomic release of self, it doesn't really matter which queue is used, thus
    // we use the current calling queue.
    return dispatch_data_create((const void*)self.bytes, self.length, dispatch_get_main_queue(), ^{
        // trick to have the block capture the data, thus retain/releasing
        [self length];
    });
}

#pragma clang diagnostic pop

+ (NSData *)dataWithContentsOfDispatchData:(dispatch_data_t)data {
    if (!data) {
        return nil;
    }
    uint8_t *buffer = NULL;
    size_t bufferSize = 0;
    PT_PRECISE_LIFETIME dispatch_data_t contiguousData = dispatch_data_create_map(data, (const void **)&buffer, &bufferSize);
    if (!contiguousData) {
        return nil;
    }
    
    _PTDispatchData *dispatchDataRef = [[_PTDispatchData alloc] initWithDispatchData:contiguousData];
    NSData *newData = [NSData dataWithBytesNoCopy:(void*)buffer length:bufferSize freeWhenDone:NO];
#if PT_DISPATCH_RETAIN_RELEASE
    dispatch_release(contiguousData);
#endif
    static const bool kDispatchDataRefKey;
    objc_setAssociatedObject(newData, (const void*)kDispatchDataRefKey, dispatchDataRef, OBJC_ASSOCIATION_RETAIN);
    
    return newData;
}

@end


@implementation NSDictionary (PTProtocol)

- (dispatch_data_t)createReferencingDispatchData {
    NSError *error = nil;
    NSData *plistData = [NSPropertyListSerialization dataWithPropertyList:self format:NSPropertyListBinaryFormat_v1_0 options:0 error:&error];
    if (!plistData) {
        NSLog(@"Failed to serialize property list: %@", error);
        return nil;
    } else {
        return [plistData createReferencingDispatchData];
    }
}

// Decode *data* as a peroperty list-encoded dictionary. Returns nil on failure.
+ (NSDictionary*)dictionaryWithContentsOfDispatchData:(dispatch_data_t)data {
    if (!data) {
        return nil;
    }
    uint8_t *buffer = NULL;
    size_t bufferSize = 0;
    PT_PRECISE_LIFETIME dispatch_data_t contiguousData = dispatch_data_create_map(data, (const void **)&buffer, &bufferSize);
    if (!contiguousData) {
        return nil;
    }
    NSDictionary *dict = [NSPropertyListSerialization propertyListWithData:[NSData dataWithBytesNoCopy:(void*)buffer length:bufferSize freeWhenDone:NO] options:NSPropertyListImmutable format:NULL error:nil];
#if PT_DISPATCH_RETAIN_RELEASE
    dispatch_release(contiguousData);
#endif
    return dict;
}

@end
