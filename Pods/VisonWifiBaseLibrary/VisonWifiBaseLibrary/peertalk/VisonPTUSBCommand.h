//
//  VisonPTUSBCommand.h
//  peertalk
//
//  Created by Xu pan on 2021/6/17.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface VisonPTUSBCommand : NSObject


/// 心跳数据命令(功能字01)
+(NSData *)heartBeatCommand;

/// 请求中继信息命令(功能字02)
+(NSData *)relayInfoCommand;

/// 请求图传(功能字03)
+(NSData *)videoCommand;

/// 拍照命令(功能字04)
/// @param sizeType 差值尺寸
/// @param rec 是否接收原图
+(NSData *)photoCommandWithPhtotSizeType:(char)sizeType
                    receiveOriginalPhtot:(BOOL)rec;

/// 飞机连接状态(功能字07)
+(NSData *)droneConnectStatusCommand;

@end

NS_ASSUME_NONNULL_END
