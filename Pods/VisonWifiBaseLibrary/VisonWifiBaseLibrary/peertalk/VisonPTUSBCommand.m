//
//  VisonPTUSBCommand.m
//  peertalk
//
//  Created by Xu pan on 2021/6/17.
//

#import "VisonPTUSBCommand.h"

@implementation VisonPTUSBCommand

#pragma mark - 心跳命令
+(NSData *)heartBeatCommand{
    unsigned char bytes[10] ={0};
    for (int i = 0; i < 10; i++) {
        bytes[i] = i%10;
    }
    return [NSData dataWithBytes:bytes length:10];
}



#pragma mark - 请求中继信息命令
+(NSData *)relayInfoCommand{
    unsigned char bytes[1] ={0};
    return [NSData dataWithBytes:bytes length:1];
}



#pragma mark - 请求图传命令
+(NSData *)videoCommand{
    unsigned char bytes[11] ={0};
    for (int i = 1; i < 12; i++) {
        if (i < 10) {
            bytes[i] = i;
        }else{
            bytes[i] = 0x28;
        }
    }
    return [NSData dataWithBytes:bytes length:11];
}

#pragma mark - 拍照命令
+(NSData *)photoCommandWithPhtotSizeType:(char)sizeType
                    receiveOriginalPhtot:(BOOL)rec{
    unsigned char bytes[14] ={0};
    for (int i = 0; i < 14; i++) {
        if (i < 10) {
            bytes[i] = i;
        }else if(i == 10 || i == 11){
            bytes[i] = 11;
        }else if(i == 12){
            bytes[i] = sizeType;
        }else if(i == 13){
            bytes[i] = rec ? 0 : 1;
        }
    }
    return [NSData dataWithBytes:bytes length:14];
}

#pragma mark - 飞机连接状态
+(NSData *)droneConnectStatusCommand{
    unsigned char bytes[1] ={0};
    return [NSData dataWithBytes:bytes length:1];
}
@end
