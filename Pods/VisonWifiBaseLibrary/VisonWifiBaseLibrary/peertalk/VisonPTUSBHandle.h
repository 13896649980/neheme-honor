//
//  VisonPTUSBHandle.h
//  peertalk
//
//  Created by Xu pan on 2021/6/17.
//

#import <Foundation/Foundation.h>


#define USBTakePhotoData @"USBTakePhotoData"
#define USBImageData @"USBImageData"
#define USBDISCONNECT @"USBDISCONNECT"



@interface VisonPTUSBHandle : NSObject

@property   (nonatomic,assign)      BOOL        isConnected;
@property   (nonatomic,assign)      BOOL        isListening;
+(instancetype)shareInstance;
/// 开始监听USB连接
+(void)starLinteningUSBConnecStatus;

/// 主动断开连接
+(void)close;

/// usb数据发送
/// @param data 数据
/// @param type  type
/// @param callback 回调
+(void)sendData:(NSData *)data
      frameType:(uint16_t)type
       callback:(void (^)(NSError *))callback;


@end

