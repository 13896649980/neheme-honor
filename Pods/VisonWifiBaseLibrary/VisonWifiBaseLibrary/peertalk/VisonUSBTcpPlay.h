//
//  VisonUSBTcpPlay.h
//  GPS Project
//
//  Created by pyz on 2021/6/18.
//  Copyright © 2021 Xu pan. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "Includes.h"

#ifdef __cplusplus
extern "C" {
#endif
    typedef void (*fDataCallBack)(void* handle, VFrameHead_t* head, char* buf, U32 bufLen, void* user);
    typedef void (*H264DataCallBack)(char* buf, U32 bufLen);
    void* StartUSBHMDTcpPlay(DeviceType_e dtype, fDataCallBack fun, void* user);
    void StopUSBHMDTcpPlay(void* handle);
    H264DataCallBack* useRec();
#ifdef __cplusplus
}
#endif



@interface VisonUSBTcpPlay : NSObject
{
@public
    void *handle;

}
- (void)startObserver;
@end


