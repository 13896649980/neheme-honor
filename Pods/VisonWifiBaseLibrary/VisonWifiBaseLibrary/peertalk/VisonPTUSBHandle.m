//
//  VisonPTUSBHandle.m
//  peertalk
//
//  Created by Xu pan on 2021/6/17.
//

#import "VisonPTUSBHandle.h"
#import "Peertalk.h"
#import "DataTransferManager.h"
#import "VisonPTUSBCommand.h"

static const int VisonPTUSBPortNumber = 2345;

@interface VisonPTUSBHandle()<PTChannelDelegate>{
    __weak PTChannel *serverChannel_;
    __weak PTChannel *peerChannel_;
    int count;
}

@end

@implementation VisonPTUSBHandle

static VisonPTUSBHandle *_handle = nil;
static dispatch_once_t onceToken;
+(instancetype)shareInstance{
    
    dispatch_once(&onceToken, ^{
        _handle = [[self alloc]init];
    });
    return _handle;
}

-(instancetype)init{
    if (self = [super init]) {
        
        NSTimer * timer = [NSTimer timerWithTimeInterval:1 repeats:YES block:^(NSTimer * _Nonnull timer) {
            [VisonPTUSBHandle sendData:[VisonPTUSBCommand heartBeatCommand] frameType:PTFrameTypeHeartBeat callback:nil];            
        }];
        [[NSRunLoop currentRunLoop]addTimer:timer forMode:NSRunLoopCommonModes];
        
    }
    return self;
}

-(BOOL)isConnected{
    if (peerChannel_) {
        return peerChannel_.isConnected;
    }
    return NO;
}

-(BOOL)isListening{
    if (peerChannel_) {
        return peerChannel_.isListening;
    }
    return NO;
}


#pragma mark - 开始监听USB连接
+(void)starLinteningUSBConnecStatus{
    PTChannel *channel = [PTChannel channelWithDelegate:[VisonPTUSBHandle shareInstance]];
    [channel listenOnPort:VisonPTUSBPortNumber IPv4Address:INADDR_LOOPBACK callback:^(NSError *error) {
        if (error) {
            NSLog(@"Error:%@",[NSString stringWithFormat:@"Failed to listen on 127.0.0.1:%d: %@", VisonPTUSBPortNumber, error]);
           
        } else {
            NSLog(@"success:%@",[NSString stringWithFormat:@"Listening on 127.0.0.1:%d", VisonPTUSBPortNumber]);
            [VisonPTUSBHandle shareInstance]->serverChannel_ = channel;
        }
    }];
    
    
}

#pragma mark - 关闭连接
+(void)close{
    if ([VisonPTUSBHandle shareInstance]->peerChannel_) {
        NSLog(@"%s",__FUNCTION__);
        [[VisonPTUSBHandle shareInstance]->peerChannel_ close];
    }
}

#pragma mark - 发送数据
+(void)sendData:(NSData *)data frameType:(uint16_t)type callback:(void (^)(NSError * _Nonnull))callback{

    if ([VisonPTUSBHandle shareInstance]->peerChannel_&& [VisonPTUSBHandle shareInstance]->peerChannel_.isConnected) {
        [[VisonPTUSBHandle shareInstance]->peerChannel_ sendFrameOfType:type data:data callback:callback];
    }else{
//        NSLog(@"peerChannel_ = nil,请检查是否连接设备");
        return;
    }
}


// 监听peerChannel_ 状态 KVO 回调
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    
    NSLog(@"observeValueForKeyPath == %@",keyPath);
    
    if ([keyPath isEqualToString:@"isConnected"]) {
        self.isConnected = peerChannel_.isConnected;
        NSLog(@"isConnected : %d",peerChannel_.isConnected);
    }
    
    if ([keyPath isEqualToString:@"isListening"]) {
        self.isConnected = peerChannel_.isListening;
        NSLog(@"isListening : %d",peerChannel_.isListening);
    }
}


#pragma mark - PTChannelDelegate
//有一个新的USB设备连接
- (void)ioFrameChannel:(PTChannel*)channel didAcceptConnection:(PTChannel*)otherChannel fromAddress:(PTAddress*)address {
    if (peerChannel_) {
        [peerChannel_ cancel];
    }
    peerChannel_ = otherChannel;
    peerChannel_.userInfo = address;
    NSLog(@"新的USB设备连接：%@",address);
    
    [peerChannel_ addObserver:self forKeyPath:@"isConnected" options:NSKeyValueObservingOptionNew context:nil];
    [peerChannel_ addObserver:self forKeyPath:@"isListening" options:NSKeyValueObservingOptionNew context:nil];

}

//usb连接断开
- (void)ioFrameChannel:(PTChannel*)channel didEndWithError:(NSError*)error {
    if (error) {
        NSLog(@"连接断开：%@ ended with error: %@",channel, error);
    } else {
        NSLog(@"连接断开：Disconnected from %@",channel.userInfo);
    }
    [[NSNotificationCenter defaultCenter]postNotificationName:USBDISCONNECT object:nil];
}

//询问此帧消息是否被接收
- (BOOL)ioFrameChannel:(PTChannel*)channel shouldAcceptFrameOfHeard:(PTFrameHeard *)heard{
    //检查功能字
    switch (heard->type) {
        case PTFrameTypeHeartBeat: //心跳
        case PTFrameTypeRelayInfo: //中继信息
        case PTFrameTypeVideo: //图传
        case PTFrameTypePhoto: //拍照
        case PTFrameTypeTcpData: //tcp数据
        case PTFrameTypeUdpData: //udp数据
        case PTFrameTypeDroneConnectStatus: //飞机连接中继的状态
        case PTFrameTypeDeviceMedia: //sd卡相册
            return YES;
            break;
        default:
            NSLog(@"Unexpected frame of type %u", heard->type);
            return NO;
            break;
    }
}

//一帧消息回调
- (void)ioFrameChannel:(PTChannel*)channel didReceiveFrame:(PTFrame *)ptFrame{
    count++;
    
    NSData * data = [NSData dataWithBytes:ptFrame->buf length:ptFrame->heard.length];

    NSLog(@"USB数据%@",data);

    
    switch (ptFrame->heard.type) {
        case PTFrameTypeHeartBeat: //心跳
            
            break;
        case PTFrameTypeRelayInfo: //中继信息
            
            break;
        case PTFrameTypeVideo: //图传
            [[NSNotificationCenter defaultCenter]postNotificationName:USBImageData object:[NSData dataWithBytes:ptFrame->buf length:ptFrame->heard.length]];
            break;
        case PTFrameTypePhoto: //拍照
            [[NSNotificationCenter defaultCenter]postNotificationName:USBTakePhotoData object:[NSData dataWithBytes:ptFrame->buf length:ptFrame->heard.length]];
            break;
        case PTFrameTypeTcpData: //tcp数据
            [[NSNotificationCenter defaultCenter]postNotificationName:TransferTCPData object:[NSData dataWithBytes:ptFrame->buf length:ptFrame->heard.length]];
            break;
        case PTFrameTypeUdpData: //udp数据
            [[NSNotificationCenter defaultCenter]postNotificationName:TransferUDPData object:[NSData dataWithBytes:ptFrame->buf length:ptFrame->heard.length]];
            break;
        case PTFrameTypeDroneConnectStatus: //飞机连接中继的状态
            
            break;
        case PTFrameTypeDeviceMedia: //sd卡相册
            
            break;
            
        default:
            break;
    }
    free(ptFrame->buf);
}



@end
