//
//  VisonWifiBaseLibraryUtility.m
//  FuncCode
//
//  Created by Xu pan on 2020/11/5.
//  Copyright © 2020 Xu pan. All rights reserved.
//

#import "VisonWifiBaseLibraryUtility.h"
#import "WIFIDeviceModel.h"
#import "AsyncSocket.h"
#import "VisonPTUSBHandle.h"
#import "PTProtocol.h"

typedef void(^getSourceImageProgressBlock)(float);
typedef void(^getSourceImageCompleteBlock)(BOOL, UIImage *);

@interface VisonWifiBaseLibraryUtility()<AsyncSocketDelegate>

@property(nonatomic,strong)         AsyncSocket         *tcpSocket;
@property(nonatomic,assign)         size_t              totaPiclLen; //图片数据总大小
@property(nonatomic,assign)         size_t              curPicLen; //当前接受的图片数据大小
@property(nonatomic,strong)         NSMutableData       *picData;//接受的图片数据Data
@property(nonatomic,strong)         NSTimer             *timeOutTimer;
@property bool isRecvDataInDurtion;

@property(nonatomic,copy)         getSourceImageProgressBlock             sourceImageprogressBlock;
@property(nonatomic,copy)         getSourceImageCompleteBlock             sourceImageCompleteBlock;

@end

@implementation VisonWifiBaseLibraryUtility

#pragma mark - 是否需要延迟播放

static VisonWifiBaseLibraryUtility *utility = nil;
+(instancetype)shareInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        utility = [[self alloc]init];
        [[NSNotificationCenter defaultCenter]addObserver:utility selector:@selector(handlePicdata:) name:USBTakePhotoData object:nil];
    });
    return utility;
}
-(AsyncSocket *)tcpSocket{
    if (!_tcpSocket) {
        _tcpSocket = [[AsyncSocket alloc]initWithDelegate:self];
        NSError *error = nil;
        if(!([_tcpSocket connectToHost:DEFAULT_HOST_IP onPort:8888 error:&error])){
            NSLog(@"获取图片Socket连接失败:%@",error);
        }
    }
   
    return _tcpSocket;
}



#pragma mark - 是否需要延时播放
+(BOOL)isNeedDelayPlay{
    switch ([WIFIDeviceModel shareInstance].ID) {
        case 10043://hi2k
        case 10044://hi25k
        case 10042://udp27k
        case 10040://udp2k
        case 10501://8856_8812cu_4000_1280
            return YES;
            break;
        default:
            return NO;
            break;
    }
}

#pragma mark - 需要延迟播放的帧数
+(int)delayFrameCount{
    switch ([WIFIDeviceModel shareInstance].ID) {
        case 10044://hi25k
            return 5;
            break;
        case 10043://hi2k
        case 10042://udp27k
        case 10040://udp2k
        case 10501://8856_8812cu_4000_1280
            return 2;
        default:
            return 0;
            break;
    }
}

#pragma mark - 需要延迟播放的帧率
+(int)delayFrameRate{
    switch ([WIFIDeviceModel shareInstance].ID) {
        case 10043://hi2k
        case 10044://hi25k
            return 30;
            break;
        case 10042://udp27k
        case 10040://udp2k
            return 25;
            break;
        case 10501://8856_8812cu_4000_1280
            return 15;
            break;
        default:
            return 30;
            break;
    }
}

#pragma mark - 获取所有老版本版名称
+(NSArray *)getTotalOldVersionHardwareNames{
    return @[@"VGA\0\0",
             @"MJVGA\0",
             @"VSVGA\0",
             @"VGA2",
             @"720P\0",
             @"1080P",
             @"UDP720P",
             @"UDP720PYU",
             @"RTSP1080P",
             @"RTSP720P",
             @"UDP2K",
             @"UDP2K-30",
             @"UDP27K",
             @"UDP4K",
             @"1080P-5G",
             @"1080P-5g",
             @"720P-5G",
             @"720P-5g",
             @"1080P-I",
             @"HI2K",
             @"HI25K",
             @"HI4K",
             @"FH8626_ssv6x5x_24g_1920_1280",
             @"FH8626_ssv6x5x_5g_1920_1280",
             @"8856_8812cu_4000_1280",
             @"Mr100_ssv6x5x_1920_1920",
             @"Mr100_ssv6x5x_1920_1280",
             @"Mr100_8801_1920_1920",
             @"Mr100_8801_1920_1280",
             @"Mr100_ssv6x5x_2048_2048",
             @"Mr100_8801_2048_2048",
             @"Mr100_ssv6x5x_4000_2048",
             @"Mr100_ssv6x5x_4000_1920",
             @"Mr100_ssv6x5x_4000_1280",
             @"Mr100_8801_4000_2048",
             @"Mr100_8801_4000_1920",
             @"Mr100_8801_4000_1280",
             @"872ET_320_320",
             @"872AT_640_640"];
}

+(UIImage *)scaleImageWithOriginalImage:(UIImage *)originalImage
                             targetSize:(CGSize)targetSize{
    // Create a graphics image context
    CGSize newSize = targetSize;
    @autoreleasepool {
        UIGraphicsBeginImageContext(newSize);
        // Tell the old image to draw in this new context, with the desired
        // new size
        [originalImage drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
        // Get the new image from the context
        UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
        // End the context
        UIGraphicsEndImageContext();
        // Return the new image.
        return newImage;
    }
}

#pragma mark - 录像的默认规则
+(CGSize)getTheRecordSize{
    
    switch ([WIFIDeviceModel shareInstance].ID) {
        case 13://8626_5g_8801_8603_1080P
            return PhotoSize_1080p;
            break;
        case 1://872
        case 2://872
        case 10101://872
        case 10102://872
            return PhotoSize_720p;
            break;
        case 10044://hi25k
            return PhotoSize_720p;
        default: //回传分辨率
            return [[WIFIDeviceModel shareInstance] getPixelSize:[WIFIDeviceModel shareInstance].mainLensTransmissionPixelSize];
            break;
    }
}


#pragma mark - 拍照的默认规则
+(CGSize)getTheTakePhotoSize{
    CGSize size = [[WIFIDeviceModel shareInstance] getPixelSize:[WIFIDeviceModel shareInstance].mainLensTransmissionPixelSize];
    
    if ([WIFIDeviceModel shareInstance].realPixelSizeID>=20&&[WIFIDeviceModel shareInstance].realPixelSizeID<=29) {
        size =  PhotoSize_720p;
    }else if ([WIFIDeviceModel shareInstance].realPixelSizeID>=30&&[WIFIDeviceModel shareInstance].realPixelSizeID<=39){
        size =  PhotoSize_1080p;
    }
    
    switch ([WIFIDeviceModel shareInstance].ID) {
        case 10051://udp4k
            size =   PhotoSize_4K;
            break;
        case 10031://1080p-i
            size =   PhotoSize_1080p;
            break;
        case 10401://Mr100_ssv6x5x_2048_2048
        case 10402://Mr100_8801_2048_2048
            size =   PhotoSize_4K;
            break;
        case 13://8626_5g_8801_8603_1080P
            size =   PhotoSize_1080p;
            break;
        case 1://872
        case 2://872
            size =   PhotoSize_720p;
            break;
        case 10044://hi25k
            size =   PhotoSize_720p;
            break;
        default:
            break;
    }
    return size;
}


#pragma mark ------------------ 板端取原图 ------------------
+(void)getTheSourceImageInProgress:(void (^)(float))progress
                          complete:(void (^)(BOOL, UIImage * _Nonnull))complate{
    if (!complate) {
        return;
    }
    
    VisonWifiBaseLibraryUtility *utility = [VisonWifiBaseLibraryUtility shareInstance];
    utility.sourceImageprogressBlock = progress;
    utility.sourceImageCompleteBlock = complate;
    utility.isRecvDataInDurtion = NO;
    utility.totaPiclLen = 0;
    utility.curPicLen = 0;
    
    if ([VisonPTUSBHandle shareInstance].isConnected) {
        char str[14] ;
        str[0] = 0x00;
        str[1] = 0x01;
        str[2] = 0x02;
        str[3] = 0x03;
        str[4] = 0x04;
        str[5] = 0x05;
        str[6] = 0x06;
        str[7] = 0x07;
        str[8] = 0x08;
        str[9] = 0x09;
        str[10] = 0x11;
        str[11] = 0x11;
        str[12] = 0x00;
        str[13] = 0x00;
        NSData *data = [NSData dataWithBytes:str length:14];
        [VisonPTUSBHandle sendData:data frameType:PTFrameTypePhoto callback:nil];
    }else{
        
        utility.tcpSocket = [[AsyncSocket alloc]initWithDelegate:self];
        NSError *error = nil;
        if(!([utility.tcpSocket connectToHost:DEFAULT_HOST_IP onPort:8888 error:&error])){
            NSLog(@"获取图片Socket连接失败:%@",error);
            [utility receivedDataOperation];
        }else{
            char str[12] ;
            str[0] = 0x00;
            str[1] = 0x01;
            str[2] = 0x02;
            str[3] = 0x03;
            str[4] = 0x04;
            str[5] = 0x05;
            str[6] = 0x06;
            str[7] = 0x07;
            str[8] = 0x08;
            str[9] = 0x09;
            str[10] = 0x11;
            str[11] = 0x11;
            
            NSData *data = [NSData dataWithBytes:str length:12];
            [utility.tcpSocket writeData:data withTimeout:-1 tag:0];
            [utility.tcpSocket readDataWithTimeout:-1 tag:0];
            
            if (utility.timeOutTimer) {
                [utility.timeOutTimer invalidate];
                utility.timeOutTimer = nil;
            }
        }
    }
    
    utility.timeOutTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:[VisonWifiBaseLibraryUtility shareInstance] selector:@selector(timerOutAction) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:utility.timeOutTimer forMode:NSRunLoopCommonModes];

}

-(void)receivedDataOperation{
    if (_tcpSocket) {
        [_tcpSocket disconnect];
        _tcpSocket.delegate = nil;
        _tcpSocket = nil;
    }
    self.picData = nil;
    self.totaPiclLen = self.curPicLen = 0;
    
    if (self.timeOutTimer) {
        [self.timeOutTimer invalidate];
        self.timeOutTimer = nil;
    }
}



-(void)timerOutAction{
    
    if (self.isRecvDataInDurtion) {
        self.isRecvDataInDurtion = NO;
    }else{
        
        [self receivedDataOperation];
        
        if (self.sourceImageCompleteBlock) {
            self.sourceImageCompleteBlock(NO, nil);
        }
        NSLog(@"2秒钟无图片数据传输");
    }
}

#pragma mark -TcpSocketDelegate
- (void)onSocket:(AsyncSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port{
 if (sock == self.tcpSocket){
        NSLog(@"照片连接 Socket:%p didConnectToHost:%@ port:%hu", sock, host, port);
    }
}

- (void)onSocketDidDisconnect:(AsyncSocket *)sock{
    if(sock == self.tcpSocket){
        self.tcpSocket.delegate = nil;
        self.tcpSocket = nil;
        NSLog(@"照片连接断开 onSocketDidDisconnect!");
    }
}
- (void)onSocket:(AsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag{
    @synchronized (self){
        if (sock == self.tcpSocket){
            [self handlePicTcpSocket:sock data:data withTag:tag];//只发命令不处理
        }
    }
    [sock readDataWithTimeout:-1 tag:tag];
}

#pragma mark - 接受照片的数据
- (void)handlePicTcpSocket:(AsyncSocket *)sock data:(NSData *)data withTag:(long)tag{
    //ff ff 04 03 02 01 data data data data
    self.isRecvDataInDurtion = YES;
    Byte *picbytes = (Byte *)data.bytes;
    if (picbytes[0] == 0xff && picbytes[1] == 0xff){
        
        uint32_t len = *(uint32_t *)&picbytes[2];
        self.picData = [NSMutableData data];
        [self.picData appendBytes:picbytes+6 length:data.length-6];
        
        self.totaPiclLen = htonl(len);
        self.curPicLen = data.length-6;
    }else{
        [self.picData appendBytes:picbytes length:data.length];
        self.curPicLen += data.length;
    }
    
    //回调进度
    if (self.sourceImageprogressBlock) {
        self.sourceImageprogressBlock((self.curPicLen*1.0)/self.totaPiclLen);
    }
    
    //接收完成
    if (self.totaPiclLen && self.totaPiclLen == self.curPicLen) {
        if (self.sourceImageCompleteBlock) {
            NSLog(@"接受图片完成");
            UIImage *picImg = [[UIImage alloc] initWithData:self.picData];
            picImg = [VisonWifiBaseLibraryUtility scaleImageWithOriginalImage:picImg targetSize:[VisonWifiBaseLibraryUtility getTheTakePhotoSize]];
            self.sourceImageCompleteBlock(YES, picImg);
        }
        
        [self receivedDataOperation];
    }
}

#pragma mark - 接受照片的数据
- (void)handlePicdata:(NSNotification *)ns{
    //ff ff 04 03 02 01 data data data data
    NSData * data = ns.object;
    self.isRecvDataInDurtion = YES;
    Byte *picbytes = (Byte *)data.bytes;
    if (picbytes[0] == 0xff && picbytes[1] == 0xff){
        
        uint32_t len = *(uint32_t *)&picbytes[2];
        self.picData = [NSMutableData data];
        [self.picData appendBytes:picbytes+6 length:data.length-6];
        
        self.totaPiclLen = htonl(len);
        self.curPicLen = data.length-6;
    }else{
        [self.picData appendBytes:picbytes length:data.length];
        self.curPicLen += data.length;
    }
    
    //回调进度
    if (self.sourceImageprogressBlock) {
        self.sourceImageprogressBlock((self.curPicLen*1.0)/self.totaPiclLen);
    }
    
    
    //接收完成
    if (self.totaPiclLen && self.totaPiclLen == self.curPicLen) {
        if (self.sourceImageCompleteBlock) {
            NSLog(@"接受图片完成");
            UIImage *picImg = [[UIImage alloc] initWithData:self.picData];
            picImg = [VisonWifiBaseLibraryUtility scaleImageWithOriginalImage:picImg targetSize:[VisonWifiBaseLibraryUtility getTheTakePhotoSize]];
            self.sourceImageCompleteBlock(YES, picImg);
        }
        
        [self receivedDataOperation];
    }
}

#pragma mark - 复制YUV CVPixelBufferRef
+(CVPixelBufferRef)YUVBufferCopyWithPixelBuffer:(CVPixelBufferRef)pixelBuffer{
    
    // Get pixel buffer info
    CVPixelBufferLockBaseAddress(pixelBuffer, 0);
    int bufferWidth = (int)CVPixelBufferGetWidth(pixelBuffer);
    int bufferHeight = (int)CVPixelBufferGetHeight(pixelBuffer);
    OSType pixelFormat = CVPixelBufferGetPixelFormatType(pixelBuffer);
    
    NSDictionary *pixelBufferAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                           @(CVPixelBufferGetBytesPerRowOfPlane(pixelBuffer, 0)), kCVPixelBufferBytesPerRowAlignmentKey,
                                           nil];
    
    // Copy the pixel buffer
    CVPixelBufferRef pixelBufferCopy = NULL;
    CVReturn status = CVPixelBufferCreate(kCFAllocatorDefault,
                                          bufferWidth,
                                          bufferHeight,
                                          pixelFormat,
                                          (__bridge CFDictionaryRef)(pixelBufferAttributes),
                                          &pixelBufferCopy);
    if (status != kCVReturnSuccess) {
        NSLog(@"YUVBufferCopyWithPixelBuffer :: failed");
    }
    
    CVPixelBufferLockBaseAddress(pixelBufferCopy, 0);
    
    
    
    size_t bytesPerRowY = CVPixelBufferGetBytesPerRowOfPlane(pixelBufferCopy, 0);
    long Y_Height = CVPixelBufferGetHeightOfPlane(pixelBufferCopy, 0);
    // Y
    uint8_t *YDestPlane = (uint8_t *) CVPixelBufferGetBaseAddressOfPlane(pixelBufferCopy, 0);
    memset(YDestPlane, 0x80, Y_Height * bytesPerRowY);
    uint8_t *yPlane = CVPixelBufferGetBaseAddressOfPlane(pixelBuffer, 0);
    for (int row = 0; row < Y_Height; ++row) {
        memcpy(YDestPlane + row * bytesPerRowY,
                yPlane + row * bytesPerRowY,
                bytesPerRowY);
    }
 
    size_t bytesPerRowUV = CVPixelBufferGetBytesPerRowOfPlane(pixelBufferCopy, 1);
    long UV_Height = CVPixelBufferGetHeightOfPlane(pixelBufferCopy, 1);

    // Chrominance
    uint8_t *uvDestPlane = (uint8_t *) CVPixelBufferGetBaseAddressOfPlane(pixelBufferCopy, 1);
    memset(uvDestPlane, 0x80, UV_Height * bytesPerRowUV);
    uint8_t *uvPlane = CVPixelBufferGetBaseAddressOfPlane(pixelBuffer, 1);
    for (int row = 0; row < UV_Height; ++row) {
        memcpy(uvDestPlane + row * bytesPerRowUV,
                uvPlane + row * bytesPerRowUV,
                bytesPerRowUV);
    }
    
    /*
    uint8_t *yDestPlane = CVPixelBufferGetBaseAddressOfPlane(pixelBufferCopy, 0);
    //YUV
    uint8_t *yPlane = CVPixelBufferGetBaseAddressOfPlane(pixelBuffer, 0);
    memcpy(yDestPlane, yPlane, bufferWidth * bufferHeight);
    uint8_t *uvDestPlane = CVPixelBufferGetBaseAddressOfPlane(pixelBufferCopy, 1);
    uint8_t *uvPlane = CVPixelBufferGetBaseAddressOfPlane(pixelBuffer, 1);
    memcpy(uvDestPlane, uvPlane, bufferWidth * bufferHeight/2);
    */
    CVPixelBufferUnlockBaseAddress(pixelBuffer, 0);
    CVPixelBufferUnlockBaseAddress(pixelBufferCopy, 0);
    
    return pixelBufferCopy;
}


#pragma mark - 复制BGRA CVPixelBufferRef
+(CVPixelBufferRef)BGRABufferCopyWithPixelBuffer:(CVPixelBufferRef)pixelBuffer{
    // Get pixel buffer info
    CVPixelBufferLockBaseAddress(pixelBuffer, 0);
    int bufferWidth = (int)CVPixelBufferGetWidth(pixelBuffer);
    int bufferHeight = (int)CVPixelBufferGetHeight(pixelBuffer);
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(pixelBuffer);
    uint8_t *baseAddress = CVPixelBufferGetBaseAddress(pixelBuffer);

    // Copy the pixel buffer
    CVPixelBufferRef pixelBufferCopy = NULL;
    CVReturn status = CVPixelBufferCreate(kCFAllocatorDefault, bufferWidth, bufferHeight, kCVPixelFormatType_32ARGB, NULL, &pixelBufferCopy);
    if (status != kCVReturnSuccess) {
        NSLog(@"BGRABufferCopyWithPixelBuffer :: failed");
    }
    CVPixelBufferLockBaseAddress(pixelBufferCopy, 0);
    uint8_t *copyBaseAddress = CVPixelBufferGetBaseAddress(pixelBufferCopy);
    memcpy(copyBaseAddress, baseAddress, bufferHeight * bytesPerRow);
    CVPixelBufferUnlockBaseAddress(pixelBuffer, 0);
    CVPixelBufferUnlockBaseAddress(pixelBufferCopy, 0);
    
    return pixelBufferCopy;
}


#pragma mark - image 转换 PixelBuffer
+(CVPixelBufferRef)pixelBufferFromCGImage:(CGImageRef)image{
    
    if (!image) {
        return NULL;
    }
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], kCVPixelBufferCGImageCompatibilityKey,
                             [NSNumber numberWithBool:YES], kCVPixelBufferCGBitmapContextCompatibilityKey,
                             nil];
    
    CVPixelBufferRef pxbuffer = NULL;
    
    CGFloat frameWidth = CGImageGetWidth(image);
    CGFloat frameHeight = CGImageGetHeight(image);
    CVReturn status = CVPixelBufferCreate(kCFAllocatorDefault,frameWidth,frameHeight,kCVPixelFormatType_32ARGB,(__bridge CFDictionaryRef) options, &pxbuffer);
    
    if ( status != kCVReturnSuccess || !pxbuffer )
        return nil;
    
    CVPixelBufferLockBaseAddress(pxbuffer, 0);
    void *pxdata = CVPixelBufferGetBaseAddress(pxbuffer);
    if (!pxdata)
        return nil;
    
    CGColorSpaceRef rgbColorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(pxdata, frameWidth, frameHeight, 8,CVPixelBufferGetBytesPerRow(pxbuffer),rgbColorSpace,(CGBitmapInfo)kCGImageAlphaNoneSkipFirst);
    CGContextConcatCTM(context, CGAffineTransformIdentity);
    CGContextDrawImage(context, CGRectMake(0, 0,frameWidth,frameHeight),  image);
    CGColorSpaceRelease(rgbColorSpace);
    CGContextRelease(context);
    
    CVPixelBufferUnlockBaseAddress(pxbuffer, 0);
    
    return pxbuffer;
}

@end
