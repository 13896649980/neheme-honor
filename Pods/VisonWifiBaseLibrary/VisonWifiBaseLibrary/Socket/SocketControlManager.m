//
//  SocketControlManager.m
//  GPS Project
//
//  Created by Xu pan on 2020/8/3.
//  Copyright © 2020 Xu pan. All rights reserved.
//

#import "SocketControlManager.h"
#import "AsyncSocket.h"
#import <ifaddrs.h>
#import "getgateway.h"
#include <net/if.h>
#import <arpa/inet.h>
#import "Device_Wifi_Config_Command.h"
#import "WIFIConnectManager.h"
#import "SocketControlManager.h"
#import "VisonWifiBaseLibraryDefine.h"
#import "StreamPreplayConfig.h"
#import "APReverseGeocoding.h"
#import "DataTransferManager.h"


@interface SocketControlManager()<AsyncUdpSocketDelegate,AsyncSocketDelegate>

@property   (nonatomic,strong)      AsyncSocket             *tcpSocket;
@property   (nonatomic,strong)      AsyncUdpSocket          *udpSocket;



@property   (nonatomic,strong)      NSTimer                 *TCPTimeoutTimer; //tcp超时计时器
@property   (nonatomic,strong)      NSTimer                 *UDP_Play_HeartBeatTimer; //UDP播放时需要持续发送ip地址

@property   (nonatomic,assign)      int                     tcpTimeoutTag; //15秒超时

@property   (nonatomic,strong)      NSTimer                 *UDP_Communication_HeartBeatTimer;
@property   (nonatomic,strong)      AsyncUdpSocket          *communication_UdpSocket;

@end

@implementation SocketControlManager


#pragma mark ---------- 获取wifi-IP地址 ----------
- (NSString *) localWiFiIPAddress{
    BOOL success;
    struct ifaddrs * addrs;
    const struct ifaddrs * cursor;
    
    success = getifaddrs(&addrs) == 0;
    if (success) {
        cursor = addrs;
        while (cursor != NULL) {
            // the second test keeps from picking up the loopback address
            if (cursor->ifa_addr->sa_family == AF_INET && (cursor->ifa_flags & IFF_LOOPBACK) == 0){
                NSString *name = [NSString stringWithUTF8String:cursor->ifa_name];
                if ([name isEqualToString:@"en0"])  // Wi-Fi adapter
                    return [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)cursor->ifa_addr)->sin_addr)];
            }
            cursor = cursor->ifa_next;
        }
        freeifaddrs(addrs);
    }
    return nil;
}

#pragma mark ---------- 获取路由网关 ----------
- (NSString *) getGateWayIP{
    struct in_addr addr;
    getdefaultgateway(&(addr.s_addr));
    return [NSString stringWithUTF8String:inet_ntoa(addr)];
}


#pragma mark ------------------------------------- 对象初始化 -------------------------------------
static SocketControlManager *_mamager = nil;
+(instancetype)shareInstance{
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _mamager = [[self alloc]init];
    });
    return _mamager;
}

-(instancetype)init{
    if (self = [super init]) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startDroneConnect) name:StartDroneConnect object:nil];
    }
    return self;
}


- (void)startDroneConnect{
    [self initTcpSocket];
    //如果是UDP通信方式
    [self creatCommnuicationUDPSocket];
    [self starUDPCommunicationSendHeartBeatPacket];
}



#pragma mark ----------- 清除数据/timer -----------
-(void)clearData{
    NSLog(@"SocketManager - 清空数据");
    
    if (self.TCPTimeoutTimer) {
        [self.TCPTimeoutTimer invalidate];
        self.TCPTimeoutTimer = nil;
        self.tcpTimeoutTag = 0;
    }
    
    if (self.tcpSocket) {
        [self.tcpSocket disconnect];
        self.tcpSocket.delegate = nil;
        self.tcpSocket = nil;
    }
    
    if (self.communication_UdpSocket) {
        [self.communication_UdpSocket close];
        self.communication_UdpSocket.delegate = nil;
        self.communication_UdpSocket = nil;
    }
    
    if (self.UDP_Play_HeartBeatTimer) {
        [self.UDP_Play_HeartBeatTimer invalidate];
        self.UDP_Play_HeartBeatTimer = nil;
    }
    
    if (self.UDP_Communication_HeartBeatTimer) {
        [self.UDP_Communication_HeartBeatTimer invalidate];
        self.UDP_Communication_HeartBeatTimer = nil;
    }
    
  
}




#pragma mark ------------------------------------- UDP -------------------------------------
-(AsyncUdpSocket *)udpSocket{
    if (!_udpSocket) {
        _udpSocket = [[AsyncUdpSocket alloc]initWithDelegate:self];
        [_udpSocket receiveWithTimeout:-1 tag:VGS_AIRPLANE_TAG];
    }
    return _udpSocket;
}

#pragma mark ----------- UPDSocketDelegate -----------
- (void)onUdpSocket:(AsyncUdpSocket *)sock didSendDataWithTag:(long)tag{
}

- (void)onUdpSocket:(AsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag dueToError:(NSError *)error{
}

- (void)onUdpSocket:(AsyncUdpSocket *)sock didNotReceiveDataWithTag:(long)tag dueToError:(NSError *)erro{
}

#pragma mark ----------- udp收到数据 -----------
- (BOOL)onUdpSocket:(AsyncUdpSocket *)sock didReceiveData:(NSData *)data withTag:(long)tag fromHost:(NSString *)host port:(UInt16)port{
    
    //协议通信UDPSocket回调
    if (self.communication_UdpSocket) {
        if (sock == self.communication_UdpSocket) {
            [[NSNotificationCenter defaultCenter]postNotificationName:TransferUDPComData object:data];
            [sock receiveWithTimeout:-1 tag:tag];
        }
    }
    
    
    [[NSNotificationCenter defaultCenter]postNotificationName:TransferUDPData object:data];
    [sock receiveWithTimeout:-1 tag:tag];
    
    return YES;
    
}

#pragma mark ------------------------------------- TCP -------------------------------------
#pragma mark ----------- 初始化TCPSocket -----------
-(void)initTcpSocket{
    NSLog(@"通信TCP初始化");
    if (!self.tcpSocket) {
        self.tcpSocket.delegate = nil;
        self.tcpSocket = nil;
        self.tcpSocket = [[AsyncSocket alloc] initWithDelegate:self];
        NSError *error = nil;
        if(!([self.tcpSocket connectToHost:DEFAULT_HOST_IP onPort:TCP_PORT error:&error])){
            NSLog(@"TCP连接错误:%@",error);
        }else{
            [[DataTransferManager shareInstance]creatTCPHeartBeatTimer];
        }
    }else{
        if (self.tcpSocket.connectedPort != 8888){
            [self.tcpSocket disconnect];
            self.tcpSocket.delegate = nil;
            self.tcpSocket = nil;
            self.tcpSocket = [[AsyncSocket alloc] initWithDelegate:self];
            NSError *error = nil;
            if(!([self.tcpSocket connectToHost:DEFAULT_HOST_IP onPort:TCP_PORT error:&error])){
                NSLog(@"TCP连接错误:%@",error);
            }else{
                [[DataTransferManager shareInstance]creatTCPHeartBeatTimer];
            }
        }
    }
}


#pragma mark ----------- tcp连接超时 -----------
-(void)creatTCPConnectionTimeoutTimerAction{
    if (self.TCPTimeoutTimer) return;
    self.TCPTimeoutTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(tcpTimeoutAction) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:self.TCPTimeoutTimer forMode:NSRunLoopCommonModes];
}

-(void)tcpTimeoutAction{
    if (self.tcpSocket.isConnected){
        self.tcpTimeoutTag++;
        if (self.tcpTimeoutTag >= 15){
            NSLog(@"tcp超时15秒，主动断开");
            self.tcpTimeoutTag = 0;
            [self.tcpSocket disconnect];
            self.tcpSocket.delegate = nil;
            self.tcpSocket = nil;
            self.tcpTimeoutTag = 0;
           
            [[DataTransferManager shareInstance]stopTCPHeartBeatTimer];
        }
    }
    
    if (!self.tcpSocket) {
        NSLog(@"tcp超时断开后重连...");
        [self initTcpSocket];
    }
}

#pragma mark ----------- TcpSocketDelegate -----------
- (void)onSocket:(AsyncSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port{
    [self creatTCPConnectionTimeoutTimerAction];
    [sock readDataWithTimeout:-1 tag:0];
    NSLog(@"通信TCPSocket已连接 Host:%@ port:%hu", host, port);
}

- (void)onSocketDidDisconnect:(AsyncSocket *)sock{
    NSLog(@"通信TCPSocket未连接");

    [[DataTransferManager shareInstance]stopTCPHeartBeatTimer];
    
    self.tcpTimeoutTag = 15;
    
    [self.tcpSocket disconnect];
    self.tcpSocket.delegate = self;
    self.tcpSocket = nil;
}

#pragma mark ----------- tcp收到数据 -----------
- (void)onSocket:(AsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag{
    
    self.tcpTimeoutTag = 0;
    
    NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    if ([string isEqualToString:@"noact\r\n"]) {
        [sock readDataWithTimeout:-1 tag:tag];
        return;
    }
    
    [[NSNotificationCenter defaultCenter]postNotificationName:TransferTCPData object:data];
    [sock readDataWithTimeout:-1 tag:tag];
}

#pragma mark ------------------------------------- Private Method -------------------------------------
#pragma mark ----------- udp通信方式 发送心跳包 -----------
-(void)starUDPCommunicationSendHeartBeatPacket{
    //新版本872 UDP通信，这里相当于是发送通信心跳包
    if ([WIFIDeviceModel shareInstance].ID == 1 ||
        [WIFIDeviceModel shareInstance].ID == 2 ||
        [WIFIDeviceModel shareInstance].ID == 10101 ||
        [WIFIDeviceModel shareInstance].ID == 10102) {
        if (self.UDP_Communication_HeartBeatTimer) {
            [self.UDP_Communication_HeartBeatTimer invalidate];
            self.UDP_Communication_HeartBeatTimer = nil;
        }
        self.UDP_Communication_HeartBeatTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(UDPCommunicationSendHeartBeatPacketAction) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:self.UDP_Communication_HeartBeatTimer forMode:NSRunLoopCommonModes];
    }
}

-(void)UDPCommunicationSendHeartBeatPacketAction{
    if (![DataTransferManager shareInstance].isConnectedDevice) return;
    [self sendUdpCommand:[Device_Wifi_Config_Command getUDPNeedSendIpAddressCommandWithIP_09:[USER_DEFAULT objectForKey:VGS_WIFI_IP]] tag:VGS_AIRPLANE_TAG];
}

#pragma mark ----------- 停止UDP通信方式 -----------
-(void)stopSendUDPCommunicationHeartBeatPacket{
    if (![DataTransferManager shareInstance].isConnectedDevice) return;
    if (self.UDP_Communication_HeartBeatTimer) {
        [self.UDP_Communication_HeartBeatTimer invalidate];
        self.UDP_Communication_HeartBeatTimer = nil;
    }
}

#pragma mark ----------- 创建UDP通信Socket -----------
-(void)creatCommnuicationUDPSocket{
    if ([WIFIDeviceModel shareInstance].ID == 1 ||
        [WIFIDeviceModel shareInstance].ID == 2 ||
        [WIFIDeviceModel shareInstance].ID == 10101 ||
        [WIFIDeviceModel shareInstance].ID == 10102) {
        if (self.communication_UdpSocket) {
            [self.communication_UdpSocket close];
            self.communication_UdpSocket.delegate = nil;
            self.communication_UdpSocket = nil;
        }
        
        self.communication_UdpSocket = [[AsyncUdpSocket alloc] initWithDelegate:self];
        NSError *error1 = nil;
        [self.communication_UdpSocket bindToPort:UDP_COMMUNICATION_PORT error:&error1];
        if (error1) {
            NSLog(@"sendSocket bindToPort error : %@", error1);
        }
        [self.communication_UdpSocket receiveWithTimeout:-1 tag:VGS_AIRPLANE_TAG];
    }
}



#pragma mark ------------------------------------- Publish Method -------------------------------------

#pragma mark ----------- 发送udp命令 -----------
-(BOOL)sendUdpCommand:(NSData *)command tag:(int)tag{
    if (!DEFAULT_HOST_IP) {
        NSLog(@"UDP发送的地址为空,请检查是否连接wifi");
        return NO;
    }
    
    @synchronized (self){
        return [self.udpSocket sendData:command toHost:DEFAULT_HOST_IP port:UDP_PORT withTimeout:-1 tag:tag];
    }
}

#pragma mark ----------- 发送tcp命令 -----------
-(void)sendTcpCommand:(NSData *)command tag:(int)tag{
    if (self.tcpSocket.isConnected){
        @synchronized (self){
            [self.tcpSocket writeData:command withTimeout:-1 tag:0];
            [self.tcpSocket readDataWithTimeout:-1 tag:0];
        }
    }
}

@end
