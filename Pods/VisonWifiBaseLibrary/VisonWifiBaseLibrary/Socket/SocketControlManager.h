//
//  WIFIDeviceModel.h
//  Version: 1.0
//
//  该文件用于tcp、udp和设备通信，以及对板端初始化的命令操作、断线重连等功能
//
//  Created by Xu pan on 2020/8/3.
//  Copyright © 2020 Xu pan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AsyncSocket.h"
#import "AsyncUdpSocket.h"
#import "VisonWifiBaseLibraryUtility.h"



@interface SocketControlManager : NSObject


+(instancetype)shareInstance;

/// 发送UDP指令
/// @param command 指令data
/// @param tag 指令标记
-(BOOL)sendUdpCommand:(NSData *)command tag:(int)tag;

/// 发送TCP指令
/// @param command 指令
/// @param tag 指令标记
-(void)sendTcpCommand:(NSData *)command tag:(int)tag;



/// 清空数据
-(void)clearData;

@end


