//
//  VisonWifiBaseLibraryMethods.m
//  FuncCode
//
//  Created by Xu pan on 2021/4/14.
//  Copyright © 2021 Xu pan. All rights reserved.
//

#import "VisonWifiBaseLibraryMethods.h"
#import "WIFIConnectManager.h"
#import "SocketControlManager.h"
#import "Device_Wifi_Config_Command.h"
#import "WIFIDeviceModel.h"
#import "SDCardInfomationTool.h"
#import "DataTransferManager.h"
@interface VisonWifiBaseLibraryMethods()

@property   (nonatomic,assign)          bool                                        isInloadingGestureModel;
@property   (nonatomic,assign)          bool                                        isloadGestureModel;

@end

@implementation VisonWifiBaseLibraryMethods

static VisonWifiBaseLibraryMethods *_visonMethods = nil;
+(instancetype)shareInstance{
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _visonMethods = [[self alloc]init];
    });
    return _visonMethods;
}

#pragma mark - 开始wifi检测
+(void)starWifiMonitoring{
    [WIFIConnectManager shareInstance];
}

#pragma mark  是否有连接wifi
+(BOOL)isConnectedWifi{
    return [WIFIConnectManager shareInstance].isConnectedWIFI;
}

#pragma mark - 当前连接的SSID
+(NSString *)getCurrentSSID{
    return [WIFIConnectManager shareInstance].currentSSID;
}

#pragma mark - 获取wifi信号强度
+(int)getWifiSignalQuality{
    return [WIFIConnectManager shareInstance].wifiSignalQuality;
}

#pragma mark - 是否连接上wifi
+(BOOL)isConnectedVisonWifiDevice{
    return [DataTransferManager shareInstance].isConnectedDevice;
}

#pragma mark - 初始化visonwifi板
+(void)initializeVisonWifiDevice{
    [SocketControlManager shareInstance];
}

#pragma mark - 镜头画面翻转
+(void)imageFileAction{
    [[SocketControlManager shareInstance] sendUdpCommand:[Device_Wifi_Config_Command getFlipLensData] tag:10001];
}

#pragma mark - 镜头切换动作
+(void)switchLensActionFromStream:(StreamControlManager *)streamManager{
    
    if ([WIFIDeviceModel shareInstance].isNewVersion) {
        if ([WIFIDeviceModel shareInstance].isDoubleCamera == 1) {
            if (!CURRENT_LENS_STATUS) {
                [[SocketControlManager  shareInstance] sendUdpCommand:[Device_Wifi_Config_Command getAuxiliaryLensCommand] tag:10000];
            }else{
                [[SocketControlManager  shareInstance] sendUdpCommand:[Device_Wifi_Config_Command getChangeMainLensCommand] tag:10000];
            }
            return;
        }else if ([WIFIDeviceModel shareInstance].isDoubleCamera == 2){
            if (!CURRENT_LENS_STATUS) {
                [USER_DEFAULT setInteger:1 forKey:LENS_STATUS];
                [USER_DEFAULT synchronize];
            }else{
                [USER_DEFAULT setInteger:0 forKey:LENS_STATUS];
                [USER_DEFAULT synchronize];
            }
            [streamManager stopReceiveStreamComplete:nil];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [streamManager startReceiveStreamcComplete:nil];
            });
        }
    }else{
        if (!CURRENT_LENS_STATUS) {
            [USER_DEFAULT setInteger:1 forKey:LENS_STATUS];
            [USER_DEFAULT synchronize];
        }else{
            [USER_DEFAULT setInteger:0 forKey:LENS_STATUS];
            [USER_DEFAULT synchronize];
        }
        [streamManager stopReceiveStreamComplete:nil];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [streamManager startReceiveStreamcComplete:nil];
        });
    }
}

#pragma mark - 发送tcp指令
+(void)sendTcpCommand:(NSData *)command tag:(int)tag{
    [[SocketControlManager shareInstance] sendTcpCommand:command tag:tag];
}

#pragma mark - 发送udp指令
+(BOOL)sendUdpCommand:(NSData *)command tag:(int)tag{
    return [[SocketControlManager shareInstance] sendUdpCommand:command tag:tag];
}

#pragma mark - 停流
+(void)stopReceiveStreamFromStreamManager:(StreamControlManager *)streamManager complete:(void (^)(void))complete{
    [streamManager stopReceiveStreamComplete:complete];
}

#pragma mark - 收流
+(void)startReceiveStreamFromStreamManager:(StreamControlManager *)streamManager complete:(void (^)(void))complete{
    [streamManager startReceiveStreamcComplete:complete];
}

#pragma mark - 拍照动作
+(void)asyncTakePhotoFromStream:(StreamControlManager *)streamManager
                needSourceImage:(BOOL)need
                       complete:(void (^ _Nullable)(UIImage * _Nullable, float, UIImage * _Nullable))complete{
    
    if (need) {
        [streamManager asyncGetStreamPhotoAndSourcePhotoInProgress:^(float progress) {
                    if (complete) {
                        complete(nil,progress,nil);
                    }
                } complete:^(UIImage *streamImage, UIImage *sourceImage) {
                    if (complete) {
                        complete(streamImage,1,sourceImage);
                    }
                }];
    }else{
        dispatch_async(dispatch_get_main_queue(), ^{
            [[SocketControlManager shareInstance] sendTcpCommand:[Device_Wifi_Config_Command getTakePhotoCommand] tag:8787];
        });
        
        [streamManager asyncGetPhotoFromStream:^(BOOL isRec, UIImage *image) {
            if (complete) {
                complete(image,1,nil);
            }
        }];
    }
}

#pragma mark - 录像状态
+(BOOL)isRecordingInStream:(StreamControlManager *)streamManager{
    return streamManager.isWritingMovie;
}

#pragma mark 开始录像（录屏）
+(void)starRecordScreenIsSaveLocal:(BOOL)isSaveLocal
                    isSaveTFcard:(BOOL)isSaveTFCard
                      FromStream:(StreamControlManager *)streamManager
                      ToFilePath:(NSString *)filePath
                       frameRate:(int)frameRate
                       frameSize:(CGSize)frameSize
                     audioEnable:(BOOL)audioEnable
                        complete:(void (^)(void))complete{
    
    [streamManager starWriteMovieIsSaveLocal:isSaveLocal
                                isSaveTFcard:isSaveTFCard
                                  ToFilePath:filePath
                                   frameRate:frameRate
                                   frameSize:frameSize
                                 audioEnable:audioEnable
                                    complete:complete];
}

#pragma mark 停止录像
+(void)stopRecordScreenFromStream:(StreamControlManager *)streamManager
                         complete:(void (^)(NSString * _Nullable))complete{
    
    [streamManager stopWriteMovieComplete:complete];
}

#pragma mark - TF信息读取
+(void)getTFCardInfomation:(void (^)(NSDictionary *))complete{
    [[SDCardInfomationTool shareInstance] getSdcardCurrentInfomationComplete:complete];
}


#pragma mark - TF卡格式化
+(void)formatTFCardActionComplete:(void (^)(BOOL))complete{
    [[SDCardInfomationTool shareInstance] formatSDCardActionComplete:complete];
}


@end
