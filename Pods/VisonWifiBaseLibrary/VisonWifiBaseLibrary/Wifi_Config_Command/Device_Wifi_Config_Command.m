//
//  Device_Wifi_Config_CommandCommand.m
//  GPS Project
//
//  Created by Xu pan on 2020/8/3.
//  Copyright © 2020 Xu pan. All rights reserved.
//

#import "Device_Wifi_Config_Command.h"
#import "WIFIDeviceModel.h"
#import "VisonWifiBaseLibraryDefine.h"

@implementation Device_Wifi_Config_Command

typedef struct
{
    int  year;      /*!< year 1970 ~2039 现加了1900*/
    int  month;     /*!< month (1-12) 本为0起 现加了1*/
    int  day;       /*!< day of the month(1-31)*/
    int  wday;      /*!< day of week (0-6)*/
    int  hour;      /*!< hours (0 - 23)    */
    int  minute;    /*!< minutes (0 - 59)*/
    int  second;    /*!< seconds (0 - 59)*/
}SYSTEM_TIME;


#pragma mark - 获取wifi型号命令
+(NSData *)getWifiModelCommandWithIP:(NSString *)ip{
    NSArray *ipArray = [ip componentsSeparatedByString:@"."];

    char command [5];
    bzero(command,5);
    command[0] = 0x0f; //读设备型号

    for (int i = 0; i < ipArray.count; i++) {
        NSString *obj = ipArray[i];
        command[i+1] = obj.intValue;
    }
    
    return [NSData dataWithBytes:command length:5];
}

#pragma mark - 获取wifi版本号命令
+(NSData *)getWifiVisonCommand{
    char ver[2] = {0};
    ver[0] = 0x28;
    ver[1] = 0x00;
    return [NSData dataWithBytes:ver length:2];
}

#pragma mark - udp出图方式需要发ip地址命令
+(NSData *)getUDPNeedSendIpAddressCommandWithIP:(NSString *)ip{
    NSArray *ipArray = [ip componentsSeparatedByString:@"."];
    
    char command [5];
    bzero(command,5);
    command[0] = 0x08;
    
    for (int i = 0; i < ipArray.count; i++) {
        NSString *obj = ipArray[i];
        command[i+1] = obj.intValue;
    }
    
    return [NSData dataWithBytes:command length:5];
}

+(NSData *)getUDPNeedSendIpAddressCommandWithIP_09:(NSString *)ip{
    NSArray *ipArray = [ip componentsSeparatedByString:@"."];
    
    char command [5];
    bzero(command,5);
    command[0] = 0x09;
    
    for (int i = 0; i < ipArray.count; i++) {
        NSString *obj = ipArray[i];
        command[i+1] = obj.intValue;
    }
    
    return [NSData dataWithBytes:command length:5];
}

#pragma mark - udp停止发送视频流命令
+(NSData *)getUDPStopSendIPAddressCommandWithIP:(NSString *)ip{
    NSArray *ipArray = [ip componentsSeparatedByString:@"."];
    char command [5];
    bzero(command,5);
    command[0] = 0x0d; //停止发送视频流
    
    for (int i = 0; i < ipArray.count; i++) {
        NSString *obj = ipArray[i];
        command[i+1] = obj.intValue;
    }
    return [NSData dataWithBytes:command length:5];
}

#pragma mark - 发送时间命令
+(NSData *)getTimeCommand{
    
    NSDate* now = [NSDate date];
    NSCalendar *cal = [NSCalendar currentCalendar];
    
    unsigned int unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit |NSSecondCalendarUnit;
    NSDateComponents *dd = [cal components:unitFlags fromDate:now];
    
    SYSTEM_TIME time ;
    time.year = (int)dd.year;
    time.month = (int)dd.month;
    time.day = (int)dd.day;
    time.wday = 0;
    time.hour = (int)dd.hour;
    time.minute = (int)dd.minute;
    time.second = (int)dd.second;
    
    char command [29];
    bzero(command,29);
    command[0] = 0x26;
    memcpy(command+1, &time, 28);
    return [NSData dataWithBytes:command length:29];
}

#pragma mark - 发送时间命令RTSP
+(NSData *)getTimeCommandRTSP{
    NSDate* now = [NSDate date];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyy-MM-dd HH:mm:ss"];
    NSString *dateString = [df stringFromDate:now];
    dateString = [NSString stringWithFormat:@"date -s \"%@\"",dateString];
    // # date -s "2008-08-08 12:00:00"
    return [NSData dataWithBytes:dateString.UTF8String length:dateString.length];
}

#pragma mark - tcp心跳包命令
+(NSData *)getTcpHeartbeatCommand{
    char command [12];
    bzero(command,12);
    command[0] = 0x00;
    command[1] = 0x01;
    command[2] = 0x02;
    command[3] = 0x03;
    command[4] = 0x04;
    command[5] = 0x05;
    command[6] = 0x06;
    command[7] = 0x07;
    command[8] = 0x08;
    command[9] = 0x09;
    command[10] = 0x25;
    command[11] = 0x25;
    return [[NSData alloc]initWithBytes:command length:12];
}

#pragma mark - 获取飞机跟随码
+(NSData *)getDroneTypeCommand{
    char command [1];
    bzero(command,1);
    command[0] = 'B'; //读取飞机型号
    return [[NSData alloc] initWithBytes:command length:1];
}

#pragma mark - 获取wifi板国家码命令
+(NSData *)getWifiDeviceCountryCodeCommand{
    unsigned char getCode [1] = {0};
    getCode[0] = 'G';
    return [NSData dataWithBytes:getCode length:1];
}

#pragma mark - 设置wifi板国家码命令
+(NSData *)setWifiDeviceCountryCodeCommandWithCountryCode:(NSString *)countryCode{
    NSString *cmd = [NSString stringWithFormat:@"D%@",countryCode];
    char *getCode = (char *)cmd.UTF8String;
    return [NSData dataWithBytes:getCode length:cmd.length];
}

#pragma mark - 获取画面方向命令
+(NSData *)getVideoOrientationCommand{
    unsigned char data [1] = {0};
    data[0] = 0x2c;
    return [NSData dataWithBytes:data length:1];
}

#pragma mark - 切换主镜头命令
+(NSData *)getChangeMainLensCommand{
    unsigned char data [2] = {0};
    if ([WIFIDeviceModel shareInstance].isDoubleCamera && [WIFIDeviceModel shareInstance].chipModel == ChipModel_QuanZhi_872) {
        data[0] = 0x0b;
        data[0] = 0x00;
    }
    return [NSData dataWithBytes:data length:2];
}

#pragma mark - 切换副镜头命令
+(NSData *)getAuxiliaryLensCommand{
    unsigned char data [2] = {0};
    if ([WIFIDeviceModel shareInstance].isDoubleCamera && [WIFIDeviceModel shareInstance].chipModel == ChipModel_QuanZhi_872) {
        data[0] = 0x0b;
        data[0] = 0x01;
    }
    return [NSData dataWithBytes:data length:2];
}

#pragma mark - 获取镜头切换命令
+(NSData *)getFlipLensData{
    NSInteger status = [USER_DEFAULT integerForKey:FLIP_STATE_VGS];
    char command [2];
    bzero(command, 2);
    if (status == 1){  //如果当前是反转的 则还原
        command[0] = 0x02;
        command[1] = 0x00;
    }else if (status == 2){ //如果当前是还原则反转
        command[0] = 0x01;
        command[1] = 0x00;
    }else{  //如果没设置过 则先还原
        command[0] = 0x01;
        command[1] = 0x00;
    }
    return [NSData dataWithBytes:command length:strlen(command)];
}

unsigned char buf1[]={'0','1','2','3','4','5','6','7','8','9',
    'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V'};

#pragma mark - 板端开始录像命令
+(NSData *)getStartRecordCommand{
    
    NSDate* now = [NSDate date];
    NSCalendar *cal = [NSCalendar currentCalendar];
    
    unsigned int unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute |NSCalendarUnitSecond;
    NSDateComponents *dd = [cal components:unitFlags fromDate:now];
    
    NSInteger year = [dd year];
    NSInteger month = [dd month];
    NSInteger day = [dd day];
    
    NSInteger hour = [dd hour];
    NSInteger minite = [dd minute];
    NSInteger second = [dd second];
    
    year = year - 2000;
    year &= 0x3f;
    
    unsigned char str[23] = {0};
    str[0] = 0x00;
    str[1] = 0x01;
    str[2] = 0x02;
    str[3] = 0x03;
    str[4] = 0x04;
    str[5] = 0x05;
    str[6] = 0x06;
    str[7] = 0x07;
    str[8] = 0x08;
    str[9] = 0x09;
    str[10] = 0x12;
    str[11] = 0x12;
    str[12] = buf1[year/10];
    str[13] = buf1[year%10];
    str[14] = buf1[month];
    str[15] = buf1[day];
    str[16] = buf1[hour];
    str[17] = buf1[minite/10];
    str[18] = buf1[minite%10];
    str[19] = buf1[second/2];
    str[20] = 'A';
    str[21] = 'V';
    str[22] = 'I';
    
    return [NSData dataWithBytes:str length:23];
}


#pragma mark - 板端停止录像命令
+(NSData *)getStopRecordCommand{
    NSDate* now = [NSDate date];
    NSCalendar *cal = [NSCalendar currentCalendar];
    
    unsigned int unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit |NSSecondCalendarUnit;
    NSDateComponents *dd = [cal components:unitFlags fromDate:now];
    NSInteger y1 = [dd year];
    NSInteger m1 = [dd month];
    NSInteger d1 = [dd day];
    
    NSInteger hour1 = [dd hour];
    NSInteger min1 = [dd minute];
    NSInteger sec1 = [dd second];
    
    
    //文件创建时间（ H*2048 + M*32 + S/2 ）
    //文件创建日期（（ Y - 1980 ） *512 + M*32 + D
    
    int time1 = ((int)hour1*2048 + (int)min1*32 +(int)sec1/2);
    int data1 = (((int)y1-1980)*512 +(int)m1*32+(int)d1);
    
    unsigned char cdate[16] = {0};
    cdate[0] = 0x00;
    cdate[1] = 0x01;
    cdate[2] = 0x02;
    cdate[3] = 0x03;
    cdate[4] = 0x04;
    cdate[5] = 0x05;
    cdate[6] = 0x06;
    cdate[7] = 0x07;
    cdate[8] = 0x08;
    cdate[9] = 0x09;
    cdate[10] = 0x13;
    cdate[11] = 0x13;
    cdate[12] = (data1&0xff00)>>8;
    cdate[13] = data1&0xff;
    cdate[14] = (time1&0xff00)>>8;
    cdate[15] = time1&0xff;
    
    return [NSData dataWithBytes:cdate length:16];
}





#pragma mark ============ USB通信指令

///获取板型号
+(NSData *)getUSBWifiModelCommand{
    unsigned char bytes[1] ={0};
    bytes[0]=0x0f;
    return [NSData dataWithBytes:bytes length:1];
}

/// 获取wifi版本号命令
+(NSData *)getUSBWifiVisonCommand{
    char ver[1] = {0};
    ver[0] = 0x28;
    return [NSData dataWithBytes:ver length:1];
}


@end
