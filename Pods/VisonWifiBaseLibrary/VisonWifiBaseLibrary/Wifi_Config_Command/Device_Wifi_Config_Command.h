//
//  Device_Wifi_Config_CommandCommand.h
//  Version: 1.0
//
//  该文件用于对板端初始化的命令集合
//
//  Created by Xu pan on 2020/8/3.
//  Copyright © 2020 Xu pan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Device_Wifi_Config_Command : NSObject


/// 获取wifi型号命令
/// @param ip ip地址
+(NSData *)getWifiModelCommandWithIP:(NSString *)ip;

/// 获取wifi版本号命令
+(NSData *)getWifiVisonCommand;

/// udp出图方式需要发ip地址命令
/// @param ip ip地址
+(NSData *)getUDPNeedSendIpAddressCommandWithIP:(NSString *)ip;

/// 872udp出图方式需要发ip地址命令(09功能字)
/// @param ip ip地址
+(NSData *)getUDPNeedSendIpAddressCommandWithIP_09:(NSString *)ip;

/// udp停止发送视频流
/// @param ip ip地址
+(NSData *)getUDPStopSendIPAddressCommandWithIP:(NSString *)ip;

/// 发送时间命令
+(NSData *)getTimeCommand;

/// 发送时间命令RTSP
+(NSData *)getTimeCommandRTSP;

/// tcp连接心跳包命令
+(NSData *)getTcpHeartbeatCommand;

/// 获取飞机跟随码
+(NSData *)getDroneTypeCommand;

/// 获取wifi板国家码命令
+(NSData *)getWifiDeviceCountryCodeCommand;

/// 设置wifi板国家码命令
/// @param countryCode 国家码
+(NSData *)setWifiDeviceCountryCodeCommandWithCountryCode:(NSString *)countryCode;

/// 获取画面方向
+(NSData *)getVideoOrientationCommand;

/// 全志UDP出图方式切换到主镜头命令
+(NSData *)getChangeMainLensCommand;

/// 全志UDP出图方式切换到副镜头命令
+(NSData *)getAuxiliaryLensCommand;

/// 获取镜头切换命令
+(NSData *)getFlipLensData;

/// 板端开始录像指令
+(NSData *)getStartRecordCommand;

/// 板端停止录像指令
+(NSData *)getStopRecordCommand;

/// 板端拍照命令
+(NSData *)getTakePhotoCommand;

#pragma mark - USB通信指令
////获取wifi版本号命令
+(NSData *)getUSBWifiVisonCommand;
//////获取板型号
+(NSData *)getUSBWifiModelCommand;

@end

NS_ASSUME_NONNULL_END
