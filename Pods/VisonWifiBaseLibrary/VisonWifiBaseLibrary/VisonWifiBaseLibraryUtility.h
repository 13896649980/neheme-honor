//
//  VisonWifiBaseLibraryUtility.h
//  FuncCode
//
//  Created by Xu pan on 2020/11/5.
//  Copyright © 2020 Xu pan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "VisonWifiBaseLibraryDefine.h"



NS_ASSUME_NONNULL_BEGIN

@interface VisonWifiBaseLibraryUtility : NSObject

/// 是否需要延迟播放
+(BOOL)isNeedDelayPlay;

/// 延迟播放的帧数
+(int)delayFrameCount;

/// 延迟播放的帧率
+(int)delayFrameRate;

/// 所有的老版本的硬件类型名称
+(NSArray *)getTotalOldVersionHardwareNames;

/// 拉伸图片
/// @param originalImage 原图
/// @param targetSize 拉伸大小
+(UIImage *)scaleImageWithOriginalImage:(UIImage *)originalImage
                             targetSize:(CGSize)targetSize;

/// 拍照默认规则
+(CGSize)getTheTakePhotoSize;

/// 录像默认规则
+(CGSize)getTheRecordSize;


/// 板端取原图
/// @param progress 数据进度回调
/// @param complate 完成回调
+(void)getTheSourceImageInProgress:(void(^)(float progress))progress
                          complete:(void(^)(BOOL isRec,UIImage *sourceImage))complate;

/// YUVCVPixelBufferRef拷贝
/// @param pixelBuffer 源CVPixelBufferRef
+(CVPixelBufferRef)YUVBufferCopyWithPixelBuffer:(CVPixelBufferRef)pixelBuffer;

/// GBRAPixelBufferRef拷贝
/// @param pixelBuffer 源CVPixelBufferRef
+(CVPixelBufferRef)BGRABufferCopyWithPixelBuffer:(CVPixelBufferRef)pixelBuffer;

/// image 转换 pixelbuf
/// @param image image
+(CVPixelBufferRef)pixelBufferFromCGImage:(CGImageRef)image;

@end

NS_ASSUME_NONNULL_END
