//
//  VisonWifiBaseLibraryDefine.h
//  GPS Project
//
//  Created by Xu pan on 2020/8/10.
//  Copyright © 2020 Xu pan. All rights reserved.
//

#ifndef VisonWifiBaseLibraryDefine_h
#define VisonWifiBaseLibraryDefine_h

#define PhotoSize_4K CGSizeMake(3840, 2160)
#define PhotoSize_2K CGSizeMake(2048, 1152)
#define PhotoSize_1080p CGSizeMake(1920, 1080)
#define PhotoSize_720p CGSizeMake(1280, 720)


/**
 通知名称 ：这个是定位通知名称：接收此通知会触发该模块的设置 国家码 到wifi板功能
            此通知需要传递国家码字符串 例如 “cn”
 **/
#define LOCATION_NOTI_NAME      @"LOCATIONNOTI"

//通知名称 ：读到板类型  可以播放
#define STREAM_NOTI_READYTOPLAY  @"readyToPlay"

//通知名称 ：这个通知名称会将当前的版本号字符串通知过去，请在首页显示版本号的地方接收此通知
#define VERSON_NOTI_NAME        @"version"

//通知名称 : wifi断开
#define WIFI_NOTI_DISCONNECT     @"wifi_Disconnect"

//通知名称 : wifi板返回wifi强度
#define WIFI_NOTI_QUALITY     @"wifi_Quality"

//通知名称 : wifi板返回sd卡信息
#define  SDCARD_NOTI_INFO      @"SdcardcInfomation"






//镜头状态 0为正 1为反
#define LENS_STATUS             @"lenStatus"
#define CURRENT_LENS_STATUS     [USER_DEFAULT integerForKey:LENS_STATUS]

//画面方向
#define FLIP_STATE_VGS          @"VGSflip"


#define TCP_PORT                8888
#define UDP_PORT                8080
#define UDP_COMMUNICATION_PORT  8081


#define VGS_WIFI_IP             @"vgsIP"
#define HOST_IP                 @"host"
#define DEFAULT_HOST_IP         [USER_DEFAULT objectForKey:HOST_IP] //@"192.168.0.1"

#define VGS_AIRPLANE_TAG        10000

#define DEVICE                  @"device"
#define CURRENT_DEVICE          [USER_DEFAULT objectForKey:DEVICE]
#define IS_NO_DEVICE            ([CURRENT_DEVICE isEqualToString:@"nodevice"])


#ifndef USER_DEFAULT
#define USER_DEFAULT            [NSUserDefaults standardUserDefaults]

#endif

#endif /* VisonWifiBaseLibraryDefine_h */
