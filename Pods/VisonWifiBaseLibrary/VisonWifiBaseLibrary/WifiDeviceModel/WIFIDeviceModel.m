//
//  WIFIDeviceModel.m
//  VS GPS PRO
//
//  Created by Xu pan on 2020/7/16.
//  Copyright © 2020 Xu pan. All rights reserved.
//

#import "WIFIDeviceModel.h"
#import "VisonWifiBaseLibraryDefine.h"

@implementation WIFIDeviceModel

static WIFIDeviceModel *type = nil;

+(instancetype)shareInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        type = [[WIFIDeviceModel alloc]init];
    });
    return type;
}

-(instancetype)init{
    if (self = [super init]) {
        _chipManufacturer = ChipManufacturer_None;
        _chipModel = ChipModel_None;
        _wifiSpectrum = WifiSpectrum_None;
        _mainLensTransmissionPixelSize = PixelSize_None;
        _mainLensRealPixelSize = PixelSize_None;
        _playType = PlayType_None;
        _isNewVersion = NO;
        _frameRate = 0;
        _ID = 0;
        _realPixelSizeID = 0;
        _isDoubleCamera = NO;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
        _isConnectedDevice = NO;
    }
    return self;
}

-(void)clearData{
    NSLog(@"WIFIDeviceModel - 清空数据");
    _chipManufacturer = ChipManufacturer_None;
    _chipModel = ChipModel_None;
    _wifiSpectrum = WifiSpectrum_None;
    _mainLensTransmissionPixelSize = PixelSize_None;
    _mainLensRealPixelSize = PixelSize_None;
    _playType = PlayType_None;
    _isNewVersion = NO;
    _frameRate = 0;
    _ID = 0;
    _realPixelSizeID = 0;
    _isDoubleCamera = NO;
    _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    _isConnectedDevice = NO;
}

#pragma mark - 老版本wifi型号识别
-(void)identifyOldVersionWifiModelFromData:(NSString *)modelString
                                  complete:(void (^)(BOOL, NSString * _Nonnull))complete{
    if (!IS_NO_DEVICE) return;
    
    _isNewVersion = NO;
    _chipManufacturer = ChipManufacturer_OldVersion;
    _chipModel = ChipModel_OldVersion;
    _wifiSpectrum = WifiSpectrum_OldVersion;
    //老版本 加100前缀做区分
    if ([modelString isEqualToString:@"VGA\0\0"]) {
        _mainLensTransmissionPixelSize = PixelSize_640_480;
        _mainLensRealPixelSize = PixelSize_640_480;
        _playType = PlayType_TCP;
        _frameRate = 25;
        _ID = 10010;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"MJVGA\0"]){
        _mainLensTransmissionPixelSize = PixelSize_640_480;
        _mainLensRealPixelSize = PixelSize_640_480;
        _playType = PlayType_TCP;
        _frameRate = 25;
        _ID = 10010;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"VSVGA\0"]){
        _mainLensTransmissionPixelSize = PixelSize_640_480;
        _mainLensRealPixelSize = PixelSize_640_480;
        _playType = PlayType_RTSP;
        _frameRate = 25;
        _ID = 10010;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"VGA2"]){
        _chipManufacturer = ChipManufacturer_QuanZhi;
        _chipModel = ChipModel_QuanZhi_872;
        _mainLensTransmissionPixelSize = PixelSize_640_480;
        _mainLensRealPixelSize = PixelSize_640_480;
        _playType = PlayType_UDP;
        _frameRate = 25;
        _ID = 10011;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"1080P"]){
        _chipManufacturer = ChipManufacturer_FuHan;
        _mainLensTransmissionPixelSize = PixelSize_1280_720;
        _mainLensRealPixelSize = PixelSize_1920_1080;
        _playType = PlayType_TCP;
        _frameRate = 25;
        _ID = 10030;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"UDP720P"]){
        _chipManufacturer = ChipManufacturer_FuHan;
        _mainLensTransmissionPixelSize = PixelSize_1280_720;
        _mainLensRealPixelSize = PixelSize_1280_720;
        _playType = PlayType_TCP;
        _frameRate = 25;
        _ID = 10021;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"UDP720PYU"]){
        _chipManufacturer = ChipManufacturer_FuHan;
        _mainLensTransmissionPixelSize = PixelSize_1280_720;
        _mainLensRealPixelSize = PixelSize_1280_720;
        _playType = PlayType_TCP;
        _frameRate = 25;
        _ID = 10024;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"RTSP1080P"]){
        _chipManufacturer = ChipManufacturer_FuHan;
        _mainLensTransmissionPixelSize = PixelSize_1920_1080;
        _mainLensRealPixelSize = PixelSize_1920_1080;
        _playType = PlayType_RTSP;
        _frameRate = 20;
        _ID = 10033;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"RTSP720P"] ||
              [modelString isEqualToString:@"720P"]){
        _chipManufacturer = ChipManufacturer_FuHan;
        _mainLensTransmissionPixelSize = PixelSize_1280_720;
        _mainLensRealPixelSize = PixelSize_1280_720;
        _playType = PlayType_RTSP;
        _frameRate = 20;
        _ID = [modelString isEqualToString:@"720P"] ? 10020 : 10021;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"UDP2K"]){
        _chipManufacturer = ChipManufacturer_FuHan;
        _mainLensTransmissionPixelSize = PixelSize_1280_720;
        _mainLensRealPixelSize = PixelSize_2048_1152;
        _playType = PlayType_TCP;
        _frameRate = 25;
        _ID = 10040;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"UDP2K-30"]){
        _chipManufacturer = ChipManufacturer_FuHan;
        _mainLensTransmissionPixelSize = PixelSize_1280_720;
        _mainLensRealPixelSize = PixelSize_2048_1152;
        _playType = PlayType_TCP;
        _frameRate = 25;
        _ID = 10041;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"UDP27K"]){
        _chipManufacturer = ChipManufacturer_FuHan;
        _mainLensTransmissionPixelSize = PixelSize_1280_720;
        _mainLensRealPixelSize = PixelSize_2048_1152;
        _playType = PlayType_TCP;
        _frameRate = 25;
        _ID = 10042;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"UDP4K"]){
        _chipManufacturer = ChipManufacturer_FuHan;
        _mainLensTransmissionPixelSize = PixelSize_1280_720;
        _mainLensRealPixelSize = PixelSize_2048_1152;
        _playType = PlayType_TCP;
        _frameRate = 25;
        _ID = 10050;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"1080P-5G"]){
        _chipManufacturer = ChipManufacturer_FuHan;
        _mainLensTransmissionPixelSize = PixelSize_1280_720;
        _mainLensRealPixelSize = PixelSize_1920_1080;
        _playType = PlayType_TCP;
        _frameRate = 20;
        _ID = 10034;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"1080P-5g"]){
        _chipManufacturer = ChipManufacturer_FuHan;
        _mainLensTransmissionPixelSize = PixelSize_1280_720;
        _mainLensRealPixelSize = PixelSize_1920_1080;
        _playType = PlayType_TCP;
        _frameRate = 25;
        _ID = 10032;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"720P-5G"]){
        _chipManufacturer = ChipManufacturer_FuHan;
        _mainLensTransmissionPixelSize = PixelSize_1280_720;
        _mainLensRealPixelSize = PixelSize_1280_720;
        _playType = PlayType_TCP;
        _frameRate = 20;
        _ID = 10023;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"720P-5g"]){
        _chipManufacturer = ChipManufacturer_FuHan;
        _mainLensTransmissionPixelSize = PixelSize_1280_720;
        _mainLensRealPixelSize = PixelSize_1280_720;
        _playType = PlayType_TCP;
        _frameRate = 20;
        _ID = 10022;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"1080P-I"]){
        _chipManufacturer = ChipManufacturer_FuHan;
        _mainLensTransmissionPixelSize = PixelSize_1280_720;
        _mainLensRealPixelSize = PixelSize_1280_720;
        _playType = PlayType_TCP;
        _frameRate = 20;
        _ID = 10031;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"HI2K"]){
        _chipManufacturer = ChipManufacturer_FuHan;
        _mainLensTransmissionPixelSize = PixelSize_2048_1152;
        _mainLensRealPixelSize = PixelSize_2048_1152;
        _playType = PlayType_TCP;
        _frameRate = 30;
        _ID = 10043;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"HI25K"]){
        _chipManufacturer = ChipManufacturer_HaiSi;
        _mainLensTransmissionPixelSize = PixelSize_1024_576;
        _mainLensRealPixelSize = PixelSize_2048_1152;
        _playType = PlayType_TCP;
        _frameRate = 30;
        _ID = 10044;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"HI4K"]){
        _chipManufacturer = ChipManufacturer_FuHan;
        _mainLensTransmissionPixelSize = PixelSize_2048_1152;
        _mainLensRealPixelSize = PixelSize_2048_1152;
        _playType = PlayType_TCP;
        _frameRate = 30;
        _ID = 10051;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"FH8626_ssv6x5x_24g_1920_1280"]){
        _chipManufacturer = ChipManufacturer_FuHan;
        _mainLensTransmissionPixelSize = PixelSize_1280_720;
        _mainLensRealPixelSize = PixelSize_1920_1080;
        _playType = PlayType_TCP;
        _frameRate = 25;
        _ID = 10301;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"FH8626_ssv6x5x_5g_1920_1280"]){
        _chipManufacturer = ChipManufacturer_FuHan;
        _mainLensTransmissionPixelSize = PixelSize_1280_720;
        _mainLensRealPixelSize = PixelSize_1920_1080;
        _playType = PlayType_TCP;
        _frameRate = 25;
        _ID = 10302;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"8856_8812cu_4000_1280"]){
        _chipManufacturer = ChipManufacturer_FuHan;
        _mainLensTransmissionPixelSize = PixelSize_1280_720;
        _mainLensRealPixelSize = PixelSize_3840_2160;
        _playType = PlayType_TCP;
        _frameRate = 25;
        _ID = 10501;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"Mr100_ssv6x5x_1920_1920"]){
        _chipManufacturer = ChipManufacturer_FuHan;
        _mainLensTransmissionPixelSize = PixelSize_1920_1080;
        _mainLensRealPixelSize = PixelSize_1920_1080;
        _playType = PlayType_TCP;
        _frameRate = 25;
        _ID = 10303;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"Mr100_ssv6x5x_1920_1280"]){
        _chipManufacturer = ChipManufacturer_FuHan;
        _mainLensTransmissionPixelSize = PixelSize_1280_720;
        _mainLensRealPixelSize = PixelSize_1920_1080;
        _playType = PlayType_TCP;
        _frameRate = 25;
        _ID = 10304;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"Mr100_ssv6x5x_2048_2048"]){
        _chipManufacturer = ChipManufacturer_FuHan;
        _mainLensTransmissionPixelSize = PixelSize_2048_1152;
        _mainLensRealPixelSize = PixelSize_2048_1152;
        _playType = PlayType_TCP;
        _frameRate = 25;
        _ID = 10401;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"Mr100_8801_2048_2048"]){
        _chipManufacturer = ChipManufacturer_FuHan;
        _mainLensTransmissionPixelSize = PixelSize_2048_1152;
        _mainLensRealPixelSize = PixelSize_2048_1152;
        _playType = PlayType_TCP;
        _frameRate = 25;
        _ID = 10402;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"Mr100_ssv6x5x_4000_2048"]){
        _chipManufacturer = ChipManufacturer_FuHan;
        _mainLensTransmissionPixelSize = PixelSize_2048_1152;
        _mainLensRealPixelSize = PixelSize_4000_3000;
        _playType = PlayType_TCP;
        _frameRate = 25;
        _ID = 10502;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"Mr100_ssv6x5x_4000_1920"]){
        _chipManufacturer = ChipManufacturer_FuHan;
        _mainLensTransmissionPixelSize = PixelSize_1920_1080;
        _mainLensRealPixelSize = PixelSize_4000_3000;
        _playType = PlayType_TCP;
        _frameRate = 25;
        _ID = 10503;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"Mr100_ssv6x5x_4000_1280"]){
        _chipManufacturer = ChipManufacturer_FuHan;
        _mainLensTransmissionPixelSize = PixelSize_1280_720;
        _mainLensRealPixelSize = PixelSize_4000_3000;
        _playType = PlayType_TCP;
        _frameRate = 25;
        _ID = 10504;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"Mr100_8801_4000_2048"]){
        _chipManufacturer = ChipManufacturer_FuHan;
        _mainLensTransmissionPixelSize = PixelSize_2048_1152;
        _mainLensRealPixelSize = PixelSize_4000_3000;
        _playType = PlayType_TCP;
        _frameRate = 25;
        _ID = 10505;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"Mr100_8801_4000_1920"]){
        _chipManufacturer = ChipManufacturer_FuHan;
        _mainLensTransmissionPixelSize = PixelSize_1920_1080;
        _mainLensRealPixelSize = PixelSize_4000_3000;
        _playType = PlayType_TCP;
        _frameRate = 25;
        _ID = 10506;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"Mr100_8801_4000_1280"]){
        _chipManufacturer = ChipManufacturer_FuHan;
        _mainLensTransmissionPixelSize = PixelSize_1280_720;
        _mainLensRealPixelSize = PixelSize_4000_3000;
        _playType = PlayType_TCP;
        _frameRate = 25;
        _ID = 10507;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"872ET_320_320"]){
        _chipManufacturer = ChipManufacturer_QuanZhi;
        _chipModel = ChipModel_QuanZhi_872;
        _mainLensTransmissionPixelSize = PixelSize_320_240;
        _mainLensRealPixelSize = PixelSize_320_240;
        _playType = PlayType_UDP;
        _frameRate = 25;
        _ID = 10101;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }else if ([modelString isEqualToString:@"872AT_640_640"]){
        _chipManufacturer = ChipManufacturer_QuanZhi;
        _chipModel = ChipModel_QuanZhi_872;
        _mainLensTransmissionPixelSize = PixelSize_640_480;
        _mainLensRealPixelSize = PixelSize_640_480;
        _playType = PlayType_UDP;
        _frameRate = 25;
        _ID = 10102;
        _isDoubleCamera = 0;
        _auxiliaryLensTransmissionPixelSize = PixelSize_None;
    }
    
    NSString *wifiModel;
    

    
    if (_ID) {
        short realID = _ID - 10000;
        if (realID > 100) {
            wifiModel = [NSString stringWithFormat:@"%d_%@",realID/100,((realID%100 > 10) ? [NSString stringWithFormat:@"%d",realID%100] : [NSString stringWithFormat:@"0%d",realID%100])];
        }else{
            wifiModel = [NSString stringWithFormat:@"%d",realID];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:wifiModel forKey:DEVICE];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }else{
        if (complete) {
            complete(NO,wifiModel);
            _isConnectedDevice = NO;
        }
        return;
    }
    
    if (complete) {
        complete(YES,wifiModel);
        _isConnectedDevice = YES;
    }
    
    
}

#pragma mark - 新版本wifi型号识别
-(void)identifyWifiModelFromData:(NSData *)data
                        complete:(void (^)(BOOL, NSString * _Nonnull))complete{
    
    if (!IS_NO_DEVICE) return;
    
    _isNewVersion = YES;
    
    Byte *byte = (Byte *)data.bytes;
    //芯片厂家
    switch (byte[2]) {
        case 0x01:
            _chipManufacturer = ChipManufacturer_FuHan;
            break;
        case 0x02:
            _chipManufacturer = ChipManufacturer_HaiSi;
            break;
        case 0x03:
            _chipManufacturer = ChipManufacturer_QuanZhi;
            break;
        default:
            break;
    }
    
    //芯片型号
    switch (byte[3]) {
        case 0x01:
            _chipModel = ChipModel_QuanZhi_872;
            break;
        default:
            break;
    }
    
    //频段
    switch (byte[4]) {
        case 0x01:
            _wifiSpectrum = WifiSpectrum_QuanZhi_24G;
            break;
        case 0x02:
            _wifiSpectrum = WifiSpectrum_NFGG_24G;
            break;
        case 200:
            _wifiSpectrum = WifiSpectrum_QuanZhi_58G;
            break;
        default:
            break;
    }
    
    //回传分辨率
    switch (byte[5]) {
        case 0x01:
            _mainLensTransmissionPixelSize = PixelSize_320_240;
            break;
        case 10:
            _mainLensTransmissionPixelSize = PixelSize_640_360;
            break;
        case 11:
            _mainLensTransmissionPixelSize = PixelSize_640_480;
            break;
        case 20:
            _mainLensTransmissionPixelSize = PixelSize_1280_720;
            break;
        case 21:
            _mainLensTransmissionPixelSize = PixelSize_1024_576;
            break;
        case 30:
            _mainLensTransmissionPixelSize = PixelSize_1920_1080;
            break;
        case 31:
            _mainLensTransmissionPixelSize = PixelSize_1632_928;
            break;
        case 40:
            _mainLensTransmissionPixelSize = PixelSize_2048_1152;
            break;
        case 41:
            _mainLensTransmissionPixelSize = PixelSize_2592_1520;
            break;
        case 42:
            _mainLensTransmissionPixelSize = PixelSize_2976_1680;
            break;
        case 50:
            _mainLensTransmissionPixelSize = PixelSize_3840_2160;
            break;
        case 60:
            _mainLensTransmissionPixelSize = PixelSize_4000_3000;
            break;
        default:
            break;
    }
    //实际分辨率ID
    _realPixelSizeID = byte[6];
    //板端分辨率
    switch (byte[6]) {
        case 0x01:
            _mainLensRealPixelSize = PixelSize_320_240;
            break;
        case 10:
            _mainLensRealPixelSize = PixelSize_640_360;
            break;
        case 11:
            _mainLensRealPixelSize = PixelSize_640_480;
            break;
        case 20:
            _mainLensRealPixelSize = PixelSize_1280_720;
            break;
        case 21:
            _mainLensRealPixelSize = PixelSize_1024_576;
            break;
        case 30:
            _mainLensRealPixelSize = PixelSize_1920_1080;
            break;
        case 31:
            _mainLensRealPixelSize = PixelSize_1632_928;
            break;
        case 40:
            _mainLensRealPixelSize = PixelSize_2048_1152;
            break;
        case 41:
            _mainLensRealPixelSize = PixelSize_2592_1520;
            break;
        case 42:
            _mainLensRealPixelSize = PixelSize_2976_1680;
            break;
        case 50:
            _mainLensRealPixelSize = PixelSize_3840_2160;
            break;
        case 60:
            _mainLensRealPixelSize = PixelSize_4000_3000;
            break;
        default:
            break;
    }

    //出图方式
    switch (byte[7]) {
        case 1:
            _playType = PlayType_UDP;
            break;
        case 2:
            _playType = PlayType_TCP;
            break;
        case 3:
            _playType = PlayType_RTSP;
            break;
        default:
            break;
    }
    //帧率
    _frameRate = byte[8];
    if (data.length == 11) {
        //ID
        _ID = ntohs(*(short *)&byte[9]);
    }else{
        //是否双镜头
        _isDoubleCamera = byte[9];
        //副镜头回传分辨率
        switch (byte[10]) {
            case 0x01:
                _auxiliaryLensTransmissionPixelSize = PixelSize_320_240;
                break;
            case 10:
                _auxiliaryLensTransmissionPixelSize = PixelSize_640_360;
                break;
            case 11:
                _auxiliaryLensTransmissionPixelSize = PixelSize_640_480;
                break;
            case 20:
                _auxiliaryLensTransmissionPixelSize = PixelSize_1280_720;
                break;
            case 21:
                _auxiliaryLensTransmissionPixelSize = PixelSize_1024_576;
                break;
            case 30:
                _auxiliaryLensTransmissionPixelSize = PixelSize_1920_1080;
                break;
            case 31:
                _auxiliaryLensTransmissionPixelSize = PixelSize_1632_928;
                break;
            case 40:
                _auxiliaryLensTransmissionPixelSize = PixelSize_2048_1152;
                break;
            case 41:
                _auxiliaryLensTransmissionPixelSize = PixelSize_2592_1520;
                break;
            case 42:
                _auxiliaryLensTransmissionPixelSize = PixelSize_2976_1680;
                break;
            case 50:
                _auxiliaryLensTransmissionPixelSize = PixelSize_3840_2160;
                break;
            case 60:
                _auxiliaryLensTransmissionPixelSize = PixelSize_4000_3000;
                break;
            default:
                break;
        }
        //ID
        _ID = ntohs(*(short *)&byte[11]);
    }
    
    NSString *wifiModel;
    if (_ID) {
        wifiModel = [NSString stringWithFormat:@"%dX",_ID];
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%dX",_ID] forKey:DEVICE];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }else{
        if (complete) {
            complete(NO,wifiModel);
            _isConnectedDevice = NO;
        }
        return;
    }
    
    if (complete) {
        complete(YES,wifiModel);
        _isConnectedDevice = YES;
    }
}

-(CGSize)getPixelSize:(PixelSize)pixelsize{
    
    switch (pixelsize) {
        case PixelSize_None:
            return CGSizeMake(1280, 720);
            break;
        case PixelSize_320_240:
            return CGSizeMake(320, 240);
            break;
        case PixelSize_640_360:
            return CGSizeMake(640, 360);
            break;
        case PixelSize_640_480:
            return CGSizeMake(640, 480);
            break;
        case PixelSize_1280_720:
            return CGSizeMake(1280, 720);
            break;
        case PixelSize_1024_576:
            return CGSizeMake(1024, 576);
            break;
        case PixelSize_1632_928:
            return CGSizeMake(1632, 928);
            break;
        case PixelSize_1920_1080:
            return CGSizeMake(1920, 1080);
            break;
        case PixelSize_2048_1152:
            return CGSizeMake(2048, 1152);
            break;
        case PixelSize_2592_1520:
            return CGSizeMake(2592, 1520);
            break;
        case PixelSize_2976_1680:
            return CGSizeMake(2976, 1680);
            break;
        case PixelSize_3840_2160:
            return CGSizeMake(3840, 2160);
            break;
        case PixelSize_4000_3000:
            return CGSizeMake(4000, 3000);
            break;
    }
    return CGSizeMake(0, 0);
}


@end
