//
//  WIFIDeviceModel.h
//  Version: 1.0
//
//  该文件用于保存设备型号以及相关的参数
//
//  Created by Xu pan on 2020/7/16.
//  Copyright © 2020 Xu pan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

//芯片厂家
typedef NS_ENUM(NSInteger,ChipManufacturer) {
    ChipManufacturer_None,
    ChipManufacturer_OldVersion,    //老版本定义的版型没有型号
    ChipManufacturer_FuHan,         //富汗
    ChipManufacturer_HaiSi,         //海思
    ChipManufacturer_QuanZhi,       //全志
};

//芯片型号
typedef NS_ENUM(NSInteger,ChipModel) {
    ChipModel_None,
    ChipModel_OldVersion,           //老版本定义的版型没有型号
    ChipModel_QuanZhi_872,          //全志872
};

//WIFI厂家频段
typedef NS_ENUM(NSInteger,WifiSpectrum) {
    WifiSpectrum_None,
    WifiSpectrum_OldVersion,                //老版本定义的版型没有型号
    WifiSpectrum_QuanZhi_24G,               //全志2.4G
    WifiSpectrum_NFGG_24G,                  //南方硅谷2.4G
    WifiSpectrum_QuanZhi_58G,               //全志5.8G
};

//分辨率
typedef NS_ENUM(NSInteger,PixelSize) {
    PixelSize_None,
    PixelSize_320_240,          //320*240
    PixelSize_640_360,          //640_360
    PixelSize_640_480,          //640_480
    PixelSize_1280_720,         //1280_720
    PixelSize_1024_576,         //1024_576
    PixelSize_1632_928,         //1632_928
    PixelSize_1920_1080,        //1920_1080
    PixelSize_2048_1152,        //2048_1152
    PixelSize_2592_1520,        //2592_1520
    PixelSize_2976_1680,        //2976_1680
    PixelSize_3840_2160,        //3840_2160
    PixelSize_4000_3000,        //4000_3000
};


//出图方式
typedef NS_ENUM(NSInteger,PlayType) {
    PlayType_None,
    PlayType_UDP,          //UDP
    PlayType_TCP,          //TCP
    PlayType_RTSP,         //RTSP
};

@interface WIFIDeviceModel : NSObject

@property   (nonatomic,assign,readonly)      ChipManufacturer                   chipManufacturer;
@property   (nonatomic,assign,readonly)      ChipModel                          chipModel;
@property   (nonatomic,assign,readonly)      WifiSpectrum                       wifiSpectrum;
@property   (nonatomic,assign,readonly)      PixelSize                          mainLensTransmissionPixelSize;
@property   (nonatomic,assign,readonly)      PixelSize                          auxiliaryLensTransmissionPixelSize;
@property   (nonatomic,assign,readonly)      PixelSize                          mainLensRealPixelSize;
@property   (nonatomic,assign,readonly)      PlayType                           playType;
@property   (nonatomic,assign,readonly)      BOOL                               isNewVersion;
@property   (nonatomic,assign,readonly)      char                               isDoubleCamera;
@property   (nonatomic,assign,readonly)      int                                frameRate;
@property   (nonatomic,assign,readonly)      short                              ID;
@property   (nonatomic,assign)      short                                       realPixelSizeID;
@property   (nonatomic,assign,readonly)      BOOL                               isConnectedDevice;

+(instancetype)shareInstance;

/// 数据清除
-(void)clearData;

/// 识别老版本板类型
/// @param modelString 型号
/// @param complete 完成回调
-(void)identifyOldVersionWifiModelFromData:(NSString *)modelString
                                  complete:(void (^)(BOOL, NSString *model))complete;


/// 识别新版本板类型
/// @param data data
/// @param complete callback
-(void)identifyWifiModelFromData:(NSData *)data
                        complete:(void (^)(BOOL, NSString *model))complete;

/// 获取分辨率大小
/// @param pixelsize PixelSize
-(CGSize)getPixelSize:(PixelSize)pixelsize;


@end

NS_ASSUME_NONNULL_END
